package com.saregama.musicstore.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.saregama.musicstore.activity.ResetPassword;

public class SmsListener extends BroadcastReceiver{

    private String msgBody;
    private String[] otp;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub

        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            if (bundle != null){
                //---retrieve the SMS message received---
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for(int i=0; i<msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
//                        msg_from = msgs[i].getOriginatingAddress();
                        if(msgs[i].getMessageBody().contains("temporary password to login to your Saregama.com account"))
                        	msgBody = msgs[i].getMessageBody();
//                        msgBody =  msgBody.replaceAll("[^0-9]", "");
                        otp = msgBody.split(" ");
                        if(otp != null && otp.length > 0)
                            msgBody = otp[2];
                        else
                            msgBody = "";
                    }
                }catch(Exception e){
//                            Log.d("Exception caught",e.getMessage());
                }
            }
            
            ResetPassword inst = ResetPassword.instance();
            if(inst != null && msgBody != null && msgBody.length() == 6)
            inst.updateList(msgBody);
        }
    }
}
