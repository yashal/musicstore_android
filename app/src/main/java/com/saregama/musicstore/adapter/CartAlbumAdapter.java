package com.saregama.musicstore.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.CartActivity;
import com.saregama.musicstore.pojo.CartItemPojo;

import java.util.ArrayList;

/**
 * Created by Arpit on 12-Apr-16.
 */
public class CartAlbumAdapter extends RecyclerView.Adapter<CartAlbumAdapter.ViewHolder> {

    private CartActivity obj;
    private ArrayList<CartItemPojo> arr;

    public CartAlbumAdapter(CartActivity obj, ArrayList<CartItemPojo> arr) {
        this.obj = obj;
        this.arr = arr;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_album_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(arr != null && arr.get(position) != null)
        {

            holder.remove_album_fron_cart.setVisibility(View.VISIBLE);

            holder.mp3hindi_albumlist_albumname.setText(arr.get(position).getTitle());
            holder.mp3hindi_albumlist_trackcount.setText("("+arr.get(position).getSongCount()+" Tracks)");

            holder.remove_album_fron_cart.setTag(R.string.key, position);
            holder.remove_album_fron_cart.setTag(R.string.data, arr.get(position));
            holder.remove_album_fron_cart.setTag(R.string.from, "album");
            holder.remove_album_fron_cart.setOnClickListener(obj);

            holder.albumLL.setTag(R.string.key, position);
            holder.albumLL.setTag(R.string.data, arr.get(position));
            holder.albumLL.setTag(R.string.from, "album");
            holder.albumLL.setOnClickListener(obj);

            obj.setImageInLayout(obj, (int)obj.getResources().getDimension(R.dimen.cart_album_image_size), (int)obj.getResources().getDimension(R.dimen.cart_album_image_size), arr.get(position).getImage(), holder.mp3hindi_albumlist_Image);
        }
    }

    public int getCount() {
        return arr.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mp3hindi_albumlist_albumname;
        private TextView mp3hindi_albumlist_trackcount;
        private LinearLayout remove_album_fron_cart;
        private LinearLayout albumLL;
        private ImageView mp3hindi_albumlist_Image;

        public ViewHolder(View itemView) {
            super(itemView);
            mp3hindi_albumlist_albumname = (TextView) itemView.findViewById(R.id.mp3hindi_albumlist_albumname);
            mp3hindi_albumlist_trackcount = (TextView) itemView.findViewById(R.id.mp3hindi_albumlist_trackcount);
            remove_album_fron_cart = (LinearLayout) itemView.findViewById(R.id.remove_album_fron_cart);
            albumLL = (LinearLayout) itemView.findViewById(R.id.albumLL);
            mp3hindi_albumlist_Image = (ImageView) itemView.findViewById(R.id.mp3hindi_albumlist_Image);
        }
    }
}