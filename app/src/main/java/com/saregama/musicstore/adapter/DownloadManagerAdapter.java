package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.saregama.musicstore.fragments.DownloadedFragment;
import com.saregama.musicstore.fragments.DownloadedNowFragment;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class DownloadManagerAdapter extends FragmentPagerAdapter {

    public DownloadManagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new DownloadedNowFragment();
            case 1:
                return new DownloadedFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }
}

