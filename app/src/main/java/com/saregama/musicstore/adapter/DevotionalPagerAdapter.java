package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.saregama.musicstore.fragments.Aartis_Devotional_Fragment;
import com.saregama.musicstore.fragments.Artistes_Devotional_Fragment;
import com.saregama.musicstore.fragments.Devi_Devta_Devotional_Fragment;
import com.saregama.musicstore.fragments.Mantras_Devotional_Fragment;

import java.util.ArrayList;

/**
 * Created by navneet on 7/3/2016.
 */
public class DevotionalPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public DevotionalPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Devi_Devta_Devotional_Fragment fragment_devi_devta = new Devi_Devta_Devotional_Fragment();
                mPageReferenceMap.add(position, fragment_devi_devta);
                return fragment_devi_devta;
            case 1:
                Mantras_Devotional_Fragment fragment_mantras = new Mantras_Devotional_Fragment();
                if (mPageReferenceMap.size() == 0) {
                    Devi_Devta_Devotional_Fragment fragment = new Devi_Devta_Devotional_Fragment();
                    mPageReferenceMap.add(0, fragment);
                }
                mPageReferenceMap.add(position, fragment_mantras);
                return fragment_mantras;
            case 2:
                Aartis_Devotional_Fragment fragment_artis = new Aartis_Devotional_Fragment();
                if (mPageReferenceMap.size() == 0) {
                    Devi_Devta_Devotional_Fragment fragment = new Devi_Devta_Devotional_Fragment();
                    mPageReferenceMap.add(0, fragment);

                    Mantras_Devotional_Fragment fragment_mantra = new Mantras_Devotional_Fragment();
                    mPageReferenceMap.add(1, fragment_mantra);
                }
                mPageReferenceMap.add(position, fragment_artis);
                return fragment_artis;
            case 3:
                Artistes_Devotional_Fragment fragment_artistes = new Artistes_Devotional_Fragment();
                if (mPageReferenceMap.size() == 0) {
                    Devi_Devta_Devotional_Fragment fragment = new Devi_Devta_Devotional_Fragment();
                    mPageReferenceMap.add(0, fragment);

                    Mantras_Devotional_Fragment fragment_mantra = new Mantras_Devotional_Fragment();
                    mPageReferenceMap.add(1, fragment_mantra);

                    Aartis_Devotional_Fragment fragment_aarti = new Aartis_Devotional_Fragment();
                    mPageReferenceMap.add(2, fragment_aarti);
                }
                mPageReferenceMap.add(position, fragment_artistes);
                return fragment_artistes;
        }
        return null;
    }

//    @Override
//    public Fragment getItem(int position) {
//        switch (position) {
//            case 0:
//                return new Devi_Devta_Devotional_Fragment();
//            case 1:
//                return new Mantras_Devotional_Fragment();
//            case 2:
//                return new Aartis_Devotional_Fragment();
//            case 3:
//                return  new Artistes_Devotional_Fragment();
//        }
//        return null;
//    }

    @Override
    public int getCount() {
        return 4;
    }

    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}
