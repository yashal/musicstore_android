package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.CategoryListActivity;
import com.saregama.musicstore.activity.ClassicalListActivity;
import com.saregama.musicstore.activity.LanguageListActivity;
import com.saregama.musicstore.activity.OtherLandingListActivity;
import com.saregama.musicstore.pojo.HomeBannerPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    private ArrayList<HomeBannerPojo> arrayCategoryList;
    private int height, width;

    private Activity context;
    private String from;

    public CategoryListAdapter(Activity context, ArrayList<HomeBannerPojo> arrayCategoryList, String from) {
        this.from = from;
        this.context = context;
        this.arrayCategoryList = arrayCategoryList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final HomeBannerPojo data = arrayCategoryList.get(position);

        DisplayMetrics metrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        if (arrayCategoryList.size() < 4) {
            height = (metrics.heightPixels - context.getResources().getInteger(R.integer.subtract)) / arrayCategoryList.size();
        } else {
            height = metrics.heightPixels / 4;
        }
        width = metrics.widthPixels;
        if (position == getItemCount() - 1)
            holder.view_divider.setVisibility(View.GONE);
        else
            holder.view_divider.setVisibility(View.VISIBLE);

        holder.banner.requestLayout();
        holder.banner.getLayoutParams().height = height;


        holder.itemView.setTag(R.string.key, position);
        holder.itemView.setTag(R.string.data, data);

        if (from.equals("language")) {
            holder.itemView.setOnClickListener((LanguageListActivity) context);
            ((LanguageListActivity) context).setImageInLayoutHome(context.getApplicationContext(), width, height, data.getImg_1000(), holder.banner);
        } else if (from.equals("classical")) {
            holder.itemView.setOnClickListener((ClassicalListActivity) context);
            ((ClassicalListActivity) context).setImageInLayoutHome(context.getApplicationContext(), width, height, data.getImg_1000(), holder.banner);
        } else if (from.equals("others")) {
            holder.itemView.setOnClickListener((OtherLandingListActivity) context);
            ((OtherLandingListActivity) context).setImageInLayoutHome(context.getApplicationContext(), width, height, data.getImg_1000(), holder.banner);
        } else {
            holder.itemView.setOnClickListener((CategoryListActivity) context);
            ((CategoryListActivity) context).setImageInLayoutHome(context.getApplicationContext(), width, height, data.getImg_1000(), holder.banner);
        }
    }

    @Override
    public int getItemCount() {
        return arrayCategoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView banner;
        private View view_divider;

        public ViewHolder(View itemView) {
            super(itemView);
            banner = (ImageView) itemView.findViewById(R.id.category_row_bannerImage);
            view_divider = itemView.findViewById(R.id.view_divider);
        }
    }
}
