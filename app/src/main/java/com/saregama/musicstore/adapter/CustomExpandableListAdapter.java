package com.saregama.musicstore.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.CartNewActivity;
import com.saregama.musicstore.pojo.CartItemPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by arpit on 1/18/2017.
 */

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<CartItemPojo>> _listDataChild;

    private TextView mp3hindi_songlist_songname;
    private TextView mp3hindi_songlist_moviename, cart_song_price, buy_again;
    private TextView song_type;
    private LinearLayout remove_album_fron_cart, cart_song_row_root, already_purchased_layout;
    private ImageView mp3hindi_albumlist_Image;
    private CheckBox addtocart;
    private ImageView song_album_image_cart;
    private CartNewActivity obj;

    public CustomExpandableListAdapter(Context context, List<String> listDataHeader,
                                       HashMap<String, ArrayList<CartItemPojo>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        obj = (CartNewActivity) context;
    }

    @Override
    public CartItemPojo getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final CartItemPojo childText = getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.cart_song_row_layout, null);
        }

        if (childPosition == getChildrenCount(groupPosition) - 1) {
            convertView.setPadding(0, 0, 0, 20);
        } else
            convertView.setPadding(0, 0, 0, 0);

        mp3hindi_songlist_songname = (TextView) convertView.findViewById(R.id.mp3hindi_songlist_songname);
        mp3hindi_songlist_moviename = (TextView) convertView.findViewById(R.id.mp3hindi_songlist_moviename);
        song_album_image_cart = (ImageView) convertView.findViewById(R.id.song_album_image_cart);
        cart_song_price = (TextView) convertView.findViewById(R.id.cart_song_price);
        buy_again = (TextView) convertView.findViewById(R.id.buy_again);
        remove_album_fron_cart = (LinearLayout) convertView.findViewById(R.id.remove_album_fron_cart);
        cart_song_row_root = (LinearLayout) convertView.findViewById(R.id.cart_song_row_root);
        already_purchased_layout = (LinearLayout) convertView.findViewById(R.id.already_purchased_layout);
        song_type = (TextView) convertView.findViewById(R.id.song_type);
        mp3hindi_albumlist_Image = (ImageView) convertView.findViewById(R.id.playIcon);
        addtocart = (CheckBox) convertView.findViewById(R.id.addtocart);
        remove_album_fron_cart.setVisibility(View.VISIBLE);
        addtocart.setVisibility(View.GONE);
        System.out.println("xhxhxhxhx "+childText.getImage());
        mp3hindi_songlist_songname.setText(childText.getTitle());
        if (childText.getImage() != null) {
            obj.setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), childText.getImage(), song_album_image_cart);
        }
        if (childText != null) {
            buy_again.setTag(R.string.data, childText);
            buy_again.setOnClickListener(obj);
            if (childText.getSongCount() != null) {
                remove_album_fron_cart.setTag(R.string.key, childPosition);
                remove_album_fron_cart.setTag(R.string.data, childText);
                remove_album_fron_cart.setTag(R.string.from, "album");
                remove_album_fron_cart.setOnClickListener(obj);

                cart_song_row_root.setTag(R.string.key, childPosition);
                cart_song_row_root.setTag(R.string.data, childText);
                cart_song_row_root.setOnClickListener(obj);

                mp3hindi_songlist_moviename.setText("(" + childText.getSongCount() + " Tracks)");
                obj.setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.cart_album_image_size), (int) obj.getResources().getDimension(R.dimen.cart_album_image_size), childText.getImage(), mp3hindi_albumlist_Image);
                song_type.setVisibility(View.VISIBLE);
                if (obj.getAppConfigJson().getCurrency().equals("Rs."))
                    cart_song_price.setText(obj.getResources().getString(R.string.Rs) + " " + Math.round(childText.getPrice()));
                else
                    cart_song_price.setText(obj.getAppConfigJson().getCurrency() + " " + childText.getPrice());
                if (childText.getCtype().equals(obj.getAppConfigJson().getC_type().getALBUM() + "")) {
                    song_type.setText(".MP3");
                } else {
                    song_type.setText(".HD");
                }
            } else /*if (childText.getSongCount() == null)*/ {
                remove_album_fron_cart.setTag(R.string.key, childPosition);
                remove_album_fron_cart.setTag(R.string.data, childText);
                remove_album_fron_cart.setTag(R.string.from, "songs");
                mp3hindi_songlist_moviename.setText(childText.getAlbum());
                remove_album_fron_cart.setOnClickListener(obj);
                mp3hindi_albumlist_Image.setImageResource(R.mipmap.play_icon);
                cart_song_row_root.setClickable(false);
                song_type.setVisibility(View.VISIBLE);

                if (childText.getCtype().equals(obj.getAppConfigJson().getC_type().getSONG() + "")) {
                    song_type.setText(".MP3");
                    if (obj.getAppConfigJson().getCurrency().equals("Rs."))
                        cart_song_price.setText(obj.getResources().getString(R.string.Rs) + " " + obj.getAppConfigJson().getMp3_price());
                    else
                        cart_song_price.setText(obj.getAppConfigJson().getCurrency() + " " + obj.getAppConfigJson().getMp3_price());
                } else {
                    song_type.setText(".HD");
                    if (obj.getAppConfigJson().getCurrency().equals("Rs."))
                        cart_song_price.setText(obj.getResources().getString(R.string.Rs) + " " + obj.getAppConfigJson().getHp_price());
                    else
                        cart_song_price.setText(obj.getAppConfigJson().getCurrency() + " " + obj.getAppConfigJson().getHp_price());
                }
            }

            if (childText.getStatus() == 1) {
                already_purchased_layout.setVisibility(View.GONE);
                cart_song_price.setVisibility(View.VISIBLE);
                remove_album_fron_cart.setVisibility(View.VISIBLE);
                song_type.setTextColor(ContextCompat.getColor(obj, R.color.offer_valid));
                mp3hindi_songlist_songname.setTextColor(ContextCompat.getColor(obj, R.color.song_name_text_color));
                mp3hindi_songlist_moviename.setTextColor(ContextCompat.getColor(obj, R.color.offer_valid));
            } else {
                already_purchased_layout.setVisibility(View.VISIBLE);
                cart_song_price.setVisibility(View.GONE);
                remove_album_fron_cart.setVisibility(View.GONE);
                song_type.setTextColor(ContextCompat.getColor(obj, R.color.already_purchased_song_color));
                mp3hindi_songlist_songname.setTextColor(ContextCompat.getColor(obj, R.color.already_purchased_song_color));
                mp3hindi_songlist_moviename.setTextColor(ContextCompat.getColor(obj, R.color.already_purchased_song_color));
            }

        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.cart_view, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.name_cart);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void removeGroup(int group) {
        //TODO: Remove the according group. Dont forget to remove the children aswell!
        Log.v("Adapter", "Removing group"+group);
        notifyDataSetChanged();
    }

    public void removeChild(int group, int child) {
        //TODO: Remove the according child
        Log.v("Adapter", "Removing child "+child+" in group "+group);
        notifyDataSetChanged();
    }
}
