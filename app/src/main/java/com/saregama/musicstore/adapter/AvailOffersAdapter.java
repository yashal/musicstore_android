package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.AvailOffersActivity;
import com.saregama.musicstore.pojo.OfferDataPojo;

import java.util.ArrayList;


public class AvailOffersAdapter extends RecyclerView.Adapter<AvailOffersAdapter.ViewHolder> {

    private ArrayList<OfferDataPojo> arrayCategoryList;

    private Activity context;

    public AvailOffersAdapter(Activity context, ArrayList<OfferDataPojo> arrayCategoryList){

        this.context = context;
        this.arrayCategoryList = arrayCategoryList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.avail_offers_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final OfferDataPojo data = arrayCategoryList.get(position);
        holder.title.setText(data.getTitle());
        holder.subTitle.setText(data.getSub_title());
        holder.conditions.setText("* "+data.getCondition());

        holder.itemView.setTag(R.string.key, position);
        holder.itemView.setTag(R.string.data, data);

        holder.itemView.setOnClickListener((AvailOffersActivity) context);


    }

    @Override
    public int getItemCount() {
        return arrayCategoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title, subTitle, conditions;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.availoffer_row_title);
            subTitle = (TextView) itemView.findViewById(R.id.availoffer_row_subtitle);
            conditions = (TextView) itemView.findViewById(R.id.availoffer_row_condition);
        }
    }

}

