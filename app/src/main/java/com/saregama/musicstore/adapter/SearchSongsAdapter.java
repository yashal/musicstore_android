package com.saregama.musicstore.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.DevotionalSearchActivity;
import com.saregama.musicstore.activity.HindiFilmsSearchActivity;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.QuickAction;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 27-Jul-16.
 */
public class SearchSongsAdapter extends BaseAdapter {

    private Activity obj;
    private ArrayList<MP3HindiSongListPojo> arr;
    private String keyword;
    private String search_from;

    public SearchSongsAdapter(Activity obj, ArrayList<MP3HindiSongListPojo> arr, String keyword, String search_from) {
        this.obj = obj;
        this.arr = arr;
        this.keyword = keyword.replace("%20", " ");
        this.search_from = search_from;
    }

    public int getCount() {
        return arr.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) obj.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row;
        row = inflater.inflate(R.layout.mp3hindi_songlist_layout, parent, false);

        if (arr != null && arr.get(position) != null) {
            TextView global_search_songlist_songname = (TextView) row.findViewById(R.id.mp3hindi_songlist_songname);
            TextView mp3hindi_songlist_moviename = (TextView) row.findViewById(R.id.mp3hindi_songlist_moviename);
            ImageView song_album_image = (ImageView) row.findViewById(R.id.song_album_image);

            int startPos = arr.get(position).getSong_name().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
            Spannable spannable = new SpannableString(arr.get(position).getSong_name());
            if (startPos != -1) // This should always be true, just a sanity check
            {
                int index = arr.get(position).getSong_name().toLowerCase().indexOf(keyword.toLowerCase());
                while (index >= 0) {
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(obj.getResources().getString(R.string.highlightedText))});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    if (index != 1)
                        spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    global_search_songlist_songname.setText(spannable);
                    index = arr.get(position).getSong_name().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
                }
            } else
                global_search_songlist_songname.setText(arr.get(position).getSong_name());

           final CheckBox addtocart = (CheckBox) row.findViewById(R.id.addtocart);
            mp3hindi_songlist_moviename.setText(arr.get(position).getAlbum_name());

            if (search_from.equals("devotional")) {
                if (arr.get(position).getAlbum_image() != null) {
                    ((DevotionalSearchActivity) obj).setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), arr.get(position).getAlbum_image(), song_album_image);
                }

                addtocart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        ((DevotionalSearchActivity) obj).AddToCartWithoutLogin(((DevotionalSearchActivity) obj).getAppConfigJson().getC_type().getSONG() + "", arr.get(position).getSong_id() + "");
                        QuickAction quickAction =   ((DevotionalSearchActivity) obj).setupQuickAction(arr.get(position).getSong_id(), ((DevotionalSearchActivity) obj).getAppConfigJson().getC_type().getSONG());
                        quickAction.show(addtocart);
                    }
                });
            }
            else if (search_from.equals("others")) {
                if (arr.get(position).getAlbum_image() != null) {
                    ((DevotionalSearchActivity) obj).setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), arr.get(position).getAlbum_image(), song_album_image);
                }

                addtocart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        ((DevotionalSearchActivity) obj).AddToCartWithoutLogin(((DevotionalSearchActivity) obj).getAppConfigJson().getC_type().getSONG() + "", arr.get(position).getSong_id() + "");
                        QuickAction quickAction =   ((DevotionalSearchActivity) obj).setupQuickAction(arr.get(position).getSong_id(), ((DevotionalSearchActivity) obj).getAppConfigJson().getC_type().getSONG());
                        quickAction.show(addtocart);
                    }
                });
            }
            else {
                if (arr.get(position).getAlbum_image() != null) {
                    ((HindiFilmsSearchActivity) obj).setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), arr.get(position).getAlbum_image(), song_album_image);
                }

                addtocart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        ((HindiFilmsSearchActivity) obj).AddToCartWithoutLogin(((HindiFilmsSearchActivity) obj).getAppConfigJson().getC_type().getSONG() + "", arr.get(position).getSong_id() + "");
                        QuickAction quickAction =   ((HindiFilmsSearchActivity) obj).setupQuickAction(arr.get(position).getSong_id(), ((HindiFilmsSearchActivity) obj).getAppConfigJson().getC_type().getSONG());
                        quickAction.show(addtocart);
                    }
                });
            }
        }
        return (row);
    }
}