package com.saregama.musicstore.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.DevotionalSearchActivity;
import com.saregama.musicstore.activity.HindiFilmsSearchActivity;
import com.saregama.musicstore.pojo.ClassHindArtistListPojo;
import com.saregama.musicstore.views.CircularImageView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 27-Jul-16.
 */
public class SearchArtisteAdapter extends BaseAdapter {

    private Activity obj;
    private ArrayList<ClassHindArtistListPojo> arr;
    private String keyword;
    private String search_from;

    public SearchArtisteAdapter(Activity obj, ArrayList<ClassHindArtistListPojo> arr, String keyword, String search_from) {
        this.obj = obj;
        this.arr = arr;
        this.keyword = keyword.replace("%20", " ");
        this.search_from = search_from;
    }

    public int getCount() {
        return arr.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) obj.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row;
        row = inflater.inflate(R.layout.classical_hindustani_artist, parent, false);

        if (arr != null && arr.get(position) != null) {
            TextView artisteName = (TextView) row.findViewById(R.id.classhindustani_artist_albumname);
            TextView trackCount = (TextView) row.findViewById(R.id.classhindustani_artist_trackcount);
            CircularImageView artisteImage = (CircularImageView) row.findViewById(R.id.classhindustani_artist_imgArtist);

            int startPos = arr.get(position).getName().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
            Spannable spannable = new SpannableString(arr.get(position).getName());
            if (startPos != -1) // This should always be true, just a sanity check
            {
                int index = arr.get(position).getName().toLowerCase().indexOf(keyword.toLowerCase());
                while (index >= 0) {
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(obj.getResources().getString(R.string.highlightedText))});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    if (index != 1)
                        spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    artisteName.setText(spannable);
                    index = arr.get(position).getName().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
                }
            } else
                artisteName.setText(arr.get(position).getName());

            trackCount.setText(arr.get(position).getSong_count() + " Songs");

            row.setTag(R.string.key, position);
            row.setTag(R.string.data, arr.get(position));
            if (search_from.equals("devotional")) {
                if (arr.get(position).getImage() != null) {
                    ((BaseActivity) obj).setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.artiste_list_image_size), (int) obj.getResources().getDimension(R.dimen.artiste_list_image_size), arr.get(position).getImage(), artisteImage);
                    artisteImage.setScaleType(ImageView.ScaleType.FIT_XY);
                }
                row.setOnClickListener((DevotionalSearchActivity) obj);
            }
            else if (search_from.equals("classical_hindustani")) {
                if (arr.get(position).getImage() != null) {
                    ((BaseActivity) obj).setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.artiste_list_image_size), (int) obj.getResources().getDimension(R.dimen.artiste_list_image_size), arr.get(position).getImage(), artisteImage);
                    artisteImage.setScaleType(ImageView.ScaleType.FIT_XY);
                }
                row.setOnClickListener((DevotionalSearchActivity) obj);
            }
            else {
                if (arr.get(position).getImage() != null) {
                    ((BaseActivity) obj).setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.artiste_list_image_size), (int) obj.getResources().getDimension(R.dimen.artiste_list_image_size), arr.get(position).getImage(), artisteImage);
                    artisteImage.setScaleType(ImageView.ScaleType.FIT_XY);
                }
                row.setOnClickListener((HindiFilmsSearchActivity) obj);
            }
        }
        return (row);
    }
}
