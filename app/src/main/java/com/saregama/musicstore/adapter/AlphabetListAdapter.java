package com.saregama.musicstore.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;


public class AlphabetListAdapter extends BaseAdapter {
    String[] alphabetName;
    Context context;
    private int selectedIndex = -1;
    private static LayoutInflater inflater = null;

    public AlphabetListAdapter(Context context, String[] alphabetName) {
        this.context = context;
        this.alphabetName = alphabetName;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setSelectedIndex(int ind) {
        selectedIndex = ind;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return alphabetName.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView tv;
        LinearLayout alpha_layout;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.alphabat_list_layout, null);
        holder.tv = (TextView) rowView.findViewById(R.id.alphabetName);
        holder.tv.setTextSize(TypedValue.COMPLEX_UNIT_SP,context.getResources().getInteger(R.integer.alphabetic_text_size));
        holder.alpha_layout = (LinearLayout) rowView.findViewById(R.id.alpha_layout);
        holder.tv.setText(alphabetName[position]);
        if (selectedIndex != -1 && position == selectedIndex) {
            StateListDrawable states = new StateListDrawable();
            states.addState(new int[]{}, ContextCompat.getDrawable(context, R.drawable.pressed));
            holder.alpha_layout.setBackground(states);
            holder.tv.setTextColor(Color.parseColor("white"));
        } else {
            StateListDrawable states = new StateListDrawable();
            states.addState(new int[]{}, ContextCompat.getDrawable(context, R.drawable.normal));
            holder.alpha_layout.setBackground(states);
            holder.tv.setTextColor(Color.parseColor("black"));
        }
        return rowView;
    }
}
