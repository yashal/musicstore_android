package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.ClassicalHindustaniActivity;
import com.saregama.musicstore.pojo.ClassHindInstrumentListPojo;
import com.saregama.musicstore.views.CircularImageView;

import java.util.ArrayList;
import java.util.Locale;


public class HindustaniInstrumentAdapter extends RecyclerView.Adapter<HindustaniInstrumentAdapter.ViewHolder> {

    private ArrayList<ClassHindInstrumentListPojo> arrayCategoryList;
    private String from;
    private int lastPosition = -1;
    private Activity context;
    private String keyword;

    public HindustaniInstrumentAdapter(String from, Activity context, ArrayList<ClassHindInstrumentListPojo> arrayCategoryList, String keyword) {
        this.context = context;
        this.arrayCategoryList = arrayCategoryList;
        this.keyword = keyword.replace("%20", " ");
        this.from = from;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classical_hindustani_instrument, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ClassHindInstrumentListPojo data = arrayCategoryList.get(position);
        if(keyword != null && keyword.length() > 0) {
            int startPos = data.getTag_name().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
            Spannable spannable = new SpannableString(data.getTag_name());
            if (startPos != -1) // This should always be true, just a sanity check
            {
                int index = data.getTag_name().toLowerCase().indexOf(keyword.toLowerCase());
                while (index >= 0) {
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(context.getResources().getString(R.string.highlightedText))});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    if (index != 1)
                        spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder.albumName.setText(spannable);
                    index = data.getTag_name().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
                }
            } else
                holder.albumName.setText(data.getTag_name());
        }
        else
            holder.albumName.setText(data.getTag_name());

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(10, 12, 5, 12);

        if(from.equals("raaga")) {
            holder.albumImage.setVisibility(View.GONE);
            holder.image_layout.setVisibility(View.GONE);
            holder.albumName.setLayoutParams(layoutParams);
        }
        else{
            ((BaseActivity) context).setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.artiste_list_image_size), (int) context.getResources().getDimension(R.dimen.artiste_list_image_size), data.getImage(), holder.albumImage);
            holder.albumImage.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        holder.trackCount.setVisibility(View.GONE);
        holder.itemView.setTag(R.string.key, position);
        holder.itemView.setTag(R.string.data, data);

        holder.itemView.setOnClickListener((ClassicalHindustaniActivity) context);

        setAnimation(holder.itemView, position);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return arrayCategoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView albumName, trackCount;
        private LinearLayout image_layout;
        private CircularImageView albumImage;

        public ViewHolder(View itemView) {
            super(itemView);
            albumName = (TextView) itemView.findViewById(R.id.classhindustani_instrument_albumname);
            trackCount = (TextView) itemView.findViewById(R.id.classhindustani_instrument_trackcount);
            albumImage = (CircularImageView) itemView.findViewById(R.id.classhindustani_instrument_imgArtist);
            image_layout = (LinearLayout) itemView.findViewById(R.id.image_layout);
        }
    }
}