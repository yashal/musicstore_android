package com.saregama.musicstore.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.CartActivity;
import com.saregama.musicstore.pojo.CartItemPojo;

import java.util.ArrayList;

/**
 * Created by Administrator on 12-Apr-16.
 */
public class CartSongAdapter extends BaseAdapter {

    private CartActivity obj;
    private ArrayList<CartItemPojo> arr;

    public CartSongAdapter(CartActivity obj, ArrayList<CartItemPojo> arr) {
        this.obj = obj;
        this.arr = arr;
    }

    public int getCount() {
        return arr.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) obj.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row;
        row = inflater.inflate(R.layout.cart_song_row_layout, parent, false);
        CheckBox addtocart = (CheckBox) row.findViewById(R.id.addtocart);
        addtocart.setVisibility(View.GONE);
        if(arr != null && arr.get(position) != null)
        {
            TextView mp3hindi_songlist_songname = (TextView) row.findViewById(R.id.mp3hindi_songlist_songname);
            TextView mp3hindi_songlist_moviename = (TextView) row.findViewById(R.id.mp3hindi_songlist_moviename);
            TextView song_type = (TextView) row.findViewById(R.id.song_type);

            LinearLayout remove_album_fron_cart = (LinearLayout) row.findViewById(R.id.remove_album_fron_cart);
            remove_album_fron_cart.setVisibility(View.VISIBLE);
            song_type.setVisibility(View.VISIBLE);

            if(arr.get(position).getCtype().equals("1"))
                song_type.setText(".MP3");
            else
                song_type.setText(".HD");

            mp3hindi_songlist_songname.setText(arr.get(position).getTitle());
            mp3hindi_songlist_moviename.setText(arr.get(position).getAlbum());

            remove_album_fron_cart.setTag(R.string.key, position);
            remove_album_fron_cart.setTag(R.string.data, arr.get(position));
            remove_album_fron_cart.setTag(R.string.from, "song");
            remove_album_fron_cart.setOnClickListener(obj);
        }
        return (row);
    }
}