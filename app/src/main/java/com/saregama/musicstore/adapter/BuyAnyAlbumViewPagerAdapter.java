package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.saregama.musicstore.fragments.BuyAnyAlbum_AlbumFragment;

import java.util.ArrayList;

/**
 * Created by yesh on 3/9/2016.
 */
public class BuyAnyAlbumViewPagerAdapter  extends FragmentPagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public BuyAnyAlbumViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                BuyAnyAlbum_AlbumFragment fragment = new BuyAnyAlbum_AlbumFragment();
                mPageReferenceMap.add(index, fragment);
                return fragment;
//            case 1:
//                return new BuyAnyAlbum_GeetmalaFragment();
        }
        return null;
    }

    @Override
    public int getCount() {

        return 1;
    }

    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }


}
