package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.DownloadMgrAlbumDetails;
import com.saregama.musicstore.pojo.DownloadManagerAlbumSongs;

import java.util.ArrayList;

/**
 * Created by Administrator on 12-Apr-16.
 */
public class DownloadAlbumDetailsAdapter extends RecyclerView.Adapter<DownloadAlbumDetailsAdapter.ViewHolder> {

    private ArrayList<DownloadManagerAlbumSongs> arraySongList;
    private Activity context;
    private String totalsize = "";
    private String from;
    private int download_count;
    private String album_image;

    public DownloadAlbumDetailsAdapter(Activity context, ArrayList<DownloadManagerAlbumSongs> arraySongList, String from, String album_image) {
        this.context = context;
        this.arraySongList = arraySongList;
        this.from = from;
        this.album_image = album_image;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mp3hindi_songlist_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DownloadManagerAlbumSongs data = arraySongList.get(position);

        String _totalsize;
        if (arraySongList.get(position).getC_type().equals(((DownloadMgrAlbumDetails) context).getAppConfigJson().getC_type().getSONGHD()+""))
            _totalsize = arraySongList.get(position).getWav_file_size();
        else
            _totalsize = arraySongList.get(position).getFile_size();

        double temp_size = (Double.parseDouble(_totalsize)) / 1024;
        totalsize = String.format("%.2f", temp_size);

        holder.song_name.setText(arraySongList.get(position).getSong_name());
        holder.album_name.setText(arraySongList.get(position).getAlbum_name());

        if (album_image != null)
            ((DownloadMgrAlbumDetails) context).setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.song_listing_album_image), (int) context.getResources().getDimension(R.dimen.song_listing_album_image), album_image, holder.song_album_image);

        download_count = Integer.parseInt(data.getDownload_count());
        if (download_count < 10) {
            holder.downloaded.setVisibility(View.VISIBLE);
        } else {
            holder.downloaded.setVisibility(View.INVISIBLE);
        }

        holder.addtocart.setVisibility(View.GONE);
        holder.song_type.setVisibility(View.VISIBLE);

        if (from.equals("downloaded")) {
            holder.downloaded.setBackgroundResource(R.mipmap.download_icon);
        } else if (from.equals("download_now")) {
            holder.downloaded.setBackgroundResource(R.mipmap.download_green);
        }

        holder.downloaded.setTag(R.string.key, position);
        holder.downloaded.setTag(R.string.data, data);
        holder.downloaded.setOnClickListener((DownloadMgrAlbumDetails) context);

//        holder.song_type.setText(totalsize + "mb");
    }

    @Override
    public int getItemCount() {
        return arraySongList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView song_name;
        private TextView album_name;
        private CheckBox addtocart;
        private TextView song_type;
        private ImageView downloaded, song_album_image;

        public ViewHolder(View itemView) {
            super(itemView);
            song_name = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_songname);
            album_name = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_moviename);
            addtocart = (CheckBox) itemView.findViewById(R.id.addtocart);
            downloaded = (ImageView) itemView.findViewById(R.id.mp3hindi_songlist_cart);
            song_album_image = (ImageView) itemView.findViewById(R.id.song_album_image);
            song_type = (TextView) itemView.findViewById(R.id.song_type);
        }
    }
}
