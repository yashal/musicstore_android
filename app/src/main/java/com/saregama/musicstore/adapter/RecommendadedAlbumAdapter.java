package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.RecommendationActivity;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;
import java.util.Locale;


public class RecommendadedAlbumAdapter extends RecyclerView.Adapter<RecommendadedAlbumAdapter.ViewHolder> {

    private ArrayList<MP3HindiAlbumListPojo> arrayCategoryList;
    private String from;
    private int lastPosition = -1;
    private Activity context;
    private String keyword, c_type;

    public RecommendadedAlbumAdapter(Activity context, ArrayList<MP3HindiAlbumListPojo> arrayCategoryList, String c_type) {
        this.context = context;
        this.arrayCategoryList = arrayCategoryList;
        this.c_type = c_type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recommended_albumlist_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MP3HindiAlbumListPojo data = arrayCategoryList.get(position);
        if (keyword != null && keyword.length() > 0) {
            int startPos = data.getAlbum_name().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
            Spannable spannable = new SpannableString(data.getAlbum_name());
            if (startPos != -1) // This should always be true, just a sanity check
            {
                int index = data.getAlbum_name().toLowerCase().indexOf(keyword.toLowerCase());
                while (index >= 0) {
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(context.getResources().getString(R.string.highlightedText))});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    if (index != 1)
                        spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder.songname.setText(spannable);
                    index = data.getAlbum_name().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
                }
            } else
                holder.songname.setText(data.getAlbum_name());
        } else
            holder.songname.setText(data.getAlbum_name());

        holder.song_count.setText("("+data.getSong_count() +" Tracks)");

        float alpha = Float.valueOf(context.getResources().getString(R.string.alpha_value));

        holder.seekbar.setVisibility(View.GONE);
        holder.song_album_image.setVisibility(View.VISIBLE);
        ((BaseActivity) context).setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.song_listing_album_image), (int) context.getResources().getDimension(R.dimen.song_listing_album_image), data.getAlbum_image(), holder.song_album_image);
//        holder.seekbar.setImageResource(R.mipmap.play_icon);

        holder.addtocart.setTag(R.string.data, data);
        holder.addtocart.setTag(R.string.addtocart_id, holder.addtocart);
        holder.addtocart.setTag(R.string.c_type, c_type);

        holder.addtocart.setOnClickListener((RecommendationActivity)context);
        setAnimation(holder.itemView, position);

        holder.itemView.setTag(R.string.key, position);
        holder.itemView.setTag(R.string.data, data);
        holder.itemView.setTag(R.string.from, from);
        holder.itemView.setOnClickListener((RecommendationActivity) context);
    }


    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return arrayCategoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView songname, song_count;
        private CheckBox addtocart;
        private ImageView seekbar, song_album_image;

        public ViewHolder(View itemView) {
            super(itemView);
            songname = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_songname);
            song_count = (TextView) itemView.findViewById(R.id.song_count_recommended);
            addtocart = (CheckBox) itemView.findViewById(R.id.addtocart_album);
            seekbar = (ImageView) itemView.findViewById(R.id.playIcon);
            song_album_image = (ImageView) itemView.findViewById(R.id.song_album_image);
        }
    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = context.getSharedPreferences(SaregamaConstants.PREF_NAME, context.MODE_PRIVATE);
        return prefs.getString(key, SaregamaConstants.DEFAULT_VALUE);
    }
}