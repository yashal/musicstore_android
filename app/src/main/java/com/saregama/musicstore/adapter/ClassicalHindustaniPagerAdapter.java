package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.saregama.musicstore.fragments.HindustaniArtistFragment;
import com.saregama.musicstore.fragments.HindustaniInstrumentsFragment;
import com.saregama.musicstore.fragments.HindustaniRaagasFragment;

import java.util.ArrayList;

/**
 * Created by yesh on 3/7/2016.
 */
public class ClassicalHindustaniPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public ClassicalHindustaniPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                HindustaniArtistFragment fragment_hind_artis = new HindustaniArtistFragment();
                mPageReferenceMap.add(position, fragment_hind_artis);
                return fragment_hind_artis;
            case 1:
                HindustaniInstrumentsFragment fragment_hind_instru = new HindustaniInstrumentsFragment();
                mPageReferenceMap.add(position, fragment_hind_instru);
                return fragment_hind_instru;
            case 2:
                HindustaniRaagasFragment fragment_hind_ragaas = new HindustaniRaagasFragment();
                mPageReferenceMap.add(position, fragment_hind_ragaas);
                return fragment_hind_ragaas;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}
