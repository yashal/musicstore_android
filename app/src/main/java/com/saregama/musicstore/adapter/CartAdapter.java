package com.saregama.musicstore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.CartActivity;
import com.saregama.musicstore.pojo.Cart;
import com.saregama.musicstore.pojo.CartItemPojo;
import com.saregama.musicstore.views.CartIngredientViewHolder;
import com.saregama.musicstore.views.CartViewHolder;

import java.util.List;

/**
 * Created by Yash ji on 6/24/2016.
 */
public class CartAdapter extends ExpandableRecyclerAdapter<CartViewHolder, CartIngredientViewHolder> {

    private LayoutInflater mInflator;
    private CartActivity obj;
    private String mName;
    private int songs_count,albumCount;

    public CartAdapter(CartActivity obj, Context context,  List<? extends ParentListItem> parentItemList, int songs_count, int albumCount) {
        super(parentItemList);
        mInflator = LayoutInflater.from(context);
        this.obj = obj;
        this.songs_count =  songs_count;
        this.albumCount =  albumCount;
    }

    @Override
    public CartViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View recipeView = mInflator.inflate(R.layout.cart_view, parentViewGroup, false);
        return new CartViewHolder(recipeView);
    }

    @Override
    public CartIngredientViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View ingredientView = mInflator.inflate(R.layout.cart_song_row_layout, childViewGroup, false);
        return new CartIngredientViewHolder(ingredientView);
    }

    @Override
    public void onBindParentViewHolder(CartViewHolder recipeViewHolder, int position, ParentListItem parentListItem) {
        Cart cart = (Cart) parentListItem;
        recipeViewHolder.bind(cart, songs_count, albumCount);
    }

    @Override
    public void onBindChildViewHolder(CartIngredientViewHolder ingredientViewHolder, int position, Object childListItem) {
        CartItemPojo cartItem = (CartItemPojo) childListItem;
        ingredientViewHolder.bind(cartItem, obj, position);
    }

}
