package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.saregama.musicstore.fragments.GlobalAlbumsFragment;
import com.saregama.musicstore.fragments.GlobalArtistesFragment;
import com.saregama.musicstore.fragments.GlobalSongsFragment;

/**
 * Created by quepplin1 on 5/12/2016.
 */
public class GlobalSearchTabListAdapter extends FragmentPagerAdapter {

    public GlobalSearchTabListAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new GlobalSongsFragment();
            case 1:
                return new GlobalAlbumsFragment();
            case 2:
                return new GlobalArtistesFragment();
        }

            return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
