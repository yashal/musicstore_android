package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.ClassicalFusionListActivity;
import com.saregama.musicstore.activity.ClassicalHindustaniActivity;
import com.saregama.musicstore.activity.GajalSufiActivity;
import com.saregama.musicstore.activity.Mp3Devotional;
import com.saregama.musicstore.activity.Mp3RegionalActivity;
import com.saregama.musicstore.pojo.ClassHindArtistListPojo;
import com.saregama.musicstore.views.CircularImageView;

import java.util.ArrayList;
import java.util.Locale;


public class HindustaniArtistAdapter extends RecyclerView.Adapter<HindustaniArtistAdapter.ViewHolder> {

    private ArrayList<ClassHindArtistListPojo> arrayCategoryList;
    private String from;
    private Activity context;
    private int lastPosition = -1;
    private String keyword;

    public HindustaniArtistAdapter(Activity context, ArrayList<ClassHindArtistListPojo> arrayCategoryList, String from, String keyword) {
        this.from = from;
        this.context = context;
        this.arrayCategoryList = arrayCategoryList;
        this.keyword = keyword.replace("%20", " ");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classical_hindustani_artist, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ClassHindArtistListPojo data = arrayCategoryList.get(position);

        if(keyword != null && keyword.length() > 0) {
            int startPos = data.getName().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
            Spannable spannable = new SpannableString(data.getName());
            if (startPos != -1) // This should always be true, just a sanity check
            {
                int index = data.getName().toLowerCase().indexOf(keyword.toLowerCase());
                while (index >= 0) {
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(context.getResources().getString(R.string.highlightedText))});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    if (index != 1)
                        spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder.albumName.setText(spannable);
                    index = data.getName().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
                }
            } else
                holder.albumName.setText(data.getName());
        }
        else
            holder.albumName.setText(data.getName());
        ((BaseActivity) context).setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.artiste_list_image_size), (int) context.getResources().getDimension(R.dimen.artiste_list_image_size), data.getImage(), holder.albumImage);
        holder.albumImage.setScaleType(ImageView.ScaleType.FIT_XY);

        holder.itemView.setTag(R.string.key, position);
        holder.itemView.setTag(R.string.data, data);

        if (from.equals("devotionl_mp3")) {
            holder.trackCount.setVisibility(View.VISIBLE);
            holder.trackCount.setText(data.getSong_count() + " Songs");
            holder.itemView.setOnClickListener((Mp3Devotional) context);
        } else if (from.equals("fusion_mp3")) {
            holder.itemView.setOnClickListener((ClassicalFusionListActivity) context);
        } else if (from.equals("regional_mp3")) {
            holder.trackCount.setVisibility(View.VISIBLE);
            holder.trackCount.setText(data.getSong_count() + " songs");
            holder.itemView.setOnClickListener((Mp3RegionalActivity) context);
        } else if (from.equals("gajal_sufi")) {
            holder.trackCount.setVisibility(View.VISIBLE);
            holder.trackCount.setText(data.getSong_count() + " songs");
            holder.itemView.setOnClickListener((GajalSufiActivity) context);
        } else {
            holder.itemView.setOnClickListener((ClassicalHindustaniActivity) context);
        }

        setAnimation(holder.itemView, position);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return arrayCategoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView albumName, trackCount;
        private CircularImageView albumImage;

        public ViewHolder(View itemView) {
            super(itemView);
            albumName = (TextView) itemView.findViewById(R.id.classhindustani_artist_albumname);
            trackCount = (TextView) itemView.findViewById(R.id.classhindustani_artist_trackcount);
            albumImage = (CircularImageView) itemView.findViewById(R.id.classhindustani_artist_imgArtist);
        }
    }
}

