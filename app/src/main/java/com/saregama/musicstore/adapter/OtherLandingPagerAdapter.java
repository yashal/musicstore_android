package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.saregama.musicstore.fragments.FolkFragment;
import com.saregama.musicstore.fragments.PopFragment;

import java.util.ArrayList;

/**
 * Created by navneet on 7/3/2016.
 */
public class OtherLandingPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public OtherLandingPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PopFragment fragment_pop = new PopFragment();
                mPageReferenceMap.add(position, fragment_pop);
                return fragment_pop;
            case 1:
                FolkFragment fragment_folk = new FolkFragment();
                mPageReferenceMap.add(position, fragment_folk);
                return fragment_folk;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}
