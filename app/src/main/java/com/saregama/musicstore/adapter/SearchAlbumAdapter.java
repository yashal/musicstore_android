package com.saregama.musicstore.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.HindiFilmsSearchActivity;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 27-Jul-16.
 */
public class SearchAlbumAdapter extends BaseAdapter {

    private Activity obj;
    private ArrayList<MP3HindiAlbumListPojo> arr;
    private String keyword;
    private String search_from;

    public SearchAlbumAdapter(Activity obj, ArrayList<MP3HindiAlbumListPojo> arr, String keyword, String search_from) {
        this.obj = obj;
        this.arr = arr;
        this.keyword = keyword.replace("%20", " ");
        this.search_from = search_from;
    }

    public int getCount() {
        return arr.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) obj.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row;
        row = inflater.inflate(R.layout.mp3hindi_albumlist_layout, parent, false);

        if (arr != null && arr.get(position) != null) {
            TextView albumName = (TextView) row.findViewById(R.id.mp3hindi_albumlist_albumname);
            TextView trackCount = (TextView) row.findViewById(R.id.mp3hindi_albumlist_trackcount);
            ImageView albumImage = (ImageView) row.findViewById(R.id.mp3hindi_albumlist_Image);
            CheckBox album_addtocart = (CheckBox) row.findViewById(R.id.album_addtocart);

            int startPos = arr.get(position).getAlbum_name().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
            Spannable spannable = new SpannableString(arr.get(position).getAlbum_name());
            if (startPos != -1) // This should always be true, just a sanity check
            {
                int index = arr.get(position).getAlbum_name().toLowerCase().indexOf(keyword.toLowerCase());
                while (index >= 0) {
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(obj.getResources().getString(R.string.highlightedText))});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    if (index != 1)
                        spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    albumName.setText(spannable);
                    index = arr.get(position).getAlbum_name().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
                }
            } else
                albumName.setText(arr.get(position).getAlbum_name());

            trackCount.setText("("+arr.get(position).getSong_count()+" tracks)");
            row.setTag(R.string.key, position);
            row.setTag(R.string.data, arr.get(position));
            row.setTag(R.string.from, search_from);

            if (arr.get(position).getAlbum_image() != null) {
                ((BaseActivity) obj).setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), arr.get(position).getAlbum_image(), albumImage);
            }

            row.setOnClickListener((HindiFilmsSearchActivity) obj);

            album_addtocart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HindiFilmsSearchActivity) obj).AddToCartWithoutLogin(((HindiFilmsSearchActivity) obj).getAppConfigJson().getC_type().getALBUM() + "", arr.get(position).getAlbum_id() + "");
                }
            });
        }
        return (row);
    }
}