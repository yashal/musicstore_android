package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.saregama.musicstore.fragments.GajalSufiAlbumFragment;
import com.saregama.musicstore.fragments.GajalSufiArtisteFragment;
import com.saregama.musicstore.fragments.GajalSufiSongFragment;

import java.util.ArrayList;

/**
 * Created by yesh on 3/7/2016.
 */
public class GajalSufiPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public GajalSufiPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                GajalSufiSongFragment fragment_gajal_song = new GajalSufiSongFragment();
                mPageReferenceMap.add(position, fragment_gajal_song);
                return fragment_gajal_song;
            case 1:
                GajalSufiAlbumFragment fragment_gajal_album = new GajalSufiAlbumFragment();
                if (mPageReferenceMap.size() == 0) {
                    GajalSufiSongFragment fragment = new GajalSufiSongFragment();
                    mPageReferenceMap.add(0, fragment);
                }
                mPageReferenceMap.add(position, fragment_gajal_album);
                return fragment_gajal_album;
            case 2:
                GajalSufiArtisteFragment fragment_gajal_artist = new GajalSufiArtisteFragment();
                if (mPageReferenceMap.size() == 0) {
                    GajalSufiSongFragment fragment = new GajalSufiSongFragment();
                    mPageReferenceMap.add(0, fragment);

                    GajalSufiAlbumFragment fragment_album = new GajalSufiAlbumFragment();
                    mPageReferenceMap.add(1, fragment_album);
                }
                mPageReferenceMap.add(position, fragment_gajal_artist);
                return fragment_gajal_artist;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}
