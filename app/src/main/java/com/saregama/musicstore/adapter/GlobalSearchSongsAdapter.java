package com.saregama.musicstore.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.GlobalSearchActivity;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;
import java.util.Locale;

import static android.R.attr.alpha;

/**
 * Created by bhanu on 26-Apr-16.
 */
public class GlobalSearchSongsAdapter extends BaseAdapter {

    private GlobalSearchActivity obj;
    private ArrayList<MP3HindiSongListPojo> arr;
    private String keyword;

    public GlobalSearchSongsAdapter(GlobalSearchActivity obj, ArrayList<MP3HindiSongListPojo> arr, String keyword) {
        this.obj = obj;
        this.arr = arr;
        this.keyword = keyword.replace("%20", " ");
    }

    public int getCount() {
        return arr.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) obj.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row;
        row = inflater.inflate(R.layout.mp3hindi_songlist_layout, parent, false);

        if (arr != null && arr.get(position) != null) {
            final MP3HindiSongListPojo data = arr.get(position);
            if (getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id())) {
                data.setIs_playing(true);
            } else {
                data.setIs_playing(false);
            }
            TextView global_search_songlist_songname = (TextView) row.findViewById(R.id.mp3hindi_songlist_songname);
            TextView mp3hindi_songlist_moviename = (TextView) row.findViewById(R.id.mp3hindi_songlist_moviename);
            ImageView song_album_image = (ImageView) row.findViewById(R.id.song_album_image);
            final ImageView seekbar = (ImageView) row.findViewById(R.id.playIcon);
            LinearLayout opacityFilter = (LinearLayout) row.findViewById(R.id.opacityFilter);
            LinearLayout slider_transparent = (LinearLayout) row.findViewById(R.id.slider_transparent);

            if (data.is_playing()) {
                if (SaregamaConstants.IS_PLAYING) {
                    seekbar.setOnClickListener(obj);
                    global_search_songlist_songname.setOnClickListener(obj);
                    slider_transparent.setOnClickListener(obj);
                } else {
                    seekbar.setOnClickListener(null);
                    global_search_songlist_songname.setOnClickListener(null);
                    slider_transparent.setOnClickListener(null);
                }
                seekbar.setVisibility(View.VISIBLE);
                song_album_image.setAlpha(alpha);
                opacityFilter.setVisibility(View.VISIBLE);
                seekbar.setImageResource(R.mipmap.pause_icon);
            } else {
                seekbar.setOnClickListener(obj);
                global_search_songlist_songname.setOnClickListener(obj);
                slider_transparent.setOnClickListener(obj);
                seekbar.setVisibility(View.GONE);
                song_album_image.setAlpha(1.0f);
                opacityFilter.setVisibility(View.GONE);
                seekbar.setImageResource(R.mipmap.play_icon);
            }

            if (getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id()) && SaregamaConstants.IS_PLAYING && !SaregamaConstants.IS_PAUSED) {
                seekbar.setVisibility(View.VISIBLE);
                song_album_image.setAlpha(alpha);
                opacityFilter.setVisibility(View.VISIBLE);
                seekbar.setImageResource(R.mipmap.pause_icon);
            } else if (getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id()) && SaregamaConstants.IS_PLAYING && SaregamaConstants.IS_PAUSED) {
                seekbar.setVisibility(View.VISIBLE);
                song_album_image.setAlpha(alpha);
                opacityFilter.setVisibility(View.VISIBLE);
                seekbar.setImageResource(R.mipmap.play_icon);
            } else if (!getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id()) && !SaregamaConstants.IS_PLAYING && !SaregamaConstants.IS_PAUSED) {
                seekbar.setVisibility(View.GONE);
                song_album_image.setAlpha(1.0f);
                opacityFilter.setVisibility(View.GONE);
                seekbar.setImageResource(R.mipmap.play_icon);
            }

            // song name click handle
            global_search_songlist_songname.setTag(R.string.key, position);
            global_search_songlist_songname.setTag(R.string.data, data);
            global_search_songlist_songname.setTag(R.string.seekbar_id, seekbar);

            // row click handle
            slider_transparent.setTag(R.string.key, position);
            slider_transparent.setTag(R.string.data, data);
            slider_transparent.setTag(R.string.seekbar_id, seekbar);

            seekbar.setTag(R.string.key, position);
            seekbar.setTag(R.string.data, data);
            seekbar.setTag(R.string.from, R.id.playIcon);

            song_album_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    seekbar.performClick();
                }
            });

            int startPos = data.getSong_name().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
            Spannable spannable = new SpannableString(data.getSong_name());
            if (startPos != -1) // This should always be true, just a sanity check
            {
                int index = data.getSong_name().toLowerCase().indexOf(keyword.toLowerCase());
                while (index >= 0) {
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(obj.getResources().getString(R.string.highlightedText))});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    if (index != 1)
                        spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    global_search_songlist_songname.setText(spannable);
                    index = data.getSong_name().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
                }
            } else
                global_search_songlist_songname.setText(data.getSong_name());

            if (data.getImage() != null) {
                obj.setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), data.getImage(), song_album_image);
            }

            final CheckBox addtocart = (CheckBox) row.findViewById(R.id.addtocart);

            mp3hindi_songlist_moviename.setText(data.getAlbum_name());

            addtocart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuickAction quickAction = obj.setupQuickAction(data.getSong_id() + "", obj.getAppConfigJson().getC_type().getSONG());
                    quickAction.show(addtocart);
//                    obj.AddToCartWithoutLogin(obj.getAppConfigJson().getC_type().getSONG()+"", data.getContent_id() + "");
                }
            });
        }
        return (row);
    }

    private String getFromPrefs(String key) {
        SharedPreferences prefs = obj.getSharedPreferences(SaregamaConstants.PREF_NAME, obj.MODE_PRIVATE);
        return prefs.getString(key, SaregamaConstants.DEFAULT_VALUE);
    }
}
