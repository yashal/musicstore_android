package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.DownloadManagerActivity;
import com.saregama.musicstore.pojo.DownloadManagerSongPojo;

import java.util.ArrayList;

/**
 * Created by Administrator on 12-Apr-16.
 */
public class DownloadSongAdapter extends RecyclerView.Adapter<DownloadSongAdapter.ViewHolder> {

    private ArrayList<DownloadManagerSongPojo> arraySongList;
    private DownloadManagerActivity context;
    private String totalsize = "";
    private String from;
    private int download_count;

    public DownloadSongAdapter(Activity context, ArrayList<DownloadManagerSongPojo> arraySongList, String from) {
        this.context = (DownloadManagerActivity) context;
        this.arraySongList = arraySongList;
        this.from = from;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mp3hindi_songlist_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DownloadManagerSongPojo data = arraySongList.get(position);
        holder.song_type.setVisibility(View.VISIBLE);

        String _totalsize;
        if (arraySongList.get(position).getC_type().equals(context.getAppConfigJson().getC_type().getSONGHD()+"")) {
//            _totalsize = arraySongList.get(position).getWav_file_size();
//            holder.song_type.setText(".HD(" + totalsize + "mb)");
            holder.song_type.setText(".HD");
        }
        else {
//            _totalsize = arraySongList.get(position).getFile_size();
//            holder.song_type.setText(".MP3(" + totalsize + "mb)");
            holder.song_type.setText(".MP3");
        }

//        double temp_size = (Double.parseDouble(_totalsize)) / 1024;
//        totalsize = String.format("%.2f", temp_size);

        holder.mp3hindi_songlist_songname.setText(arraySongList.get(position).getSong_name());
//        holder.mp3hindi_songlist_moviename.setText(arraySongList.get(position).get());
        if (data.getImage() != null)
           context.setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.song_listing_album_image), (int) context.getResources().getDimension(R.dimen.song_listing_album_image), data.getImage(), holder.song_album_image);

        holder.addtocart.setVisibility(View.GONE);

        download_count = Integer.parseInt(data.getDownload_count());
        if (download_count < 10) {
            holder.downloaded.setVisibility(View.VISIBLE);
        } else {
            holder.downloaded.setVisibility(View.INVISIBLE);
        }

        if (from.equals("downloaded")) {
            holder.downloaded.setBackgroundResource(R.mipmap.download_icon);
        } else if (from.equals("download_now")) {
            holder.downloaded.setBackgroundResource(R.mipmap.download_green);
        }
        holder.song_type.setVisibility(View.VISIBLE);

        holder.downloaded.setTag(R.string.key, position);
        holder.downloaded.setTag(R.string.data, data);
        holder.downloaded.setOnClickListener(context);
    }

    @Override
    public int getItemCount() {
        return arraySongList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mp3hindi_songlist_songname;
        private TextView mp3hindi_songlist_moviename;
        private TextView song_type;
        private CheckBox addtocart;
        private ImageView downloaded, song_album_image;
        private LinearLayout opacityFilter;

        public ViewHolder(View itemView) {
            super(itemView);
            mp3hindi_songlist_songname = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_songname);
            mp3hindi_songlist_moviename = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_moviename);
            song_type = (TextView) itemView.findViewById(R.id.song_type);
            addtocart = (CheckBox) itemView.findViewById(R.id.addtocart);
            downloaded = (ImageView) itemView.findViewById(R.id.mp3hindi_songlist_cart);
            opacityFilter = (LinearLayout) itemView.findViewById(R.id.opacityFilter);
            song_album_image = (ImageView) itemView.findViewById(R.id.song_album_image);
        }
    }
}