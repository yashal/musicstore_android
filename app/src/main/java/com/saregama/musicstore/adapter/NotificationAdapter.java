package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.NotificationsActivity;
import com.saregama.musicstore.pojo.NotificationDataPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 5/31/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private ArrayList<NotificationDataPojo> arraynotificationList;
    private Activity context;

    public NotificationAdapter(Activity context, ArrayList<NotificationDataPojo> arraynotificationList) {
        this.arraynotificationList = arraynotificationList;
        this.context = (NotificationsActivity) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final NotificationDataPojo data = arraynotificationList.get(position);
        DisplayMetrics metrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        holder.banner.requestLayout();
        holder.notification_row_root_layout.setTag(R.string.data, data);
        holder.notification_row_root_layout.setOnClickListener((NotificationsActivity) context);
        if (data.getImage() == null || data.getImage().isEmpty()) {
            holder.banner.setVisibility(View.GONE);
        } else {
            holder.banner.setVisibility(View.VISIBLE);
            ((NotificationsActivity) context).setImageInLayout(context.getApplicationContext(), (int) context.getResources().getDimension(R.dimen.image_width_drawer_fragment), (int) context.getResources().getDimension(R.dimen.image_width_drawer_fragment), data.getImage(), holder.banner);
        }
        holder.notificationMessage.setText(data.getMessage());
        holder.notification_title_text.setText(data.getTitle());
    }

    @Override
    public int getItemCount() {
        return arraynotificationList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView banner;
        private TextView notificationMessage, notification_title_text;
        private LinearLayout notification_row_root_layout;

        public ViewHolder(View itemView) {
            super(itemView);
            notificationMessage = (TextView) itemView.findViewById(R.id.notification_message_text);
            notification_title_text = (TextView) itemView.findViewById(R.id.notification_title_text);
            banner = (ImageView) itemView.findViewById(R.id.notification_row_bannerImage);
            notification_row_root_layout = (LinearLayout) itemView.findViewById(R.id.notification_row_root_layout);
        }
    }
}
