package com.saregama.musicstore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.saregama.musicstore.R;
import com.saregama.musicstore.pojo.GlobalArtistPojo;
import com.saregama.musicstore.views.CustomViewGlobalSearchAlbum;

import java.util.List;

/**
 * Created by bhanu on 26-Apr-16.
 */
public class GlobalSearchAlbumAdapter extends ArrayAdapter<GlobalArtistPojo> {

    LayoutInflater inflater;
    List<GlobalArtistPojo> list;
    Context context;
    private String keyword;

    public GlobalSearchAlbumAdapter(Context context, List<GlobalArtistPojo> objects_list, String keyword) {
        super(context, R.layout.customview_global_search_ablum, objects_list);

        list = objects_list;
        this.context = context;
        this.keyword = keyword.replace("%20"," ");
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomViewGlobalSearchAlbum customViewGlobalSearchAlbum;

        if (convertView == null) {
            customViewGlobalSearchAlbum = (CustomViewGlobalSearchAlbum) inflater.inflate(
                    R.layout.customview_global_search_ablum, null);
        } else {
            customViewGlobalSearchAlbum = (CustomViewGlobalSearchAlbum) convertView;
        }
        customViewGlobalSearchAlbum.rowContent(list.get(position), (short) position, context, keyword);

        return customViewGlobalSearchAlbum;
    }
}
