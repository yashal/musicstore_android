package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.Mp3HindiAlbumDetail;
import com.saregama.musicstore.activity.RegionalMp3AlbumDetail;
import com.saregama.musicstore.pojo.AppConfigDataPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;

/**
 * Created by navneet on 8/3/2016.
 */
public class Mp3HindiAlbumDetailAdapter extends RecyclerView.Adapter<Mp3HindiAlbumDetailAdapter.ViewHolder> {

    private ArrayList<MP3HindiSongListPojo> arrayCategoryList;
    private Activity context;
    private String from;
    private int c_type;
    private String album_image;
    private int lastPosition = -1;

    public Mp3HindiAlbumDetailAdapter(Activity context, ArrayList<MP3HindiSongListPojo> arrayCategoryList, String from, int c_type, String album_image) {
        this.context = context;
        this.arrayCategoryList = arrayCategoryList;
        this.from = from;
        this.c_type = c_type;
        this.album_image = album_image;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mp3hindi_songlist_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MP3HindiSongListPojo data = arrayCategoryList.get(position);
        holder.songname.setText(data.getSong_name());
        String str_artistName = "";
        for (int i = 0; i < data.getArtist().size(); i++) {
            str_artistName = str_artistName + data.getArtist().get(i).getName() + ", ";
        }
        if (str_artistName != null & str_artistName.length() > 0)
            str_artistName = str_artistName.substring(0, str_artistName.length() - 2);
        if (c_type == getAppConfigJson().getC_type().getALBUM()) {
            holder.addtocart.setVisibility(View.GONE);
            holder.song_price.setVisibility(View.GONE);
        } else {
            holder.addtocart.setVisibility(View.VISIBLE);
            holder.song_price.setVisibility(View.VISIBLE);
        }
        if (getAppConfigJson().getCurrency().equals("Rs."))
            holder.song_price.setText("MP3 " + context.getResources().getString(R.string.Rs) + " " + getAppConfigJson().getMp3_price() + "  HD " + context.getResources().getString(R.string.Rs) + " " + getAppConfigJson().getHp_price());
        else
            holder.song_price.setText("MP3 " + getAppConfigJson().getCurrency() + " " + getAppConfigJson().getMp3_price() + "  HD " + getAppConfigJson().getCurrency() + " " + getAppConfigJson().getHp_price());
        holder.addtocart.setTag(R.string.data, data);
        holder.addtocart.setTag(R.string.addtocart_id, holder.addtocart);
        holder.artistName.setText(str_artistName);

        if (album_image != null && from.equals("mp3_album")) {
            ((Mp3HindiAlbumDetail) context).setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.song_listing_album_image), (int) context.getResources().getDimension(R.dimen.song_listing_album_image), album_image, holder.song_album_image);
        } else if (album_image != null && from.equals("mp3_regional")) {
            ((RegionalMp3AlbumDetail) context).setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.song_listing_album_image), (int) context.getResources().getDimension(R.dimen.song_listing_album_image), album_image, holder.song_album_image);
        }

        if (getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id())) {
            data.setIs_playing(true);
        } else {
            data.setIs_playing(false);
        }

        float alpha = Float.valueOf(context.getResources().getString(R.string.alpha_value));

        if (data.is_playing()) {
            if (SaregamaConstants.IS_PLAYING) {
                setClick(holder);
            } else {
                if (from.equals("mp3_album")) {
                    holder.addtocart.setOnClickListener((Mp3HindiAlbumDetail) context);
                    holder.seekbar.setOnClickListener(null);
                    holder.songname.setOnClickListener(null);
                    holder.slider_transparent.setOnClickListener(null);
                } else if (from.equals("mp3_regional")) {
                    holder.addtocart.setOnClickListener((RegionalMp3AlbumDetail) context);
                    holder.seekbar.setOnClickListener(null);
                    holder.songname.setOnClickListener(null);
                    holder.slider_transparent.setOnClickListener(null);
                }
            }
            holder.seekbar.setVisibility(View.VISIBLE);
            holder.song_album_image.setAlpha(alpha);
            holder.opacityFilter.setVisibility(View.VISIBLE);
            holder.seekbar.setImageResource(R.mipmap.pause_icon);
        } else {
            setClick(holder);
            holder.seekbar.setVisibility(View.GONE);
            holder.song_album_image.setAlpha(1.0f);
            holder.opacityFilter.setVisibility(View.GONE);
            holder.seekbar.setImageResource(R.mipmap.play_icon);
        }

        if (getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id()) && SaregamaConstants.IS_PLAYING && !SaregamaConstants.IS_PAUSED) {
            holder.seekbar.setVisibility(View.VISIBLE);
            holder.song_album_image.setAlpha(alpha);
            holder.opacityFilter.setVisibility(View.VISIBLE);
            holder.seekbar.setImageResource(R.mipmap.pause_icon);
        } else if (getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id()) && SaregamaConstants.IS_PLAYING && SaregamaConstants.IS_PAUSED) {
            holder.seekbar.setVisibility(View.VISIBLE);
            holder.song_album_image.setAlpha(alpha);
            holder.opacityFilter.setVisibility(View.VISIBLE);
            holder.seekbar.setImageResource(R.mipmap.play_icon);
        } else if (!getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id()) && !SaregamaConstants.IS_PLAYING && !SaregamaConstants.IS_PAUSED) {
            holder.seekbar.setVisibility(View.GONE);
            holder.song_album_image.setAlpha(1.0f);
            holder.opacityFilter.setVisibility(View.GONE);
            holder.seekbar.setImageResource(R.mipmap.play_icon);
        }

        holder.seekbar.setTag(R.string.key, position);
        holder.seekbar.setTag(R.string.data, data);
        holder.seekbar.setTag(R.string.from, R.id.playIcon);

        // song name click handle
        holder.songname.setTag(R.string.key, position);
        holder.songname.setTag(R.string.data, data);
        holder.songname.setTag(R.string.seekbar_id, holder.seekbar);

         // row name click handle
        holder.slider_transparent.setTag(R.string.key, position);
        holder.slider_transparent.setTag(R.string.data, data);
        holder.slider_transparent.setTag(R.string.seekbar_id, holder.seekbar);

        holder.song_album_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.seekbar.performClick();
            }
        });

        setAnimation(holder.itemView, position);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        if (arrayCategoryList != null)
            return arrayCategoryList.size();
        else
            return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView songname, artistName, song_price;
        private CheckBox addtocart;
        private ImageView seekbar, song_album_image;
        private LinearLayout opacityFilter, slider_transparent;

        public ViewHolder(View itemView) {
            super(itemView);
            songname = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_songname);
            artistName = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_moviename);
            song_price = (TextView) itemView.findViewById(R.id.song_price);
            addtocart = (CheckBox) itemView.findViewById(R.id.addtocart);
            seekbar = (ImageView) itemView.findViewById(R.id.playIcon);
            opacityFilter = (LinearLayout) itemView.findViewById(R.id.opacityFilter);
            song_album_image = (ImageView) itemView.findViewById(R.id.song_album_image);

            slider_transparent = (LinearLayout) itemView.findViewById(R.id.slider_transparent);
        }
    }

    private String getFromPrefs(String key) {
        SharedPreferences prefs = context.getSharedPreferences(SaregamaConstants.PREF_NAME, context.MODE_PRIVATE);
        return prefs.getString(key, SaregamaConstants.DEFAULT_VALUE);
    }

    private AppConfigDataPojo getAppConfigJson() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        final Gson gson = new Gson();
        String json = sharedPreferences.getString("AppConfigObject", "");
        AppConfigDataPojo obj = gson.fromJson(json, AppConfigDataPojo.class);

        if (obj != null) {
            return obj;
        }
        return null;
    }

    private void setClick(ViewHolder holder) {
        if (from.equals("mp3_album")) {
            holder.addtocart.setOnClickListener((Mp3HindiAlbumDetail) context);
            holder.seekbar.setOnClickListener((Mp3HindiAlbumDetail) context);
            holder.songname.setOnClickListener((Mp3HindiAlbumDetail) context);
            holder.slider_transparent.setOnClickListener((Mp3HindiAlbumDetail) context);
        } else if (from.equals("mp3_regional")) {
            holder.addtocart.setOnClickListener((RegionalMp3AlbumDetail) context);
            holder.seekbar.setOnClickListener((RegionalMp3AlbumDetail) context);
            holder.songname.setOnClickListener((RegionalMp3AlbumDetail) context);
            holder.slider_transparent.setOnClickListener((RegionalMp3AlbumDetail) context);
        }
    }
}