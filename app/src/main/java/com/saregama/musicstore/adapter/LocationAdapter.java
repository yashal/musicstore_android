package com.saregama.musicstore.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.Mp3HindiLandingActivity;
import com.saregama.musicstore.activity.PayNowActivity;
import com.saregama.musicstore.activity.SettingsActivity;

import java.util.ArrayList;


/**
 * Created by Yash on 5/13/2017.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {
    private ArrayList<String> arr;
    private Context context;
    private int lastPosition = -1;
    private String from;

    public LocationAdapter(Context context, ArrayList<String> arr, String from) {
        this.arr = arr;
        this.context = context;
        this.from = from;
    }

    @Override
    public LocationAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.location_row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LocationAdapter.ViewHolder viewHolder, int i) {

        viewHolder.location_name.setText(arr.get(i));

        viewHolder.itemView.setTag(R.string.key, arr.get(i));
        if (from.equalsIgnoreCase("PayNow")) {
            viewHolder.itemView.setOnClickListener((PayNowActivity) context);
        }
        else if (from.equalsIgnoreCase("Settings")) {
            viewHolder.itemView.setOnClickListener((SettingsActivity) context);
        }

//        setAnimation(viewHolder.itemView, i);
    }

    @Override
    public int getItemCount() {
        return arr.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView location_name;
        public ViewHolder(View view) {
            super(view);

            location_name = (TextView)view.findViewById(R.id.location_name);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
