package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.SearchArtistSongListActivity;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by navneet on 8/3/2016.
 */
public class SearchArtistDetailAdapter extends RecyclerView.Adapter<SearchArtistDetailAdapter.ViewHolder> {

    private ArrayList<MP3HindiSongListPojo> arrayCategoryList;
    private Activity context;
    private int lastPosition = -1;
    private String keyword;

    public SearchArtistDetailAdapter(Activity context, ArrayList<MP3HindiSongListPojo> arrayCategoryList, String keyword) {
        this.context = context;
        this.arrayCategoryList = arrayCategoryList;
        this.keyword = keyword.replace("%20", " ");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mp3hindi_songlist_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MP3HindiSongListPojo data = arrayCategoryList.get(position);
        if(keyword != null && keyword.length() > 0) {
            int startPos = data.getSong_name().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
            Spannable spannable = new SpannableString(data.getSong_name());
            if (startPos != -1) // This should always be true, just a sanity check
            {
                int index = data.getSong_name().toLowerCase().indexOf(keyword.toLowerCase());
                while (index >= 0) {
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(context.getResources().getString(R.string.highlightedText))});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    if (index != 1)
                        spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder.songname.setText(spannable);
                    index = data.getSong_name().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
                }
            } else
                holder.songname.setText(data.getSong_name());
        }
        else
            holder.songname.setText(data.getSong_name());


        holder.artistName.setText(data.getAlbum_name());

        if (getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id())) {
            data.setIs_playing(true);
        } else {
            data.setIs_playing(false);
        }

        if (data.getAlbum_image() != null) {
            ((SearchArtistSongListActivity) context).setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.song_listing_album_image), (int) context.getResources().getDimension(R.dimen.song_listing_album_image), data.getAlbum_image(), holder.song_album_image);
        }

        float alpha = Float.valueOf(context.getResources().getString(R.string.alpha_value));

        if (data.is_playing()) {
            if (SaregamaConstants.IS_PLAYING) {
                setClick(holder);
            } else {
                holder.addtocart.setOnClickListener((SearchArtistSongListActivity) context);
                holder.seekbar.setOnClickListener(null);
                holder.songname.setOnClickListener(null);
                holder.slider_transparent.setOnClickListener(null);
            }
            holder.seekbar.setVisibility(View.VISIBLE);
            holder.song_album_image.setAlpha(alpha);
            holder.opacityFilter.setVisibility(View.VISIBLE);
            holder.seekbar.setImageResource(R.mipmap.pause_icon);
        } else {
            setClick(holder);
            holder.seekbar.setVisibility(View.GONE);
            holder.song_album_image.setAlpha(1.0f);
            holder.opacityFilter.setVisibility(View.GONE);
            holder.seekbar.setImageResource(R.mipmap.play_icon);
        }

        if (getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id()) && SaregamaConstants.IS_PLAYING && !SaregamaConstants.IS_PAUSED) {
            holder.seekbar.setVisibility(View.VISIBLE);
            holder.song_album_image.setAlpha(alpha);
            holder.opacityFilter.setVisibility(View.VISIBLE);
            holder.seekbar.setImageResource(R.mipmap.pause_icon);
        } else if (getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id()) && SaregamaConstants.IS_PLAYING && SaregamaConstants.IS_PAUSED) {
            holder.seekbar.setVisibility(View.VISIBLE);
            holder.song_album_image.setAlpha(alpha);
            holder.opacityFilter.setVisibility(View.VISIBLE);
            holder.seekbar.setImageResource(R.mipmap.play_icon);
        } else if (!getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(data.getSong_id()) && !SaregamaConstants.IS_PLAYING && !SaregamaConstants.IS_PAUSED) {
            holder.seekbar.setVisibility(View.GONE);
            holder.song_album_image.setAlpha(1.0f);
            holder.opacityFilter.setVisibility(View.GONE);
            holder.seekbar.setImageResource(R.mipmap.play_icon);
        }

        holder.song_album_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.seekbar.performClick();
            }
        });

        holder.addtocart.setTag(R.string.data, data);
        holder.addtocart.setTag(R.string.addtocart_id, holder.addtocart);

        holder.seekbar.setTag(R.string.key, position);
        holder.seekbar.setTag(R.string.data, data);
        holder.seekbar.setTag(R.string.from, R.id.playIcon);

        // song name click handle
        holder.songname.setTag(R.string.key, position);
        holder.songname.setTag(R.string.data, data);
        holder.songname.setTag(R.string.seekbar_id, holder.seekbar);

        // row name click handle
        holder.slider_transparent.setTag(R.string.key, position);
        holder.slider_transparent.setTag(R.string.data, data);
        holder.slider_transparent.setTag(R.string.seekbar_id, holder.seekbar);

        setAnimation(holder.itemView, position);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return arrayCategoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView songname, artistName;
        private CheckBox addtocart;
        private ImageView seekbar, song_album_image;
        private LinearLayout opacityFilter, slider_transparent;

        public ViewHolder(View itemView) {
            super(itemView);
            songname = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_songname);
            artistName = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_moviename);
            addtocart = (CheckBox) itemView.findViewById(R.id.addtocart);
            seekbar = (ImageView) itemView.findViewById(R.id.playIcon);
            opacityFilter = (LinearLayout) itemView.findViewById(R.id.opacityFilter);
            song_album_image = (ImageView) itemView.findViewById(R.id.song_album_image);

            slider_transparent = (LinearLayout) itemView.findViewById(R.id.slider_transparent);
        }
    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = context.getSharedPreferences(SaregamaConstants.PREF_NAME, context.MODE_PRIVATE);
        return prefs.getString(key, SaregamaConstants.DEFAULT_VALUE);
    }
    private void setClick(ViewHolder holder) {

        holder.addtocart.setOnClickListener((SearchArtistSongListActivity) context);
        holder.seekbar.setOnClickListener((SearchArtistSongListActivity) context);
        holder.songname.setOnClickListener((SearchArtistSongListActivity) context);
        holder.slider_transparent.setOnClickListener((SearchArtistSongListActivity) context);

    }
}