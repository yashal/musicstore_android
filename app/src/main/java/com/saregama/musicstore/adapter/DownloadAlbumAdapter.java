package com.saregama.musicstore.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.DownloadManagerActivity;
import com.saregama.musicstore.pojo.DownloadManagerAlbumPojo;

import java.util.ArrayList;

/**
 * Created by Administrator on 12-Apr-16.
 */
public class DownloadAlbumAdapter extends RecyclerView.Adapter<DownloadAlbumAdapter.ViewHolder> {

    private ArrayList<DownloadManagerAlbumPojo> arrayAlbumList;
    private DownloadManagerActivity context;
    private String from;

    public DownloadAlbumAdapter(DownloadManagerActivity  context, ArrayList<DownloadManagerAlbumPojo> arrayAlbumList, String from) {
        this.context = context;
        this.arrayAlbumList = arrayAlbumList;
        this.from = from;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.download_albumlist_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DownloadManagerAlbumPojo data = arrayAlbumList.get(position);

       /*  String _totalsize =  arrayAlbumList.get(position).getFile_size();*/

       /*  double temp_size = (Double.parseDouble(_totalsize))/1024;
        totalsize =  String.format("%.2f", temp_size);
        if(arrayAlbumList.get(position).getC_type().equals("1"))
            holder.song_type.setText(".MP3("+totalsize+"mb)");
        else
            holder.song_type.setText(".HD("+totalsize+"mb)");*/

        holder.albumName.setText(arrayAlbumList.get(position).getName());
        if (arrayAlbumList.get(position).getAlbum_songs() != null)
        {
            holder.songCount.setText(arrayAlbumList.get(position).getAlbum_songs().size()+" Songs");
        }

        holder.downloaded.setVisibility(View.VISIBLE);
        if(from.equals("downloaded"))
        {
            holder.downloaded.setBackgroundResource(R.mipmap.download_icon);
        }
        else if(from.equals("download_now"))
        {
            holder.downloaded.setBackgroundResource(R.mipmap.download_green);
        }
        if (arrayAlbumList.get(position).getAlbum_type() == context.getAppConfigJson().getC_type().getALBUM())
            holder.album_type.setText(".MP3");
        else
            holder.album_type.setText(".HD");

        ((BaseActivity) context).setImageInLayout(context, (int)context.getResources().getDimension(R.dimen.cart_album_image_size), (int)context.getResources().getDimension(R.dimen.cart_album_image_size), arrayAlbumList.get(position).getImage(), holder.albumImage);
        holder.albumImage.setScaleType(ImageView.ScaleType.FIT_XY);

        holder.downlaodAlbumLL.setTag(R.string.key, position);
        holder.downlaodAlbumLL.setTag(R.string.data, data);
        holder.downlaodAlbumLL.setTag(R.string.navigate_from, from);
        holder.downlaodAlbumLL.setOnClickListener((DownloadManagerActivity)context);
    }

    @Override
    public int getItemCount() {
        return arrayAlbumList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView albumName, songCount, album_type;
        ImageView downloaded,albumImage;
        LinearLayout downlaodAlbumLL;

        public ViewHolder(View itemView) {
            super(itemView);
            albumName = (TextView) itemView.findViewById(R.id.download_albumlist_albumname);
            songCount = (TextView) itemView.findViewById(R.id.album_song_count);
            album_type = (TextView) itemView.findViewById(R.id.album_type);
            downloaded = (ImageView) itemView.findViewById(R.id.album_download_img);
            albumImage = (ImageView) itemView.findViewById(R.id.download_albumlist_Image);
            downlaodAlbumLL = (LinearLayout) itemView.findViewById(R.id.downlaod_albumlist_LL);
        }
    }
}