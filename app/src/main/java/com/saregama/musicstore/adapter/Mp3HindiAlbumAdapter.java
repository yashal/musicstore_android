package com.saregama.musicstore.adapter;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.BuyAnyAlbumHindiLanding;
import com.saregama.musicstore.activity.BuyAnyAlbumListing;
import com.saregama.musicstore.activity.GajalSufiActivity;
import com.saregama.musicstore.activity.Mp3HindiLandingActivity;
import com.saregama.musicstore.activity.Mp3RegionalActivity;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;
import java.util.Locale;


public class Mp3HindiAlbumAdapter extends RecyclerView.Adapter<Mp3HindiAlbumAdapter.ViewHolder> {

    private ArrayList<MP3HindiAlbumListPojo> arrayCategoryList;
    private String from;
    private int lastPosition = -1;
    private Activity context;
    private String keyword;

    public Mp3HindiAlbumAdapter(Activity context, ArrayList<MP3HindiAlbumListPojo> arrayCategoryList, String from, String keyword) {
        this.from = from;
        this.context = context;
        this.arrayCategoryList = arrayCategoryList;
        this.keyword = keyword.replace("%20", " ");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mp3hindi_albumlist_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MP3HindiAlbumListPojo data = arrayCategoryList.get(position);
        if(keyword != null && keyword.length() > 0) {
            int startPos = data.getAlbum_name().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
            Spannable spannable = new SpannableString(data.getAlbum_name());
            if (startPos != -1) // This should always be true, just a sanity check
            {
                int index = data.getAlbum_name().toLowerCase().indexOf(keyword.toLowerCase());
                while (index >= 0) {
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(context.getResources().getString(R.string.highlightedText))});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    if (index != 1)
                        spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder.albumName.setText(spannable);
                    index = data.getAlbum_name().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
                }
            } else
                holder.albumName.setText(data.getAlbum_name());
        }
        else
            holder.albumName.setText(data.getAlbum_name());

        holder.trackCount.setText("(" + data.getSong_count() + " Tracks)");
        ((BaseActivity) context).setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.cart_album_image_size), (int) context.getResources().getDimension(R.dimen.cart_album_image_size), data.getAlbum_image(), holder.albumImage);
        holder.albumImage.setScaleType(ImageView.ScaleType.FIT_XY);

        holder.itemView.setTag(R.string.key, position);
        holder.itemView.setTag(R.string.data, data);
        holder.itemView.setTag(R.string.from, from);

        holder.album_addtocart.setTag(R.string.data, data);
        holder.album_addtocart.setTag(R.string.addtocart_id, holder.album_addtocart);

        if (from.equals("hindi")) {
            holder.itemView.setOnClickListener((Mp3HindiLandingActivity) context);
        }else if (from.equals("geetmala")) {
            holder.itemView.setOnClickListener((Mp3HindiLandingActivity) context);
        } else if (from.equals("buyanyalbum")) {
            holder.itemView.setOnClickListener((BuyAnyAlbumHindiLanding) context);
            if(Integer.parseInt(data.getSong_count()) <= SaregamaConstants.SONG_LIMIT)
                holder.album_addtocart.setVisibility(View.GONE);
            else
                holder.album_addtocart.setVisibility(View.VISIBLE);
            holder.album_addtocart.setOnClickListener((BuyAnyAlbumHindiLanding) context);
        } else if (from.equals("buyanyalbum_gazal")) {
            holder.itemView.setOnClickListener((BuyAnyAlbumListing) context);
            if(Integer.parseInt(data.getSong_count()) <= SaregamaConstants.SONG_LIMIT)
                holder.album_addtocart.setVisibility(View.GONE);
            else
                holder.album_addtocart.setVisibility(View.VISIBLE);
            holder.album_addtocart.setOnClickListener((BuyAnyAlbumListing) context);
        } else if (from.equals("gajal_sufi")) {
            holder.itemView.setOnClickListener((GajalSufiActivity) context);
        } else {
            holder.itemView.setOnClickListener((Mp3RegionalActivity) context);
        }
        setAnimation(holder.itemView, position);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return arrayCategoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView albumName, trackCount;
        private ImageView albumImage;
        private CheckBox album_addtocart;

        public ViewHolder(View itemView) {
            super(itemView);
            albumName = (TextView) itemView.findViewById(R.id.mp3hindi_albumlist_albumname);
            trackCount = (TextView) itemView.findViewById(R.id.mp3hindi_albumlist_trackcount);
            albumImage = (ImageView) itemView.findViewById(R.id.mp3hindi_albumlist_Image);
            album_addtocart = (CheckBox) itemView.findViewById(R.id.album_addtocart);
        }
    }
}