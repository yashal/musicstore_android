package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.saregama.musicstore.fragments.Mp3HindiAlbumFragment;
import com.saregama.musicstore.fragments.Mp3HindiArtistFragment;
import com.saregama.musicstore.fragments.Mp3HindiGeetmalaFragment;
import com.saregama.musicstore.fragments.Mp3HindiSongFragment;

import java.util.ArrayList;

/**
 * Created by navneet on 7/3/2016.
 */
public class Mp3HindiPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public Mp3HindiPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                Mp3HindiSongFragment fragment = new Mp3HindiSongFragment();
                mPageReferenceMap.add(position, fragment);
                return fragment;
            }
            case 1:
                Mp3HindiAlbumFragment fragment_album = new Mp3HindiAlbumFragment();
                if(mPageReferenceMap.size() == 0) {
                    Mp3HindiSongFragment fragment = new Mp3HindiSongFragment();
                    mPageReferenceMap.add(0, fragment);
                }
                mPageReferenceMap.add(position, fragment_album);
                return fragment_album;
            case 2:
                return new Mp3HindiArtistFragment();
            case 3:
                return new Mp3HindiGeetmalaFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}
