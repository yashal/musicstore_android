package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.saregama.musicstore.fragments.RabindraSangeetArtistesFragment;
import com.saregama.musicstore.fragments.RabindraSangeetSongFragment;

import java.util.ArrayList;

/**
 * Created by Arpit on 17-Feb-16.
 */
public class RabindraSangeetPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public RabindraSangeetPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                RabindraSangeetSongFragment fragment_song = new RabindraSangeetSongFragment();
                mPageReferenceMap.add(index, fragment_song);
                return fragment_song;
            case 1:
                RabindraSangeetArtistesFragment fragment_artist = new RabindraSangeetArtistesFragment();
                mPageReferenceMap.add(index, fragment_artist);
                return fragment_artist;
        }
        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }
    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}