package com.saregama.musicstore.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.saregama.musicstore.fragments.RegionalArtistfragment;
import com.saregama.musicstore.fragments.RegionalMoviesAlbumFragment;
import com.saregama.musicstore.fragments.RegionalMp3SongFragment;

import java.util.ArrayList;

/**
 * Created by yesh on 3/7/2016.
 */
public class RegionalMp3songPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public RegionalMp3songPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                RegionalMp3SongFragment fragment_reg_mp3 = new RegionalMp3SongFragment();
                mPageReferenceMap.add(position, fragment_reg_mp3);
                return fragment_reg_mp3;
            case 1:
                RegionalMoviesAlbumFragment fragment_reg_album = new RegionalMoviesAlbumFragment();
                mPageReferenceMap.add(position, fragment_reg_album);
                return fragment_reg_album;
            case 2:
                RegionalArtistfragment fragment_reg_artist = new RegionalArtistfragment();
                mPageReferenceMap.add(position, fragment_reg_artist);
                return fragment_reg_artist;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}
