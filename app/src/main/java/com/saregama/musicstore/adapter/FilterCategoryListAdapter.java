package com.saregama.musicstore.adapter;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.BuyAnyAlbumHindiLanding;
import com.saregama.musicstore.activity.BuyAnyAlbumListing;
import com.saregama.musicstore.activity.ClassicalListActivity;
import com.saregama.musicstore.activity.GajalSufiActivity;
import com.saregama.musicstore.activity.LanguageListActivity;
import com.saregama.musicstore.activity.Mp3Devotional;
import com.saregama.musicstore.activity.Mp3HindiLandingActivity;
import com.saregama.musicstore.activity.OtherLandingListActivity;
import com.saregama.musicstore.activity.RabindraSangeetActivity;
import com.saregama.musicstore.activity.RemixSongsActivity;
import com.saregama.musicstore.pojo.HomeBannerPojo;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;

/**
 * Created by arpit on 5/10/2017.
 */

public class FilterCategoryListAdapter extends BaseAdapter {
    private ArrayList<HomeBannerPojo> arr;
    private Context context;
    private LayoutInflater inflater = null;
    private TextView header_list;
    private PopupWindow pwindo;

    public FilterCategoryListAdapter(Context context, ArrayList<HomeBannerPojo> arr, TextView header_list, PopupWindow pwindo) {
        this.context = context;
        this.arr = arr;
        this.pwindo = pwindo;
        this.header_list = header_list;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        private TextView cat_name;
        private LinearLayout layout_filter_bg;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final FilterCategoryListAdapter.Holder holder = new FilterCategoryListAdapter.Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.filter_category_list, null);
        holder.cat_name = (TextView) rowView.findViewById(R.id.language_name);
        holder.layout_filter_bg = (LinearLayout) rowView.findViewById(R.id.layout_filter_bg);

        holder.layout_filter_bg.setTag(R.string.data, arr.get(position));
        holder.cat_name.setText(CapsFirst(arr.get(position).getD_type_name()));
        holder.layout_filter_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pwindo.dismiss();
                HomeBannerPojo data = (HomeBannerPojo) v.getTag(R.string.data);
                int dtypecount = data.getD_type();
//                header_list.setText(CapsFirst(arr.get(position).getD_type_name()));
                ((BaseActivity) context).saveIntIntoPrefs(SaregamaConstants.D_TYPE, data.getD_type());
                ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

                if (data.getC_type() == ((BaseActivity) context).getAppConfigJson().getC_type().getSONG()) {
                    if (dtypecount == ((BaseActivity) context).getAppConfigJson().getD_type().getHINDI_FILMS()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.Mp3HindiLandingActivity")) {
                            ((BaseActivity) context).removeActivity("Mp3HindiLandingActivity");
                            Intent i = new Intent(context, Mp3HindiLandingActivity.class);
                            i.putExtra("from", "mp3");
                            i.putExtra("c_type", data.getC_type());
                            i.putExtra("link", data.getUrl());
                            context.startActivity(i);
                        }
                    } else if (dtypecount == ((BaseActivity) context).getAppConfigJson().getD_type().getREGIONAL_FILMS()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.LanguageListActivity")) {
                            ((BaseActivity) context).removeActivity("LanguageListActivity");
                            Intent i = new Intent(context, LanguageListActivity.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("d_type", data.getD_type());
                            i.putExtra("c_type", data.getC_type());
                            context.startActivity(i);
                        }
                    } else if (dtypecount == ((BaseActivity) context).getAppConfigJson().getD_type().getDEVOTIONAL()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.Mp3Devotional")) {
                            ((BaseActivity) context).removeActivity("Mp3Devotional");
                            Intent i = new Intent(context, Mp3Devotional.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("c_type", data.getC_type());
                            i.putExtra("from", "mp3");
                            context.startActivity(i);
                        }
                    } else if (dtypecount == ((BaseActivity) context).getAppConfigJson().getD_type().getCLASSICAL()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.ClassicalListActivity")) {
                            ((BaseActivity) context).removeActivity("ClassicalListActivity");
                            Intent i = new Intent(context, ClassicalListActivity.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("c_type", data.getC_type());
                            context.startActivity(i);
                        }
                    } else if (dtypecount == ((BaseActivity) context).getAppConfigJson().getD_type().getRABINDRA_SANGEET()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.RabindraSangeetActivity")) {
                            ((BaseActivity) context).removeActivity("RabindraSangeetActivity");
                            Intent i = new Intent(context, RabindraSangeetActivity.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("c_type", data.getC_type());
                            i.putExtra("from", "Rabindra Sangeet");
                            context.startActivity(i);
                        }
                    } else if (dtypecount == ((BaseActivity) context).getAppConfigJson().getD_type().getOTHERS()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.OtherLandingListActivity")) {
                            ((BaseActivity) context).removeActivity("OtherLandingListActivity");
                            Intent i = new Intent(context, OtherLandingListActivity.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("c_type", data.getC_type());
                            context.startActivity(i);
                        }
                    } else if (dtypecount == ((BaseActivity) context).getAppConfigJson().getD_type().getNAZRUL_GEET()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.RabindraSangeetActivity")) {
                            ((BaseActivity) context).removeActivity("RabindraSangeetActivity");
                            Intent i = new Intent(context, RabindraSangeetActivity.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("c_type", data.getC_type());
                            i.putExtra("from", "Nazrul Geet");
                            context.startActivity(i);
                        }
                    } else if (dtypecount == ((BaseActivity) context).getAppConfigJson().getD_type().getREMIX()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.RemixSongsActivity")) {
                            ((BaseActivity) context).removeActivity("RemixSongsActivity");
                            Intent i = new Intent(context, RemixSongsActivity.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("c_type", data.getC_type());
                            context.startActivity(i);
                        }
                    } else if (dtypecount == ((BaseActivity) context).getAppConfigJson().getD_type().getGHAZALS_SUFI()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.GajalSufiActivity")) {
                            ((BaseActivity) context).removeActivity("GajalSufiActivity");
                            Intent i = new Intent(context, GajalSufiActivity.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("c_type", data.getC_type());
                            i.putExtra("from", "mp3");
                            context.startActivity(i);
                        }
                    }
                } else if (data.getC_type() == ((BaseActivity) context).getAppConfigJson().getC_type().getALBUM()) {
                    if (data.getD_type() == ((BaseActivity) context).getAppConfigJson().getD_type().getHINDI_FILMS()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.BuyAnyAlbumHindiLanding")) {
                            ((BaseActivity) context).removeActivity("BuyAnyAlbumHindiLanding");
                            Intent i = new Intent(context, BuyAnyAlbumHindiLanding.class);
                            i.putExtra("c_type", data.getC_type());
                            i.putExtra("url", data.getUrl());
                            context.startActivity(i);
                        }
                    } else if (data.getD_type() == ((BaseActivity) context).getAppConfigJson().getD_type().getGHAZALS_SUFI()) {
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.BuyAnyAlbumListing")) {
                            ((BaseActivity) context).removeActivity("BuyAnyAlbumListing");
                            Intent i = new Intent(context, BuyAnyAlbumListing.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("c_type", data.getC_type());
                            context.startActivity(i);
                        }
                        //gazal song
                    } else if (data.getD_type() == ((BaseActivity) context).getAppConfigJson().getD_type().getREGIONAL_FILMS()) {
                        //regional listing
                        if (!cn.getShortClassName().equalsIgnoreCase(".activity.LanguageListActivity")) {
                            ((BaseActivity) context).removeActivity("LanguageListActivity");
                            Intent i = new Intent(context, LanguageListActivity.class);
                            i.putExtra("url", data.getUrl());
                            i.putExtra("c_type", data.getC_type());
                            context.startActivity(i);
                        }
                    }
                }
                ((BaseActivity) context).overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                notifyDataSetChanged();

            }
        });

        return rowView;
    }

    private String CapsFirst(String str) {
        String[] words = str.split(" ");
        StringBuilder ret = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            ret.append(Character.toUpperCase(words[i].charAt(0)));
            ret.append(words[i].substring(1));
            if (i < words.length - 1) {
                ret.append(' ');
            }
        }
        return ret.toString();
    }
}
