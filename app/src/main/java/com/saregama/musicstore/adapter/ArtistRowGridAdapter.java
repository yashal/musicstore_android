package com.saregama.musicstore.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.SearchArtistSongListActivity;
import com.saregama.musicstore.pojo.ArtistListPojo;

import java.util.ArrayList;

/**
 * Created by navneet on 16/3/2016.
 */


// show user arena gridlayout start
public class ArtistRowGridAdapter extends BaseAdapter {
    private SearchArtistSongListActivity mContext;
    private ArrayList<ArtistListPojo> arr;

    public ArtistRowGridAdapter(SearchArtistSongListActivity c, ArrayList<ArtistListPojo> arr) {
        mContext = c;
        this.arr = arr;
    }

    @Override
    public int getCount() {
        return arr.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ViewHolder holder;

        if (convertView == null) {
            //  convertView = new View(mContext);
            convertView = inflater.inflate(R.layout.artist_row_grid, null);
            holder = new ViewHolder();
            holder.artistName = (TextView) convertView.findViewById(R.id.row_grid_artistName);
            holder.row_grid_artistLL = (LinearLayout) convertView.findViewById(R.id.row_grid_artistLL);
            holder.addArtistIMG = (ImageView) convertView.findViewById(R.id.addArtistIMG);
            holder.removeArtistIMG = (ImageView) convertView.findViewById(R.id.removeArtistIMG);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position == arr.size()) {
            holder.row_grid_artistLL.setBackgroundResource(R.drawable.aboutus_green_button);
            holder.artistName.setText("Add Artiste");
            holder.addArtistIMG.setVisibility(View.VISIBLE);
            holder.removeArtistIMG.setVisibility(View.GONE);
            holder.artistName.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        } else {
            holder.row_grid_artistLL.setBackgroundResource(R.drawable.artist_grid_btn);
            holder.artistName.setText(arr.get(position).getName());
            holder.addArtistIMG.setVisibility(View.GONE);
            holder.removeArtistIMG.setVisibility(View.VISIBLE);
            holder.artistName.setTextColor(ContextCompat.getColor(mContext, R.color.light_black));
        }

        convertView.setTag(holder);

        return convertView;
    }

    // convert view class
    class ViewHolder {
        TextView artistName;
        LinearLayout row_grid_artistLL;
        ImageView addArtistIMG, removeArtistIMG;
    }
}