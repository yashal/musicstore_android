package com.saregama.musicstore.model;


import com.saregama.musicstore.pojo.AddToCartPojo;
import com.saregama.musicstore.pojo.AnalyticsBasePojo;
import com.saregama.musicstore.pojo.AppConfigPojo;
import com.saregama.musicstore.pojo.ApplyCouponPojo;
import com.saregama.musicstore.pojo.AvailOfferPojo;
import com.saregama.musicstore.pojo.BasePojo;
import com.saregama.musicstore.pojo.CartPojo;
import com.saregama.musicstore.pojo.DownloadManagerPojo;
import com.saregama.musicstore.pojo.DownloadSongsPojo;
import com.saregama.musicstore.pojo.ForgotPasswordMailPojo;
import com.saregama.musicstore.pojo.ForgotPasswordPojo;
import com.saregama.musicstore.pojo.GetSatePojo;
import com.saregama.musicstore.pojo.GetpaymentlogPojo;
import com.saregama.musicstore.pojo.GlobalSearchAutoSuggestsDataPojo;
import com.saregama.musicstore.pojo.HomeDataPojo;
import com.saregama.musicstore.pojo.LanguageBasePojo;
import com.saregama.musicstore.pojo.LogInPojo;
import com.saregama.musicstore.pojo.LogOutPojo;
import com.saregama.musicstore.pojo.Mp3HindiAlbumDetailPojo;
import com.saregama.musicstore.pojo.Mp3HindiAlbumPojo;
import com.saregama.musicstore.pojo.NotificationPojo;
import com.saregama.musicstore.pojo.PaymentPojo;
import com.saregama.musicstore.pojo.ProfilePojo;
import com.saregama.musicstore.pojo.RecommendationsAlbumPojo;
import com.saregama.musicstore.pojo.RecommendationsPojo;
import com.saregama.musicstore.pojo.RemoveCartPojo;
import com.saregama.musicstore.pojo.ResetPasswordPojo;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;

public interface Api {


    @FormUrlEncoded
    @POST("/login")
    void postRegister(@Field("guest_id") String guest_id, @Field("name") String name, @Field("email") String email, @Field("mobile") String mobile, @Field("password") String password, @Field("action") String action, Callback<LogInPojo> callback);

    @FormUrlEncoded
    @POST("/login")
    void checkEmailExist( @Field("email") String email, @Field("action") String action, @Field("access_source") String access_source, Callback<LogInPojo> callback);

    @FormUrlEncoded
    @POST("/login")
    void postLoginEmail(@Field("guest_id") String guest_id, @Field("email") String email, @Field("password") String password, @Field("action") String action, Callback<LogInPojo> callback);

    @FormUrlEncoded
    @POST("/login")
    void newUserLoginEmail(@Field("guest_id") String guest_id, @Field("email") String email, @Field("password") String password, @Field("action") String action, @Field("access_source") String access_source, @Field("setpassword") String  setpassword, Callback<LogInPojo> callback);

    @FormUrlEncoded
    @POST("/login")
    void postLoginSocialMedia(@Field("guest_id") String guest_id, @Field("access_source") String access_source, @Field("access_token") String access_token, @Field("social_id") String social_id, @Field("action") String action, Callback<LogInPojo> callback);

    @GET("/getnotificationid")
    void sendRegistrationId(@Header("SRGM_AUTH") String header, @Query("device_id") String device_id, @Query("device_type") String device_type, @Query("gcm_reg_id") String gcm_reg_id, Callback<BasePojo> callback);

    @GET("/search")
    void globalSearch(@Query("type") String type, @Query("query") String query, Callback<GlobalSearchAutoSuggestsDataPojo> callback);

    @GET("/appconfig")
    void getAppConfigDetails(Callback<AppConfigPojo> callback);

    @GET("/getgeolocation")
    void getGeolocation(Callback<AppConfigPojo> callback);

    @GET("/home")
    void GetHomeData(@Query("song_type") String song_type, @Query("grid") String grid, Callback<HomeDataPojo> callback);

    @GET("/logout")
    void logout(@Header("SRGM_AUTH") String header, @Query("action") String action, @Query("listner_id") String listner_id, Callback<LogOutPojo> callback);

    @GET("/home")
    void getCategoryList(@Query("song_type") String song_type, @Query("banner_type") String banner_type,
                         @Query("c_type") String c_type, Callback<HomeDataPojo> callback);

    @FormUrlEncoded
    @POST("/forgotpassword")
    void postForgotPassword(@Field("guest_id") String guest_id, @Field("email") String email, @Field("action") String action, Callback<ForgotPasswordPojo> callback);

    @FormUrlEncoded
    @POST("/forgotpassword")
    void postResetPassword(@Field("guest_id") String guest_id, @Field("email") String email, @Field("listener_id") String listener_id, @Field("password") String password, @Field("action") String action, Callback<ResetPasswordPojo> callback);

    @GET("/album")
    void getMp3HindiAlbumList(@Query("d_type") String d_type, @Query("start") String start,
                              @Query("limit") String limit, Callback<Mp3HindiAlbumPojo> callback);

    @GET("/album")
    void getMp3HindiAlbumDetails(@Query("album_id") String album_id, Callback<Mp3HindiAlbumDetailPojo> callback);

    @GET("/album")
    void getglobalAlbumDetails(@Query("album_id") String album_id, Callback<Mp3HindiAlbumDetailPojo> callback);

    @GET("/album")
    void getRegionalMp3Album(@Query("c_type") String c_type, @Query("d_type") String d_type, @Query("lang") String lang, @Query("song_type") String song_type, @Query("start") String start,
                             @Query("limit") String limit, Callback<Mp3HindiAlbumPojo> callback);

    @GET("/language?tagg_type=bhajan")
    void getFilterLanguageList(Callback<LanguageBasePojo> callback);

    @GET("/getgeetmala")
    void getFilterGeetMalaList(Callback<LanguageBasePojo> callback);

    @GET("/offer")
    void getavailOffers(Callback<AvailOfferPojo> callback);

    @GET("/addtocart")
    void addToCartWithoutLogin(@Header("SRGM_AUTH") String header, @Query("listener_id") String listener_id, @Query("cartval") String cartval, @Query("guest_id") String guest_id, @Query("recom_flag") String recom_flag, Callback<AddToCartPojo> callback);

    @GET("/getfromcart")
    void getCartData(@Header("SRGM_AUTH") String header, @Query("guest_id") String guest_id, Callback<CartPojo> callback);

    @GET("/getprofile")
    void getProfileData(@Header("SRGM_AUTH") String header,  @Query("login_from") String login_from, Callback<ProfilePojo> callback);

    @FormUrlEncoded
    @POST("/getpaymentdetails")
    void getPaymentDetails(@Header("SRGM_AUTH") String header, @Field("order_no") String order_no, @Field("guest_id") String guest_id, @Field("listener_id") String listener_id, @Field("price_total") float price_total, @Field("price_paid") float price_paid, @Field("coupon_status") int coupon_status, @Field("coupon_code") String coupon_code, @Field("country_code") String country_code, Callback<PaymentPojo> callback);

    @GET("/removefromcart")
    void removeCartData(@Header("SRGM_AUTH") String header, @Query("cartval") String cartval, @Query("guest_id") String guest_id, Callback<RemoveCartPojo> callback);

    @GET("/applycoupon")
    void applyCoupon(@Header("SRGM_AUTH") String header, @Query("coupon_code") String coupon_code, @Query("guest_id") String guest_id, @Query("listener_id") String listener_id, @Query("country_code") String country_code, Callback<ApplyCouponPojo> callback);

    @GET("/download")
    void downloadAllItems(@Header("SRGM_AUTH") String header, @Query("tab") String tab, @Query("start") String start,
                          @Query("limit") String limit, Callback<DownloadManagerPojo> callback);

    @GET("/downloadsongs")
    void downloadSongs(@Header("SRGM_AUTH") String header, @Query("order_no") String order_no, @Query("d_id") String d_id, Callback<DownloadSongsPojo> callback);

    @FormUrlEncoded
    @POST("/incentivization")
    void postmobileno(@Header("SRGM_AUTH") String header, @Field("mobile") String mobile, Callback<BasePojo> callback);

    @GET("/getpaymentlog")
    void getpaymentlog(@Header("SRGM_AUTH") String header,@Query("order_no") String order_no, Callback<GetpaymentlogPojo> callback);

    @GET("/notificationlist")
    void getNotificationList(@Header("SRGM_AUTH") String header, @Query("device_id") String device_id, @Query("update_status") int update_status, Callback<NotificationPojo> callback);

    @FormUrlEncoded
    @POST("/a")
    void postAnalytics(@Field("version") String version,@Field("log") String log,@Field("method") String method,
                       @Field("deviceId") String deviceId,@Field("listenerId") String listenerId,@Field("url") String url,Callback<AnalyticsBasePojo> callback);

    @FormUrlEncoded
    @POST("/login")
    void postLoginFacebookAccountKit(@Field("guest_id") String guest_id, @Field("mobile") String mobile, @Field("access_source") String access_source, @Field("action") String action, Callback<LogInPojo> callback);

    @FormUrlEncoded
    @POST("/forgotpassword_mail")
    void forgotPasswordMail(@Field("key") String key, Callback<ForgotPasswordMailPojo> callback);

    @FormUrlEncoded
    @POST("/buyagaincart")
    void buyAgain(@Header("SRGM_AUTH") String header, @Field("ctype") String ctype, @Field("cid") String cid, Callback<CartPojo> callback);

    @GET("/recommendation")
    void recommendation(@Query("guest_id") String guest_id, @Query("listener_id") String listener_id, @Query("c_type") String c_type, @Query("c_id") String c_id, Callback<RecommendationsPojo> callback);

    @GET("/recommendation")
    void recommendationAlbum(@Query("guest_id") String guest_id, @Query("listener_id") String listener_id, @Query("c_type") String c_type, @Query("c_id") String c_id, Callback<RecommendationsAlbumPojo> callback);

    @GET("/artist_language")
    void getArtisteLanguage(@Query("id") String id, Callback<LanguageBasePojo> callback);

    @GET("/getstate")
    void getState(@Header("SRGM_AUTH") String header, Callback<GetSatePojo> callback);

    @GET("/setstate")
    void setState(@Header("SRGM_AUTH") String header, @Query("state") String state,  @Query("order_id") String order_id, Callback<GetSatePojo> callback);
}
