package com.saregama.musicstore.model;

import com.saregama.musicstore.pojo.ProfilePojo;
import com.saregama.musicstore.util.SaregamaConstants;

import retrofit.Callback;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by yesh on 10/14/2015.
 */
public interface FileUploadSer {

    String BASE_URL = SaregamaConstants.BASE_URL;

    @Multipart
    @POST("/updateprofile")
    void updateProfile(@Header("SRGM_AUTH") String header, @Part("profile_img") TypedFile profile_img, @Part("name") String name, @Part("listener_id") String listener_id,
                       @Part("state") String state, Callback<ProfilePojo> cb);

}
