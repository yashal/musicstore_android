package com.saregama.musicstore.model;


import com.saregama.musicstore.pojo.AnalyticsBasePojo;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface ApiAnalytics {


    @FormUrlEncoded
    @POST("/")
    void postAnalytics(@Field("version") String version,@Field("log") String log,@Field("method") String method,
                       @Field("deviceId") String deviceId,@Field("listenerId") String listenerId,@Field("url") String url,Callback<AnalyticsBasePojo> callback);


}
