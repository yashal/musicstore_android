package com.saregama.musicstore.model;

import com.saregama.musicstore.util.SaregamaConstants;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class RestClientAnalytics {

    private static ApiAnalytics REST_CLIENT;
    private static String ROOT = SaregamaConstants.BASE_URL_ANALYTICS;

    static {
        setupRestClient();
    }

    private RestClientAnalytics() {
    }

    public static ApiAnalytics get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(ROOT)
                .setClient(new OkClient(new OkHttpClient()));
        builder.setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(ApiAnalytics.class);
    }
}