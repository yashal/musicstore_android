package com.saregama.musicstore.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.GlobalSearchActivity;
import com.saregama.musicstore.pojo.GlobalArtistPojo;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.Locale;

/**
 * Created by bhanu on 26-Apr-16.
 */
public class CustomViewGlobalSearchAlbum extends LinearLayout {

    private TextView global_search_songlist_songname;
    private TextView global_search_albumlist_trackcount;
    private ImageView album_image;
    private CheckBox add_to_cart_album_global;
    private Context ob;
    private LinearLayout slider_transparent;

    public CustomViewGlobalSearchAlbum(Context context) {
        super(context);
    }

    public CustomViewGlobalSearchAlbum(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomViewGlobalSearchAlbum(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        global_search_songlist_songname = (TextView) findViewById(R.id.global_search_songlist_songname);
        global_search_albumlist_trackcount = (TextView) findViewById(R.id.global_search_albumlist_trackcount);
        album_image = (ImageView) findViewById(R.id.album_image);
        add_to_cart_album_global = (CheckBox) findViewById(R.id.add_to_cart_album_global);
        slider_transparent = (LinearLayout) findViewById(R.id.slider_transparent_album);
    }

    public void rowContent(final GlobalArtistPojo globalAlbumPojo, int position, final Context context, String keyword) {

        slider_transparent.setTag(R.string.key, position);
        slider_transparent.setTag(R.string.data, globalAlbumPojo);
        slider_transparent.setOnClickListener((GlobalSearchActivity) context);

        if(globalAlbumPojo.getSong_count() <= SaregamaConstants.SONG_LIMIT)
            add_to_cart_album_global.setVisibility(GONE);
        else
            add_to_cart_album_global.setVisibility(VISIBLE);

        add_to_cart_album_global.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((GlobalSearchActivity)context).AddToCartWithoutLogin(((GlobalSearchActivity)context).getAppConfigJson().getC_type().getALBUM()+"", globalAlbumPojo.getContent_id() + "");
                QuickAction quickActionAlbum = ((GlobalSearchActivity)context).setupQuickActionAlbumGlobal(globalAlbumPojo.getContent_id()+"", ((GlobalSearchActivity)context).getAppConfigJson().getC_type().getALBUM(), globalAlbumPojo.getCurrency(), globalAlbumPojo.getPrice(), globalAlbumPojo.getPrice_hd());
                quickActionAlbum.show(add_to_cart_album_global);
            }
        });

        int startPos = globalAlbumPojo.getName().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
        Spannable spannable = new SpannableString(globalAlbumPojo.getName());

        if (startPos != -1) // This should always be true, just a sanity check
        {
            int index = globalAlbumPojo.getName().toLowerCase().indexOf(keyword.toLowerCase());
            while (index >= 0) {
                ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(context.getResources().getString(R.string.highlightedText))});
                TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                if (index != 1)
                    spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                global_search_songlist_songname.setText(spannable);
                index = globalAlbumPojo.getName().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
            }
        } else
            global_search_songlist_songname.setText(globalAlbumPojo.getName());

        global_search_albumlist_trackcount.setText("("+ globalAlbumPojo.getSong_count() +" Tracks)");
        //album_image
        ((BaseActivity) context).setImageInLayout(context, (int)context.getResources().getDimension(R.dimen.cart_album_image_size), (int)context.getResources().getDimension(R.dimen.cart_album_image_size), globalAlbumPojo.getImage(), album_image);
    }
}
