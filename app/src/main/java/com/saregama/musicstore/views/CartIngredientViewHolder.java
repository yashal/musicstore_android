package com.saregama.musicstore.views;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.CartActivity;
import com.saregama.musicstore.pojo.CartItemPojo;

public class CartIngredientViewHolder extends ChildViewHolder {

    private TextView mp3hindi_songlist_songname;
    private TextView mp3hindi_songlist_moviename, cart_song_price, buy_again;
    private TextView song_type;
    private LinearLayout remove_album_fron_cart, cart_song_row_root, already_purchased_layout;
    private ImageView mp3hindi_albumlist_Image;
    private CheckBox addtocart;
    private ImageView song_album_image_cart;

    public CartIngredientViewHolder(View itemView) {
        super(itemView);
        mp3hindi_songlist_songname = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_songname);
        mp3hindi_songlist_moviename = (TextView) itemView.findViewById(R.id.mp3hindi_songlist_moviename);
        song_album_image_cart = (ImageView) itemView.findViewById(R.id.song_album_image_cart);
        cart_song_price = (TextView) itemView.findViewById(R.id.cart_song_price);
        buy_again = (TextView) itemView.findViewById(R.id.buy_again);
        remove_album_fron_cart = (LinearLayout) itemView.findViewById(R.id.remove_album_fron_cart);
        cart_song_row_root = (LinearLayout) itemView.findViewById(R.id.cart_song_row_root);
        already_purchased_layout = (LinearLayout) itemView.findViewById(R.id.already_purchased_layout);
        song_type = (TextView) itemView.findViewById(R.id.song_type);
        mp3hindi_albumlist_Image = (ImageView) itemView.findViewById(R.id.playIcon);
        addtocart = (CheckBox) itemView.findViewById(R.id.addtocart);
        remove_album_fron_cart.setVisibility(View.VISIBLE);
        addtocart.setVisibility(View.GONE);
    }

    public void bind(CartItemPojo ingredient, CartActivity obj, int position) {

        mp3hindi_songlist_songname.setText(ingredient.getTitle());
        if (ingredient.getImage() != null) {
            obj.setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), (int) obj.getResources().getDimension(R.dimen.song_listing_album_image), ingredient.getImage(), song_album_image_cart);
        }
        if (ingredient != null) {
            buy_again.setTag(R.string.data, ingredient);
            buy_again.setOnClickListener(obj);
            if (ingredient.getSongCount() != null) {
                remove_album_fron_cart.setTag(R.string.key, position);
                remove_album_fron_cart.setTag(R.string.data, ingredient);
                remove_album_fron_cart.setTag(R.string.from, "album");
                remove_album_fron_cart.setOnClickListener(obj);

                cart_song_row_root.setTag(R.string.key, position);
                cart_song_row_root.setTag(R.string.data, ingredient);
                cart_song_row_root.setOnClickListener(obj);

                mp3hindi_songlist_moviename.setText("(" + ingredient.getSongCount() + " Tracks)");
                obj.setImageInLayout(obj, (int) obj.getResources().getDimension(R.dimen.cart_album_image_size), (int) obj.getResources().getDimension(R.dimen.cart_album_image_size), ingredient.getImage(), mp3hindi_albumlist_Image);
                song_type.setVisibility(View.VISIBLE);
                if (obj.getAppConfigJson().getCurrency().equals("Rs."))
                    cart_song_price.setText(obj.getResources().getString(R.string.Rs) + " " + Math.round(ingredient.getPrice()));
                else
                    cart_song_price.setText(obj.getAppConfigJson().getCurrency() + " " + ingredient.getPrice());
                if (ingredient.getCtype().equals(obj.getAppConfigJson().getC_type().getALBUM() + "")) {
                    song_type.setText(".MP3");
                } else {
                    song_type.setText(".HD");
                }
            } else /*if (ingredient.getSongCount() == null)*/ {
                remove_album_fron_cart.setTag(R.string.key, position);
                remove_album_fron_cart.setTag(R.string.data, ingredient);
                remove_album_fron_cart.setTag(R.string.from, "songs");
                mp3hindi_songlist_moviename.setText(ingredient.getAlbum());
                remove_album_fron_cart.setOnClickListener(obj);
                mp3hindi_albumlist_Image.setImageResource(R.mipmap.play_icon);
                cart_song_row_root.setClickable(false);
                song_type.setVisibility(View.VISIBLE);

                if (ingredient.getCtype().equals(obj.getAppConfigJson().getC_type().getSONG() + "")) {
                    song_type.setText(".MP3");
                    if (obj.getAppConfigJson().getCurrency().equals("Rs."))
                        cart_song_price.setText(obj.getResources().getString(R.string.Rs) + " " + obj.getAppConfigJson().getMp3_price());
                    else
                        cart_song_price.setText(obj.getAppConfigJson().getCurrency() + " " + obj.getAppConfigJson().getMp3_price());
                } else {
                    song_type.setText(".HD");
                    if (obj.getAppConfigJson().getCurrency().equals("Rs."))
                        cart_song_price.setText(obj.getResources().getString(R.string.Rs) + " " + obj.getAppConfigJson().getHp_price());
                    else
                        cart_song_price.setText(obj.getAppConfigJson().getCurrency() + " " + obj.getAppConfigJson().getHp_price());
                }
            }

            if (ingredient.getStatus() == 1) {
                already_purchased_layout.setVisibility(View.GONE);
                cart_song_price.setVisibility(View.VISIBLE);
                remove_album_fron_cart.setVisibility(View.VISIBLE);
                song_type.setTextColor(ContextCompat.getColor(obj, R.color.offer_valid));
                mp3hindi_songlist_songname.setTextColor(ContextCompat.getColor(obj, R.color.song_name_text_color));
                mp3hindi_songlist_moviename.setTextColor(ContextCompat.getColor(obj, R.color.offer_valid));
            } else {
                already_purchased_layout.setVisibility(View.VISIBLE);
                cart_song_price.setVisibility(View.GONE);
//                remove_album_fron_cart.setVisibility(View.GONE);
                song_type.setTextColor(ContextCompat.getColor(obj, R.color.already_purchased_song_color));
                mp3hindi_songlist_songname.setTextColor(ContextCompat.getColor(obj, R.color.already_purchased_song_color));
                mp3hindi_songlist_moviename.setTextColor(ContextCompat.getColor(obj, R.color.already_purchased_song_color));
            }

        }
    }
}
