package com.saregama.musicstore.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.GlobalSearchActivity;
import com.saregama.musicstore.pojo.GlobalArtistPojo;

/**
 * Created by bhanu on 26-Apr-16.
 */
public class CustomViewGlobalSearchSongs extends LinearLayout {

    private TextView global_search_songlist_songname;
    private CheckBox addtocart;
    private GlobalSearchActivity obj;

    public CustomViewGlobalSearchSongs(Context context) {
        super(context);
        obj = (GlobalSearchActivity) context;
    }

    public CustomViewGlobalSearchSongs(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomViewGlobalSearchSongs(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        global_search_songlist_songname = (TextView) findViewById(R.id.global_search_songlist_songname);
        addtocart = (CheckBox) findViewById(R.id.addtocart);
    }

    public void rowContent(final GlobalArtistPojo globalArtistPojo, int position) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        global_search_songlist_songname.setText(globalArtistPojo.getName());
        addtocart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                obj.AddToCartWithoutLogin(obj.getAppConfigJson().getC_type().getSONG()+"", globalArtistPojo.getContent_id()+"");
            }
        });
    }
}
