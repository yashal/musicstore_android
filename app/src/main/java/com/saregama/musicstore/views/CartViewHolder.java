package com.saregama.musicstore.views;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.saregama.musicstore.R;
import com.saregama.musicstore.pojo.Cart;

public class CartViewHolder extends ParentViewHolder {

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;

    private final ImageView mArrowExpandImageView;
    private TextView countText, nameText;
    private LinearLayout songs_layout_cart;

    public CartViewHolder(View itemView) {
        super(itemView);
        countText = (TextView) itemView.findViewById(R.id.count_cart);
        nameText = (TextView) itemView.findViewById(R.id.name_cart);
        songs_layout_cart = (LinearLayout) itemView.findViewById(R.id.songs_layout_cart);

        mArrowExpandImageView = (ImageView) itemView.findViewById(R.id.arrow_expand_imageview);
    }

    public void bind(Cart cart, int songs_count, int albumCount) {

        nameText.setText(cart.getName());
        if (cart.getName().equals("Song(s)")){
            if (songs_count > 0)
                songs_layout_cart.setVisibility(View.VISIBLE);
            countText.setText(""+songs_count);
        }else if (cart.getName().equals("Album(s)")){
            if (albumCount > 0)
                songs_layout_cart.setVisibility(View.VISIBLE);
            countText.setText(""+albumCount);
        }

    }

    @SuppressLint("NewApi")
    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (expanded) {
                mArrowExpandImageView.setRotation(ROTATED_POSITION);
            } else {
                mArrowExpandImageView.setRotation(INITIAL_POSITION);
            }
        }
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            RotateAnimation rotateAnimation;
            if (expanded) { // rotate clockwise
                 rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            } else { // rotate counterclockwise
                rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            }

            rotateAnimation.setDuration(200);
            rotateAnimation.setFillAfter(true);
            mArrowExpandImageView.startAnimation(rotateAnimation);
        }
    }
}
