package com.saregama.musicstore.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.GlobalSearchActivity;
import com.saregama.musicstore.pojo.GlobalArtistPojo;

import java.util.Locale;

/**
 * Created by bhanu on 26-Apr-16.
 */
public class CustomViewGlobalSearchArtist extends LinearLayout {

    private TextView global_search_songlist_songname;
    private Context ob;
    private CircularImageView artist_image;
    private LinearLayout slider_transparent;

    public CustomViewGlobalSearchArtist(Context context) {
        super(context);
    }

    public CustomViewGlobalSearchArtist(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomViewGlobalSearchArtist(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        global_search_songlist_songname = (TextView) findViewById(R.id.global_search_songlist_songname);
        artist_image = (CircularImageView) findViewById(R.id.artist_image);
        slider_transparent = (LinearLayout) findViewById(R.id.slider_transparent_artist);

    }

    public void rowContent(GlobalArtistPojo globalArtistPojo, int position,Context context, String keyword) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        slider_transparent.setTag(R.string.key, position);
        slider_transparent.setTag(R.string.data, globalArtistPojo);
        slider_transparent.setOnClickListener((GlobalSearchActivity) context);

        int startPos = globalArtistPojo.getName().toLowerCase(Locale.US).indexOf(keyword.toLowerCase());
        Spannable spannable = new SpannableString(globalArtistPojo.getName());

        if (startPos != -1) // This should always be true, just a sanity check
        {
            int index = globalArtistPojo.getName().toLowerCase().indexOf(keyword.toLowerCase());
            while (index >= 0) {
                ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.parseColor(context.getResources().getString(R.string.highlightedText))});
                TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                if (index != 1)
                    spannable.setSpan(highlightSpan, index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                global_search_songlist_songname.setText(spannable);
                index = globalArtistPojo.getName().toLowerCase().indexOf(keyword.toLowerCase(), index + keyword.length());
            }
        } else
            global_search_songlist_songname.setText(globalArtistPojo.getName());

        ((BaseActivity) context).setImageInLayout(context, (int)context.getResources().getDimension(R.dimen.artiste_list_image_size), (int)context.getResources().getDimension(R.dimen.artiste_list_image_size), globalArtistPojo.getImage(), artist_image);
    }
}
