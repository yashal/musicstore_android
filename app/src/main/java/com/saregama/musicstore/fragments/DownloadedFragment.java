package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.DownloadManagerActivity;
import com.saregama.musicstore.adapter.DownloadAlbumAdapter;
import com.saregama.musicstore.adapter.DownloadSongAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.DownloadManagerAlbumPojo;
import com.saregama.musicstore.pojo.DownloadManagerPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;
import com.saregama.musicstore.views.CustomTextViewBold;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class DownloadedFragment extends Fragment {

    private DownloadManagerActivity ctx;
    private View view;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private CustomTextViewBold album_count_download, songs_count_download;
    private LinearLayout album_layout_download, songs_layout_download;
    private RecyclerView album_list_download;
    private RecyclerView songs_list_download;
    private int start = 0;

    private ArrayList<DownloadManagerAlbumPojo> albumList;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private int totalItemCount_album, firstVisibleItemPosition_album = 0, visibleThreshold_album = 1, visibleItemCount_album;

    private boolean loading = true;
    private boolean loading_album = true;
    private int previousTotal = 0;
    private int previousTotal_album = 0;

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.LayoutManager mLayoutManager_album;

    private RecyclerView.Adapter mAdapter_album;
    private int songs_count = 0;
    private int album_count = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_download_now, container, false);

        ctx = (DownloadManagerActivity) getActivity();

        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);

        album_count_download = (CustomTextViewBold) view.findViewById(R.id.album_count_download);
        songs_count_download = (CustomTextViewBold) view.findViewById(R.id.songs_count_download);
        album_layout_download = (LinearLayout) view.findViewById(R.id.album_layout_download);
        songs_layout_download = (LinearLayout) view.findViewById(R.id.songs_layout_download);
        album_list_download = (RecyclerView) view.findViewById(R.id.album_list_download);
        album_list_download.setHasFixedSize(true);
        mLayoutManager_album = new LinearLayoutManager(getActivity());
        album_list_download.setLayoutManager(mLayoutManager_album);

        songs_list_download = (RecyclerView) view.findViewById(R.id.songs_list_download);
        songs_list_download.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        songs_list_download.setLayoutManager(mLayoutManager);

        if (albumList == null)
            albumList = new ArrayList<>();

        if (!fragmentResume && fragmentVisible) {
            getDownloadData();
        }

        return view;
    }

    private void getDownloadData() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);


            RestClient.get().downloadAllItems(ctx.getFromPrefs(SaregamaConstants.SESSION_ID), SaregamaConstants.TAB2, "" + start, "" + SaregamaConstants.LIMIT, new Callback<DownloadManagerPojo>() {
                @Override
                public void success(DownloadManagerPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus() && basePojo.getData() != null) {
                            ctx.checkCommonURL(response.getUrl());
                            if (basePojo.getData().getList() != null) {
                                if (basePojo.getData().getList().getSongs() != null) {
                                    if (ctx.songList != null && ctx.songList.size() > 0) {
                                        ctx.songList.addAll(basePojo.getData().getList().getSongs());
                                    } else {
                                        ctx.songList = basePojo.getData().getList().getSongs();
                                        previousTotal = 0;
                                    }
                                }
                                if (ctx.songList != null && ctx.songList.size() > 0)
                                    songs_count = basePojo.getData().getSongCount();
                                if (songs_count > 0 && ctx.songList != null && ctx.songList.size() > 0) {
                                    songs_layout_download.setVisibility(View.VISIBLE);
                                    ctx.mAdapter = new DownloadSongAdapter(ctx, ctx.songList, "downloaded");
                                    songs_list_download.setAdapter(ctx.mAdapter);
                                    ctx.mAdapter.notifyDataSetChanged();
                                    songs_list_download.scrollToPosition(firstVisibleItemPosition);
                                    addScrollListener();
                                }

//                              for album listing

                                if (basePojo.getData().getList().getAlbums() != null) {
                                    if (albumList != null && albumList.size() > 0) {
                                        albumList.addAll(basePojo.getData().getList().getAlbums());
                                    } else {
                                        albumList = basePojo.getData().getList().getAlbums();
                                        previousTotal_album = 0;
                                    }
                                }
                                if (albumList != null && albumList.size() > 0)
                                    album_count = basePojo.getData().getAlbumCount();

                                if (album_count > 0 && albumList != null && albumList.size() > 0) {
                                    album_layout_download.setVisibility(View.VISIBLE);
                                    mAdapter_album = new DownloadAlbumAdapter(ctx, albumList, "downloaded");
                                    album_list_download.setAdapter(mAdapter_album);
                                    mAdapter_album.notifyDataSetChanged();
                                    album_list_download.scrollToPosition(firstVisibleItemPosition_album);
                                    addScrollListenerAlbum();
                                }
                            }

                            album_count_download.setText(album_count + "");
                            songs_count_download.setText(songs_count + "");
                            ctx.saveIntIntoPrefs(SaregamaConstants.DOWNLOADED_SIZE, album_count + songs_count);
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, ctx.getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            ctx.songList = new ArrayList<>();
            albumList = new ArrayList<>();
            albumList.clear();
            ctx.songList.clear();
            start = 0;
            getDownloadData();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void addScrollListener() {
        songs_list_download.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        getDownloadData();
                        loading = true;
                    }
                }
            }
        });

    }

    private void addScrollListenerAlbum() {
        album_list_download.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount_album = recyclerView.getChildCount();
                totalItemCount_album = layoutManager.getItemCount();
                firstVisibleItemPosition_album = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading_album) {
                        if (totalItemCount_album > previousTotal_album) {
                            loading_album = false;
                            previousTotal_album = totalItemCount_album;
                        }
                    }

                    if (!loading_album && (totalItemCount_album - visibleItemCount_album)
                            <= (firstVisibleItemPosition_album + visibleThreshold_album)) {

                        start += SaregamaConstants.LIMIT;
                        getDownloadData();
                        loading_album = true;
                    }
                }
            }
        });
    }
}
