package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.Mp3Devotional;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.Mp3HindiSongAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.LanguageBasePojo;
import com.saregama.musicstore.pojo.LanguageListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Devi_Devta_Devotional_Fragment extends Fragment {

    private Mp3Devotional ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    public static View view, v;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayout language_filter;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0, start = 0;

    private String str_alphbet = "", language_id = "";
    private Typeface face_normal, face_bold;

    public static DrawerLayout mDrawerLayout;

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    private String asyncTaskUrl;
//    private LinearLayout alphabets_list;

    private TextView filterby_language_text;
    public static ListView language_filter_list;
    private PopupWindow pwindo;
    private View layout;
    protected Drawable mBackground = null;
    private String selected_text;
    private String search_word = "";

    /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.devi_devta_devotional_fragment, container, false);

        mDrawerLayout = (DrawerLayout) view.findViewById(R.id.drawer_layout_right);

        cd = new ConnectionDetector(getActivity());
        dialog = new SaregamaDialogs(getActivity());
        ctx = (Mp3Devotional) getActivity();
        face_normal = Typeface.createFromAsset(ctx.getAssets(), "fonts/SourceSansPro_Regular.otf");
        face_bold = Typeface.createFromAsset(ctx.getAssets(), "fonts/SourceSansPro_Semibold.otf");

        filterby_language_text = (TextView) view.findViewById(R.id.filterby_language_text);

        language_filter = (LinearLayout) view.findViewById(R.id.language_filter_bhajan);
        language_filter.setVisibility(View.VISIBLE);
        language_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                ctx.hideSoftKeyboard();
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.mp3songs_recycler_view);

        /* ****scrollable alphabet list code as per change request**** */
//        alphabets_list = (LinearLayout) view.findViewById(R.id.alphabets_list);
        rowTextView = (TextView) view.findViewById(R.id.row_text_view);
        alphabetslist = (ListView)view.findViewById(R.id.mp3_landing_list);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search")) {
            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
        } else {
            if (getActivity().getIntent().getStringExtra("notification_url") != null && getActivity().getIntent().getStringExtra("notification_url").equals("devotional_home_mp3")) {
                asyncTaskUrl = getActivity().getIntent().getStringExtra("notification_url");
            } else {
                asyncTaskUrl = SaregamaConstants.BASE_URL + "song?d_type=" + ctx.getAppConfigJson().getD_type().getDEVOTIONAL() + "&c_type=" + ctx.getAppConfigJson().getC_type().getSONG() + "&e_id=" + ctx.getAppConfigJson().getDev_song().getDEVI_DEVTAS() + "&song_type=" + ctx.getAppConfigJson().getSong_type().getSONG();
            }
        }

        /* ****scrollable alphabet list code as per change request start**** */
        final AlphabetListAdapter alpha_adapter = new AlphabetListAdapter(getActivity(), SaregamaConstants.items);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                str_alphbet =  ctx.convertAlphabet(SaregamaConstants.items[position].toLowerCase());
                ctx.arrdata_bhajans.clear();
                alpha_adapter.setSelectedIndex(position);
                start = 0;
                new GetSongsList().execute();

                ctx.setBubblePosition(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search")) {
            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
        } else {
            if (!fragmentResume && fragmentVisible && ctx.arrdata_bhajans != null && ctx.arrdata_bhajans.size() == 0) { //only when first time fragment is created
                new GetSongsList().execute();
            }
        }

        FetchLanguageList();

        filterby_language_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiatePopupWindow();
            }
        });
        return view;
    }

    /* **** scrollable alphabet list code as per change request comment previous code**** */
    /*public void showListBubble(View v) {
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        ctx.arrdata_bhajans.clear();
        str_alphbet = ctx.showListBubbleCommon(v, rowTextView);
        new GetSongsList().execute();
    }*/

    private void setAdapter(ArrayList<MP3HindiSongListPojo> arr) {
        ctx.adapter_bhajans = new Mp3HindiSongAdapter(ctx, arr, "devotional", search_word);
        mRecyclerView.setAdapter(ctx.adapter_bhajans);
        ctx.adapter_bhajans.notifyDataSetChanged();
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        if (getActivity().getIntent().getStringExtra("search_text") != null && getActivity().getIntent().getStringExtra("search_text").trim().length() > 2) {
                            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
                        } else {
                            new GetSongsList().execute();
                        }
                        loading = true;
                    }
                }
            }
        });
    }

    // Devi_Devta Devotional search list
    private class GetSearchSongsList extends AsyncTask<String, Void, Void> {
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
              /* ****scrollable alphabet list code as per change request **** */
//            alphabets_list.setVisibility(View.GONE);
            alphabetslist.setVisibility(View.GONE);
            filterby_language_text.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(String... querry) {

            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getSONG() + "&query=" + querry[0] + "&mode=" + "devotional" + "&e_id=" + ctx.getAppConfigJson().getDev_song().getDEVI_DEVTAS() + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    search_word = getActivity().getIntent().getStringExtra("search_text").trim();
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (ctx.arrdata_bhajans.size() > 0 && start > 0) {
                                ctx.arrdata_bhajans.addAll(basePojo.getData().getList());
                            } else {
                                ctx.arrdata_bhajans.clear();
                                ctx.arrdata_bhajans = basePojo.getData().getList();
                                setAdapter(ctx.arrdata_bhajans);
                                previousTotal = 0;
                            }
                        }
                        if (basePojo.getStatus()) {
                            ctx.adapter_bhajans.notifyItemRangeInserted(ctx.adapter_bhajans.getItemCount(), ctx.arrdata_bhajans.size() - ctx.adapter_bhajans.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, ctx.getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(asyncTaskUrl + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&alpha=" + str_alphbet + "&lang=" + language_id), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rowTextView.setVisibility(View.GONE);
            if (d != null && d.isShowing()) {
                d.dismiss();
            }

            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (ctx.arrdata_bhajans.size() > 0 && start > 0) {
                                ctx.arrdata_bhajans.addAll(basePojo.getData().getList());
                            } else {
                                ctx.arrdata_bhajans.clear();
                                ctx.arrdata_bhajans = basePojo.getData().getList();
                                setAdapter(ctx.arrdata_bhajans);
                                previousTotal = 0;
                            }
                        }

                        if (basePojo.getStatus()) {
                            ctx.adapter_bhajans.notifyItemRangeInserted(ctx.adapter_bhajans.getItemCount(), ctx.arrdata_bhajans.size() - ctx.adapter_bhajans.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private void FetchLanguageList() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(getActivity());
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getFilterLanguageList(new Callback<LanguageBasePojo>() {
                @Override
                public void success(LanguageBasePojo basePojo, Response response) {
                    if (basePojo != null) {
                        ctx.checkCommonURL(response.getUrl());
//                        if (basePojo.getData().getArrayList().size() > 0)
//                            language_filter_list.setAdapter(new Filter_Language_List_Adapter(getActivity(), basePojo.getData().getArrayList()));

                        LayoutInflater inflater = (LayoutInflater) ctx
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        layout = inflater.inflate(R.layout.right_side_language_list,
                                (ViewGroup) ctx.findViewById(R.id.popup_element));
                        layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                        pwindo = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT, true);

                        pwindo.setWidth(getResources().getInteger(R.integer.devidevta_popup_window_width));
                        if (mBackground == null)
                            pwindo.setBackgroundDrawable(new BitmapDrawable());
                        else
                            pwindo.setBackgroundDrawable(mBackground);
                        pwindo.setOutsideTouchable(true);
                        pwindo.setFocusable(true);
                        pwindo.setTouchable(true);

                        language_filter_list = (ListView)layout.findViewById(R.id.language_list_filter);
                        language_filter_list.setAdapter(new Filter_Language_List_Adapter(getActivity(), basePojo.getData().getArrayList()));
                        TextView swipeText = (TextView) layout.findViewById(R.id.swipeText);
                        swipeText.setText("Filter By Language");

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        params.setMargins(0,getResources().getInteger(R.integer.devidevta_popup_window_margin),0,0);
                        swipeText.setLayoutParams(params);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
//            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() && ctx.arrdata_bhajans != null && ctx.arrdata_bhajans.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search"))
                new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
            else
                new GetSongsList().execute();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }


    public class Filter_Language_List_Adapter extends BaseAdapter {
        ArrayList<LanguageListPojo> arr;
        Context context;
        private LayoutInflater inflater = null;

        public Filter_Language_List_Adapter(Context context, ArrayList<LanguageListPojo> arr) {
            this.context = context;
            this.arr = arr;
            inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return arr.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class Holder {
            private ImageView radio;
            private TextView radio_text;
            private LinearLayout layout_filter_bg;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final Holder holder = new Holder();
            View rowView;
            rowView = inflater.inflate(R.layout.filter_language_list, null);
            holder.radio = (ImageView) rowView.findViewById(R.id.radio_button);
            holder.radio_text = (TextView) rowView.findViewById(R.id.language_name);
            holder.layout_filter_bg = (LinearLayout) rowView.findViewById(R.id.layout_filter_bg);
            if (arr.get(position).getIsCheck()) {
                holder.radio.setBackground(ContextCompat.getDrawable(context, R.mipmap.radio_selected));
                holder.radio_text.setTextColor(ContextCompat.getColor(context, R.color.radio_selected_text));
                holder.radio_text.setTypeface(face_bold);
            } else {
                holder.radio.setBackground(ContextCompat.getDrawable(context, R.mipmap.radio_box));
                holder.radio_text.setTextColor(ContextCompat.getColor(context, R.color.radio_text));
                holder.radio_text.setTypeface(face_normal);
            }

            holder.radio_text.setText(arr.get(position).getName());
            holder.layout_filter_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    for (int i = 0; i < arr.size(); i++) {
                        if (arr.get(i).getIsCheck()) {
                            arr.get(i).setIsCheck(false);
                            holder.radio_text.setTextColor(ContextCompat.getColor(context, R.color.radio_text));
                            holder.radio_text.setTypeface(face_normal);
                            break;
                        }
                    }

                    notifyDataSetChanged();

                    holder.radio.setBackground(ContextCompat.getDrawable(context, R.mipmap.radio_selected));
                    holder.radio_text.setTextColor(ContextCompat.getColor(context, R.color.radio_selected_text));
                    holder.radio_text.setTypeface(face_bold);
                    arr.get(position).setIsCheck(true);

                    mDrawerLayout.closeDrawers();

                    str_alphbet = "";
                    start = 0;
                    language_id = arr.get(position).getId();

                    ctx.arrdata_bhajans.clear();

                    new GetSongsList().execute();
                    selected_text = (arr.get(position).getName());
                    filterby_language_text.setText(selected_text);
                    pwindo.dismiss();
                }
            });

            return rowView;
        }
    }

//    public static class DrawerFragmentRight extends Fragment {
//
//        public ActionBarDrawerToggle mDrawerToggle;
//
//        public DrawerFragmentRight() {
//
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//            v = inflater.inflate(R.layout.right_side_decade_list, container, false);
//            language_filter_list = (ListView) v.findViewById(R.id.language_list_filter);
//            return v;
//        }
//
//        @SuppressLint("NewApi")
//        public void setUp(DrawerLayout drawerlayout, Toolbar toolbar) {
//
//            mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerlayout, R.string.open_drawer, R.string.close_drawer) {
//                @Override
//                public void onDrawerOpened(View drawerView) {
//                    super.onDrawerOpened(drawerView);
//
//                    getActivity().invalidateOptionsMenu();
//                }
//
//                @SuppressLint("NewApi")
//                @Override
//                public void onDrawerClosed(View drawerView) {
//                    super.onDrawerClosed(drawerView);
//                }
//            };
//
//            drawerlayout.setDrawerListener(mDrawerToggle);
//
//        }
//
//        @Override
//        public void onAttach(Activity activity) {
//            super.onAttach(activity);
//        }
//
//        @Override
//        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//            super.onViewCreated(view, savedInstanceState);
//        }
//
//        @Override
//        public void onActivityCreated(Bundle savedInstanceState) {
//            super.onActivityCreated(savedInstanceState);
//
//            v.setFocusableInTouchMode(true);
//            v.requestFocus();
//            v.setOnKeyListener(new View.OnKeyListener() {
//
//                @Override
//                public boolean onKey(View v, int keyCode, KeyEvent event) {
//                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
//                        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
//                            mDrawerLayout.closeDrawer(Gravity.RIGHT);
//
//                        } else {
//                            Intent i = new Intent(getActivity(), CategoryListActivity.class);
//                            startActivity(i);
//                            getActivity().finish();
//                        }
//                        return true;
//                    } else {
//                        return false;
//                    }
//                }
//            });
//        }
//    }

    private void initiatePopupWindow() {
        try {

            pwindo.showAsDropDown(filterby_language_text, getResources().getInteger(R.integer.popup_window_x_offset), getResources().getInteger(R.integer.popup_window_y_offset));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}