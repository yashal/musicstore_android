package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BuyAnyAlbumHindiLanding;
import com.saregama.musicstore.adapter.Mp3HindiAlbumAdapter;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.pojo.Mp3HindiAlbumPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;


public class BuyAnyAlbum_GeetmalaFragment extends Fragment {

    private BuyAnyAlbumHindiLanding ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
    private EditText mp3song_search;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private int start = 0;

    private ArrayList<MP3HindiAlbumListPojo> arrdata;
    private Mp3HindiAlbumAdapter adapter;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0;

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_mp3_songs, container, false);

        ctx = (BuyAnyAlbumHindiLanding) getActivity();
        cd = new ConnectionDetector(getActivity());

        mRecyclerView = (RecyclerView) view.findViewById(R.id.mp3songs_recycler_view);
        mp3song_search = (EditText) view.findViewById(R.id.search_song);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        dialog = new SaregamaDialogs(getActivity());

        if (arrdata == null)
            arrdata = new ArrayList<>();

        if (!fragmentResume && fragmentVisible && arrdata != null && arrdata.size() == 0) { //only when first time fragment is created
            new GetSongsList().execute();
        }

        mp3song_search.setHint("Search Albums");
        ctx.softKeyboardDoneClickListener(mp3song_search);

        mp3song_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (mp3song_search.getText().toString().length() > 0 && arrdata != null) {
                    ArrayList<MP3HindiAlbumListPojo> arrFiltered = new ArrayList<>();
                    for (int i = 0; i < arrdata.size(); i++) {
                        if (arrdata.get(i).getAlbum_name().toLowerCase().contains(mp3song_search.getText().toString().toLowerCase()))
                            arrFiltered.add(arrdata.get(i));
                    }
                    adapter = new Mp3HindiAlbumAdapter(ctx, arrFiltered, "buyanyalbum", "");
                    mRecyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    adapter = new Mp3HindiAlbumAdapter(ctx, arrdata, "buyanyalbum", "");
                    mRecyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        new GetSongsList().execute();
                        loading = true;
                    }
                }
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() && arrdata != null && arrdata.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            new GetSongsList().execute();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private Mp3HindiAlbumPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                // dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL+"album?" + "d_type=24" + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&c_type=" + getActivity().getIntent().getIntExtra("c_type", 1) + "&song_type=" + ctx.getAppConfigJson().getSong_type().getSONG()), Mp3HindiAlbumPojo.class);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (cd.isConnectingToInternet()) {
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            mp3song_search.setText("");
                            if (arrdata.size() > 0)
                                arrdata.addAll(basePojo.getData().getList());
                            else
                                arrdata = basePojo.getData().getList();
                        }

                        if (basePojo.getStatus()) {
                            mAdapter = new Mp3HindiAlbumAdapter(ctx, arrdata, "buyanyalbum", "");
                            mRecyclerView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            mRecyclerView.scrollToPosition(firstVisibleItemPosition);
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }
}
