package com.saregama.musicstore.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.Mp3HindiLandingActivity;
import com.saregama.musicstore.activity.SearchArtistSongListActivity;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.pojo.ArtistListPojo;
import com.saregama.musicstore.pojo.ArtistPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;
import com.saregama.musicstore.views.CustomTextView;

import java.util.ArrayList;

/**
 * Created by navneet on 7/3/2016.
 */
public class Mp3HindiArtistFragment extends Fragment {

    private Mp3HindiLandingActivity ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
    private RecyclerView mRecyclerView;
    private String from;
    private ArtistListAdapter adapter;
    private ArrayList<ArtistListPojo> arr_main, arr_all;
    private ArrayList<ArtistListPojo> arrActor, arrSinger, arrComposer;
    private LinearLayout mp3hindi_artist_ActorLL, mp3hindi_artist_SingerLL, mp3hindi_artist_musicLL;
    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0;
    private int role;
    private int start = 0;
    private CustomTextView actor_count, singer_count, composer_count;
    private Dialog artiste_dialog;
    boolean dialog_object = false;
    private String search_string = "";
    private LinearLayout alpha_layout;

    private String str_alphbet = "";
    private String search_artist_name = "";

    private boolean fragmentOnCreated = false;
    private Typeface face_normal, face_bold;
    private CustomTextView dialog_all, dialog_popular;
    private boolean mFlag = false;
    private EditText search_artist_song;
    private boolean text_changed = false;

    /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;
    private AlphabetListAdapter alpha_adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.mp3_hindi_artist, container, false);

        from = getActivity().getIntent().getStringExtra("from");

        if (from == null)
            from = "";

        ctx = (Mp3HindiLandingActivity) getActivity();
        face_normal = Typeface.createFromAsset(ctx.getAssets(), "fonts/SourceSansPro_Regular.otf");
        face_bold = Typeface.createFromAsset(ctx.getAssets(), "fonts/SourceSansPro_Semibold.otf");

        cd = new ConnectionDetector(getActivity());
        dialog = new SaregamaDialogs(getActivity());

        actor_count = (CustomTextView) view.findViewById(R.id.actor_count);
        singer_count = (CustomTextView) view.findViewById(R.id.singer_count);
        composer_count = (CustomTextView) view.findViewById(R.id.composer_count);
        TextView search = (TextView) view.findViewById(R.id.mp3Hindi_artist_search);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateSearchString();
            }
        });

        arr_main = new ArrayList<>();
        arr_all = new ArrayList<>();
        arrActor = new ArrayList<>();
        arrSinger = new ArrayList<>();
        arrComposer = new ArrayList<>();

        mp3hindi_artist_musicLL = (LinearLayout) view.findViewById(R.id.mp3hindi_artist_musicLL);
        mp3hindi_artist_SingerLL = (LinearLayout) view.findViewById(R.id.mp3hindi_artist_SingerLL);
        mp3hindi_artist_ActorLL = (LinearLayout) view.findViewById(R.id.mp3hindi_artist_ActorLL);

        mp3hindi_artist_ActorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                role = 1;
                arr_main.clear();
                new GetPopularArtisteList().execute(role);
            }
        });

        mp3hindi_artist_SingerLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                role = 8;
                arr_main.clear();
                new GetPopularArtisteList().execute(role);
            }
        });

        mp3hindi_artist_musicLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                role = 2;
                arr_main.clear();
                new GetPopularArtisteList().execute(role);
            }
        });

        return view;
    }

    /**
     * This method is use to show the list of Artiste, Singers, Music Directors
     * We can pass role = 1 to fetch the list of Actors
     * We can pass role = 8 to fetch the list of Singers
     * We can pass role = 2 to fetch the list of Music Directors
     */
    private void displayDialog() {

        artiste_dialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        artiste_dialog.setContentView(R.layout.artist_data_dialog);
        dialog_object = true;

        mRecyclerView = (RecyclerView) artiste_dialog.findViewById(R.id.mp3songs_artistdialog_recycler);
        TextView dialog_done = (TextView) artiste_dialog.findViewById(R.id.dialog_done);
        CustomTextView dialog_header = (CustomTextView) artiste_dialog.findViewById(R.id.artistdialog_header);
        dialog_all = (CustomTextView) artiste_dialog.findViewById(R.id.artist_dialog_alltext);
        dialog_popular = (CustomTextView) artiste_dialog.findViewById(R.id.artist_dialog_populartext);
        ImageView close_dialog = (ImageView) artiste_dialog.findViewById(R.id.close_dialog);
        final LinearLayout dialog_doneLL = (LinearLayout) artiste_dialog.findViewById(R.id.dialog_doneLL);
        search_artist_song = (EditText) artiste_dialog.findViewById(R.id.search_artist_song);
        if (ctx != null)
            ctx.softKeyboardDoneClickListener(search_artist_song);
        rowTextView = (TextView) artiste_dialog.findViewById(R.id.row_text_view);
        alphabetslist = (ListView)artiste_dialog.findViewById(R.id.mp3_landing_list);
        alpha_layout = (LinearLayout) artiste_dialog.findViewById(R.id.alpha_layout);

         /* ****scrollable alphabet list code as per change request start**** */
        alpha_adapter = new AlphabetListAdapter(ctx, SaregamaConstants.artist_arr);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                str_alphbet =  SaregamaConstants.artist_arr[position].toLowerCase();
//                str_alphbet =  convertAlphabet(SaregamaConstants.items_artist[position].toLowerCase());
                alpha_adapter.setSelectedIndex(position);
                arr_main.clear();
                arr_all.clear();
                start = 0;
                previousTotal = 0;
                if (mFlag) {
                    new GetAllArtisteList().execute(role);
                } else {
                    new GetPopularArtisteList().execute(role);
                }

                ctx.setBubblePositionforArtiste(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        setStyle(1);
        artiste_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        /**
         * Below code is use to show the done button. If keypad is open than done button will not be visible.
         */
        final View rootView = artiste_dialog.findViewById(R.id.root_view_artist);
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
//                int heightDiff = rootView.getRootView().getHeight() - (r.bottom - r.top);
                int heightDiffa = rootView.getRootView().getHeight() - rootView.getHeight();
                if (heightDiffa > getResources().getInteger(R.integer.artist_height)) {
                    dialog_doneLL.setVisibility(View.GONE);
                } else {
                    dialog_doneLL.setVisibility(View.VISIBLE);
                }
            }
        });

        dialog_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popular_AllClick(true, 0);
                new GetAllArtisteList().execute(role);
                alpha_adapter = new AlphabetListAdapter(ctx, SaregamaConstants.artist_arr);
                alphabetslist.setAdapter(alpha_adapter);
                addScrollListner();
            }
        });

        dialog_popular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popular_AllClick(false, 1);
                new GetPopularArtisteList().execute(role);
                addScrollListner();
            }
        });

        search_artist_song.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (search_artist_song.getText().toString().trim().length() > 2) {
                    String search_keyword = search_artist_song.getText().toString().trim();
                    search_keyword = search_keyword.replaceAll(" ", "%20");
                    text_changed = true;
                    new SearchArtisteList(role, search_keyword).execute();
                } else if (mFlag && text_changed && search_artist_song.getText().toString().trim().equals("")) {
                    text_changed = false;
                    arr_main.clear();
                    new GetAllArtisteList().execute(role);
                } else if(text_changed && search_artist_song.getText().toString().trim().equals("")){
                    text_changed = false;
                    arr_main.clear();
                    new GetPopularArtisteList().execute(role);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        setAlphabetClickListener();

        if (role == 1) {
            dialog_header.setText("Actors");
        } else if (role == 8) {
            dialog_header.setText("Singers");
        } else {
            dialog_header.setText("Music Directors");
        }
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        dialog_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCountInLayout();
                artiste_dialog.dismiss();
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                artiste_dialog.dismiss();
                ctx.hideSoftKeyboard();
            }
        });

        artiste_dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                removeFromArray();
                ctx.hideSoftKeyboard();
            }
        });

        setAdapter(arr_all);
        mRecyclerView.scrollToPosition(firstVisibleItemPosition);

        addScrollListner();

        artiste_dialog.show();
    }

    /**
     * Resets all the values for all the variables.
     *
     * @param value    boolean value to check which API needs to be called.
     * @param position integer value to set the style for all and popular tab.
     */
    private void popular_AllClick(boolean value, int position) {
        mFlag = value;
        start = 0;
        previousTotal = 0;
        totalItemCount = 0;
        visibleItemCount = 0;
        str_alphbet = "";
        setStyle(position);
        arr_main.clear();
        search_artist_song.setText("");
    }

    public String convertAlphabet(String alphabet) {
        if (alphabet.equals("#"))
            alphabet = SaregamaConstants.SPECIAL;
        else if (alphabet.equalsIgnoreCase("ALL"))
            alphabet = SaregamaConstants.ALL;

        return alphabet;
    }

    private void setStyle(int position) {
        if (position == 0) {
            if(alphabetslist != null) {
                alphabetslist.setVisibility(View.VISIBLE);
                alpha_adapter = new AlphabetListAdapter(ctx, SaregamaConstants.artist_arr);
                alphabetslist.setAdapter(alpha_adapter);
            }
            dialog_all.setTextColor(ContextCompat.getColor(ctx, R.color.white));
            dialog_popular.setTextColor(ContextCompat.getColor(ctx, R.color.black));
            dialog_all.setBackgroundResource(R.drawable.artist_all_tab_layout);
            dialog_popular.setBackgroundResource(R.drawable.artist_popular_tab_layout);
        } else if (position == 1) {
            if(alphabetslist != null)
                alphabetslist.setVisibility(View.GONE);
            dialog_all.setTextColor(ContextCompat.getColor(ctx, R.color.black));
            dialog_popular.setTextColor(ContextCompat.getColor(ctx, R.color.white));
            dialog_all.setBackgroundResource(R.drawable.artist_popular_tab_layout);
            dialog_popular.setBackgroundResource(R.drawable.artist_all_tab_layout);
        }
    }

    private void setAlphabetClickListener() {
        LinearLayout linearAll = (LinearLayout) artiste_dialog.findViewById(R.id.linearAll);
        linearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        linearAll.setVisibility(View.GONE);
        LinearLayout linearHash = (LinearLayout) artiste_dialog.findViewById(R.id.linearHash);
        linearHash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        linearHash.setVisibility(View.GONE);
        LinearLayout linearA = (LinearLayout) artiste_dialog.findViewById(R.id.linearA);
        linearA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearB = (LinearLayout) artiste_dialog.findViewById(R.id.linearB);
        linearB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearC = (LinearLayout) artiste_dialog.findViewById(R.id.linearC);
        linearC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearD = (LinearLayout) artiste_dialog.findViewById(R.id.linearD);
        linearD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearE = (LinearLayout) artiste_dialog.findViewById(R.id.linearE);
        linearE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearF = (LinearLayout) artiste_dialog.findViewById(R.id.linearF);
        linearF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });

        LinearLayout linearG = (LinearLayout) artiste_dialog.findViewById(R.id.linearG);
        linearG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearH = (LinearLayout) artiste_dialog.findViewById(R.id.linearH);
        linearH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearI = (LinearLayout) artiste_dialog.findViewById(R.id.linearI);
        linearI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearJ = (LinearLayout) artiste_dialog.findViewById(R.id.linearJ);
        linearJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearK = (LinearLayout) artiste_dialog.findViewById(R.id.linearK);
        linearK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearL = (LinearLayout) artiste_dialog.findViewById(R.id.linearL);
        linearL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearM = (LinearLayout) artiste_dialog.findViewById(R.id.linearM);
        linearM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });

        LinearLayout linearN = (LinearLayout) artiste_dialog.findViewById(R.id.linearN);
        linearN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearO = (LinearLayout) artiste_dialog.findViewById(R.id.linearO);
        linearO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearP = (LinearLayout) artiste_dialog.findViewById(R.id.linearP);
        linearP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearQ = (LinearLayout) artiste_dialog.findViewById(R.id.linearQ);
        linearQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearR = (LinearLayout) artiste_dialog.findViewById(R.id.linearR);
        linearR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearS = (LinearLayout) artiste_dialog.findViewById(R.id.linearS);
        linearS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });

        LinearLayout linearT = (LinearLayout) artiste_dialog.findViewById(R.id.linearT);
        linearT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearU = (LinearLayout) artiste_dialog.findViewById(R.id.linearU);
        linearU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearV = (LinearLayout) artiste_dialog.findViewById(R.id.linearV);
        linearV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearW = (LinearLayout) artiste_dialog.findViewById(R.id.linearW);
        linearW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearX = (LinearLayout) artiste_dialog.findViewById(R.id.linearX);
        linearX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearY = (LinearLayout) artiste_dialog.findViewById(R.id.linearY);
        linearY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearZ = (LinearLayout) artiste_dialog.findViewById(R.id.linearZ);
        linearZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
    }

    /**
     * Method is use to show the bubble on click of alphabets.
     *
     * @param v is the view on which click has been done.
     */
    public void showListBubble(View v) {
        search_artist_song.setText("");
        if (ctx != null)
            ctx.hideSoftKeyboard();
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        previousTotal = 0;
        str_alphbet = showListBubbleCommon(v, rowTextView);
        arr_main.clear();
        arr_all.clear();
        if (mFlag) {
            new GetAllArtisteList().execute(role);
        } else {
            new GetPopularArtisteList().execute(role);
        }
    }

    /**
     * Method is use to show the bubble on click of alphabets.
     *
     * @param v is the view on which click has been done.
     */
    public String showListBubbleCommon(View v, TextView rowTextView) {
        //find the index of the separator row view
        rowTextView = (TextView) artiste_dialog.findViewById(R.id.row_text_view);
        int count = alpha_layout.getChildCount();
        String alphabet = "";
        for (int i = 0; i < count; i++) {
            View inner_layout = alpha_layout.getChildAt(i);
            View view = ((ViewGroup) inner_layout).getChildAt(0);

            if (view.isPressed()) {
                rowTextView.setText(view.getTag() + "");
                alphabet = (String) view.getTag();
                rowTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.alphabetic_text_size));
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(ctx, R.drawable.pressed));
                view.setBackgroundResource(R.drawable.pressed);
            } else {
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(ctx, R.drawable.normal));
                view.setBackgroundResource(R.drawable.normal);
            }
        }
        if (alphabet.equals("#"))
            alphabet = SaregamaConstants.SPECIAL;
        else if (alphabet.equalsIgnoreCase("ALL"))
            alphabet = SaregamaConstants.ALL;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, v.getTop() - 5, 0, 0);
        rowTextView.setLayoutParams(layoutParams);
        rowTextView.setPadding(0, 0, 6, 0);
        rowTextView.setVisibility(View.VISIBLE);
        return alphabet;
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.ARTIST_LIMIT;
                        if (mFlag) {
                            new GetAllArtisteList().execute(role);
                        } else {
                            new GetPopularArtisteList().execute(role);
                        }
                        loading = true;
                    }
                }
            }
        });
    }

    private class GetPopularArtisteList extends AsyncTask<Integer, Integer, Void> {
        ProgressDialog d;
        private ArtistPojo basePojo;

        @Override
        protected void onPreExecute() {
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }
            }
        }

        @Override
        protected Void doInBackground(Integer... role) {
            if (isCancelled())
                return null;
            if (dialog_object) {
                if (!artiste_dialog.isShowing()) {
                    start = 0;
                    str_alphbet = "";
                }
            }

            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "artist?role=" + role[0] + "&start=" + start + "&limit=" + SaregamaConstants.ARTIST_LIMIT + "&d_type=" + ctx.getIntFromPrefs(SaregamaConstants.D_TYPE) + "&order=" + SaregamaConstants.ORDER + "&alpha=" + str_alphbet), ArtistPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (rowTextView != null)
                rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {

                            if (dialog_object && !artiste_dialog.isShowing())
                                arr_main.clear();
                            if (basePojo.getData().getList() != null) {
                                if (arr_main.size() > 0)
                                    arr_main.addAll(basePojo.getData().getList());
                                else
                                    arr_main = basePojo.getData().getList();
                            }

                            generateArrayList();

                            if (dialog_object) {
                                if (!artiste_dialog.isShowing()) {
                                    loading = false;
                                    previousTotal = 0;
                                    displayDialog();
                                } else {
                                    setAdapter(arr_all);
                                    mRecyclerView.scrollToPosition(firstVisibleItemPosition);
                                    addScrollListner();
                                }
                            } else
                                displayDialog();

                        } else {
                            firstVisibleItemPosition = 0;
                            start = 0;
                            mRecyclerView.clearOnScrollListeners();
                            mRecyclerView.scrollToPosition(0);
//                            Toast.makeText(getActivity(), SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetAllArtisteList extends AsyncTask<Integer, Integer, Void> {
        ProgressDialog d;
        private ArtistPojo basePojo;

        @Override
        protected void onPreExecute() {
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }
            }
        }

        @Override
        protected Void doInBackground(Integer... role) {
            if (isCancelled())
                return null;
            if (dialog_object) {
                if (!artiste_dialog.isShowing()) {
                    start = 0;
                    str_alphbet = "";
                }
            }

            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "artist?role=" + role[0] + "&start=" + start + "&limit=" + SaregamaConstants.ARTIST_LIMIT + "&d_type=" + ctx.getIntFromPrefs(SaregamaConstants.D_TYPE) + "&alpha=" + str_alphbet), ArtistPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (rowTextView != null)
                rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {
                            if (dialog_object && !artiste_dialog.isShowing())
                                arr_main.clear();
                            if (arr_main != null && arr_main.size() > 0)
                                arr_main.addAll(basePojo.getData().getList());
                            else
                                arr_main = basePojo.getData().getList();

                            generateArrayList();

                            if (dialog_object) {
                                if (!artiste_dialog.isShowing()) {
                                    loading = false;
                                    previousTotal = 0;
                                    displayDialog();
                                } else {
                                    setAdapter(arr_all);
                                    mRecyclerView.scrollToPosition(firstVisibleItemPosition);
                                    addScrollListner();
                                }
                            } else
                                displayDialog();

                        } else {
                            firstVisibleItemPosition = 0;
                            start = 0;
                            mRecyclerView.clearOnScrollListeners();
                            mRecyclerView.scrollToPosition(0);
                            Toast.makeText(getActivity(), SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private void removeFromArray() {
        if (role == 1) {
            if (arrActor != null && arrActor.size() > 0) {
                for (int i = arrActor.size() - 1; i >= 0; i--) {
                    arrActor.get(i).setRemoved(false);
                    if (!arrActor.get(i).isDone()) {
                        arrActor.remove(i);
                    }
                }
            }
        } else if (role == 8) {
            if (arrSinger != null && arrSinger.size() > 0) {
                for (int i = arrSinger.size() - 1; i >= 0; i--) {
                    arrSinger.get(i).setRemoved(false);
                    if (!arrSinger.get(i).isDone()) {
                        arrSinger.remove(i);
                    }
                }
            }
        } else {
            if (arrComposer != null && arrComposer.size() > 0) {
                for (int i = arrComposer.size() - 1; i >= 0; i--) {
                    arrComposer.get(i).setRemoved(false);
                    if (!arrComposer.get(i).isDone()) {
                        arrComposer.remove(i);
                    }
                }
            }
        }

        addCountInLayout();
    }

    private void addCountInLayout() {
        if (role == 1) {
            if (arrActor != null && arrActor.size() > 0) {
                for (int i = arrActor.size() - 1; i >= 0; i--) {
                    arrActor.get(i).setDone(true);
                    if (arrActor.get(i).isRemoved()) {
                        arrActor.remove(i);
                    }
                }
                actor_count.setVisibility(View.VISIBLE);
                actor_count.setText(arrActor.size() + "");
            } else
                actor_count.setVisibility(View.GONE);
        } else if (role == 8) {
            if (arrSinger != null && arrSinger.size() > 0) {
                for (int i = arrSinger.size() - 1; i >= 0; i--) {
                    arrSinger.get(i).setDone(true);
                    if (arrSinger.get(i).isRemoved()) {
                        arrSinger.remove(i);
                    }
                }
                singer_count.setVisibility(View.VISIBLE);
                singer_count.setText(arrSinger.size() + "");
            } else
                singer_count.setVisibility(View.GONE);
        } else {
            if (arrComposer != null && arrComposer.size() > 0) {
                for (int i = arrComposer.size() - 1; i >= 0; i--) {
                    arrComposer.get(i).setDone(true);
                    if (arrComposer.get(i).isRemoved()) {
                        arrComposer.remove(i);
                    }
                    composer_count.setVisibility(View.VISIBLE);
                    composer_count.setText(arrComposer.size() + "");
                }
            } else
                composer_count.setVisibility(View.GONE);

        }
    }

    private void removeDataFromString(String id) {
        if (role == 1) {
            for (int i = 0; i < arrActor.size(); i++) {
                if (arrActor.get(i).getId().equals(id)) {
                    if (arrActor.get(i).isDone())
                        arrActor.get(i).setRemoved(true);
                    else
                        arrActor.remove(i);
                    break;
                }
            }
        } else if (role == 8) {
            for (int i = 0; i < arrSinger.size(); i++) {
                if (arrSinger.get(i).getId().equals(id)) {
                    if (arrSinger.get(i).isDone())
                        arrSinger.get(i).setRemoved(true);
                    else
                        arrSinger.remove(i);
                    break;
                }
            }
        } else {
            for (int i = 0; i < arrComposer.size(); i++) {
                if (arrComposer.get(i).getId().equals(id)) {
                    if (arrComposer.get(i).isDone())
                        arrComposer.get(i).setRemoved(true);
                    else
                        arrComposer.remove(i);
                    break;
                }
            }
        }
        generateArrayList();
    }

    private void addDataInString(String id, String name) {
        ArtistListPojo obj = new ArtistListPojo();
        obj.setSelected(true);
        obj.setDone(false);
        obj.setId(id);
        obj.setName(name);
        obj.setRole(role);

        if (role == 1) {
            boolean add = true;
            for (int i = 0; i < arrActor.size(); i++) {
                if (arrActor.get(i).getId().equals(id)) {
                    add = false;
                    arrActor.get(i).setRemoved(false);
                    break;
                }
            }
            if (add)
                arrActor.add(obj);
        } else if (role == 8) {
            boolean add = true;
            for (int i = 0; i < arrSinger.size(); i++) {
                if (arrSinger.get(i).getId().equals(id)) {
                    add = false;
                    arrSinger.get(i).setRemoved(false);
                    break;
                }
            }
            if (add)
                arrSinger.add(obj);
        } else {
            boolean add = true;
            for (int i = 0; i < arrComposer.size(); i++) {
                if (arrComposer.get(i).getId().equals(id)) {
                    add = false;
                    arrComposer.get(i).setRemoved(false);
                    break;
                }
            }
            if (add)
                arrComposer.add(obj);
        }
        generateArrayList();
    }

    private void generateSearchString() {
        search_string = "";
        search_artist_name = "";
        if (arrActor != null && arrActor.size() > 0) {
            for (int i = 0; i < arrActor.size(); i++) {
                search_string += arrActor.get(i).getId() + ",";
                search_artist_name += arrActor.get(i).getName() + ",";
            }
            search_string = search_string.substring(0, search_string.length() - 1) + "~1|";
        }
        if (arrSinger != null && arrSinger.size() > 0) {
            for (int i = 0; i < arrSinger.size(); i++) {
                search_string += arrSinger.get(i).getId() + ",";
                search_artist_name += arrSinger.get(i).getName() + ",";
            }
            search_string = search_string.substring(0, search_string.length() - 1) + "~8|";
        }
        if (arrComposer != null && arrComposer.size() > 0) {
            for (int i = 0; i < arrComposer.size(); i++) {
                search_string += arrComposer.get(i).getId() + ",";
                search_artist_name += arrComposer.get(i).getName() + ",";
            }
            search_string = search_string.substring(0, search_string.length() - 1) + "~2|";
        }
        if (!search_string.equals("")) {
            search_string = search_string.substring(0, search_string.length() - 1);
            search_artist_name = search_artist_name.substring(0, search_artist_name.length() - 1);
        }

        if (!search_string.equals("")) {
            Intent mIntent = new Intent(getActivity(), SearchArtistSongListActivity.class);
            mIntent.putExtra("search_string", search_string);
            mIntent.putExtra("search_artist_name", search_artist_name);
            mIntent.putExtra("arrActor", arrActor);
            mIntent.putExtra("arrSinger", arrSinger);
            mIntent.putExtra("c_type", getActivity().getIntent().getIntExtra("c_type", 1));
            mIntent.putExtra("arrComposer", arrComposer);
            mIntent.putExtra("from", from);
            startActivity(mIntent);
        } else {
            dialog.displayCommonDialogWithHeader("Search Artiste", ctx.getAppConfigJson().getArtist_search());
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        if (ctx_hd != null)
//            ctx_hd.hideSoftKeyboard();
//        else if (ctx != null)
//            ctx.hideSoftKeyboard();
//        else if (ctx_hd_reg != null)
//            ctx_hd_reg.hideSoftKeyboard();
//        else if(ctx_mp3_reg != null)
//            ctx_mp3_reg.hideSoftKeyboard();
        if (mFlag) {
            /*if (isVisibleToUser && isResumed() && all_arrdata != null && all_arrdata.size() == 0) {   // only at fragment screen is resumed
                fragmentOnCreated = true;
            } else if (isVisibleToUser) {        // only at fragment onCreated
                fragmentOnCreated = true;
            } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            }*/
        } else {
            if (isVisibleToUser && isResumed() && arr_main != null && arr_main.size() == 0) {   // only at fragment screen is resumed
                fragmentOnCreated = true;
            } else if (isVisibleToUser) {        // only at fragment onCreated
                fragmentOnCreated = true;
            } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            }
        }
    }


    public class ArtistListAdapter extends RecyclerView.Adapter<ArtistListAdapter.ViewHolder> {

        private ArrayList<ArtistListPojo> arrayArtistList;
        private Activity context;

        public ArtistListAdapter(Activity context, ArrayList<ArtistListPojo> arrayArtistList) {
            this.context = context;
            this.arrayArtistList = arrayArtistList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.artist_row_layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            v.setOnClickListener(new MyOnClickListener());
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final ArtistListPojo data = arrayArtistList.get(position);

            holder.artist_name.setText(data.getName());
            if (role == 1) {
                for (int i = 0; i < arrActor.size(); i++) {
                    if (data.getId().equals(arrActor.get(i).getId()) && !arrActor.get(i).isRemoved()) {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist_selected);
                        holder.check.setChecked(true);
                        holder.artist_name.setTextColor(ContextCompat.getColor(getActivity(), R.color.light_black));
                        holder.artist_name.setTypeface(face_bold);
                        break;
                    } else {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist);
                        holder.check.setChecked(false);
                        holder.artist_name.setTextColor(ContextCompat.getColor(getActivity(), R.color.header_selected_text_color));
                        holder.artist_name.setTypeface(face_normal);
                    }
                }
            } else if (role == 8) {
                for (int i = 0; i < arrSinger.size(); i++) {
                    if (data.getId().equals(arrSinger.get(i).getId()) && !arrSinger.get(i).isRemoved()) {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist_selected);
                        holder.check.setChecked(true);
                        holder.artist_name.setTextColor(ContextCompat.getColor(getActivity(), R.color.light_black));
                        holder.artist_name.setTypeface(face_bold);
                        break;
                    } else {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist);
                        holder.check.setChecked(false);
                        holder.artist_name.setTextColor(ContextCompat.getColor(getActivity(), R.color.header_selected_text_color));
                        holder.artist_name.setTypeface(face_normal);
                    }
                }
            } else {
                for (int i = 0; i < arrComposer.size(); i++) {
                    if (data.getId().equals(arrComposer.get(i).getId()) && !arrComposer.get(i).isRemoved()) {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist_selected);
                        holder.check.setChecked(true);
                        holder.artist_name.setTextColor(ContextCompat.getColor(getActivity(), R.color.light_black));
                        holder.artist_name.setTypeface(face_bold);
                        break;
                    } else {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist);
                        holder.check.setChecked(false);
                        holder.artist_name.setTextColor(ContextCompat.getColor(getActivity(), R.color.header_selected_text_color));
                        holder.artist_name.setTypeface(face_normal);
                    }
                }
            }
        }

        class MyOnClickListener implements View.OnClickListener {
            @Override
            public void onClick(View v) {

                int itemPosition = mRecyclerView.getChildPosition(v);
                final ArtistListPojo data = arrayArtistList.get(itemPosition);
                CheckBox check = (CheckBox) v.findViewById(R.id.check_box);
                TextView artist_name = (TextView) v.findViewById(R.id.artist_name);
                if (!check.isChecked()) {
                    check.setButtonDrawable(R.mipmap.check_box_artist_selected);
                    check.setChecked(true);
                    artist_name.setTextColor(ContextCompat.getColor(getActivity(), R.color.light_black));
                    artist_name.setTypeface(face_bold);
                    addDataInString(data.getId(), data.getName());
                } else {
                    check.setButtonDrawable(R.mipmap.check_box_artist);
                    check.setChecked(false);
                    artist_name.setTextColor(ContextCompat.getColor(getActivity(), R.color.header_selected_text_color));
                    artist_name.setTypeface(face_normal);
                    removeDataFromString(data.getId());
                }
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private TextView artist_name;
            private CheckBox check;

            public ViewHolder(View itemView) {
                super(itemView);
                artist_name = (TextView) itemView.findViewById(R.id.artist_name);
                check = (CheckBox) itemView.findViewById(R.id.check_box);
            }
        }

        @Override
        public int getItemCount() {
            return arrayArtistList.size();
        }
    }

    private void generateArrayList() {
        /*coma(,) is added in below variable so all the id's comes between 2 comas(,). While removing id can be distinguished using these comas*/
        String id_list = ",";
        arr_all.clear();
        if (role == 1) {
            if (arrActor != null && arrActor.size() > 0) {
                for (int j = 0; j < arrActor.size(); j++) {
                    if (!arrActor.get(j).isRemoved()) {
                        id_list += arrActor.get(j).getId() + ",";
                        arr_all.add(arrActor.get(j));
                    }
                }
            }
        } else if (role == 8) {
            if (arrSinger != null && arrSinger.size() > 0) {
                for (int j = 0; j < arrSinger.size(); j++) {
                    if (!arrSinger.get(j).isRemoved()) {
                        id_list += arrSinger.get(j).getId() + ",";
                        arr_all.add(arrSinger.get(j));
                    }
                }
            }
        } else {
            if (arrComposer != null && arrComposer.size() > 0) {
                for (int j = 0; j < arrComposer.size(); j++) {
                    if (!arrComposer.get(j).isRemoved()) {
                        id_list += arrComposer.get(j).getId() + ",";
                        arr_all.add(arrComposer.get(j));
                    }
                }
            }
        }
        for (int k = 0; k < arr_main.size(); k++) {
            if (id_list.contains("," + arr_main.get(k).getId() + ","))
                arr_main.get(k).setSelected(true);
            else
                arr_main.get(k).setSelected(false);
        }
        for (int i = 0; i < arr_main.size(); i++) {
            if (!arr_main.get(i).isSelected()) {
                arr_all.add(arr_main.get(i));
            }
        }
        setAdapter(arr_all);
    }

    private void setAdapter(ArrayList<ArtistListPojo> arr) {
        adapter = new ArtistListAdapter(getActivity(), arr);
        if (mRecyclerView != null)
            mRecyclerView.setAdapter(adapter);
//        adapter.notifyItemRangeInserted(adapter.getItemCount(), arr.size() - adapter.getItemCount());
        adapter.notifyDataSetChanged();
    }

    private class SearchArtisteList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private ArtistPojo basePojo;
        int role_artist;
        String searchQuery;

        public SearchArtisteList(int role_artist, String searchQuery) {
            this.role_artist = role_artist;
            this.searchQuery = searchQuery;
        }

        @Override
        protected void onPreExecute() {
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
            }
        }

        @Override
        protected Void doInBackground(Void... role) {
            if (isCancelled())
                return null;

            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskDataArtisteSearch(SaregamaConstants.BASE_URL + "search?type=search" + "&ctype=" + SaregamaConstants.Artist + "&query=" + searchQuery + "&criteria=" + role_artist + "&mode=hindi"), ArtistPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {
                            arr_main.clear();
                            start = 0;
                            previousTotal = 0;
                            if (basePojo.getData().getList() != null) {
                                arr_main = basePojo.getData().getList();
                            }
                            adapter = new ArtistListAdapter(ctx, arr_main);
                            mRecyclerView.setAdapter(adapter);

                        }
                    } else {
//                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
//                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }
    }
}