package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.OtherHindiSongLanding;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.Mp3HindiSongAdapter;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

public class FolkFragment extends Fragment {

    private OtherHindiSongLanding ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
//    private EditText mp3song_search;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0, start = 0;

    private String str_alphbet = "";

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;
    private boolean flag_editText = false;
//    private TextView no_result_found;
//    private LinearLayout alphabets_list;
    private String search_word = "";

    /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mp3_songs, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.mp3songs_recycler_view);

        /* ****scrollable alphabet list code as per change request**** */
//        alphabets_list = (LinearLayout) view.findViewById(R.id.alphabets_list);
//        mp3song_search = (EditText) view.findViewById(R.id.search_song);
        rowTextView = (TextView) view.findViewById(R.id.row_text_view);
        alphabetslist = (ListView)view.findViewById(R.id.mp3_landing_list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(getActivity());

        dialog = new SaregamaDialogs(getActivity());

//        no_result_found = (TextView) view.findViewById(R.id.no_result_found);
//        mp3song_search.setHint("Search Songs, Albums");
        ctx = (OtherHindiSongLanding) getActivity();

        /* ****scrollable alphabet list code as per change request start**** */
        final AlphabetListAdapter alpha_adapter = new AlphabetListAdapter(getActivity(), SaregamaConstants.items);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                str_alphbet =  ctx.convertAlphabet(SaregamaConstants.items[position].toLowerCase());
                ctx.arrdata_folk.clear();
                alpha_adapter.setSelectedIndex(position);
                start = 0;
                new GetSongsList().execute();

                ctx.setBubblePosition(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search")) {
            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
        } else {
            if (!fragmentResume && fragmentVisible && ctx.arrdata_folk != null && ctx.arrdata_folk.size() == 0) { //only when first time fragment is created
                new GetSongsList().execute();
            }
        }

        /*mp3song_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int starts, int before, int count) {

                if (mp3song_search.getText().toString().trim().length() == 0 && flag_editText) {
                    flag_editText = false;
                    start = 0;
                    previousTotal = 0;
                    ctx.arrdata_folk.clear();
                    new GetSongsList().execute();
                } else {
                    if (mp3song_search.getText().toString().trim().length()  > 2) {
                        flag_editText = true;
                        start = 0;
                        previousTotal = 0;
                        ctx.arrdata_folk.clear();
                        new GetSearchSongsList().execute(mp3song_search.getText().toString().trim().replaceAll(" ", "%20"));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/

        return view;
    }

    /* **** scrollable alphabet list code as per change request comment previous code**** */
    /*public void showListBubble(View v) {
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        ctx.arrdata_folk.clear();
        str_alphbet = showListBubbleCommon(v, rowTextView);
        new GetSongsList().execute();
    }*/

    // abcd click listener
   /* public String showListBubbleCommon(View v, TextView rowTextView) {
        //find the index of the separator row view
        LinearLayout alpha_layout = (LinearLayout) view.findViewById(R.id.alpha_layout);
        rowTextView = (TextView) view.findViewById(R.id.row_text_view);
        int count = alpha_layout.getChildCount();
        String alphabet = "";
        for (int i = 0; i < count; i++) {
            View inner_layout = alpha_layout.getChildAt(i);
            View view = ((ViewGroup)inner_layout).getChildAt(0);

            if(view.isPressed()) {
                rowTextView.setText(view.getTag()+"");
                alphabet = (String) view.getTag();
                rowTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP,getResources().getInteger(R.integer.alphabetic_text_size));
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(ctx, R.drawable.pressed));
                view.setBackgroundResource(R.drawable.pressed);
            }
            else {
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(ctx, R.drawable.normal));
                view.setBackgroundResource(R.drawable.normal);
            }
        }
        if(alphabet.equals("#"))
            alphabet = SaregamaConstants.SPECIAL;
        else if(alphabet.equalsIgnoreCase("ALL"))
            alphabet = SaregamaConstants.ALL;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, v.getTop()-5, 0, 0);
        rowTextView.setLayoutParams(layoutParams);
        rowTextView.setPadding(0, 0, 6, 0);
        rowTextView.setVisibility(View.VISIBLE);
        return alphabet;
    }*/

    private void setAdapter(ArrayList<MP3HindiSongListPojo> arr) {
        ctx.folk_adapter = new Mp3HindiSongAdapter(ctx, arr, "others", search_word);
        mRecyclerView.setAdapter(ctx.folk_adapter);
        ctx.folk_adapter.notifyDataSetChanged();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (ctx != null)
            ctx.hideSoftKeyboard();
        if (isVisibleToUser && isResumed() && ctx.arrdata_folk != null && ctx.arrdata_folk.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            new GetSongsList().execute();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        /*if (mp3song_search.getText().toString().trim().length() > 2) {
                            new GetSearchSongsList().execute(mp3song_search.getText().toString().trim().replaceAll(" ", "%20"));
                        } else {*/
                            new GetSongsList().execute();
//                        }
                        loading = true;
                    }
                }
            }
        });
    }

    // Pop search list
    private class GetSearchSongsList extends AsyncTask<String,Void,Void> {
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /* ****scrollable alphabet list code as per change request **** */
//            alphabets_list.setVisibility(View.GONE);
            alphabetslist.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(String... querry) {

            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getSONG() + "&query=" + querry[0] + "&mode=" + "others" + "&d_type=" + ctx.getAppConfigJson().getD_type().getFOLK_SONGS() + "&lang=" + getActivity().getIntent().getIntExtra("languageid", 0)+ "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (basePojo != null) {
                    search_word = getActivity().getIntent().getStringExtra("search_text").trim();
                    if (basePojo.getData().getList() != null) {
                        if (ctx.arrdata_folk.size() > 0 && start > 0) {
                            ctx.arrdata_folk.addAll(basePojo.getData().getList());
                        } else {
                            ctx.arrdata_folk.clear();
                            ctx.arrdata_folk = basePojo.getData().getList();
                            setAdapter(ctx.arrdata_folk);
                            previousTotal = 0;
                        }
                    }
//                    no_result_found.setVisibility(View.GONE);
                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {
                            ctx.folk_adapter.notifyItemRangeInserted(ctx.folk_adapter.getItemCount(), ctx.arrdata_folk.size() - ctx.folk_adapter.getItemCount());
                            addScrollListner();
                        } else {
                            start = 0;
                            mRecyclerView.clearOnScrollListeners();
                            mRecyclerView.scrollToPosition(0);

//                            if (ctx.arrdata_folk.size() == 0) {
//                                no_result_found.setText(SaregamaConstants.NO_RESULT);
//                                no_result_found.setVisibility(View.VISIBLE);
//                            } else {
//                                no_result_found.setVisibility(View.GONE);
//                            }
                        }
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                // dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(getActivity().getIntent().getStringExtra("url") + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&alpha=" + str_alphbet), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
//                    no_result_found.setVisibility(View.GONE);
                    if (basePojo.getData().getList() != null) {
//                        mp3song_search.setText("");
                        if (ctx.arrdata_folk.size() > 0 && start > 0) {
                            ctx.arrdata_folk.addAll(basePojo.getData().getList());
                        } else {
                            ctx.arrdata_folk.clear();
                            ctx.arrdata_folk = basePojo.getData().getList();
                            setAdapter(ctx.arrdata_folk);
                            previousTotal = 0;
                        }
                    }

                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {
                            ctx.folk_adapter.notifyItemRangeInserted(ctx.folk_adapter.getItemCount(), ctx.arrdata_folk.size() - ctx.folk_adapter.getItemCount());
                            addScrollListner();
                        } else {
                            start = 0;
                            mRecyclerView.clearOnScrollListeners();
                            mRecyclerView.scrollToPosition(0);
                            Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }
}
