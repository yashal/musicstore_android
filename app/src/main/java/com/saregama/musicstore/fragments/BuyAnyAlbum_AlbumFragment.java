package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BuyAnyAlbumHindiLanding;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.Mp3HindiAlbumAdapter;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.pojo.Mp3HindiAlbumPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

/**
 * Created by navneet on 7/3/2016.
 */
public class BuyAnyAlbum_AlbumFragment extends Fragment {

    private BuyAnyAlbumHindiLanding ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
//    private EditText mp3song_search;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private int start = 0;
    private ArrayList<MP3HindiAlbumListPojo> arrdata;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 5, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0;

    private String str_alphbet = "";

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;

    private String asyncTaskUrl;
//    private boolean flag_editText = false;
//    private TextView no_result_found;
    private String search_word = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_mp3_songs, container, false);

        ctx = (BuyAnyAlbumHindiLanding) getActivity();
        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(getActivity());

        mRecyclerView = (RecyclerView) view.findViewById(R.id.mp3songs_recycler_view);
//        mp3song_search = (EditText) view.findViewById(R.id.search_song);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
//        no_result_found = (TextView) view.findViewById(R.id.no_result_found);

         /* ****scrollable alphabet list code as per change request**** */
        rowTextView = (TextView) view.findViewById(R.id.row_text_view);
        alphabetslist = (ListView) view.findViewById(R.id.mp3_landing_list);

        if (getActivity().getIntent().getStringExtra("notification_url") != null) {
            asyncTaskUrl = getActivity().getIntent().getStringExtra("notification_url");
        } else {
            asyncTaskUrl = getActivity().getIntent().getStringExtra("url");
        }

        if (arrdata == null)
            arrdata = new ArrayList<>();

        /* ****scrollable alphabet list code as per change request start**** */
        final AlphabetListAdapter alpha_adapter = new AlphabetListAdapter(getActivity(), SaregamaConstants.items);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                str_alphbet =  ctx.convertAlphabet(SaregamaConstants.items[position].toLowerCase());
                arrdata.clear();
                alpha_adapter.setSelectedIndex(position);
                new GetSongsList().execute();

                ctx.setBubblePosition(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        if (!fragmentResume && fragmentVisible && arrdata != null && arrdata.size() == 0) { //only when first time fragment is created
            new GetSongsList().execute();
        }

//        mp3song_search.setHint("Search Albums");
//        mp3song_search.setVisibility(View.VISIBLE);
//        ctx.softKeyboardDoneClickListener(mp3song_search);
//
//        mp3song_search.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int starts, int before, int count) {
//                if (mp3song_search.getText().toString().trim().length() == 0 && flag_editText) {
//                    flag_editText = false;
//                    start = 0;
//                    previousTotal = 0;
//                    arrdata.clear();
//                    search_word = "";
//                    new GetSongsList().execute();
//                } else {
//                    if (mp3song_search.getText().toString().trim().length() > 2) {
//                        flag_editText = true;
//                        start = 0;
//                        previousTotal = 0;
//                        arrdata.clear();
//                        search_word = mp3song_search.getText().toString().trim();
//                        new GetSearchSongsList().execute(mp3song_search.getText().toString().trim().replaceAll(" ", "%20"));
//                    }
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

        return view;
    }

    /* **** scrollable alphabet list code as per change request comment previous code**** */
    /*public void showListBubble(View v) {
        ctx.hideSoftKeyboard();
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        arrdata.clear();
        str_alphbet = ctx.showListBubbleCommon(v, rowTextView);
        new GetSongsList().execute();
    }
*/
    private void setAdapter(ArrayList<MP3HindiAlbumListPojo> arr) {
        mAdapter = new Mp3HindiAlbumAdapter(ctx, arr, "buyanyalbum", search_word);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (ctx != null)
            ctx.hideSoftKeyboard();
        if (isVisibleToUser && isResumed() && arrdata != null && arrdata.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            new GetSongsList().execute();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
//                        if (mp3song_search.getText().toString().trim().length() > 2) {
//                            new GetSearchSongsList().execute(mp3song_search.getText().toString().trim().replaceAll(" ", "%20"));
//                        } else {
                            new GetSongsList().execute();
//                        }
                        loading = true;
                    }
                }
            }
        });
    }

    // Album search list
    private class GetSearchSongsList extends AsyncTask<String,Void,Void> {
        private Mp3HindiAlbumPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... querry) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL+"inner_search?type=search"+ "&ctype=" + ctx.getAppConfigJson().getC_type().getALBUM() + "&query=" + querry[0] + "&mode=" + "hindi_films"+ "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), Mp3HindiAlbumPojo.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if(basePojo.getData().getList() != null) {
                            if (arrdata.size() > 0 && start > 0) {
                                arrdata.addAll(basePojo.getData().getList());
                            } else {
                                arrdata.clear();
                                arrdata = basePojo.getData().getList();
                                setAdapter(arrdata);
                                previousTotal = 0;
                            }
                        }
//                        no_result_found.setVisibility(View.GONE);
                        if (basePojo.getStatus()) {
                            mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), arrdata.size() - mAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);

//                        if (arrdata.size() == 0) {
//                            no_result_found.setText(SaregamaConstants.NO_RESULT);
//                            no_result_found.setVisibility(View.VISIBLE);
//                        } else {
//                            no_result_found.setVisibility(View.GONE);
//                        }
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private Mp3HindiAlbumPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                //dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(asyncTaskUrl + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&alpha=" + str_alphbet), Mp3HindiAlbumPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rowTextView.setVisibility(View.GONE);
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
//                    no_result_found.setVisibility(View.GONE);
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
//                            mp3song_search.setText("");
                            if (arrdata.size() > 0 && start > 0) {
                                arrdata.addAll(basePojo.getData().getList());
                            } else {
                                arrdata.clear();
                                arrdata = basePojo.getData().getList();
                                setAdapter(arrdata);
                                previousTotal = 0;
                            }
                        }
                        if (basePojo.getStatus()) {
                            mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), arrdata.size() - mAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }
}
