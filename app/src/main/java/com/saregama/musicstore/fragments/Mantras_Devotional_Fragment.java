package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.Mp3Devotional;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.Mp3HindiSongAdapter;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

/**
 * Created by yesh on 3/14/2016.
 */
public class Mantras_Devotional_Fragment extends Fragment {

    private Mp3Devotional ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0, start = 0;

    private String str_alphbet = "";

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;
//    private LinearLayout alphabets_list;
    private String search_word = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_mp3_songs, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.mp3songs_recycler_view);
        /* ****scrollable alphabet list code as per change request**** */
//        alphabets_list = (LinearLayout) view.findViewById(R.id.alphabets_list);
        rowTextView = (TextView) view.findViewById(R.id.row_text_view);
        alphabetslist = (ListView)view.findViewById(R.id.mp3_landing_list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(getActivity());


        dialog = new SaregamaDialogs(getActivity());

        ctx = (Mp3Devotional) getActivity();

        /* ****scrollable alphabet list code as per change request start**** */
        final AlphabetListAdapter alpha_adapter = new AlphabetListAdapter(getActivity(), SaregamaConstants.items);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                str_alphbet =  ctx.convertAlphabet(SaregamaConstants.items[position].toLowerCase());
                ctx.arrdata_mantras.clear();
                alpha_adapter.setSelectedIndex(position);
                start = 0;
                new GetSongsList().execute();

                ctx.setBubblePosition(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search")) {
            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
        } else {
            if (!fragmentResume && fragmentVisible && ctx.arrdata_mantras != null && ctx.arrdata_mantras.size() == 0) {   //only when first time fragment is created
                new GetSongsList().execute();
            }
        }

        return view;
    }

    /* **** scrollable alphabet list code as per change request comment previous code**** */
   /* public void showListBubble(View v) {
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        ctx.arrdata_mantras.clear();
        str_alphbet = showListBubbleCommon(v, rowTextView);
        new GetSongsList().execute();
    }*/

    // abcd click listener
    /*public String showListBubbleCommon(View v, TextView rowTextView) {
        //find the index of the separator row view
        LinearLayout alpha_layout = (LinearLayout) view.findViewById(R.id.alpha_layout);
        rowTextView = (TextView) view.findViewById(R.id.row_text_view);
        int count = alpha_layout.getChildCount();
        String alphabet = "";
        for (int i = 0; i < count; i++) {
            View inner_layout = alpha_layout.getChildAt(i);
            View view = ((ViewGroup) inner_layout).getChildAt(0);

            if (view.isPressed()) {
                rowTextView.setText(view.getTag() + "");
                alphabet = (String) view.getTag();
                rowTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.alphabetic_text_size));
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(ctx, R.drawable.pressed));
                view.setBackgroundResource(R.drawable.pressed);
            } else {
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(ctx, R.drawable.normal));
                view.setBackgroundResource(R.drawable.normal);
            }
        }
        if (alphabet.equals("#"))
            alphabet = SaregamaConstants.SPECIAL;
        else if (alphabet.equalsIgnoreCase("ALL"))
            alphabet = SaregamaConstants.ALL;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, v.getTop() - 5, 0, 0);
        rowTextView.setLayoutParams(layoutParams);
        rowTextView.setPadding(0, 0, 6, 0);
        rowTextView.setVisibility(View.VISIBLE);
        return alphabet;
    }*/

    private void setAdapter(ArrayList<MP3HindiSongListPojo> arr) {
        ctx.adapter_mantras = new Mp3HindiSongAdapter(ctx, arr, "devotional", search_word);
        mRecyclerView.setAdapter(ctx.adapter_mantras);
        ctx.adapter_mantras.notifyDataSetChanged();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() && ctx.arrdata_mantras != null && ctx.arrdata_mantras.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search"))
                new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
            else
                new GetSongsList().execute();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        if (getActivity().getIntent().getStringExtra("search_text") != null && getActivity().getIntent().getStringExtra("search_text").trim().length() > 2) {
                            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
                        } else {
                            new GetSongsList().execute();
                        }
                        loading = true;
                    }
                }
            }
        });
    }

    // Mantras Devotional search list
    private class GetSearchSongsList extends AsyncTask<String, Void, Void> {
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* ****scrollable alphabet list code as per change request **** */
//            alphabets_list.setVisibility(View.GONE);
            alphabetslist.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(String... querry) {

            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getSONG() + "&query=" + querry[0] + "&mode=" + "devotional" + "&e_id=" + "5" + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    search_word = getActivity().getIntent().getStringExtra("search_text").trim();
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (ctx.arrdata_mantras.size() > 0 && start > 0) {
                                ctx.arrdata_mantras.addAll(basePojo.getData().getList());
                            } else {
                                ctx.arrdata_mantras.clear();
                                ctx.arrdata_mantras = basePojo.getData().getList();
                                setAdapter(ctx.arrdata_mantras);
                                previousTotal = 0;
                            }
                        }

                        if (basePojo.getStatus()) {
                            ctx.adapter_mantras.notifyItemRangeInserted(ctx.adapter_mantras.getItemCount(), ctx.arrdata_mantras.size() - ctx.adapter_mantras.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
//                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "song?d_type=" + ctx.getAppConfigJson().getD_type().getDEVOTIONAL() + "&e_id=" + ctx.getAppConfigJson().getDev_song().getMANTRAS() + "&c_type=" + getActivity().getIntent().getIntExtra("c_type", 1) + "&song_type=" + ctx.getAppConfigJson().getSong_type().getSONG() + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&alpha=" + str_alphbet), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rowTextView.setVisibility(View.GONE);
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (ctx.arrdata_mantras.size() > 0 && start > 0) {
                                ctx.arrdata_mantras.addAll(basePojo.getData().getList());
                            } else {
                                ctx.arrdata_mantras.clear();
                                ctx.arrdata_mantras = basePojo.getData().getList();
                                setAdapter(ctx.arrdata_mantras);
                                previousTotal = 0;
                            }
                        }

                        if (basePojo.getStatus()) {
                            ctx.adapter_mantras.notifyItemRangeInserted(ctx.adapter_mantras.getItemCount(), ctx.arrdata_mantras.size() - ctx.adapter_mantras.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }
}