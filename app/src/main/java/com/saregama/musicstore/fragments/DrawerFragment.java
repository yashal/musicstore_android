package com.saregama.musicstore.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.accountkit.AccountKit;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.AboutUsActivity;
import com.saregama.musicstore.activity.AvailOffersActivity;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.CartActivity;
import com.saregama.musicstore.activity.DownloadManagerActivity;
import com.saregama.musicstore.activity.GeneralActivity;
import com.saregama.musicstore.activity.HomeActivity;
import com.saregama.musicstore.activity.LoginActivity;
import com.saregama.musicstore.activity.NotificationsActivity;
import com.saregama.musicstore.activity.SettingsActivity;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.LogOutPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class DrawerFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<People.LoadPeopleResult> {

    public ActionBarDrawerToggle mDrawerToggle;
    private int selectedPos = -1;
    private GoogleApiClient mGoogleApiClient;
    private DrawerLayout drawerlayout;
    private TextView notification_title, download_title;
    private TextView logout_title, userName, myAcc, cart_count_drawer;
    private ImageView userImage;
    private SaregamaDialogs dialog;
    private ConnectionDetector cd;
    private LinearLayout offers, name_text_layout, dear_user_text_layout;

    public DrawerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AccountKit.initialize(getActivity());
        View v = inflater.inflate(R.layout.left_slide_list_layout, container, false);

        selectedPos = -1;

        dialog = new SaregamaDialogs(getActivity());
        cd = new ConnectionDetector(getActivity());

        dear_user_text_layout = (LinearLayout) v.findViewById(R.id.dear_user_text_layout);
        final LinearLayout home = (LinearLayout) v.findViewById(R.id.home_menu_option);
        final LinearLayout cart = (LinearLayout) v.findViewById(R.id.cart_menu_option);
        final LinearLayout downloads = (LinearLayout) v.findViewById(R.id.downloads_menu_option);
        offers = (LinearLayout) v.findViewById(R.id.offers_menu_option);
        name_text_layout = (LinearLayout) v.findViewById(R.id.name_text_layout);
        final LinearLayout settings = (LinearLayout) v.findViewById(R.id.settings_menu_option);
        final LinearLayout general = (LinearLayout) v.findViewById(R.id.aboutUs_menu_option);
        final LinearLayout contactUs = (LinearLayout) v.findViewById(R.id.contactUs_menu_option);
        final LinearLayout notifications = (LinearLayout) v.findViewById(R.id.notifications_menu_option);
        final LinearLayout logout = (LinearLayout) v.findViewById(R.id.logout_menu_option);

        ImageView home_icon = (ImageView) v.findViewById(R.id.home_icon);
        ImageView offers_icon = (ImageView) v.findViewById(R.id.offers_icon);
        ImageView cart_icon = (ImageView) v.findViewById(R.id.cart_icon);
        ImageView downloads_icon = (ImageView) v.findViewById(R.id.downloads_icon);
        ImageView notifications_icon = (ImageView) v.findViewById(R.id.notifications_icon);
        ImageView aboutUs_icon = (ImageView) v.findViewById(R.id.aboutUs_icon);
        ImageView contactUs_icon = (ImageView) v.findViewById(R.id.contactUs_icon);
        ImageView settings_icon = (ImageView) v.findViewById(R.id.settings_icon);
        ImageView logout_icon = (ImageView) v.findViewById(R.id.logout_icon);

        TextView home_title = (TextView) v.findViewById(R.id.home_title);
        TextView offers_title = (TextView) v.findViewById(R.id.offers_title);
        TextView cart_title = (TextView) v.findViewById(R.id.cart_title);
        notification_title = (TextView) v.findViewById(R.id.notification_title);
        download_title = (TextView) v.findViewById(R.id.download_title);
        TextView downloads_title = (TextView) v.findViewById(R.id.downloads_title);
        TextView notifications_title = (TextView) v.findViewById(R.id.notifications_title);
        TextView aboutUs_title = (TextView) v.findViewById(R.id.aboutUs_title);
        TextView contactUs_title = (TextView) v.findViewById(R.id.contactUs_title);
        TextView settings_title = (TextView) v.findViewById(R.id.settings_title);
        logout_title = (TextView) v.findViewById(R.id.logout_title);
        cart_count_drawer = (TextView) v.findViewById(R.id.cart_count_drawer);

        if (((BaseActivity) getActivity()).getAppConfigJson().getCountry_code() != null && ((BaseActivity) getActivity()).getAppConfigJson().getCountry_code().equalsIgnoreCase("IN"))
            offers.setVisibility(View.VISIBLE);
        else
            offers.setVisibility(View.GONE);

        setImage(home, home_title, home_icon, ResourcesCompat.getDrawable(getResources(), R.mipmap.home_h, null), ResourcesCompat.getDrawable(getResources(), R.mipmap.home, null), 0);
        setImage(offers, offers_title, offers_icon, ResourcesCompat.getDrawable(getResources(), R.mipmap.avail_offers_h, null), ResourcesCompat.getDrawable(getResources(), R.mipmap.avail_offers, null), 1);
        setImage(cart, cart_title, cart_icon, ResourcesCompat.getDrawable(getResources(), R.mipmap.music_cart_h, null), ResourcesCompat.getDrawable(getResources(), R.mipmap.music_cart, null), 2);
        setImage(downloads, downloads_title, downloads_icon, ResourcesCompat.getDrawable(getResources(), R.mipmap.downloads_h, null), ResourcesCompat.getDrawable(getResources(), R.mipmap.downloads, null), 3);
        setImage(notifications, notifications_title, notifications_icon, ResourcesCompat.getDrawable(getResources(), R.mipmap.notifications_h, null), ResourcesCompat.getDrawable(getResources(), R.mipmap.notifications, null), 4);
        setImage(general, aboutUs_title, aboutUs_icon, ResourcesCompat.getDrawable(getResources(), R.mipmap.about_us_h, null), ResourcesCompat.getDrawable(getResources(), R.mipmap.about_us, null), 5);
        setImage(contactUs, contactUs_title, contactUs_icon, ResourcesCompat.getDrawable(getResources(), R.mipmap.phone_h, null), ResourcesCompat.getDrawable(getResources(), R.mipmap.phone, null), 6);
        setImage(settings, settings_title, settings_icon, ResourcesCompat.getDrawable(getResources(), R.mipmap.settings_h, null), ResourcesCompat.getDrawable(getResources(), R.mipmap.settings, null), 7);
        setImage(logout, logout_title, logout_icon, ResourcesCompat.getDrawable(getResources(), R.mipmap.signout_h, null), ResourcesCompat.getDrawable(getResources(), R.mipmap.signout, null), 8);

        userName = (TextView) v.findViewById(R.id.home_menu_UserName);
        myAcc = (TextView) v.findViewById(R.id.home_menu_MyAcc);
        userImage = (ImageView) v.findViewById(R.id.home_menu_UserImage);

        if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
            userName.setText(getFromPrefs(SaregamaConstants.NAME));
            myAcc.setText(getResources().getString(R.string.my_account));
            myAcc.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            layoutParams.gravity = Gravity.NO_GRAVITY;
            dear_user_text_layout.setLayoutParams(layoutParams);
            name_text_layout.setVisibility(View.VISIBLE);
            if (getFromPrefs(SaregamaConstants.USERIMAGE) != null && getFromPrefs(SaregamaConstants.USERIMAGE).length() > 0)
                ((BaseActivity) getActivity()).setProfileImageInLayout(getActivity(), (int) getResources().getDimension(R.dimen.image_width_drawer_fragment), (int) getResources().getDimension(R.dimen.image_width_drawer_fragment), getFromPrefs(SaregamaConstants.USERIMAGE), userImage);
            logout_title.setText(getResources().getString(R.string.signout));
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            layoutParams.gravity = Gravity.CENTER;
            dear_user_text_layout.setLayoutParams(layoutParams);
            userName.setText(getResources().getString(R.string.dear_user));
            myAcc.setText("");
            name_text_layout.setVisibility(View.GONE);
            myAcc.setVisibility(View.GONE);
            logout_title.setText(getResources().getString(R.string.signin));
        }

        if (((BaseActivity) getActivity()).getJsonObject() != null && ((BaseActivity) getActivity()).getJsonObject().size() > 0) {
            cart_count_drawer.setVisibility(View.VISIBLE);
            cart_count_drawer.setText(((BaseActivity) getActivity()).getJsonObject().size() + "");
        } else {
            cart_count_drawer.setVisibility(View.GONE);
        }

        if (((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE) > 0) {
            notification_title.setVisibility(View.VISIBLE);
            notification_title.setText(((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE) + "");
        } else {
            notification_title.setVisibility(View.GONE);
        }

        if (((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.DOWNLOAD_SIZE) > 0) {
            download_title.setVisibility(View.VISIBLE);
            download_title.setText(((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.DOWNLOAD_SIZE) + "");
        } else {
            download_title.setVisibility(View.GONE);
        }

        dear_user_text_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPos = 7;
                drawerlayout.closeDrawers();
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        mGoogleApiClient.connect();

        return v;
    }

    private void setImage(final LinearLayout layout, final TextView text, final ImageView image, final Drawable drawableSelected, final Drawable drawable, final int pos) {
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        image.setImageDrawable(drawableSelected);
                        layout.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.lhs_selected));
                        text.setTextColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.white));
                        break;
                    case MotionEvent.ACTION_UP:
                        image.setImageDrawable(drawable);
                        selectedPos = pos;
                        drawerlayout.closeDrawers();
                        layout.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.white));
                        text.setTextColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.black));
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        selectedPos = -1;
                        layout.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.white));
                        text.setTextColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.black));
                        image.setImageDrawable(drawable);
                        break;
                }
                return true;
            }
        });
    }

    private void googlePlusLogout() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
            // updateUI(false);
        }
    }

    @SuppressLint("NewApi")
    public void setUp(final DrawerLayout drawerlayout) {

        this.drawerlayout = drawerlayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerlayout, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                ((BaseActivity) getActivity()).hideSoftKeyboard();
                if (getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
                    userName.setText(getFromPrefs(SaregamaConstants.NAME));
                    myAcc.setText(getResources().getString(R.string.my_account));
                    myAcc.setVisibility(View.VISIBLE);
                    name_text_layout.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
                    layoutParams.gravity = Gravity.NO_GRAVITY;
                    dear_user_text_layout.setLayoutParams(layoutParams);
                    if (getFromPrefs(SaregamaConstants.USERIMAGE) != null && getFromPrefs(SaregamaConstants.USERIMAGE).length() > 0)
                        ((BaseActivity) getActivity()).setProfileImageInLayout(getActivity(), (int) getResources().getDimension(R.dimen.image_width_drawer_fragment), (int) getResources().getDimension(R.dimen.image_width_drawer_fragment), getFromPrefs(SaregamaConstants.USERIMAGE), userImage);
                    logout_title.setText(getResources().getString(R.string.signout));
                } else {
                    userName.setText(getResources().getString(R.string.dear_user));
                    myAcc.setText("");
                    myAcc.setVisibility(View.GONE);
                    name_text_layout.setVisibility(View.GONE);
                    logout_title.setText(getResources().getString(R.string.signin));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
                    layoutParams.gravity = Gravity.CENTER;
                    dear_user_text_layout.setLayoutParams(layoutParams);
                    userImage.setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.mipmap.fallback_image));
                }

                if (((BaseActivity) getActivity()).getJsonObject() != null && ((BaseActivity) getActivity()).getJsonObject().size() > 0) {
                    cart_count_drawer.setVisibility(View.VISIBLE);
                    cart_count_drawer.setText(((BaseActivity) getActivity()).getJsonObject().size() + "");
                } else {
                    cart_count_drawer.setVisibility(View.GONE);
                }
                if (((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE) > 0) {
                    notification_title.setVisibility(View.VISIBLE);
                    notification_title.setText(((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE) + "");
                } else {
                    notification_title.setVisibility(View.GONE);
                }

                if (((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.DOWNLOAD_SIZE) > 0) {
                    download_title.setVisibility(View.VISIBLE);
                    download_title.setText(((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.DOWNLOAD_SIZE) + "");
                } else {
                    download_title.setVisibility(View.GONE);
                }
                getActivity().invalidateOptionsMenu();
            }

            @SuppressLint("NewApi")
            @Override
            public void onDrawerClosed(View drawerView) {
                ActivityManager am = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                super.onDrawerClosed(drawerView);
                if (selectedPos != -1) {
                    switch (selectedPos) {
                        case 0:
                            if (!cn.getShortClassName().equals(".activity.HomeActivity")) {
                                navigateToHome();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 1:
                            if (!cn.getShortClassName().equals(".activity.AvailOffersActivity")) {
                                navigateToOffers();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 2:
                            if (!cn.getShortClassName().equals(".activity.CartActivity")) {
                                navigateToCart();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 3:
                            if (!cn.getShortClassName().equals(".activity.DownloadManagerActivity")) {
                                navigateToDownloads();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 4:
                            if (!cn.getShortClassName().equals(".activity.NotificationsActivity")) {
                                navigateToNotifications();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 5:
                            if (!cn.getShortClassName().equals(".activity.GeneralActivity")) {
                                navigateToGeneralScreen();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 6:
                            navigateToContactUs();
                            break;
                        case 7:
                            if (!cn.getShortClassName().equals(".activity.SettingsActivity")) {
                                navigateToSettings();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;
                        case 8:
                            logout();
                            break;
                        default:
                            break;
                    }
                    selectedPos = -1;
                }
            }
        };

        drawerlayout.setDrawerListener(mDrawerToggle);

    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getActivity().getSharedPreferences(SaregamaConstants.PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, SaregamaConstants.DEFAULT_VALUE);
    }

    private void navigateToHome() {
        drawerlayout.closeDrawers();
        for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
            if (SaregamaConstants.ACTIVITIES.get(i) != null)
                SaregamaConstants.ACTIVITIES.get(i).finish();
        }
        getActivity().finish();
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private void navigateToGeneralScreen() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".GeneralActivity");
        Intent intent = new Intent(getActivity(), GeneralActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private void navigateToCart() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".CartActivity");
        Intent intent = new Intent(getActivity(), CartActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private void navigateToNotifications() {
        drawerlayout.closeDrawers();
//        if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
        removeActivity(getResources().getString(R.string.package_name) + ".NotificationsActivity");
        Intent intent = new Intent(getActivity(), NotificationsActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
//        }
    }

    private void navigateToContactUs() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".AboutUsActivity");
        Intent intent = new Intent(getActivity(), AboutUsActivity.class);
        intent.putExtra("url", ((BaseActivity) getActivity()).getAppConfigJson().getContact_us());
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private void navigateToOffers() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".AvailOffersActivity");
        Intent intent = new Intent(getActivity(), AvailOffersActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private void navigateToDownloads() {
        drawerlayout.closeDrawers();
        if (getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
            if (((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.DOWNLOAD_SIZE) > 0 || ((BaseActivity) getActivity()).getIntFromPrefs(SaregamaConstants.DOWNLOADED_SIZE) > 0) {
                removeActivity(getResources().getString(R.string.package_name) + ".DownloadManagerActivity");
                Intent intent = new Intent(getActivity(), DownloadManagerActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            } else {
                dialog.displayCommonDialogWithHeader("Download Manager",(((BaseActivity) getActivity()).getAppConfigJson().getDownload_manager()));
            }
        } else {
            ((BaseActivity) getActivity()).loginRequiredAlert(((BaseActivity) getActivity()).getAppConfigJson().getSign_required_download());
        }
    }

    private void navigateToSettings() {
        drawerlayout.closeDrawers();
        if (getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
            removeActivity(getResources().getString(R.string.package_name) + ".SettingsActivity");
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        } else {
            ((BaseActivity) getActivity()).loginRequiredAlert(((BaseActivity) getActivity()).getAppConfigJson().getSignin_required());
        }
    }

    private void removeActivity(String activity) {
        for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
            if (SaregamaConstants.ACTIVITIES.get(i) != null && SaregamaConstants.ACTIVITIES.get(i).toString().contains(activity)) {
                SaregamaConstants.ACTIVITIES.get(i).finish();
                SaregamaConstants.ACTIVITIES.remove(i);
                break;
            }
        }
    }

    private void logout() {
        drawerlayout.closeDrawers();
        Button OkButtonLogout;
        final Dialog dialogLogout = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        if (getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
            if (!dialogLogout.isShowing()) {
                TextView msg_textView = (TextView) dialogLogout.findViewById(R.id.text_exit);
                TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
                dialog_header.setVisibility(View.VISIBLE);
                msg_textView.setText(((BaseActivity) getActivity()).getAppConfigJson().getSignout_popup());
                dialog_header.setText("Sign Out");
                OkButtonLogout = (Button) dialogLogout.findViewById(R.id.btn_yes_exit);
                OkButtonLogout.setText("YES");
                Button CancelButtonLogout = (Button) dialogLogout.findViewById(R.id.btn_no_exit);
                CancelButtonLogout.setText("NO");
                ImageView dialog_header_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
                dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogLogout.dismiss();
                    }
                });

                OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (cd.isConnectingToInternet()) {
                            dialogLogout.dismiss();
                            notification_title.setVisibility(View.GONE);

//                            ActivityManager am = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
//                            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

                            ((BaseActivity) getActivity()).saveIntoPrefs(SaregamaConstants.REFRESH_NEEDED, "yes");
                            ((BaseActivity) getActivity()).showCartCounter("logout");

//                            if (cn.getClassName().equals(getResources().getString(R.string.package_name) + ".SettingsActivity"))
//                                getActivity().finish();
//
//                            if (cn.getClassName().equals(getResources().getString(R.string.package_name) + ".DownloadManagerActivity"))
//                                getActivity().finish();

                            if (getFromPrefs(SaregamaConstants.LOGGEDINFROM).equals("gmail"))
                                googlePlusLogout();
                            else if (getFromPrefs(SaregamaConstants.LOGGEDINFROM).equals("fb")) {
                                FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
                                LoginManager.getInstance().logOut();
                            } else if (getFromPrefs(SaregamaConstants.LOGGEDINFROM).equals("fbkit")) {
                                AccountKit.logOut();
                            }
                            userName.setText(getResources().getString(R.string.dear_user));
                            myAcc.setText("");
                            myAcc.setVisibility(View.GONE);
                            logout_title.setText(getResources().getString(R.string.signin));
                            userImage.setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.mipmap.fallback_image));

                            for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
                                if (SaregamaConstants.ACTIVITIES.get(i) != null)
                                    SaregamaConstants.ACTIVITIES.get(i).finish();
                            }
                            // logout api call

                            RestClient.get().logout(getFromPrefs(SaregamaConstants.SESSION_ID), "logout", getFromPrefs(SaregamaConstants.ID), new Callback<LogOutPojo>() {
                                @Override
                                public void success(LogOutPojo basepojo, Response response) {

                                    if (basepojo != null) {
                                        if (basepojo.getStatus()) {
                                            String logout = basepojo.getData().getListener().getLogout();
                                        }
                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {

                                }
                            });

                            Intent intent = new Intent(getActivity(), HomeActivity.class);
                            startActivity(intent);

                            ((BaseActivity) getActivity()).saveIntoPrefs(SaregamaConstants.SESSION_ID, "");
                            ((BaseActivity) getActivity()).saveIntoPrefs(SaregamaConstants.ID, "");
                            ((BaseActivity) getActivity()).saveIntoPrefs(SaregamaConstants.USERIMAGE, "");
                            ((BaseActivity) getActivity()).saveIntoPrefs(SaregamaConstants.COUPON_CODE, "");
                            ((BaseActivity) getActivity()).saveIntIntoPrefs(SaregamaConstants.NOTIFICATION_SIZE, 0);
                            ((BaseActivity) getActivity()).saveIntIntoPrefs(SaregamaConstants.DOWNLOAD_SIZE, 0);
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, ((BaseActivity) getActivity()).getAppConfigJson().getInternat_fail() + "\n");
                        }
                    }
                });

                CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialogLogout.dismiss();
                    }
                });

                dialogLogout.show();
            }
        } else {
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(
                this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull People.LoadPeopleResult loadPeopleResult) {

    }
}
