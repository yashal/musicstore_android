package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.DownloadManagerActivity;
import com.saregama.musicstore.adapter.DownloadAlbumAdapter;
import com.saregama.musicstore.adapter.DownloadSongAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.DownloadManagerPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;
import com.saregama.musicstore.views.CustomTextViewBold;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class DownloadedNowFragment extends Fragment {

    private DownloadManagerActivity ctx;
    private View view;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private CustomTextViewBold album_count_download;
    private LinearLayout album_layout_download;
    private RecyclerView album_list_download;
    private RecyclerView songs_list_download;
    private int start = 0;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private int totalItemCount_album, firstVisibleItemPosition_album = 0, visibleThreshold_album = 1, visibleItemCount_album;

    private boolean loading = true;
    private boolean loading_album = true;
    private int previousTotal = 0;
    private int previousTotal_album = 0;

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.LayoutManager mLayoutManager_album;

    private RecyclerView.Adapter mAdapter_album;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_download_now, container, false);

        ctx = (DownloadManagerActivity) getActivity();

        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);

        album_count_download = (CustomTextViewBold) view.findViewById(R.id.album_count_download);
        ctx.songs_count_download = (CustomTextViewBold) view.findViewById(R.id.songs_count_download);
        album_layout_download = (LinearLayout) view.findViewById(R.id.album_layout_download);
        ctx.songs_layout_download = (LinearLayout) view.findViewById(R.id.songs_layout_download);
        album_list_download = (RecyclerView) view.findViewById(R.id.album_list_download);
        album_list_download.setHasFixedSize(true);
        mLayoutManager_album = new LinearLayoutManager(getActivity());
        album_list_download.setLayoutManager(mLayoutManager_album);

        songs_list_download = (RecyclerView) view.findViewById(R.id.songs_list_download);
        songs_list_download.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        songs_list_download.setLayoutManager(mLayoutManager);

        if (!fragmentResume && fragmentVisible) {
            getDownloadData();
        }

        return view;
    }

    private void getDownloadData() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().downloadAllItems(ctx.getFromPrefs(SaregamaConstants.SESSION_ID), SaregamaConstants.TAB1, "" + start, "" + SaregamaConstants.LIMIT, new Callback<DownloadManagerPojo>() {
                @Override
                public void success(DownloadManagerPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus() && basePojo.getData() != null) {
                            ctx.checkCommonURL(response.getUrl());
                            if (basePojo.getData().getList() != null) {
                                if(start == 0){
                                    ctx.songListDownloadNow = new ArrayList<>();
                                    ctx.albumList = new ArrayList<>();
                                }
                                if (basePojo.getData().getList().getSongs() != null) {
                                    ctx.songs_count = basePojo.getData().getSongCount();
                                    if (ctx.songListDownloadNow != null && ctx.songListDownloadNow.size() > 0) {
                                        ctx.songListDownloadNow.addAll(basePojo.getData().getList().getSongs());
                                    } else {
                                        ctx.songListDownloadNow = basePojo.getData().getList().getSongs();
                                        previousTotal = 0;
                                    }
                                }

                                if (ctx.songs_count > 0 && ctx.songListDownloadNow != null && ctx.songListDownloadNow.size() > 0) {
                                    ctx.songs_layout_download.setVisibility(View.VISIBLE);
                                    ctx.mAdapterDownloadNow = new DownloadSongAdapter(ctx, ctx.songListDownloadNow, "download_now");
                                    songs_list_download.setAdapter(ctx.mAdapterDownloadNow);
                                    ctx.mAdapterDownloadNow.notifyDataSetChanged();
                                    songs_list_download.scrollToPosition(firstVisibleItemPosition);
                                    addScrollListener();
                                }

//                              for album listing

                                if (basePojo.getData().getList().getAlbums() != null) {
                                    ctx.album_count = basePojo.getData().getAlbumCount();
                                    if (ctx.albumList != null && ctx.albumList.size() > 0) {
                                        ctx.albumList.addAll(basePojo.getData().getList().getAlbums());
                                    } else {
                                        ctx.albumList = basePojo.getData().getList().getAlbums();
                                        previousTotal_album = 0;
                                    }
                                }

                                if (ctx.album_count > 0 && ctx.albumList != null && ctx.albumList.size() > 0) {
                                    album_layout_download.setVisibility(View.VISIBLE);
                                    mAdapter_album = new DownloadAlbumAdapter(ctx, ctx.albumList, "download_now");
                                    album_list_download.setAdapter(mAdapter_album);
                                    mAdapter_album.notifyDataSetChanged();
                                    album_list_download.scrollToPosition(firstVisibleItemPosition_album);
                                    addScrollListenerAlbum();
                                }
                            }
                            album_count_download.setText(ctx.album_count + "");
                            ctx.songs_count_download.setText(ctx.songs_count + "");

                            ctx.saveIntIntoPrefs(SaregamaConstants.DOWNLOAD_SIZE, ctx.album_count + ctx.songs_count);
                        } else {
//                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, ctx.getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            start = 0;
            ctx.albumList = new ArrayList<>();
            ctx.albumList.clear();
            ctx.songListDownloadNow = new ArrayList<>();
            ctx.songListDownloadNow.clear();
            getDownloadData();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void addScrollListener() {
        songs_list_download.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        getDownloadData();
                        loading = true;
                    }
                }
            }
        });
    }

    private void addScrollListenerAlbum() {
        album_list_download.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount_album = recyclerView.getChildCount();
                totalItemCount_album = layoutManager.getItemCount();
                firstVisibleItemPosition_album = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading_album) {
                        if (totalItemCount_album > previousTotal_album) {
                            loading_album = false;
                            previousTotal_album = totalItemCount_album;
                        }
                    }

                    if (!loading_album && (totalItemCount_album - visibleItemCount_album)
                            <= (firstVisibleItemPosition_album + visibleThreshold_album)) {

                        start += SaregamaConstants.LIMIT;
                        getDownloadData();
                        loading_album = true;
                    }
                }
            }
        });
    }
}
