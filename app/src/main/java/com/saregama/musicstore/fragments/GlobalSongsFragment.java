package com.saregama.musicstore.fragments;




import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.GlobalSearchTabListActivity;
import com.saregama.musicstore.adapter.GlobalSongsAdapter;
import com.saregama.musicstore.pojo.GlobalSongspojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class GlobalSongsFragment extends Fragment {

    private ConnectionDetector cd;
    private SaregamaDialogs dialog;

    private View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;

    private boolean loading = true;

    private int previousTotal = 0, start = 0;

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    private GlobalSearchTabListActivity ctx;
    private String search_txt;

    public GlobalSongsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_global_songs, container, false);

        cd = new ConnectionDetector(getActivity());
        dialog = new SaregamaDialogs(getActivity());

        ctx = (GlobalSearchTabListActivity) getActivity();

        search_txt = ctx.getIntent().getStringExtra("search_text");
        search_txt = search_txt.replaceAll(" ", "%20");

        mRecyclerView = (RecyclerView) view.findViewById(R.id.global_songs_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (ctx.arrdata == null)
            ctx.arrdata = new ArrayList<>();

        new GetGlobalSongslist().execute();

        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() && ctx.arrdata != null && ctx.arrdata.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            new GetGlobalSongslist().execute();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        new GetGlobalSongslist().execute();
                        loading = true;
                    }
                }
            }
        });
    }

    private class GetGlobalSongslist extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private GlobalSongspojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, ctx.getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "search?" + "type=" + "search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getSONG() + "&query=" + search_txt + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), GlobalSongspojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (d != null && d.isShowing()) {
                d.dismiss();
            }

            if (basePojo != null) {
                if (basePojo.getData().getCount() > 0) {
                    if (basePojo.getData().getList() != null) {
                        if (ctx.arrdata.size() > 0) {
                            ctx.arrdata.addAll(basePojo.getData().getList());
                        } else {
                            ctx.arrdata.clear();
                            ctx.arrdata = basePojo.getData().getList();
                            ctx.globalSearchSongsAdapter = new GlobalSongsAdapter(ctx, ctx.arrdata, search_txt);
                            mRecyclerView.setAdapter(ctx.globalSearchSongsAdapter);
                            previousTotal = 0;
                        }
                    }

                    if (basePojo.getStatus()) {
                        ctx.globalSearchSongsAdapter.notifyItemRangeInserted(ctx.globalSearchSongsAdapter.getItemCount(), ctx.arrdata.size() - ctx.globalSearchSongsAdapter.getItemCount());
                        addScrollListner();
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    start = 0;
                    firstVisibleItemPosition = 0;
                    mRecyclerView.clearOnScrollListeners();
                    mRecyclerView.scrollToPosition(0);
                    Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                }

            } else {
                dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
            }
        }
    }
}
