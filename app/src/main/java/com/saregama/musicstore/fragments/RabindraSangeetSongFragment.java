package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.RabindraSangeetActivity;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.RabindraSangeetSongAdapter;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

public class RabindraSangeetSongFragment extends Fragment {

    private RabindraSangeetActivity ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0, start = 0;


    private String str_alphbet = "";

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    private String asyncTaskUrl;
    private String d_type;
    private String mode;
    //    private LinearLayout alphabets_list;
     /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;

    private String search_word = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_mp3_songs, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.mp3songs_recycler_view);

        /* ****scrollable alphabet list code as per change request**** */
//        alphabets_list = (LinearLayout) view.findViewById(R.id.alphabets_list);
        rowTextView = (TextView) view.findViewById(R.id.row_text_view);
        alphabetslist = (ListView)view.findViewById(R.id.mp3_landing_list);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(getActivity());

        dialog = new SaregamaDialogs(getActivity());

        ctx = (RabindraSangeetActivity) getActivity();

        if (getActivity().getIntent().getStringExtra("from") != null) {
            if (getActivity().getIntent().getStringExtra("from").equalsIgnoreCase("Rabindra Sangeet")) {
                mode = "rabindra_sangeet";
                d_type = ((RabindraSangeetActivity) getActivity()).getAppConfigJson().getD_type().getRABINDRA_SANGEET() + "";
            } else {
                mode = "nazrul_geet";
                d_type = ((RabindraSangeetActivity) getActivity()).getAppConfigJson().getD_type().getNAZRUL_GEET() + "";
            }
        }

        cd = new ConnectionDetector(getActivity());
        dialog = new SaregamaDialogs(getActivity());

        if (getActivity().getIntent().getStringExtra("notification_url") != null) {
            asyncTaskUrl = getActivity().getIntent().getStringExtra("notification_url");
        } else {
            asyncTaskUrl = SaregamaConstants.BASE_URL + "song?d_type=" + d_type + "&c_type=" + getActivity().getIntent().getIntExtra("c_type", 1) + "&song_type=" + ctx.getAppConfigJson().getSong_type().getSONG();
        }

        if (ctx.arrdata == null)
            ctx.arrdata = new ArrayList<>();

        /* ****scrollable alphabet list code as per change request start**** */
        final AlphabetListAdapter alpha_adapter = new AlphabetListAdapter(getActivity(), SaregamaConstants.items);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                str_alphbet =  ctx.convertAlphabet(SaregamaConstants.items[position].toLowerCase());
                ctx.arrdata.clear();
                alpha_adapter.setSelectedIndex(position);
                start = 0;
                new GetSongsList().execute();

                ctx.setBubblePosition(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search")) {
            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
        } else {
            if (!fragmentResume && fragmentVisible && ctx.arrdata != null && ctx.arrdata.size() == 0) { //only when first time fragment is created
                new GetSongsList().execute();
            }
        }

        return view;
    }

    /* **** scrollable alphabet list code as per change request comment previous code**** */
   /* public void showListBubble(View v) {
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        ctx.arrdata.clear();
        str_alphbet = ctx.showListBubbleCommon(v, rowTextView);
        new GetSongsList().execute();
    }*/

    private void setAdapter(ArrayList<MP3HindiSongListPojo> arr) {
        ctx.rabindra_sangeet_adapter = new RabindraSangeetSongAdapter(ctx, arr, "rabindra", search_word);
        mRecyclerView.setAdapter(ctx.rabindra_sangeet_adapter);
        ctx.rabindra_sangeet_adapter.notifyDataSetChanged();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (ctx != null)
            ctx.hideSoftKeyboard();
        if (isVisibleToUser && isResumed() && ctx.arrdata != null && ctx.arrdata.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search")) {
                new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
            } else {
                new GetSongsList().execute();
            }
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        if (getActivity().getIntent().getStringExtra("search_text") != null && getActivity().getIntent().getStringExtra("search_text").trim().length() > 2) {
                            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
                        } else {
                            new GetSongsList().execute();
                        }
                        loading = true;
                    }
                }
            }
        });
    }

    // Rabindra Sangeet Song list

    private class GetSearchSongsList extends AsyncTask<String, Void, Void> {
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
               /* ****scrollable alphabet list code as per change request **** */
//            alphabets_list.setVisibility(View.GONE);
            alphabetslist.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(String... querry) {

            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getSONG() + "&query=" + querry[0] + "&mode=" + mode + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (basePojo != null) {
                    search_word = getActivity().getIntent().getStringExtra("search_text").trim();
                    if (basePojo.getData().getList() != null) {
                        if (ctx.arrdata.size() > 0 && start > 0) {
                            ctx.arrdata.addAll(basePojo.getData().getList());
                        } else {
                            ctx.arrdata.clear();
                            ctx.arrdata = basePojo.getData().getList();
                            setAdapter(ctx.arrdata);
                            previousTotal = 0;
                        }
                    }
                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {
                            ctx.rabindra_sangeet_adapter.notifyItemRangeInserted(ctx.rabindra_sangeet_adapter.getItemCount(), ctx.arrdata.size() - ctx.rabindra_sangeet_adapter.getItemCount());
                            addScrollListner();
                        } else {
                            start = 0;
                            mRecyclerView.clearOnScrollListeners();
                            mRecyclerView.scrollToPosition(0);
                        }
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(asyncTaskUrl + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&alpha=" + str_alphbet), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getData().getList() != null) {
                        if (ctx.arrdata.size() > 0 && start > 0) {
                            ctx.arrdata.addAll(basePojo.getData().getList());
                        } else {
                            ctx.arrdata.clear();
                            ctx.arrdata = basePojo.getData().getList();
                            setAdapter(ctx.arrdata);
                            previousTotal = 0;
                        }
                    }

                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {
                            ctx.rabindra_sangeet_adapter.notifyItemRangeInserted(ctx.rabindra_sangeet_adapter.getItemCount(), ctx.arrdata.size() - ctx.rabindra_sangeet_adapter.getItemCount());
                            addScrollListner();
                        } else {
                            start = 0;
                            mRecyclerView.clearOnScrollListeners();
                            mRecyclerView.scrollToPosition(0);
                            Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }
}