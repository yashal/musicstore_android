package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.HomeActivity;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.HomeBannerPojo;
import com.saregama.musicstore.pojo.HomeDataPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Mp3Fragment extends Fragment {

    private HomeActivity ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
    private List<HomeBannerPojo> items;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.mp3_hd_layout, container, false);

        ctx = (HomeActivity) getActivity();

        cd = new ConnectionDetector(getActivity());
        dialog = new SaregamaDialogs(getActivity());

        items = new ArrayList<>();

            if (!fragmentResume && fragmentVisible) {   //only when first time fragment is created
                getMp3Data();
            }

        return view;
    }

    private void getMp3Data() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(getActivity());
            d.setCanceledOnTouchOutside(false);

            RestClient.get().GetHomeData(ctx.appConfigData.getSong_type().getSONG()+"", "1", new Callback<HomeDataPojo>() {
                @Override
                public void success(HomeDataPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            ctx.checkCommonURL(response.getUrl());
                            items = basePojo.getData();
                            ctx.displayData(view, basePojo.getData());
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, ctx.getAppConfigJson().getInternat_fail()+"\n");
        }
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);

        if (visible && isResumed() && items != null && items.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;

            getMp3Data();
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }
}
