package com.saregama.musicstore.fragments;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.Mp3RegionalActivity;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.RegionalMp3SongsListingAdapter;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;


public class RegionalMp3SongFragment extends Fragment {
    private Mp3RegionalActivity ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 5, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0, start = 0;

    private String str_alphbet = "";
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    /* ****scrollable alphabet list code as per change request**** */
    //    private LinearLayout alphabets_list;
    private TextView rowTextView;
    private ListView alphabetslist;
    private String asyncTaskUrl;

    private String search_word = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mp3_songs, container, false);
        ctx = (Mp3RegionalActivity) getActivity();

        cd = new ConnectionDetector(getActivity());
        dialog = new SaregamaDialogs(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.mp3songs_recycler_view);
        /* ****scrollable alphabet list code as per change request**** */
       // alphabets_list = (LinearLayout) view.findViewById(R.id.alphabets_list);
        rowTextView = (TextView) view.findViewById(R.id.row_text_view);
        alphabetslist = (ListView)view.findViewById(R.id.mp3_landing_list);


        if (getActivity().getIntent().getStringExtra("notification_url") != null) {
            asyncTaskUrl = getActivity().getIntent().getStringExtra("notification_url");
        } else {
            asyncTaskUrl = getActivity().getIntent().getStringExtra("url");
        }

        /* ****scrollable alphabet list code as per change request start**** */
        final AlphabetListAdapter alpha_adapter = new AlphabetListAdapter(getActivity(), SaregamaConstants.items);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                str_alphbet =  ctx.convertAlphabet(SaregamaConstants.items[position].toLowerCase());
                ctx.arrdata.clear();
                alpha_adapter.setSelectedIndex(position);
                start = 0;
                new GetRegionalSongs().execute();

                ctx.setBubblePosition(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search")) {
            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
        } else {
            if (!fragmentResume && fragmentVisible && ctx.arrdata != null && ctx.arrdata.size() == 0) { //only when first time fragment is created
                new GetRegionalSongs().execute();
            }
        }

        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

         /* **** scrollable alphabet list code as per change request comment previous code**** */

    /*public void showListBubble(View v) {
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        ctx.arrdata.clear();
        str_alphbet = ctx.showListBubbleCommon(v, rowTextView);
        new GetRegionalSongs().execute();
    }*/

    private void setAdapter(ArrayList<MP3HindiSongListPojo> arr) {
        ctx.regionalSongsFragmentAdapter = new RegionalMp3SongsListingAdapter(ctx, arr, "regional", search_word);
        recyclerView.setAdapter(ctx.regionalSongsFragmentAdapter);
        ctx.regionalSongsFragmentAdapter.notifyDataSetChanged();
    }

    private void addScrollListner() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        if (getActivity().getIntent().getStringExtra("search_text") != null && getActivity().getIntent().getStringExtra("search_text").trim().length() > 2) {
                            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim().replaceAll(" ", "%20"));
                        } else {
                            new GetRegionalSongs().execute();
                        }
                        loading = true;
                    }
                }
            }
        });
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() && ctx.arrdata != null && ctx.arrdata.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search")) {
                new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
            } else
                new GetRegionalSongs().execute();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    // Regional song list

    private class GetSearchSongsList extends AsyncTask<String, Void, Void> {
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
             /* ****scrollable alphabet list code as per change request **** */
//            alphabets_list.setVisibility(View.GONE);
            alphabetslist.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(String... querry) {

            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getSONG() + "&query=" + querry[0] + "&mode=" + "regional_films" + "&lang=" + getActivity().getIntent().getIntExtra("languageid", 0) + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    search_word = getActivity().getIntent().getStringExtra("search_text").trim();
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (ctx.arrdata.size() > 0 && start > 0) {
                                ctx.arrdata.addAll(basePojo.getData().getList());
                            } else {
                                ctx.arrdata.clear();
                                ctx.arrdata = basePojo.getData().getList();
                                setAdapter(ctx.arrdata);
                                previousTotal = 0;
                            }
                        }
                        if (basePojo.getStatus()) {
                            ctx.regionalSongsFragmentAdapter.notifyItemRangeInserted(ctx.regionalSongsFragmentAdapter.getItemCount(), ctx.arrdata.size() - ctx.regionalSongsFragmentAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        recyclerView.clearOnScrollListeners();
                        recyclerView.scrollToPosition(0);
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetRegionalSongs extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ctx.hideSoftKeyboard();
            if (cd.isConnectingToInternet()) {
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, ctx.getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(asyncTaskUrl + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&alpha=" + str_alphbet), MP3HindiSongPojo.class);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }
                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (ctx.arrdata.size() > 0 && start > 0)
                                ctx.arrdata.addAll(basePojo.getData().getList());
                            else {
                                ctx.arrdata.clear();
                                ctx.arrdata = basePojo.getData().getList();
                                setAdapter(ctx.arrdata);
                                previousTotal = 0;
                            }
                        }
                        if (basePojo.getStatus()) {
                            ctx.regionalSongsFragmentAdapter.notifyItemRangeInserted(ctx.regionalSongsFragmentAdapter.getItemCount(), ctx.arrdata.size() - ctx.regionalSongsFragmentAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        recyclerView.clearOnScrollListeners();
                        recyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }
}
