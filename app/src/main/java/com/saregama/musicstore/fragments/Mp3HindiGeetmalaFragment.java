package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.Mp3HindiLandingActivity;
import com.saregama.musicstore.adapter.Mp3HindiAlbumAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.LanguageBasePojo;
import com.saregama.musicstore.pojo.LanguageListPojo;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.pojo.Mp3HindiAlbumPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Mp3HindiGeetmalaFragment extends Fragment {

    private Mp3HindiLandingActivity ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
    public static View v;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private int start = 0;

    private ArrayList<MP3HindiAlbumListPojo> arrdata;
    private Mp3HindiAlbumAdapter adapter;
    private Typeface face_normal, face_bold;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0;

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    private String year_id = "";
    private String asyncTaskUrl;
    private TextView filterby_decades_text;

    private ListView decade_filter_list;

    private PopupWindow pwindo;
    private View layout;
    protected Drawable mBackground = null;
    private String selected_text;
    private String search_word = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.mp3_hindi_geetmala_fragment, container, false);
        ctx = (Mp3HindiLandingActivity) getActivity();

        face_normal = Typeface.createFromAsset(ctx.getAssets(), "fonts/SourceSansPro_Regular.otf");
        face_bold = Typeface.createFromAsset(ctx.getAssets(), "fonts/SourceSansPro_Semibold.otf");

        filterby_decades_text = (TextView) view.findViewById(R.id.filterby_decades_text);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.mp3songs_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        dialog = new SaregamaDialogs(getActivity());

        cd = new ConnectionDetector(getActivity());

        if (arrdata == null)
            arrdata = new ArrayList<>();

        if (getActivity().getIntent().getStringExtra("notification_url") != null) {
            asyncTaskUrl = getActivity().getIntent().getStringExtra("notification_url");
        } else {
            createAsyncURL(year_id);
        }

        if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search") && arrdata != null && arrdata.size() == 0) {
            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
            fetchGeetmalaList();
        } else {
            if (!fragmentResume && fragmentVisible && arrdata != null && arrdata.size() == 0) { //only when first time fragment is created
                new GetSongsList().execute();
                fetchGeetmalaList();
            }
        }

        filterby_decades_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiatePopupWindow();
            }
        });

        return view;
    }

    private void setAdapter(ArrayList<MP3HindiAlbumListPojo> arr) {
        adapter = new Mp3HindiAlbumAdapter(ctx, arr, "geetmala", search_word);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {
                        start += SaregamaConstants.LIMIT;
                        if (getActivity().getIntent().getStringExtra("search_text") != null && getActivity().getIntent().getStringExtra("search_text").trim().length() > 2) {
                            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim().replace(" ", "%20"));
                        } else {
                            new GetSongsList().execute();
                        }
                        loading = true;
                    }
                }
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() && arrdata != null && arrdata.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search") && arrdata != null && arrdata.size() == 0) {
                new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim().replace(" ", "%20"));
            } else {
                new GetSongsList().execute();
            }
            fetchGeetmalaList();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }

    }

    private class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private Mp3HindiAlbumPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, ctx.getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(asyncTaskUrl + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&c_type=" + getActivity().getIntent().getIntExtra("c_type", 1) + "&song_type=" + ctx.getAppConfigJson().getSong_type().getSONG()), Mp3HindiAlbumPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (cd.isConnectingToInternet()) {

                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (basePojo.getData().getList() != null) {
                                if (arrdata.size() > 0) {
                                    arrdata.addAll(basePojo.getData().getList());
                                } else {
                                    arrdata.clear();
                                    arrdata = basePojo.getData().getList();
                                    setAdapter(arrdata);
                                    previousTotal = 0;
                                }
                            }
                        }

                        if (basePojo.getStatus()) {
                            adapter.notifyItemRangeInserted(adapter.getItemCount(), arrdata.size() - adapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    // Albums song list
    private class GetSearchSongsList extends AsyncTask<String, Void, Void> {
        private Mp3HindiAlbumPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            filterby_decades_text.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(String... querry) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=geetmala&query=" + querry[0] + "&mode=" + "hindi_films" + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), Mp3HindiAlbumPojo.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    search_word = getActivity().getIntent().getStringExtra("search_text").trim();
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (arrdata.size() > 0 && start > 0) {
                                arrdata.addAll(basePojo.getData().getList());
                            } else {
                                arrdata.clear();
                                arrdata = basePojo.getData().getList();
                                setAdapter(arrdata);
                                previousTotal = 0;
                            }
                        }
                        if (basePojo.getStatus()) {
                            adapter.notifyItemRangeInserted(adapter.getItemCount(), arrdata.size() - adapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    public void fetchGeetmalaList() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(getActivity());
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getFilterGeetMalaList(new Callback<LanguageBasePojo>() {
                @Override
                public void success(LanguageBasePojo basePojo, Response response) {
                    if (basePojo != null) {
                        ctx.checkCommonURL(response.getUrl());
                        if (basePojo.getData().getArrayList().size() > 0) {
                            LayoutInflater inflater = (LayoutInflater) ctx
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            layout = inflater.inflate(R.layout.right_side_language_list,
                                    (ViewGroup) ctx.findViewById(R.id.popup_element));
                            layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                            pwindo = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, true);

                            pwindo.setWidth(getResources().getInteger(R.integer.popup_window_width));
                            if (mBackground == null)
                                pwindo.setBackgroundDrawable(new BitmapDrawable());
                            else
                                pwindo.setBackgroundDrawable(mBackground);
                            pwindo.setOutsideTouchable(true);
                            pwindo.setFocusable(true);
                            pwindo.setTouchable(true);

                            decade_filter_list = (ListView) layout.findViewById(R.id.language_list_filter);
                            decade_filter_list.setAdapter(new Filter_GeetMala_YearWise_List_Adapter(getActivity(), basePojo.getData().getArrayList()));
                            TextView swipeText = (TextView) layout.findViewById(R.id.swipeText);
                            swipeText.setText("Filter By Decades");
                        }
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
//            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
        }
    }


    private class Filter_GeetMala_YearWise_List_Adapter extends BaseAdapter {
        ArrayList<LanguageListPojo> arr;
        Context context;
        private LayoutInflater inflater = null;

        public Filter_GeetMala_YearWise_List_Adapter(Context context, ArrayList<LanguageListPojo> arr) {
            this.context = context;
            this.arr = arr;
            inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return arr.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class Holder {
            private ImageView radio;
            private TextView radio_text;
            private LinearLayout layout_filter_bg;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final Holder holder = new Holder();
            View rowView;
            rowView = inflater.inflate(R.layout.filter_language_list, null);
            holder.radio = (ImageView) rowView.findViewById(R.id.radio_button);
            holder.radio_text = (TextView) rowView.findViewById(R.id.language_name);
            holder.layout_filter_bg = (LinearLayout) rowView.findViewById(R.id.layout_filter_bg);
            if (arr.get(position).getIsCheck()) {
                holder.radio.setBackground(ContextCompat.getDrawable(context, R.mipmap.radio_selected));
                holder.radio_text.setTextColor(ContextCompat.getColor(context, R.color.radio_selected_text));
                holder.radio_text.setTypeface(face_bold);
            } else {
                holder.radio.setBackground(ContextCompat.getDrawable(context, R.mipmap.radio_box));
                holder.radio_text.setTextColor(ContextCompat.getColor(context, R.color.radio_text));
                holder.radio_text.setTypeface(face_normal);
            }

            holder.radio_text.setText(arr.get(position).getName());
            holder.layout_filter_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    for (int i = 0; i < arr.size(); i++) {
                        if (arr.get(i).getIsCheck()) {
                            arr.get(i).setIsCheck(false);
                            holder.radio_text.setTextColor(ContextCompat.getColor(context, R.color.radio_text));
                            holder.radio_text.setTypeface(face_normal);
                            break;
                        }
                    }

                    notifyDataSetChanged();

                    holder.radio.setBackground(ContextCompat.getDrawable(context, R.mipmap.radio_selected));
                    holder.radio_text.setTextColor(ContextCompat.getColor(context, R.color.radio_selected_text));
                    holder.radio_text.setTypeface(face_bold);
                    arr.get(position).setIsCheck(true);

                    start = 0;
                    year_id = arr.get(position).getId();

                    createAsyncURL(year_id);

                    arrdata.clear();

                    new GetSongsList().execute();
                    selected_text = (arr.get(position).getName());
                    filterby_decades_text.setText(selected_text);
                    pwindo.dismiss();
                }
            });

            return rowView;
        }
    }

    private void createAsyncURL(String year_id) {
        asyncTaskUrl = SaregamaConstants.BASE_URL + "album?d_type=24&year=" + year_id;
    }

    private void initiatePopupWindow() {
        try {
            pwindo.showAsDropDown(filterby_decades_text, getResources().getInteger(R.integer.popup_window_x_offset), getResources().getInteger(R.integer.popup_window_y_offset));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
