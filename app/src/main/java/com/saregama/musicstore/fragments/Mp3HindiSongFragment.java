package com.saregama.musicstore.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.Mp3HindiLandingActivity;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.Mp3HindiSongAdapter;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

public class Mp3HindiSongFragment extends Fragment {

    private Mp3HindiLandingActivity ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0, start = 0;

    private String from;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    private String asyncTaskUrl;

    private String str_alphbet = "";
//    private TextView no_result_found;
//    private LinearLayout alphabets_list;
    private String search_word = "";

    /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_mp3_songs, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.mp3songs_recycler_view);

        /* ****scrollable alphabet list code as per change request**** */
//      no_result_found = (TextView) view.findViewById(R.id.no_result_found);
//      alphabets_list = (LinearLayout) view.findViewById(R.id.alphabets_list);
        rowTextView = (TextView) view.findViewById(R.id.row_text_view);
        alphabetslist = (ListView)view.findViewById(R.id.mp3_landing_list);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(getActivity());

        dialog = new SaregamaDialogs(getActivity());

        from = getActivity().getIntent().getStringExtra("from");
        if (from == null)
            from = "";

        ctx = (Mp3HindiLandingActivity) getActivity();

        /* ****scrollable alphabet list code as per change request start**** */
        final AlphabetListAdapter alpha_adapter = new AlphabetListAdapter(getActivity(), SaregamaConstants.items);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                str_alphbet =  ctx.convertAlphabet(SaregamaConstants.items[position].toLowerCase());
                ctx.arrdata_mp3songs.clear();
                alpha_adapter.setSelectedIndex(position);
                start = 0;
                new GetSongsList().execute();

                ctx.setBubblePosition(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search")) {
            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
        } else {
            if (getActivity().getIntent().getStringExtra("notification_url") != null) {
                asyncTaskUrl = SaregamaConstants.BASE_URL + "song?d_type=" + ctx.getAppConfigJson().getD_type().getHINDI_FILMS() + "&c_type=" + ctx.getAppConfigJson().getC_type().getSONG();
            } else {
                if (from.equals("mp3")) {
                    asyncTaskUrl = SaregamaConstants.BASE_URL + "song?d_type=" + ctx.getAppConfigJson().getD_type().getHINDI_FILMS() + "&c_type=" + ctx.getAppConfigJson().getC_type().getSONG();
                } else {
                    asyncTaskUrl = getActivity().getIntent().getStringExtra("link");
                }
            }
        }

        setData();

        return view;
    }

     /* **** scrollable alphabet list code as per change request comment previous code**** */

    /*public void showListBubble(View v) {
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        previousTotal = 0;
        clearArrData();
        str_alphbet = ctx.showListBubbleCommon(v, rowTextView);
        hideSoftKeypad();
        new GetSongsList().execute();
    }*/

    private void setAdapter(ArrayList<MP3HindiSongListPojo> arr) {
        ctx.mp3SongAdapter = new Mp3HindiSongAdapter(ctx, arr, "mp3_hindi", search_word);
        mRecyclerView.setAdapter(ctx.mp3SongAdapter);
        ctx.mp3SongAdapter.notifyDataSetChanged();
    }

    private void setData() {
        if (ctx.arrdata_mp3songs == null)
            ctx.arrdata_mp3songs = new ArrayList<>();
        if (!fragmentResume && fragmentVisible && ctx != null && ctx.arrdata_mp3songs != null && ctx.arrdata_mp3songs.size() == 0) { //only when first time fragment is created
            if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search"))
                new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
            else
                new GetSongsList().execute();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed() && ctx != null && ctx.arrdata_mp3songs != null && ctx.arrdata_mp3songs.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            if (getActivity().getIntent().getStringExtra("coming_from") != null && getActivity().getIntent().getStringExtra("coming_from").equals("search"))
                new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
            else
                new GetSongsList().execute();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        if (getActivity().getIntent().getStringExtra("search_text") != null && getActivity().getIntent().getStringExtra("search_text").trim().length() > 2) {
                            new GetSearchSongsList().execute(getActivity().getIntent().getStringExtra("search_text").trim());
                        } else {
                            new GetSongsList().execute();
                        }
                        loading = true;
                    }
                }
            }
        });
    }

    // song search list

    private class GetSearchSongsList extends AsyncTask<String, Void, Void> {
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
             /* ****scrollable alphabet list code as per change request **** */
//            alphabets_list.setVisibility(View.GONE);
            alphabetslist.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(String... querry) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getSONG() + "&query=" + querry[0] + "&mode=" + "hindi_films" + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

//            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    search_word = getActivity().getIntent().getStringExtra("search_text").trim();
                    if (basePojo.getData().getList() != null) {
                        if (ctx.arrdata_mp3songs.size() > 0 && start > 0) {
                            ctx.arrdata_mp3songs.addAll(basePojo.getData().getList());
                        } else {
                            ctx.arrdata_mp3songs.clear();
                            ctx.arrdata_mp3songs = basePojo.getData().getList();
                            setAdapter(ctx.arrdata_mp3songs);
                            previousTotal = 0;
                        }
                    }
//                    no_result_found.setVisibility(View.GONE);
                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {
                            ctx.mp3SongAdapter.notifyItemRangeInserted(ctx.mp3SongAdapter.getItemCount(), ctx.arrdata_mp3songs.size() - ctx.mp3SongAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            start = 0;
                            mRecyclerView.clearOnScrollListeners();
                            mRecyclerView.scrollToPosition(0);

         /* **** scrollable alphabet list code as per change request
            comment previous code
         **** */

//                            if (ctx.arrdata_mp3songs.size() == 0) {
//                                no_result_found.setText(SaregamaConstants.NO_RESULT);
//                                no_result_found.setVisibility(View.VISIBLE);
//                            } else {
//                                no_result_found.setVisibility(View.GONE);
//                            }
                        }
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
                // dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(asyncTaskUrl + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&alpha=" + str_alphbet), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
//                    no_result_found.setVisibility(View.GONE);
                    if (basePojo.getData().getList() != null) {
                        if (ctx.arrdata_mp3songs.size() > 0 && start > 0) {
                            ctx.arrdata_mp3songs.addAll(basePojo.getData().getList());
                        } else {
                            ctx.arrdata_mp3songs.clear();
                            ctx.arrdata_mp3songs = basePojo.getData().getList();
                            previousTotal = 0;
                            setAdapter(ctx.arrdata_mp3songs);
                        }
                    }

                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {
                            ctx.mp3SongAdapter.notifyItemRangeInserted(ctx.mp3SongAdapter.getItemCount(), ctx.arrdata_mp3songs.size() - ctx.mp3SongAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            start = 0;
                            mRecyclerView.clearOnScrollListeners();
                            mRecyclerView.scrollToPosition(0);
                            Toast.makeText(getActivity(), SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
    }
}