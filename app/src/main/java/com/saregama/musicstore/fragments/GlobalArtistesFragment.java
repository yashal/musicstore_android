package com.saregama.musicstore.fragments;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.GlobalSearchTabListActivity;
import com.saregama.musicstore.adapter.GlobalArtistAdapter;
import com.saregama.musicstore.pojo.GlobalArtistSearchListPojo;
import com.saregama.musicstore.pojo.GlobalArtistSearchPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class GlobalArtistesFragment extends Fragment {

    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private GlobalSearchTabListActivity ctx;

    private View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    private ArrayList<GlobalArtistSearchListPojo> arrdata;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private int previousTotal = 0;

    private boolean loading = true;

    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    private String search_txt;
    private int start = 0;

    public GlobalArtistesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_global_artistes, container, false);

        cd = new ConnectionDetector(getActivity());
        dialog = new SaregamaDialogs(getActivity());

        ctx = (GlobalSearchTabListActivity) getActivity();

        search_txt = ctx.getIntent().getStringExtra("search_text");
        search_txt = search_txt.replaceAll(" ", "%20");

        if (arrdata == null)
            arrdata = new ArrayList<>();

        new GetArtisteList().execute();

        if (!fragmentResume && fragmentVisible &&arrdata!=null && arrdata.size() == 0) {   //only when first time fragment is created
            new GetArtisteList().execute();
        }

        mRecyclerView = (RecyclerView) view.findViewById(R.id.global_artistes_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        new GetArtisteList().execute();
                        loading = true;
                    }
                }
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed() && arrdata != null && arrdata.size() == 0) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            new GetArtisteList().execute();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private class GetArtisteList extends AsyncTask<Void,Void,Void> {

        ProgressDialog d;
        private GlobalArtistSearchPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(getActivity());
                d.setCanceledOnTouchOutside(false);
            } else {
//                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL+"search?" + "type=" + "search" + "&ctype=" + "3" + "&query=" + search_txt + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), GlobalArtistSearchPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (cd.isConnectingToInternet()) {

                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {

                            if (arrdata.size() > 0) {
                                arrdata.addAll(basePojo.getData().getList());
                            } else {
                                arrdata.clear();
                                arrdata = basePojo.getData().getList();
                                mAdapter = new GlobalArtistAdapter(ctx,arrdata,"global_artist", search_txt);
                                mRecyclerView.setAdapter(mAdapter);
                                previousTotal = 0;
                            }
                        }

                        if (basePojo.getStatus()) {
                            mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), arrdata.size() - mAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(ctx.getAppConfigJson().getServer_error());
                }
            }
        }
        }
}
