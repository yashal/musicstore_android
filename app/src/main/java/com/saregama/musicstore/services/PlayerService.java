package com.saregama.musicstore.services;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.saregama.musicstore.pojo.AppConfigDataPojo;
import com.saregama.musicstore.util.SaregamaConstants;

import java.io.IOException;

import static android.media.AudioManager.AUDIOFOCUS_GAIN;

/**
 * Created by navdeep on 31/5/16.
 */
public class PlayerService extends Service implements AudioManager.OnAudioFocusChangeListener {
    public static final int IS_PLAYING = 1;
    public static final int PLAY = 2;
    public static final int PAUSE = 3;
    public static final int SEEK_POSITION = 4;
    public static final int SEEK_TO = 5;
    public static final int START = 6;
    public static final int STOP = 7;

    public static final String ACTION_IS_PLAYING = "is_playing";
    public static final String ACTION_SEEK_POSITION = "seek_position";

    public long remaining_time = 0;
    public long millis_remaining_time = 0;
    public CountDownTimer timer = null;
    private AudioManager mAudioManager = null;

    private final MediaPlayer mediaPlayer = new MediaPlayer();

    Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case IS_PLAYING:
                    is_playing();
                    break;
                case PLAY:
                    play();
                    break;
                case PAUSE:
                    pause();
                    break;
                case SEEK_POSITION:
                    seek_position();
                    break;
                case SEEK_TO:
                    seek_to(msg.getData().getInt("seek_to", 0));
                    break;
                case START:
                    start_player(msg);
                    break;
                case STOP:
                    stop();
                    break;

            }
            super.handleMessage(msg);
        }
    };
    Messenger messenger = new Messenger(msgHandler);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void play() {
        Intent message = new Intent(ACTION_IS_PLAYING);
        mediaPlayer.start();
        message.putExtra("is_playing", true);
        message.putExtra("is_paused", false);
        message.putExtra("is_stopped", false);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                AUDIOFOCUS_GAIN);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
        callSeekPosition();
    }

    private void pause() {
        Intent message = new Intent(ACTION_IS_PLAYING);
        message.putExtra("is_playing", true);
        message.putExtra("is_paused", true);
        message.putExtra("is_stopped", false);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
        if(timer != null)
            timer.cancel();
        remaining_time = millis_remaining_time;
        mediaPlayer.pause();
    }

    private void stop() {
        mediaPlayer.reset();
        if(timer != null)
            timer.cancel();
        millis_remaining_time = 0;
        SaregamaConstants.REMAINING_TIME = 0;
        remaining_time = SaregamaConstants.PLAYING_TIME;
        if(mAudioManager != null)
            mAudioManager.abandonAudioFocus(this);
        Intent message = new Intent(ACTION_IS_PLAYING);
        message.putExtra("is_playing", false);
        message.putExtra("is_paused", false);
        message.putExtra("is_stopped", true);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
    }

    private void is_playing() {
        Intent message = new Intent(ACTION_IS_PLAYING);
        if (mediaPlayer.isPlaying()) {
            message.putExtra("is_playing", true);
        } else {
            if (millis_remaining_time > 0) {
                message.putExtra("is_playing", true);
                message.putExtra("is_paused", true);
            }
            else
                message.putExtra("is_playing", false);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
    }

    private void seek_position() {
        Intent message = new Intent(ACTION_SEEK_POSITION);
        message.putExtra("current_position", mediaPlayer.getCurrentPosition());
        message.putExtra("duration", mediaPlayer.getDuration());
        message.putExtra("millis_remaining_time", millis_remaining_time);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
    }

    public void callSeekPosition() {
        timer = new CountDownTimer(remaining_time, 100) {
            public void onTick(long millisUntilFinished) {
                millis_remaining_time = millisUntilFinished;
                seek_position();
            }

            public void onFinish() {
                millis_remaining_time = 0;
                stop();
            }
        }.start();
    }

    private void seek_to(int position) {
        mediaPlayer.seekTo(position);
    }

    public void start_player(Message msg) {
        try {
            millis_remaining_time = 0;
            if(timer != null)
                timer.cancel();
            mediaPlayer.reset();
            mediaPlayer.setDataSource(getAppConfigJson().getStream_url()+"&isrc=" + msg.getData().getString("isrc", "") + "&id=" + msg.getData().getString("song_id"));
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                    Intent message = new Intent(ACTION_IS_PLAYING);
                    message.putExtra("is_playing", true);
                    message.putExtra("is_stopped", false);
                    message.putExtra("is_paused", false);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
                    remaining_time = SaregamaConstants.PLAYING_TIME;
                    callSeekPosition();
                }
            });
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Intent message = new Intent(ACTION_IS_PLAYING);
                    message.putExtra("is_playing", false);
                    message.putExtra("is_stopped", true);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
                }
            });
            mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                    AUDIOFOCUS_GAIN);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    public AppConfigDataPojo getAppConfigJson() {
        SharedPreferences sharedPreferences = getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        final Gson gson = new Gson();
        String json = sharedPreferences.getString("AppConfigObject", "");
        AppConfigDataPojo obj = gson.fromJson(json, AppConfigDataPojo.class);

        if (obj != null) {
            return obj;
        }
        return null;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange)
        {
            case AUDIOFOCUS_GAIN:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                play(); // Resume your media player here
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                pause();// Pause your media player here
                break;
        }
    }
}
