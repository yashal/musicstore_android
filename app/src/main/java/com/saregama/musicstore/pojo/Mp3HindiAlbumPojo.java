package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 8/3/2016.
 */
public class Mp3HindiAlbumPojo extends BasePojo {

    private MP3HindiAlbumDataPojo data;

    public MP3HindiAlbumDataPojo getData() {
        return data;
    }

    public void setData(MP3HindiAlbumDataPojo data) {
        this.data = data;
    }
}
