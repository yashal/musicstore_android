package com.saregama.musicstore.pojo;

/**
 * Created by Arpit on 23-Feb-16.
 */
public class BasePojo {

    private Boolean status;
    private String error;
    private GlobalImagePathPojo global_img_path;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public GlobalImagePathPojo getGlobal_img_path() {
        return global_img_path;
    }

    public void setGlobal_img_path(GlobalImagePathPojo global_img_path) {
        this.global_img_path = global_img_path;
    }
}
