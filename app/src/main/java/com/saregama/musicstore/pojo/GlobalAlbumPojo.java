package com.saregama.musicstore.pojo;

/**
 * Created by quepplin1 on 5/12/2016.
 */
public class GlobalAlbumPojo extends BasePojo {

    private GlobalAlbumDataPojo data;

    public GlobalAlbumDataPojo getData() {
        return data;
    }

    public void setData(GlobalAlbumDataPojo data) {
        this.data = data;
    }


}
