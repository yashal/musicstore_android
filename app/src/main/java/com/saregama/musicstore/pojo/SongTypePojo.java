package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 18-Apr-16.
 */
public class SongTypePojo {
    private int SONG, SONGHD;

    public int getSONG() {
        return SONG;
    }

    public void setSONG(int SONG) {
        this.SONG = SONG;
    }

    public int getSONGHD() {
        return SONGHD;
    }

    public void setSONGHD(int SONGHD) {
        this.SONGHD = SONGHD;
    }
}
