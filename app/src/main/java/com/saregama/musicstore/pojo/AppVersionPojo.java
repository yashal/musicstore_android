package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 8/22/2016.
 */
public class AppVersionPojo {

    private String version;
    private int version_code;
    private boolean mandatory_update;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isMandatory_update() {
        return mandatory_update;
    }

    public void setMandatory_update(boolean mandatory_update) {
        this.mandatory_update = mandatory_update;
    }

    public int getVersion_code() {
        return version_code;
    }

    public void setVersion_code(int version_code) {
        this.version_code = version_code;
    }
}
