package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 14/3/2016.
 */
public class ClassHindArtistPojo extends BasePojo {

    private ClassHindArtistDataPojo data;

    public ClassHindArtistDataPojo getData() {
        return data;
    }

    public void setData(ClassHindArtistDataPojo data) {
        this.data = data;
    }
}
