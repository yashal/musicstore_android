package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 8/3/2016.
 */
public class Mp3HindiAlbumDetailListArtist {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
