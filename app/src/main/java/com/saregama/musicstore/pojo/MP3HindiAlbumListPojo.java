package com.saregama.musicstore.pojo;

import java.io.Serializable;

/**
 * Created by navneet on 8/3/2016.
 */
public class MP3HindiAlbumListPojo implements Serializable{

    private String album_id;
    private String album_name;
    private String album_image;
    private String status;
    private String language_id;
    private String price;
    private String price_hd;
    private String currency;
    private String song_count;

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getAlbum_image() {
        return album_image;
    }

    public void setAlbum_image(String album_image) {
        this.album_image = album_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(String language_id) {
        this.language_id = language_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_hd() {
        return price_hd;
    }

    public void setPrice_hd(String price_hd) {
        this.price_hd = price_hd;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSong_count() {
        return song_count;
    }

    public void setSong_count(String song_count) {
        this.song_count = song_count;
    }

}
