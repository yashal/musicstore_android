package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 18-Apr-16.
 */
public class DTypePojo {

    private int CARNATIC;
    private int CLASSICAL;
    private int DEVOTIONAL;
    private int FOLK_SONGS;
    private int FUSION;
    private int GHAZALS_SUFI;
    private int HINDI_FILMS;
    private int HINDUSTANI;
    private int NAZRUL_GEET;
    private int OTHERS;
    private int POP_SONGS;
    private int RABINDRA_SANGEET;
    private int REGIONAL_FILMS;
    private int REMIX;
    private int SEARCH;

    public int getCARNATIC() {
        return CARNATIC;
    }

    public void setCARNATIC(int CARNATIC) {
        this.CARNATIC = CARNATIC;
    }

    public int getCLASSICAL() {
        return CLASSICAL;
    }

    public void setCLASSICAL(int CLASSICAL) {
        this.CLASSICAL = CLASSICAL;
    }

    public int getDEVOTIONAL() {
        return DEVOTIONAL;
    }

    public void setDEVOTIONAL(int DEVOTIONAL) {
        this.DEVOTIONAL = DEVOTIONAL;
    }

    public int getFOLK_SONGS() {
        return FOLK_SONGS;
    }

    public void setFOLK_SONGS(int FOLK_SONGS) {
        this.FOLK_SONGS = FOLK_SONGS;
    }

    public int getFUSION() {
        return FUSION;
    }

    public void setFUSION(int FUSION) {
        this.FUSION = FUSION;
    }

    public int getGHAZALS_SUFI() {
        return GHAZALS_SUFI;
    }

    public void setGHAZALS_SUFI(int GHAZALS_SUFI) {
        this.GHAZALS_SUFI = GHAZALS_SUFI;
    }

    public int getHINDI_FILMS() {
        return HINDI_FILMS;
    }

    public void setHINDI_FILMS(int HINDI_FILMS) {
        this.HINDI_FILMS = HINDI_FILMS;
    }

    public int getHINDUSTANI() {
        return HINDUSTANI;
    }

    public void setHINDUSTANI(int HINDUSTANI) {
        this.HINDUSTANI = HINDUSTANI;
    }

    public int getNAZRUL_GEET() {
        return NAZRUL_GEET;
    }

    public void setNAZRUL_GEET(int NAZRUL_GEET) {
        this.NAZRUL_GEET = NAZRUL_GEET;
    }

    public int getOTHERS() {
        return OTHERS;
    }

    public void setOTHERS(int OTHERS) {
        this.OTHERS = OTHERS;
    }

    public int getPOP_SONGS() {
        return POP_SONGS;
    }

    public void setPOP_SONGS(int POP_SONGS) {
        this.POP_SONGS = POP_SONGS;
    }

    public int getRABINDRA_SANGEET() {
        return RABINDRA_SANGEET;
    }

    public void setRABINDRA_SANGEET(int RABINDRA_SANGEET) {
        this.RABINDRA_SANGEET = RABINDRA_SANGEET;
    }

    public int getREGIONAL_FILMS() {
        return REGIONAL_FILMS;
    }

    public void setREGIONAL_FILMS(int REGIONAL_FILMS) {
        this.REGIONAL_FILMS = REGIONAL_FILMS;
    }

    public int getREMIX() {
        return REMIX;
    }

    public void setREMIX(int REMIX) {
        this.REMIX = REMIX;
    }

    public int getSEARCH() {
        return SEARCH;
    }

    public void setSEARCH(int SEARCH) {
        this.SEARCH = SEARCH;
    }
}
