package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 4/3/2016.
 */
public class HomeHDPojo extends BasePojo {

    private HomeHDDataPojo data;

    public HomeHDDataPojo getData() {
        return data;
    }

    public void setData(HomeHDDataPojo data) {
        this.data = data;
    }
}
