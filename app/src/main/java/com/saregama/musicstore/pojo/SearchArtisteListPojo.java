package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by Administrator on 27-Jul-16.
 */
public class SearchArtisteListPojo {

    private ArrayList<ClassHindArtistListPojo> list;
    private int count;

    public ArrayList<ClassHindArtistListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<ClassHindArtistListPojo> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
