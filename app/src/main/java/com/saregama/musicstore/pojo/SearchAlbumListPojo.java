package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by Administrator on 27-Jul-16.
 */
public class SearchAlbumListPojo {

    private ArrayList<MP3HindiAlbumListPojo> list;
    private int count;

    public ArrayList<MP3HindiAlbumListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<MP3HindiAlbumListPojo> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
