package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 9/21/2016.
 */

public class ForgotPasswordMailPojo extends BasePojo {

    private ForgotPasswordMailDataPojo data;

    public ForgotPasswordMailDataPojo getData() {
        return data;
    }

    public void setData(ForgotPasswordMailDataPojo data) {
        this.data = data;
    }
}
