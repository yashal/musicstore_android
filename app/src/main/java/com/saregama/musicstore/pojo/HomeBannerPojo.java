package com.saregama.musicstore.pojo;

/**
 * Created by Arpit on 24-Feb-16.
 */
public class HomeBannerPojo {

    private String img_320, url, img_540, img_1000, type, grid;
    private String d_type_name,language_name;
    private int d_type,language_id,c_type,song_type;

    public int getSong_type() {
        return song_type;
    }

    public void setSong_type(int song_type) {
        this.song_type = song_type;
    }

    public int getC_type() {
        return c_type;
    }

    public void setC_type(int c_type) {
        this.c_type = c_type;
    }

    public String getLanguage_name() {
        return language_name;
    }

    public void setLanguage_name(String language_name) {
        this.language_name = language_name;
    }

    public int getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }

    public String getImg_320() {
        return img_320;
    }

    public void setImg_320(String img_320) {
        this.img_320 = img_320;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg_540() {
        return img_540;
    }

    public void setImg_540(String img_540) {
        this.img_540 = img_540;
    }

    public String getImg_1000() {
        return img_1000;
    }

    public void setImg_1000(String img_1000) {
        this.img_1000 = img_1000;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public int getD_type() {
        return d_type;
    }

    public void setD_type(int d_type) {
        this.d_type = d_type;
    }

    public String getD_type_name() {
        return d_type_name;
    }

    public void setD_type_name(String d_type_name) {
        this.d_type_name = d_type_name;
    }
}
