package com.saregama.musicstore.pojo;

/**
 * Created by quepplin1 on 5/13/2016.
 */
public class GlobalArtistSearchListPojo {

    private String id;
    private String name;
    private String image;
    private String song_count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSong_count() {
        return song_count;
    }

    public void setSong_count(String song_count) {
        this.song_count = song_count;
    }
}
