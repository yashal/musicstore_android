package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 9/26/2016.
 */

public class ResetPasswordPojo extends BasePojo {

    private ResetPasswordDataPojo data;

    public ResetPasswordDataPojo getData() {
        return data;
    }

    public void setData(ResetPasswordDataPojo data) {
        this.data = data;
    }
}
