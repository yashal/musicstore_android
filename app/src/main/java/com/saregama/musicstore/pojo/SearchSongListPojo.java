package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by Administrator on 27-Jul-16.
 */
public class SearchSongListPojo {

    private ArrayList<MP3HindiSongListPojo> list;
    private int count;

    public ArrayList<MP3HindiSongListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<MP3HindiSongListPojo> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
