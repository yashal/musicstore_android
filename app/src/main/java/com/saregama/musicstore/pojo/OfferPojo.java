package com.saregama.musicstore.pojo;

/**
 * Created by Arpit on 07-Mar-16.
 */
public class OfferPojo extends BasePojo{

    private OfferDataPojo data;

    public OfferDataPojo getData() {
        return data;
    }

    public void setData(OfferDataPojo data) {
        this.data = data;
    }
}
