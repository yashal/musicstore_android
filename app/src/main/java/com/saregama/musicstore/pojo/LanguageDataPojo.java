package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by yesh on 3/14/2016.
 */
public class LanguageDataPojo {

    ArrayList<LanguageListPojo> list;

    public ArrayList<LanguageListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<LanguageListPojo> list) {
        this.list = list;
    }

    public ArrayList<LanguageListPojo> getArrayList() {
        return list;
    }

    public void setArrayList(ArrayList<LanguageListPojo> arrayList) {
        this.list = arrayList;
    }
}
