package com.saregama.musicstore.pojo;

/**
 * Created by yesh on 3/14/2016.
 */
public class LanguageBasePojo extends BasePojo{

    private LanguageDataPojo data;

    public LanguageDataPojo getData() {
        return data;
    }

    public void setData(LanguageDataPojo data) {
        this.data = data;
    }
}
