package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by Arpit on 12-Apr-16.
 */
public class CartListItemPojo {
    private ArrayList<CartItemPojo> mp3;
    private ArrayList<CartItemPojo> hd;
    private ArrayList<CartItemPojo> album;

    public ArrayList<CartItemPojo> getMp3() {
        return mp3;
    }

    public void setMp3(ArrayList<CartItemPojo> mp3) {
        this.mp3 = mp3;
    }

    public ArrayList<CartItemPojo> getHd() {
        return hd;
    }

    public void setHd(ArrayList<CartItemPojo> hd) {
        this.hd = hd;
    }

    public ArrayList<CartItemPojo> getAlbum() {
        return album;
    }

    public void setAlbum(ArrayList<CartItemPojo> album) {
        this.album = album;
    }
}
