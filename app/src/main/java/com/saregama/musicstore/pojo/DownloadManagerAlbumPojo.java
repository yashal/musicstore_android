package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by Yash ji on 4/29/2016.
 */
public class DownloadManagerAlbumPojo {

    private String id;
    private String name;
    private String image;
    private int album_type;
    private ArrayList<DownloadManagerAlbumSongs> album_songs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getAlbum_type() {
        return album_type;
    }

    public void setAlbum_type(int album_type) {
        this.album_type = album_type;
    }

    public ArrayList<DownloadManagerAlbumSongs> getAlbum_songs() {
        return album_songs;
    }

    public void setAlbum_songs(ArrayList<DownloadManagerAlbumSongs> album_songs) {
        this.album_songs = album_songs;
    }
}
