package com.saregama.musicstore.pojo;

/**
 * Created by Arpit on 23-Feb-16.
 */
public class LoginDataPojo {

    private LoginListenerPojo listener;
    private String cart, download;

    public LoginListenerPojo getListener() {
        return listener;
    }

    public void setListener(LoginListenerPojo listener) {
        this.listener = listener;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }
}
