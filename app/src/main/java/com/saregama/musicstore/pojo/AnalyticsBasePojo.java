package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 5/18/2016.
 */
public class AnalyticsBasePojo {

    private int response;
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getResponse() {
        return response;
    }

    public void setResponse(int response) {
        this.response = response;
    }
}
