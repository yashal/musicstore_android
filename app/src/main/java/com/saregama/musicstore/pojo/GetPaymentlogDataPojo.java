package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 5/24/2016.
 */
public class GetPaymentlogDataPojo {

    private ArrayList<GetpaymentlogSongMp3Pojo> song_mp3;
    private ArrayList<GetpaymentlogSongMp3Pojo> song_hd;
    private ArrayList<GetpaymentlogAlbumPojo> albums;
    private ArrayList<GetpaymentlogAlbumPojo> albums_hd;
    private String order_no;
    private String coupon_id;
    private String paid_price;
    private String order_price;
    private String payment_gateway;
    private String currency;
    private String country;
    private String transaction_date;

    public String getPayment_gateway() {
        return payment_gateway;
    }

    public void setPayment_gateway(String payment_gateway) {
        this.payment_gateway = payment_gateway;
    }

    public ArrayList<GetpaymentlogSongMp3Pojo> getSong_mp3() {
        return song_mp3;
    }

    public void setSong_mp3(ArrayList<GetpaymentlogSongMp3Pojo> song_mp3) {
        this.song_mp3 = song_mp3;
    }

    public ArrayList<GetpaymentlogSongMp3Pojo> getSong_hd() {
        return song_hd;
    }

    public void setSong_hd(ArrayList<GetpaymentlogSongMp3Pojo> song_hd) {
        this.song_hd = song_hd;
    }

    public ArrayList<GetpaymentlogAlbumPojo> getAlbums() {
        return albums;
    }

    public void setAlbums(ArrayList<GetpaymentlogAlbumPojo> albums) {
        this.albums = albums;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getOrder_price() {
        return order_price;
    }

    public void setOrder_price(String order_price) {
        this.order_price = order_price;
    }

    public String getPaid_price() {
        return paid_price;
    }

    public void setPaid_price(String paid_price) {
        this.paid_price = paid_price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(String transaction_date) {
        this.transaction_date = transaction_date;
    }

    public ArrayList<GetpaymentlogAlbumPojo> getAlbums_hd() {
        return albums_hd;
    }

    public void setAlbums_hd(ArrayList<GetpaymentlogAlbumPojo> albums_hd) {
        this.albums_hd = albums_hd;
    }
}
