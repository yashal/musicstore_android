package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 27-Jul-16.
 */
public class HindiFilmsSearchPojo extends BasePojo{

    private HindiFilmsSearchDataPojo data;

    public HindiFilmsSearchDataPojo getData() {
        return data;
    }

    public void setData(HindiFilmsSearchDataPojo data) {
        this.data = data;
    }
}
