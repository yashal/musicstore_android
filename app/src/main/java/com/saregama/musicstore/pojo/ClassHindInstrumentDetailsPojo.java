package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 15/3/2016.
 */
public class ClassHindInstrumentDetailsPojo extends BasePojo {

    private ClassHindInstrumentDetailsDataPojo data;

    public ClassHindInstrumentDetailsDataPojo getData() {
        return data;
    }

    public void setData(ClassHindInstrumentDetailsDataPojo data) {
        this.data = data;
    }
}
