package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by navneet on 8/3/2016.
 */
public class Mp3HindiAlbumDetailDataPojo {

    private String album_id;
    private String album_name;
    private String album_img;
    private String status;
    private String language_id;
    private int song_count;
    private String music_director;
    private String music_director_id;
    private String price;
    private String price_hd;
    private String currency;
    private ArrayList<MP3HindiSongListPojo> list;

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getAlbum_img() {
        return album_img;
    }

    public void setAlbum_img(String album_img) {
        this.album_img = album_img;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(String language_id) {
        this.language_id = language_id;
    }

    public int getSong_count() {
        return song_count;
    }

    public void setSong_count(int song_count) {
        this.song_count = song_count;
    }

    public String getMusic_director() {
        return music_director;
    }

    public void setMusic_director(String music_director) {
        this.music_director = music_director;
    }

    public String getMusic_director_id() {
        return music_director_id;
    }

    public void setMusic_director_id(String music_director_id) {
        this.music_director_id = music_director_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_hd() {
        return price_hd;
    }

    public void setPrice_hd(String price_hd) {
        this.price_hd = price_hd;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ArrayList<MP3HindiSongListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<MP3HindiSongListPojo> list) {
        this.list = list;
    }
}
