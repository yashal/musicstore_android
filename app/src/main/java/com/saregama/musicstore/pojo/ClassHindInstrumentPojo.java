package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 14/3/2016.
 */
public class ClassHindInstrumentPojo extends BasePojo {

    private ClassHindInstrumentDataPojo data;

    public ClassHindInstrumentDataPojo getData() {
        return data;
    }

    public void setData(ClassHindInstrumentDataPojo data) {
        this.data = data;
    }
}
