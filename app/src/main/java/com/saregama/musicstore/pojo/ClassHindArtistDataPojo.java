package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by navneet on 14/3/2016.
 */
public class ClassHindArtistDataPojo {

    private ArrayList<ClassHindArtistListPojo> list;
    private int count;

    public ArrayList<ClassHindArtistListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<ClassHindArtistListPojo> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
