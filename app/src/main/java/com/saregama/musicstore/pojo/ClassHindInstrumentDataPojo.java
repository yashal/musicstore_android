package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by navneet on 14/3/2016.
 */
public class ClassHindInstrumentDataPojo {

    private ArrayList<ClassHindInstrumentListPojo> list;
    private int count;

    public ArrayList<ClassHindInstrumentListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<ClassHindInstrumentListPojo> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
