package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by navneet on 15/3/2016.
 */
public class ClassHindInstrumentDetailsDataPojo {

    private ArrayList<MP3HindiSongListPojo> list;
    private String d_type;
    private String d_type_name;
    private String tag_ig;
    private String tag_name;
    private String tag_image;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getInstrument_image() {
        return tag_image;
    }

    public void setInstrument_image(String instrument_image) {
        this.tag_image = instrument_image;
    }

    public ArrayList<MP3HindiSongListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<MP3HindiSongListPojo> list) {
        this.list = list;
    }


    public String getD_type() {
        return d_type;
    }

    public void setD_type(String d_type) {
        this.d_type = d_type;
    }

    public String getD_type_name() {
        return d_type_name;
    }

    public void setD_type_name(String d_type_name) {
        this.d_type_name = d_type_name;
    }

    public String getInstrument_id() {
        return tag_ig;
    }

    public void setInstrument_id(String instrument_id) {
        this.tag_ig = instrument_id;
    }

    public String getInstrument_name() {
        return tag_name;
    }

    public void setInstrument_name(String instrument_name) {
        this.tag_name = instrument_name;
    }
}
