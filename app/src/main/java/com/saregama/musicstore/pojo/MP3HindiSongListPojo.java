package com.saregama.musicstore.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by navneet on 7/3/2016.
 */
public class MP3HindiSongListPojo implements Serializable {

    private String song_id;
    private String song_name;
    private String audio_file;
    private String insert_date;
    private String isrc;
    private String language_id;
    private String is_320kbps;
    private String album_id;
    private String album_name;
    private String status;
    private String release_date;
    private String tempo;
    private String song_duration;
    private String album_image;
    private boolean is_playing = false;
    private String artist_id;
    private String artist_name;
    private String artist_role;
    private String id;
    private String name;
    private String image;

    private ArrayList<Mp3HindiAlbumDetailListArtist> artist;

    public boolean is_playing() {
        return is_playing;
    }

    public void setIs_playing(boolean is_playing) {
        this.is_playing = is_playing;
    }

    public String getSong_id() {
        return song_id;
    }

    public void setSong_id(String song_id) {
        this.song_id = song_id;
    }

    public String getSong_name() {
        return song_name;
    }

    public void setSong_name(String song_name) {
        this.song_name = song_name;
    }

    public String getAudio_file() {
        return audio_file;
    }

    public void setAudio_file(String audio_file) {
        this.audio_file = audio_file;
    }

    public String getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(String insert_date) {
        this.insert_date = insert_date;
    }

    public String getIsrc() {
        return isrc;
    }

    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    public String getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(String language_id) {
        this.language_id = language_id;
    }

    public String getIs_320kbps() {
        return is_320kbps;
    }

    public void setIs_320kbps(String is_320kbps) {
        this.is_320kbps = is_320kbps;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

    public String getSong_duration() {
        return song_duration;
    }

    public void setSong_duration(String song_duration) {
        this.song_duration = song_duration;
    }

    public String getAlbum_image() {
        return album_image;
    }

    public void setAlbum_image(String album_image) {
        this.album_image = album_image;
    }

    public ArrayList<Mp3HindiAlbumDetailListArtist> getArtist() {
        return artist;
    }

    public void setArtist(ArrayList<Mp3HindiAlbumDetailListArtist> artist) {
        this.artist = artist;
    }

    public String getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(String artist_id) {
        this.artist_id = artist_id;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getArtist_role() {
        return artist_role;
    }

    public void setArtist_role(String artist_role) {
        this.artist_role = artist_role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
