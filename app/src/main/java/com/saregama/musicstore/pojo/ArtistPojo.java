package com.saregama.musicstore.pojo;

/**
 * Created by Arpit on 10-Mar-16.
 */
public class ArtistPojo extends BasePojo{

    private ArtistDataPojo data;
    private int count;

    public ArtistDataPojo getData() {
        return data;
    }

    public void setData(ArtistDataPojo data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
