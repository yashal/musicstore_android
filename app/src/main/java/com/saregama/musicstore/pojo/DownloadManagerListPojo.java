package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class DownloadManagerListPojo {

    private ArrayList<DownloadManagerSongPojo> songs;
    private ArrayList<DownloadManagerAlbumPojo> albums;

    public ArrayList<DownloadManagerSongPojo> getSongs() {
        return songs;
    }

    public void setSongs(ArrayList<DownloadManagerSongPojo> songs) {
        this.songs = songs;
    }

    public ArrayList<DownloadManagerAlbumPojo> getAlbums() {
        return albums;
    }

    public void setAlbums(ArrayList<DownloadManagerAlbumPojo> albums) {
        this.albums = albums;
    }
}
