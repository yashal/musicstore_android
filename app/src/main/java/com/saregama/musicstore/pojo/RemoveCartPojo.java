package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 13-Apr-16.
 */
public class RemoveCartPojo extends BasePojo {

    private RemoveCartDataPojo data;

    public RemoveCartDataPojo getData() {
        return data;
    }

    public void setData(RemoveCartDataPojo data) {
        this.data = data;
    }
}
