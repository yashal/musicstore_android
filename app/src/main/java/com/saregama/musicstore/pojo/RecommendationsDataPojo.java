package com.saregama.musicstore.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by arpit on 11/25/2016.
 */

public class RecommendationsDataPojo  implements Serializable {
    private ArrayList<MP3HindiSongListPojo> list;

    public ArrayList<MP3HindiSongListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<MP3HindiSongListPojo> list) {
        this.list = list;
    }
}
