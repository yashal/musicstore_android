package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 25-Apr-16.
 */
public class ProfilePojo extends BasePojo {

    private ProfileDataPojo data;

    public ProfileDataPojo getData() {
        return data;
    }

    public void setData(ProfileDataPojo data) {
        this.data = data;
    }
}
