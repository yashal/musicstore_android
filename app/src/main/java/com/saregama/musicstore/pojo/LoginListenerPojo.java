package com.saregama.musicstore.pojo;

/**
 * Created by Arpit on 23-Feb-16.
 */
public class LoginListenerPojo {

    private String id, title, image, email, mobile, session_id, expiry_time, access, name;
    private String city;
    private boolean returning_user;
    private boolean user_exits;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getExpiry_time() {
        return expiry_time;
    }

    public void setExpiry_time(String expiry_time) {
        this.expiry_time = expiry_time;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isReturning_user() {
        return returning_user;
    }

    public void setReturning_user(boolean returning_user) {
        this.returning_user = returning_user;
    }

    public boolean isUser_exits() {
        return user_exits;
    }

    public void setUser_exits(boolean user_exits) {
        this.user_exits = user_exits;
    }
}
