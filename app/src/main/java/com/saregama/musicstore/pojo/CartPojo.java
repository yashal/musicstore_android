package com.saregama.musicstore.pojo;

/**
 * Created by Arpit on 12-Apr-16.
 */
public class CartPojo extends BasePojo {
    private CartDataPojo data;

    public CartDataPojo getData() {
        return data;
    }

    public void setData(CartDataPojo data) {
        this.data = data;
    }
}
