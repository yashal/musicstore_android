package com.saregama.musicstore.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by arpit on 11/25/2016.
 */

public class RecommendationsAlbumDataPojo implements Serializable {
    private ArrayList<MP3HindiAlbumListPojo> list;

    public ArrayList<MP3HindiAlbumListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<MP3HindiAlbumListPojo> list) {
        this.list = list;
    }
}
