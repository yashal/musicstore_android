package com.saregama.musicstore.pojo;

/**
 * Created by arpit on 11/25/2016.
 */

public class RecommendationsAlbumPojo extends BasePojo{
    private RecommendationsAlbumDataPojo data;

    public RecommendationsAlbumDataPojo getData() {
        return data;
    }

    public void setData(RecommendationsAlbumDataPojo data) {
        this.data = data;
    }
}
