package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 18-Apr-16.
 */
public class DevotionalConfigPojo {
    private int DEVI_DEVTAS, AARTI, MANTRAS;

    public int getDEVI_DEVTAS() {
        return DEVI_DEVTAS;
    }

    public void setDEVI_DEVTAS(int DEVI_DEVTAS) {
        this.DEVI_DEVTAS = DEVI_DEVTAS;
    }

    public int getAARTI() {
        return AARTI;
    }

    public void setAARTI(int AARTI) {
        this.AARTI = AARTI;
    }

    public int getMANTRAS() {
        return MANTRAS;
    }

    public void setMANTRAS(int MANTRAS) {
        this.MANTRAS = MANTRAS;
    }
}
