package com.saregama.musicstore.pojo;

/**
 * Created by Arpit on 12-Apr-16.
 */
public class CartDataPojo {

    private CartListItemPojo cart;
    private float songs_total, albums_total, grand_total, applycoupon;
    private String listener_id, guest_id;
    private String currency;

    public CartListItemPojo getCart() {
        return cart;
    }

    public void setCart(CartListItemPojo cart) {
        this.cart = cart;
    }

    public float getSongs_total() {
        return songs_total;
    }

    public void setSongs_total(float songs_total) {
        this.songs_total = songs_total;
    }

    public float getAlbums_total() {
        return albums_total;
    }

    public void setAlbums_total(float albums_total) {
        this.albums_total = albums_total;
    }

    public float getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(float grand_total) {
        this.grand_total = grand_total;
    }

    public float getApplycoupon() {
        return applycoupon;
    }

    public void setApplycoupon(int applycoupon) {
        this.applycoupon = applycoupon;
    }

    public String getListener_id() {
        return listener_id;
    }

    public void setListener_id(String listener_id) {
        this.listener_id = listener_id;
    }

    public String getGuest_id() {
        return guest_id;
    }

    public void setGuest_id(String guest_id) {
        this.guest_id = guest_id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
