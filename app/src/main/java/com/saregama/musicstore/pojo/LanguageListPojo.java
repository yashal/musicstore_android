package com.saregama.musicstore.pojo;

/**
 * Created by yesh on 3/14/2016.
 */
public class LanguageListPojo {

    String id,name;
    private Boolean isCheck=false;

    public Boolean getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(Boolean isCheck) {
        this.isCheck = isCheck;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
