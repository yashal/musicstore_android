package com.saregama.musicstore.pojo;

/**
 * Created by yash on 6/23/2017.
 */

public class GetSatePojo extends BasePojo {

    private GetSateDataPojo data;

    public GetSateDataPojo getData() {
        return data;
    }

    public void setData(GetSateDataPojo data) {
        this.data = data;
    }
}
