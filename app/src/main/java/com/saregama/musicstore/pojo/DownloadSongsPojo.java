package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 5/6/2016.
 */
public class DownloadSongsPojo extends BasePojo{

        private DownloadSongDataPojo data;

    public DownloadSongDataPojo getData() {
        return data;
    }

    public void setData(DownloadSongDataPojo data) {
        this.data = data;
    }
}
