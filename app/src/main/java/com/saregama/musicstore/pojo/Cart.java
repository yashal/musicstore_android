package com.saregama.musicstore.pojo;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

public class Cart implements ParentListItem {

    private String mName;
    private int mCount;
    private ArrayList<CartItemPojo> mIngredients;

    public Cart(){}

    public Cart(String name/*, int count*/, ArrayList<CartItemPojo> ingredients) {
        mName = name;
//        mCount = count;
        mIngredients = ingredients;
    }

    public String getName() {
        return mName;
    }

//    public int getCount() {
//        return mCount;
//    }

    @Override
    public List<?> getChildItemList() {
        return mIngredients;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return true;
    }
}
