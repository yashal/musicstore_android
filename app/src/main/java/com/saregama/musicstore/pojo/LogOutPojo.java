package com.saregama.musicstore.pojo;

/**
 * Created by quepplin1 on 5/25/2016.
 */
public class LogOutPojo extends BasePojo {

    private LogOutDataPojo data;

    public LogOutDataPojo getData() {
        return data;
    }

    public void setData(LogOutDataPojo data) {
        this.data = data;
    }


}
