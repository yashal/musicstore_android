package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 18-Apr-16.
 */
public class CTypePojo {
    private int ALBUM, SONGHD, SONG, ALBUMHD;

    public int getALBUM() {
        return ALBUM;
    }

    public void setALBUM(int ALBUM) {
        this.ALBUM = ALBUM;
    }

    public int getSONGHD() {
        return SONGHD;
    }

    public void setSONGHD(int SONGHD) {
        this.SONGHD = SONGHD;
    }

    public int getSONG() {
        return SONG;
    }

    public void setSONG(int SONG) {
        this.SONG = SONG;
    }

    public int getALBUMHD() {
        return ALBUMHD;
    }

    public void setALBUMHD(int ALBUMHD) {
        this.ALBUMHD = ALBUMHD;
    }
}
