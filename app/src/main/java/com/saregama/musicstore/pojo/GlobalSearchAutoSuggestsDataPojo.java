package com.saregama.musicstore.pojo;


public class GlobalSearchAutoSuggestsDataPojo extends BasePojo {

    private GlobalSearchDataPojo data;

    public GlobalSearchDataPojo getData() {
        return data;
    }

    public void setData(GlobalSearchDataPojo data) {
        this.data = data;
    }

}
