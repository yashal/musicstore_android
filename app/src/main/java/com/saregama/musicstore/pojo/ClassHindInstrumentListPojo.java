package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 14/3/2016.
 */
public class ClassHindInstrumentListPojo {
    private String tag_id;
    private String tag_name;
    private String image;

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getTag_name() {
        return tag_name;
    }

    public void setTag_name(String tag_name) {
        this.tag_name = tag_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
