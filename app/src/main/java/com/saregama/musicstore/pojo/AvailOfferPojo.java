package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by navneet on 21/3/2016.
 */
public class AvailOfferPojo extends BasePojo{

    ArrayList<OfferDataPojo> data;

    public ArrayList<OfferDataPojo> getData() {
        return data;
    }

    public void setData(ArrayList<OfferDataPojo> data) {
        this.data = data;
    }
}
