package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 5/31/2016.
 */
public class NotificationPojo extends BasePojo {

    private ArrayList<NotificationDataPojo> data;

    public ArrayList<NotificationDataPojo> getData() {
        return data;
    }

    public void setData(ArrayList<NotificationDataPojo> data) {
        this.data = data;
    }
}
