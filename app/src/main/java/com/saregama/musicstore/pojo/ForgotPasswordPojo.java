package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 3/3/2016.
 */
public class ForgotPasswordPojo extends BasePojo {

    private ForgotPasswordDataPojo data;

    public ForgotPasswordDataPojo getData() {
        return data;
    }

    public void setData(ForgotPasswordDataPojo data) {
        this.data = data;
    }
}
