package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by bhanu on 26-Apr-16.
 */
public class GlobalSearchListPojo {

    private ArrayList<GlobalArtistPojo> artist;
    private ArrayList<MP3HindiSongListPojo> song;
    private ArrayList<GlobalArtistPojo> album;
    private ArrayList<GlobalArtistPojo> pack;

    public ArrayList<GlobalArtistPojo> getArtist() {
        return artist;
    }

    public void setArtist(ArrayList<GlobalArtistPojo> artist) {
        this.artist = artist;
    }

    public ArrayList<MP3HindiSongListPojo> getSong() {
        return song;
    }

    public void setSong(ArrayList<MP3HindiSongListPojo> song) {
        this.song = song;
    }

    public ArrayList<GlobalArtistPojo> getAlbum() {
        return album;
    }

    public void setAlbum(ArrayList<GlobalArtistPojo> album) {
        this.album = album;
    }

    public ArrayList<GlobalArtistPojo> getPack() {
        return pack;
    }

    public void setPack(ArrayList<GlobalArtistPojo> pack) {
        this.pack = pack;
    }
}
