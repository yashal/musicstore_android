package com.saregama.musicstore.pojo;

/**
 * Created by bhanu on 26-Apr-16.
 */
public class GlobalSearchDataPojo {

    private GlobalSearchListPojo list;

    public GlobalSearchListPojo getList() {
        return list;
    }

    public void setList(GlobalSearchListPojo list) {
        this.list = list;
    }
}
