package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 9/21/2016.
 */

public class ForgotPasswordMailDataPojo {

    private String email;
    private String listener_id;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getListener_id() {
        return listener_id;
    }

    public void setListener_id(String listener_id) {
        this.listener_id = listener_id;
    }
}
