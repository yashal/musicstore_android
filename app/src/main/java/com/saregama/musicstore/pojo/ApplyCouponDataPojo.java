package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 4/20/2016.
 */
public class ApplyCouponDataPojo {

    private String coupon;
    private float grand_total;
    private float netpayment;
    private float discount;
    private String coupon_type;
    private String listener_id;
    private String guest_id;
    private int applycoupon;
    private String currency;

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public float getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(float grand_total) {
        this.grand_total = grand_total;
    }

    public float getNetpayment() {
        return netpayment;
    }

    public void setNetpayment(float netpayment) {
        this.netpayment = netpayment;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getCoupon_type() {
        return coupon_type;
    }

    public void setCoupon_type(String coupon_type) {
        this.coupon_type = coupon_type;
    }

    public String getListener_id() {
        return listener_id;
    }

    public void setListener_id(String listener_id) {
        this.listener_id = listener_id;
    }

    public String getGuest_id() {
        return guest_id;
    }

    public void setGuest_id(String guest_id) {
        this.guest_id = guest_id;
    }

    public int getApplycoupon() {
        return applycoupon;
    }

    public void setApplycoupon(int applycoupon) {
        this.applycoupon = applycoupon;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
