package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 4/3/2016.
 */
public class HomeHDDataPojo {

    private HomeBannerPojo home_hd_banner;

    public HomeBannerPojo getHome_hd_banner() {
        return home_hd_banner;
    }

    public void setHome_hd_banner(HomeBannerPojo home_hd_banner) {
        this.home_hd_banner = home_hd_banner;
    }
}
