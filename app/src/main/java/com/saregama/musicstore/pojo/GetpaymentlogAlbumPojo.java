package com.saregama.musicstore.pojo;

/**
 * Created by quepplin1 on 5/24/2016.
 */
public class GetpaymentlogAlbumPojo {

    private String album_id;

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }
}
