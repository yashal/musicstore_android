package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 5/13/2016.
 */
public class GlobalArtistSearchDataPojo {

    private ArrayList<GlobalArtistSearchListPojo> list;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<GlobalArtistSearchListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<GlobalArtistSearchListPojo> list) {
        this.list = list;
    }
}
