package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class DownloadManagerDataPojo {

    private DownloadManagerListPojo list;
    private Integer albumCount;
    private Integer songCount;

    public DownloadManagerListPojo getList() {
        return list;
    }

    public void setList(DownloadManagerListPojo list) {
        this.list = list;
    }

    public Integer getAlbumCount() {
        return albumCount;
    }

    public void setAlbumCount(Integer albumCount) {
        this.albumCount = albumCount;
    }

    public Integer getSongCount() {
        return songCount;
    }

    public void setSongCount(Integer songCount) {
        this.songCount = songCount;
    }
}
