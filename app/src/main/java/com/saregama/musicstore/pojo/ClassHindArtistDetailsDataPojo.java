package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by navneet on 14/3/2016.
 */
public class ClassHindArtistDetailsDataPojo {

    private String artist_id;
    private String artist_name;
    private String artist_role;
    private String artist_image;
    private int count;
    private ArrayList<MP3HindiSongListPojo> list;

    public String getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(String artist_id) {
        this.artist_id = artist_id;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getArtist_role() {
        return artist_role;
    }

    public void setArtist_role(String artist_role) {
        this.artist_role = artist_role;
    }

    public String getArtist_image() {
        return artist_image;
    }

    public void setArtist_image(String artist_image) {
        this.artist_image = artist_image;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<MP3HindiSongListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<MP3HindiSongListPojo> list) {
        this.list = list;
    }
}
