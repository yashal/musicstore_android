package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 26-Apr-16.
 */
public class PaymentPojo extends BasePojo {

    private PaymentDataPojo data;

    public PaymentDataPojo getData() {
        return data;
    }

    public void setData(PaymentDataPojo data) {
        this.data = data;
    }
}
