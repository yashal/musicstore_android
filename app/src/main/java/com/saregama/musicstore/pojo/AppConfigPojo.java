package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 18-Apr-16.
 */
public class AppConfigPojo extends BasePojo {

    private AppConfigDataPojo data;

    public AppConfigDataPojo getData() {
        return data;
    }

    public void setData(AppConfigDataPojo data) {
        this.data = data;
    }
}
