package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 3/3/2016.
 */
public class ForgotPasswordDataPojo {

    private String email;
    private String access;
    private String msg;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
