package com.saregama.musicstore.pojo;

/**
 * Created by Arpit on 23-Feb-16.
 */
public class LogInPojo extends BasePojo {

    private LoginDataPojo data;

    public LoginDataPojo getData() {
        return data;
    }

    public void setData(LoginDataPojo data) {
        this.data = data;
    }
}
