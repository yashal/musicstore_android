package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by Yash ji on 8/2/2016.
 */
public class SearchInstrumentListPojo {

    private ArrayList<ClassHindInstrumentListPojo> list;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<ClassHindInstrumentListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<ClassHindInstrumentListPojo> list) {
        this.list = list;
    }
}
