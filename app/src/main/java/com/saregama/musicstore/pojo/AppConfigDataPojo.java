package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 18-Apr-16.
 */
public class AppConfigDataPojo {
    private DTypePojo d_type;
    private CTypePojo c_type;
    private SongTypePojo song_type;
    private OffsetPojo offset;
    private DevotionalConfigPojo dev_song;
    private AppVersionPojo app_version;
    private String stream_url;
    private int stream_time_second;
    private boolean incentivization_popup;
    private String default_artist;
    private String analytics_log_url;
    private String bulk_url;
    private String city;
    private String state;
    private String country;
    private String ip;
    private String forgotpassword1;
    private String forgotpassword2;
    private String resetpassword_text;
    private String general;
    private String fail;
    private String success;
    private String contact_us;
    private String country_code;
    private String currency;
    private String song_preview_text;
    private String mp3_price;
    private String hp_price;
    private String download_complete;
    private int album_song_limit;
    private String emailsent_text;
    private String about_us;
    private String terms_of_service;
    private String privacy_policy;
    private String faq;
    private String blank_email;
    private String blank_password;
    private String createpassword_text;
    private String signout_popup;
    private String password_reset_popup;
    private String new_password;
    private String confirm_password;
    private String short_length_password_popup;
    private String cancel_transaction_popup;
    private String transaction_failed_popup;
    private String valid_email;
    private String remove_artist;
    private String coupon_expired;
    private String sign_required_download;
    private String internat_fail;
    private String profile_update_name;
    private String profile_update_invalid_image;
    private String signin_required;
    private String profile_update_location;
    private String server_error;
    private String song_exists_cart;
    private String album_exists_cart;
    private String app_version_msg;
    private String artist_search;
    private String coupon_offer;
    private String app_update_compulsorily_msg;
    private String download_manager;
    private String wrong_coupon;
    private String common_payment_failed;
    private String download_initiated;
    private String app_share;
    private String download_already;
    private String max_download;
    private int max_download_size;
    private String download_setting;
    private String payment_ssl_alert;
    private IndianStatesPojo indian_states;
    private String country_name;
    private String state_name;
    private String state_of_residence;

    public DTypePojo getD_type() {
        return d_type;
    }

    public void setD_type(DTypePojo d_type) {
        this.d_type = d_type;
    }

    public CTypePojo getC_type() {
        return c_type;
    }

    public void setC_type(CTypePojo c_type) {
        this.c_type = c_type;
    }

    public SongTypePojo getSong_type() {
        return song_type;
    }

    public void setSong_type(SongTypePojo song_type) {
        this.song_type = song_type;
    }

    public OffsetPojo getOffset() {
        return offset;
    }

    public void setOffset(OffsetPojo offset) {
        this.offset = offset;
    }

    public DevotionalConfigPojo getDev_song() {
        return dev_song;
    }

    public void setDev_song(DevotionalConfigPojo dev_song) {
        this.dev_song = dev_song;
    }

    public String getStream_url() {
        return stream_url;
    }

    public void setStream_url(String stream_url) {
        this.stream_url = stream_url;
    }

    public int getStream_time_second() {
        return stream_time_second;
    }

    public void setStream_time_second(int stream_time_second) {
        this.stream_time_second = stream_time_second;
    }

    public boolean isIncentivization_popup() {
        return incentivization_popup;
    }

    public void setIncentivization_popup(boolean incentivization_popup) {
        this.incentivization_popup = incentivization_popup;
    }

    public String getDefault_artist() {
        return default_artist;
    }

    public void setDefault_artist(String default_artist) {
        this.default_artist = default_artist;
    }

    public String getAnalytics_log_url() {
        return analytics_log_url;
    }

    public void setAnalytics_log_url(String analytics_log_url) {
        this.analytics_log_url = analytics_log_url;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getForgotpassword1() {
        return forgotpassword1;
    }

    public void setForgotpassword1(String forgotpassword1) {
        this.forgotpassword1 = forgotpassword1;
    }

    public String getForgotpassword2() {
        return forgotpassword2;
    }

    public void setForgotpassword2(String forgotpassword2) {
        this.forgotpassword2 = forgotpassword2;
    }

    public String getBulk_url() {
        return bulk_url;
    }

    public void setBulk_url(String bulk_url) {
        this.bulk_url = bulk_url;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public String getFail() {
        return fail;
    }

    public void setFail(String fail) {
        this.fail = fail;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getContact_us() {
        return contact_us;
    }

    public void setContact_us(String contact_us) {
        this.contact_us = contact_us;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSong_preview_text() {
        return song_preview_text;
    }

    public void setSong_preview_text(String song_preview_text) {
        this.song_preview_text = song_preview_text;
    }

    public String getMp3_price() {
        return mp3_price;
    }

    public void setMp3_price(String mp3_price) {
        this.mp3_price = mp3_price;
    }

    public String getHp_price() {
        return hp_price;
    }

    public void setHp_price(String hp_price) {
        this.hp_price = hp_price;
    }

    public String getDownload_complete() {
        return download_complete;
    }

    public void setDownload_complete(String download_complete) {
        this.download_complete = download_complete;
    }

    public int getAlbum_song_limit() {
        return album_song_limit;
    }

    public void setAlbum_song_limit(int album_song_limit) {
        this.album_song_limit = album_song_limit;
    }

    public AppVersionPojo getApp_version() {
        return app_version;
    }

    public void setApp_version(AppVersionPojo app_version) {
        this.app_version = app_version;
    }

    public String getResetpassword_text() {
        return resetpassword_text;
    }

    public void setResetpassword_text(String resetpassword_text) {
        this.resetpassword_text = resetpassword_text;
    }

    public String getEmailsent_text() {
        return emailsent_text;
    }

    public void setEmailsent_text(String emailsent_text) {
        this.emailsent_text = emailsent_text;
    }

    public String getAbout_us() {
        return about_us;
    }

    public void setAbout_us(String about_us) {
        this.about_us = about_us;
    }

    public String getTerms_of_service() {
        return terms_of_service;
    }

    public void setTerms_of_service(String terms_of_service) {
        this.terms_of_service = terms_of_service;
    }

    public String getPrivacy_policy() {
        return privacy_policy;
    }

    public void setPrivacy_policy(String privacy_policy) {
        this.privacy_policy = privacy_policy;
    }

    public String getFaq() {
        return faq;
    }

    public void setFaq(String faq) {
        this.faq = faq;
    }

    public String getBlank_email() {
        return blank_email;
    }

    public void setBlank_email(String blank_email) {
        this.blank_email = blank_email;
    }

    public String getBlank_password() {
        return blank_password;
    }

    public void setBlank_password(String blank_password) {
        this.blank_password = blank_password;
    }

    public String getCreatepassword_text() {
        return createpassword_text;
    }

    public void setCreatepassword_text(String createpassword_text) {
        this.createpassword_text = createpassword_text;
    }

    public String getSignout_popup() {
        return signout_popup;
    }

    public void setSignout_popup(String signout_popup) {
        this.signout_popup = signout_popup;
    }

    public String getPassword_reset_popup() {
        return password_reset_popup;
    }

    public void setPassword_reset_popup(String password_reset_popup) {
        this.password_reset_popup = password_reset_popup;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    public String getShort_length_password_popup() {
        return short_length_password_popup;
    }

    public void setShort_length_password_popup(String short_length_password_popup) {
        this.short_length_password_popup = short_length_password_popup;
    }

    public String getCancel_transaction_popup() {
        return cancel_transaction_popup;
    }

    public void setCancel_transaction_popup(String cancel_transaction_popup) {
        this.cancel_transaction_popup = cancel_transaction_popup;
    }

    public String getTransaction_failed_popup() {
        return transaction_failed_popup;
    }

    public void setTransaction_failed_popup(String transaction_failed_popup) {
        this.transaction_failed_popup = transaction_failed_popup;
    }

    public String getValid_email() {
        return valid_email;
    }

    public void setValid_email(String valid_email) {
        this.valid_email = valid_email;
    }

    public String getRemove_artist() {
        return remove_artist;
    }

    public void setRemove_artist(String remove_artist) {
        this.remove_artist = remove_artist;
    }

    public String getCoupon_expired() {
        return coupon_expired;
    }

    public void setCoupon_expired(String coupon_expired) {
        this.coupon_expired = coupon_expired;
    }

    public String getSign_required_download() {
        return sign_required_download;
    }

    public void setSign_required_download(String sign_required_download) {
        this.sign_required_download = sign_required_download;
    }

    public String getInternat_fail() {
        return internat_fail;
    }

    public void setInternat_fail(String internat_fail) {
        this.internat_fail = internat_fail;
    }

    public String getProfile_update_name() {
        return profile_update_name;
    }

    public void setProfile_update_name(String profile_update_name) {
        this.profile_update_name = profile_update_name;
    }

    public String getProfile_update_invalid_image() {
        return profile_update_invalid_image;
    }

    public void setProfile_update_invalid_image(String profile_update_invalid_image) {
        this.profile_update_invalid_image = profile_update_invalid_image;
    }

    public String getSignin_required() {
        return signin_required;
    }

    public void setSignin_required(String signin_required) {
        this.signin_required = signin_required;
    }

    public String getProfile_update_location() {
        return profile_update_location;
    }

    public void setProfile_update_location(String profile_update_location) {
        this.profile_update_location = profile_update_location;
    }

    public String getServer_error() {
        return server_error;
    }

    public void setServer_error(String server_error) {
        this.server_error = server_error;
    }

    public String getSong_exists_cart() {
        return song_exists_cart;
    }

    public void setSong_exists_cart(String song_exists_cart) {
        this.song_exists_cart = song_exists_cart;
    }

    public String getAlbum_exists_cart() {
        return album_exists_cart;
    }

    public void setAlbum_exists_cart(String album_exists_cart) {
        this.album_exists_cart = album_exists_cart;
    }

    public String getApp_version_msg() {
        return app_version_msg;
    }

    public void setApp_version_msg(String app_version_msg) {
        this.app_version_msg = app_version_msg;
    }

    public String getArtist_search() {
        return artist_search;
    }

    public void setArtist_search(String artist_search) {
        this.artist_search = artist_search;
    }

    public String getCoupon_offer() {
        return coupon_offer;
    }

    public void setCoupon_offer(String coupon_offer) {
        this.coupon_offer = coupon_offer;
    }

    public String getApp_update_compulsorily_msg() {
        return app_update_compulsorily_msg;
    }

    public void setApp_update_compulsorily_msg(String app_update_compulsorily_msg) {
        this.app_update_compulsorily_msg = app_update_compulsorily_msg;
    }

    public String getDownload_manager() {
        return download_manager;
    }

    public void setDownload_manager(String download_manager) {
        this.download_manager = download_manager;
    }

    public String getWrong_coupon() {
        return wrong_coupon;
    }

    public void setWrong_coupon(String wrong_coupon) {
        this.wrong_coupon = wrong_coupon;
    }

    public String getCommon_payment_failed() {
        return common_payment_failed;
    }

    public void setCommon_payment_failed(String common_payment_failed) {
        this.common_payment_failed = common_payment_failed;
    }

    public String getDownload_initiated() {
        return download_initiated;
    }

    public void setDownload_initiated(String download_initiated) {
        this.download_initiated = download_initiated;
    }

    public String getApp_share() {
        return app_share;
    }

    public void setApp_share(String app_share) {
        this.app_share = app_share;
    }

    public String getDownload_already() {
        return download_already;
    }

    public void setDownload_already(String download_already) {
        this.download_already = download_already;
    }

    public String getMax_download() {
        return max_download;
    }

    public void setMax_download(String max_download) {
        this.max_download = max_download;
    }

    public int getMax_download_size() {
        return max_download_size;
    }

    public void setMax_download_size(int max_download_size) {
        this.max_download_size = max_download_size;
    }

    public String getDownload_setting() {
        return download_setting;
    }

    public void setDownload_setting(String download_setting) {
        this.download_setting = download_setting;
    }

    public String getPayment_ssl_alert() {
        return payment_ssl_alert;
    }

    public void setPayment_ssl_alert(String payment_ssl_alert) {
        this.payment_ssl_alert = payment_ssl_alert;
    }

    public IndianStatesPojo getIndian_states() {
        return indian_states;
    }

    public void setIndian_states(IndianStatesPojo indian_states) {
        this.indian_states = indian_states;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getState_of_residence() {
        return state_of_residence;
    }

    public void setState_of_residence(String state_of_residence) {
        this.state_of_residence = state_of_residence;
    }
}
