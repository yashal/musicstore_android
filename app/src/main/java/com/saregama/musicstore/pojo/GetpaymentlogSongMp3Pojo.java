package com.saregama.musicstore.pojo;

/**
 * Created by quepplin1 on 5/24/2016.
 */
public class GetpaymentlogSongMp3Pojo {

    private String song_id;
    private String isrc;

    public String getIsrc() {
        return isrc;
    }

    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    public String getSong_id() {
        return song_id;
    }

    public void setSong_id(String song_id) {
        this.song_id = song_id;
    }
}
