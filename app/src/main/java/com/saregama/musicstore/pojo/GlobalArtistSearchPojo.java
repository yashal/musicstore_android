package com.saregama.musicstore.pojo;

/**
 * Created by quepplin1 on 5/13/2016.
 */
public class GlobalArtistSearchPojo extends BasePojo {

    private GlobalArtistSearchDataPojo data;

    public GlobalArtistSearchDataPojo getData() {
        return data;
    }

    public void setData(GlobalArtistSearchDataPojo data) {
        this.data = data;
    }
  }
