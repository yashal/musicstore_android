package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 4/25/2016.
 */
public class ProfileDataPojo {

    private String listener_id;
    private String name;
    private String fname;
    private String image;
    private String email;
    private String city;
    private String state;

    public String getListener_id() {
        return listener_id;
    }

    public void setListener_id(String listener_id) {
        this.listener_id = listener_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
