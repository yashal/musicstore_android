package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by navneet on 7/3/2016.
 */
public class MP3HindiSongDataPojo {

    ArrayList<MP3HindiSongListPojo> list;
    private int count;

    public ArrayList<MP3HindiSongListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<MP3HindiSongListPojo> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
