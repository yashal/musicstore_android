package com.saregama.musicstore.pojo;

/**
 * Created by quepplin1 on 5/12/2016.
 */
public class GlobalAlbumListPojo {

    private String id;
    private String name;
    private String image;
    private String song_count;
    private String price;
    private String price_hd;
    private String currency;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSong_count() {
        return song_count;
    }

    public void setSong_count(String song_count) {
        this.song_count = song_count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_hd() {
        return price_hd;
    }

    public void setPrice_hd(String price_hd) {
        this.price_hd = price_hd;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
