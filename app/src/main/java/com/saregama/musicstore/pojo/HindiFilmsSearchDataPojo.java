package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 27-Jul-16.
 */
public class HindiFilmsSearchDataPojo {

    private SearchSongListPojo song;
    private SearchSongListPojo bhajans;
    private SearchSongListPojo aartis;
    private SearchSongListPojo mantras;
    private SearchAlbumListPojo album;
    private SearchArtisteListPojo artist;
    private SearchInstrumentListPojo raagas;
    private SearchInstrumentListPojo instruments;
    private SearchAlbumListPojo geetmala;
    private SearchSongListPojo pop;
    private SearchSongListPojo folk;

    public SearchSongListPojo getSong() {
        return song;
    }

    public void setSong(SearchSongListPojo song) {
        this.song = song;
    }

    public SearchAlbumListPojo getAlbum() {
        return album;
    }

    public void setAlbum(SearchAlbumListPojo album) {
        this.album = album;
    }

    public SearchArtisteListPojo getArtist() {
        return artist;
    }

    public void setArtist(SearchArtisteListPojo artist) {
        this.artist = artist;
    }

    public SearchSongListPojo getBhajans() {
        return bhajans;
    }

    public void setBhajans(SearchSongListPojo bhajans) {
        this.bhajans = bhajans;
    }

    public SearchSongListPojo getAartis() {
        return aartis;
    }

    public void setAartis(SearchSongListPojo aartis) {
        this.aartis = aartis;
    }

    public SearchAlbumListPojo getGeetmala() {
        return geetmala;
    }

    public void setGeetmala(SearchAlbumListPojo geetmala) {
        this.geetmala = geetmala;
    }

    public SearchSongListPojo getMantras() {
        return mantras;
    }

    public void setMantras(SearchSongListPojo mantras) {
        this.mantras = mantras;
    }

    public SearchSongListPojo getPop() {
        return pop;
    }

    public void setPop(SearchSongListPojo pop) {
        this.pop = pop;
    }

    public SearchSongListPojo getFolk() {
        return folk;
    }

    public void setFolk(SearchSongListPojo folk) {
        this.folk = folk;
    }

    public SearchInstrumentListPojo getRaagas() {
        return raagas;
    }

    public void setRaagas(SearchInstrumentListPojo raagas) {
        this.raagas = raagas;
    }

    public SearchInstrumentListPojo getInstruments() {
        return instruments;
    }

    public void setInstruments(SearchInstrumentListPojo instruments) {
        this.instruments = instruments;
    }
}
