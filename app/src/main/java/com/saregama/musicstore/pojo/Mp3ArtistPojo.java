package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 16/3/2016.
 */
public class Mp3ArtistPojo extends BasePojo {

    private Mp3ArtistDataPojo data;

    public Mp3ArtistDataPojo getData() {
        return data;
    }

    public void setData(Mp3ArtistDataPojo data) {
        this.data = data;
    }
}
