package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 5/18/2016.
 */
public class CommonUrlPOjo {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
