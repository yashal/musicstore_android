package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 5/12/2016.
 */
public class GlobalSongsDataPojo {

    private ArrayList<MP3HindiSongListPojo> list;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<MP3HindiSongListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<MP3HindiSongListPojo> list) {
        this.list = list;
    }


}
