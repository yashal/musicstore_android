package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 7/3/2016.
 */
public class MP3HindiSongPojo extends BasePojo {

    private MP3HindiSongDataPojo data;

    public MP3HindiSongDataPojo getData() {
        return data;
    }

    public void setData(MP3HindiSongDataPojo data) {
        this.data = data;
    }
}
