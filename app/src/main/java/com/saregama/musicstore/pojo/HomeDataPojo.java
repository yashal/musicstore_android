package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by Arpit on 24-Feb-16.
 */
public class HomeDataPojo extends BasePojo{

    private ArrayList<HomeBannerPojo> data;

    public ArrayList<HomeBannerPojo> getData() {
        return data;
    }

    public void setData(ArrayList<HomeBannerPojo> data) {
        this.data = data;
    }
}
