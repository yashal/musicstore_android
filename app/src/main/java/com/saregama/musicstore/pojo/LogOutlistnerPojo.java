package com.saregama.musicstore.pojo;

/**
 * Created by quepplin1 on 5/25/2016.
 */
public class LogOutlistnerPojo {

    private String id;
    private String logout;

    public String getLogout() {
        return logout;
    }

    public void setLogout(String logout) {
        this.logout = logout;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
