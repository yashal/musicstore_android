package com.saregama.musicstore.pojo;

/**
 * Created by navneet on 14/3/2016.
 */
public class ClassHindArtistListPojo {

    private String id;
    private String name;
    private String role;
    private String d_type;
    private String d_type_name;
    private String song_count;
    private String image;

    public String getSong_count() {
        return song_count;
    }

    public void setSong_count(String song_count) {
        this.song_count = song_count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getD_type() {
        return d_type;
    }

    public void setD_type(String d_type) {
        this.d_type = d_type;
    }

    public String getD_type_name() {
        return d_type_name;
    }

    public void setD_type_name(String d_type_name) {
        this.d_type_name = d_type_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
