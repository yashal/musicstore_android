package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 6/23/2017.
 */

public class IndianStatesPojo {

    private ArrayList<String> state_name;

    public ArrayList<String> getState_name() {
        return state_name;
    }

    public void setState_name(ArrayList<String> state_name) {
        this.state_name = state_name;
    }
}
