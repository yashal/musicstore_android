package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 18-Apr-16.
 */
public class OffsetPojo {
    private int START, LIMIT;

    public int getSTART() {
        return START;
    }

    public void setSTART(int START) {
        this.START = START;
    }

    public int getLIMIT() {
        return LIMIT;
    }

    public void setLIMIT(int LIMIT) {
        this.LIMIT = LIMIT;
    }
}
