package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class DownloadManagerPojo extends BasePojo {

    private DownloadManagerDataPojo data;

    public DownloadManagerDataPojo getData() {
        return data;
    }

    public void setData(DownloadManagerDataPojo data) {
        this.data = data;
    }
}
