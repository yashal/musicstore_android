package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 13-Apr-16.
 */
public class RemoveCartDataPojo {

    private String id;
    private String c_type;
    private String listener_id;
    private String guest_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getC_type() {
        return c_type;
    }

    public void setC_type(String c_type) {
        this.c_type = c_type;
    }

    public String getListener_id() {
        return listener_id;
    }

    public void setListener_id(String listener_id) {
        this.listener_id = listener_id;
    }

    public String getGuest_id() {
        return guest_id;
    }

    public void setGuest_id(String guest_id) {
        this.guest_id = guest_id;
    }
}
