package com.saregama.musicstore.pojo;

/**
 * Created by arpit on 11/25/2016.
 */

public class RecommendationsPojo extends BasePojo{
    private RecommendationsDataPojo data;

    public RecommendationsDataPojo getData() {
        return data;
    }

    public void setData(RecommendationsDataPojo data) {
        this.data = data;
    }
}
