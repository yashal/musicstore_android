package com.saregama.musicstore.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 5/12/2016.
 */
public class GlobalAlbumDataPojo {

    private ArrayList<GlobalAlbumListPojo> list;
    private int count;

    public ArrayList<GlobalAlbumListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<GlobalAlbumListPojo> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


}
