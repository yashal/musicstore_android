package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 26-Apr-16.
 */
public class ItemsAddedInCartPojo {

    private String type;
    private String id;
    private boolean isLoggedIn = false;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }
}
