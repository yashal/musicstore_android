package com.saregama.musicstore.pojo;

/**
 * Created by Administrator on 23-Feb-16.
 */
public class GlobalImagePathPojo {

    private String s, t, m, l;

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }
}
