package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 4/20/2016.
 */
public class ApplyCouponPojo extends BasePojo {

    private ApplyCouponDataPojo data;

    public ApplyCouponDataPojo getData() {
        return data;
    }

    public void setData(ApplyCouponDataPojo data) {
        this.data = data;
    }
}
