package com.saregama.musicstore.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 10-Mar-16.
 */
public class ArtistDataPojo implements Serializable{
    private ArrayList<ArtistListPojo> list;
    private int count;

    public ArrayList<ArtistListPojo> getList() {
        return list;
    }

    public void setList(ArrayList<ArtistListPojo> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
