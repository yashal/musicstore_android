package com.saregama.musicstore.pojo;

/**
 * Created by Yash ji on 9/26/2016.
 */

public class ResetPasswordDataPojo {

    private String email;
    private String mobile;
    private String access;
    private String msg;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
