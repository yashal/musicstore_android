package com.saregama.musicstore.pojo;

/**
 * Created by yash on 6/23/2017.
 */

public class GetSateDataPojo {

    private String state;
    private int listener_id;
    private boolean status = false;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getListener_id() {
        return listener_id;
    }

    public void setListener_id(int listener_id) {
        this.listener_id = listener_id;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
