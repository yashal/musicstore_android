package com.saregama.musicstore.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.activity.BaseActivity;
import com.saregama.musicstore.activity.ResetPassword;
import com.saregama.musicstore.adapter.RecommendadedAlbumAdapter;
import com.saregama.musicstore.adapter.RecommendadedSongAdapter;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;

import java.util.ArrayList;

/**
 * Created by Administrator on 15-Feb-16.
 */
public class SaregamaDialogs {

    private Activity ctx;
    public static final String PREFS_NAME = "MyPrefsFile1";

    public static ProgressDialog showLoading(Activity activity) {
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        if (!activity.isFinishing() && !mProgressDialog.isShowing())
            mProgressDialog.show();
        return mProgressDialog;
    }

    public SaregamaDialogs(Activity ctx) {
        this.ctx = ctx;
    }

    public void displayCommonDialog(String msg) {
        Button OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog_withheader);

        TextView msg_textView = (TextView) DialogLogOut.findViewById(R.id.text_exit);

        msg_textView.setText(msg);
        OkButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_yes_exit);

        ImageView dialog_header_cross = (ImageView) DialogLogOut.findViewById(R.id.dialog_header_cross);
        dialog_header_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogLogOut.dismiss();
            }
        });

        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });

        if (ctx != null && !ctx.isFinishing())
            DialogLogOut.show();
    }

    public void displayCommonDialogWithHeader(final String header, String msg) {
        Button OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog_small);
        if (!DialogLogOut.isShowing()) {
            TextView msg_textView = (TextView) DialogLogOut.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) DialogLogOut.findViewById(R.id.dialog_header);
            if (!header.equals(""))
                dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(msg);
            dialog_header.setText(header);
            OkButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_yes_exit);

            ImageView dialog_header_cross = (ImageView) DialogLogOut.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (header.equals(SaregamaConstants.LINK_EXPIRED)) {
                        ResetPassword act = (ResetPassword) ctx;
                        act.navigateToForgotPassword();
                    }
                    DialogLogOut.dismiss();
                }
            });

            OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (header.equals(SaregamaConstants.LINK_EXPIRED)) {
                        ResetPassword act = (ResetPassword) ctx;
                        act.navigateToForgotPassword();
                    }
                    DialogLogOut.dismiss();
                }
            });
            if (ctx != null && !ctx.isFinishing())
                DialogLogOut.show();
        }
    }

    public void displayCommonDialogWithHeaderSmall(String header, String msg) {
        Button OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog_small);
        if (!DialogLogOut.isShowing()) {
            TextView msg_textView = (TextView) DialogLogOut.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) DialogLogOut.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(msg);
            dialog_header.setText(header);
            OkButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_yes_exit);
            ImageView dialog_header_cross = (ImageView) DialogLogOut.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogLogOut.dismiss();
                }
            });

            OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    DialogLogOut.dismiss();
                }
            });
            if (ctx != null && !ctx.isFinishing())
                DialogLogOut.show();
        }
    }

    public void displayDialogWithCancel(String msg, final String from) {
        Button OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog);
        TextView msg_textView = (TextView) DialogLogOut.findViewById(R.id.text_exit);
        msg_textView.setText(msg);
        OkButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_yes_exit);
        Button CancelButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_no_exit);

        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();

            }
        });
        CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });
        DialogLogOut.show();
    }

    public void displayHDDialog(String msg) {
        LinearLayout OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.hd_dialog);
        TextView msg_textView = (TextView) DialogLogOut.findViewById(R.id.text_hd);
        ImageView hd_dialog_cross = (ImageView) DialogLogOut.findViewById(R.id.hd_dialog_cross);
        msg_textView.setText(msg);

        final CheckBox dont_show = (CheckBox) DialogLogOut.findViewById(R.id.donot_show);

        OkButtonLogout = (LinearLayout) DialogLogOut.findViewById(R.id.hd_dialog_ok);

        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String checkBoxResult = "NOT checked";
                if (dont_show.isChecked())
                    checkBoxResult = "checked";
                SharedPreferences settings = ((BaseActivity) ctx).getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("skipMessage", checkBoxResult);
                // Commit the edits!
                editor.commit();
                DialogLogOut.dismiss();
            }
        });

        hd_dialog_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });

        DialogLogOut.show();
    }

    public void displayRecommendedDialog(ArrayList<MP3HindiSongListPojo> listRecommendation, String c_type) {

        LinearLayout recommend_close, recommend_continueLL;
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        final Dialog DialogRecommendedation = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogRecommendedation.setContentView(R.layout.dialog_recommendation);

        Window window = DialogRecommendedation.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        recommend_close = (LinearLayout) DialogRecommendedation.findViewById(R.id.recommend_close);
        recommend_continueLL = (LinearLayout) DialogRecommendedation.findViewById(R.id.recommend_continueLL);
        mRecyclerView = (RecyclerView) DialogRecommendedation.findViewById(R.id.recommend_recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

        RecommendadedSongAdapter recommendationAdapter = new RecommendadedSongAdapter(ctx, listRecommendation, c_type);
        mRecyclerView.setAdapter(recommendationAdapter);
        recommendationAdapter.notifyDataSetChanged();

        recommend_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogRecommendedation.dismiss();
            }
        });

        recommend_continueLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogRecommendedation.dismiss();
            }
        });

        DialogRecommendedation.show();
    }

    public void displayRecommendedDialogAlbum(ArrayList<MP3HindiAlbumListPojo> listRecommendation, String c_type) {

        LinearLayout recommend_close, recommend_continueLL;
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        final Dialog DialogRecommendedation = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogRecommendedation.setContentView(R.layout.dialog_recommendation);

        Window window = DialogRecommendedation.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        recommend_close = (LinearLayout) DialogRecommendedation.findViewById(R.id.recommend_close);
        recommend_continueLL = (LinearLayout) DialogRecommendedation.findViewById(R.id.recommend_continueLL);
        mRecyclerView = (RecyclerView) DialogRecommendedation.findViewById(R.id.recommend_recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

        RecommendadedAlbumAdapter recommendationAdapter = new RecommendadedAlbumAdapter(ctx, listRecommendation, c_type);
        mRecyclerView.setAdapter(recommendationAdapter);
        recommendationAdapter.notifyDataSetChanged();

        recommend_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogRecommendedation.dismiss();
            }
        });

        recommend_continueLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogRecommendedation.dismiss();
            }
        });

        DialogRecommendedation.show();
    }

}
