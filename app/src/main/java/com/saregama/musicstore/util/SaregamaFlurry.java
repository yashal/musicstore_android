package com.saregama.musicstore.util;

import android.app.Application;

import com.flurry.android.FlurryAgent;
import com.saregama.musicstore.R;
import com.splunk.mint.Mint;

/**
 * Created by quepplin1 on 5/17/2016.
 */
public class SaregamaFlurry extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

      /*  new FlurryAgent.Builder()
                .withLogEnabled(false)
                .build(this, String.valueOf(R.string.flurry_id));*/

        // configure Flurry
        FlurryAgent.setLogEnabled(false);

        // init Flurry
        FlurryAgent.init(this, getResources().getString(R.string.flurry_id));

        FlurryAgent.onStartSession(this, getResources().getString(R.string.flurry_id));

        Mint.initAndStartSession(this, "e02fb00a");
    }


}
