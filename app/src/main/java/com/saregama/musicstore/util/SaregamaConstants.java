package com.saregama.musicstore.util;

import android.app.Activity;

import com.saregama.musicstore.activity.CartActivity;

import java.util.ArrayList;

/**
 * Created by Arpit on 15-Feb-16.
 */
public class SaregamaConstants {

    public final static String PREF_NAME = "com.saregama.musicstore.prefs";
    //    Staging URL's
//    public static String BASE_URL = "http://sapi.saregama.com/api/";
//    public static String PAYMENT_URL = "http://sapi.saregama.com/api/payment/web/coupon?";
//    public static String DOWNLOAD_URL = "http://sarega.ma/dnld";
//        Live URL's
    public static String BASE_URL = "http://api.saregama.com/api/";
    public static String PAYMENT_URL = "http://api.saregama.com/api/payment/web/coupon?";
    public static String DOWNLOAD_URL = "http://sarega.ma/dnld";
    public static String BASE_URL_ANALYTICS;
    public final static String DEFAULT_VALUE = "";
    public final static String EMAIL = "email";
    public final static String NAME = "name";
    public final static String CITY = "city";
    public final static String SESSION_ID = "session_id";
    public final static String MOBILE = "mobile";
    public final static String ID = "id";
    public final static String CART = "cart";
    public final static String DOWNLOAD = "download";
    public final static String USERIMAGE = "userimage";
    public final static String IMEI = "imei";
    public static String HEADER = "";
    public static boolean STATUS = false;
    public static String D_TYPE = "d_type";
    public static String COUPON_CODE = "coupon_code";
    public static String COUNTER = "counter";
    public static String LOGGEDINFROM = "logged_in_from";
    public static String REGISTRATION_FAILED = "Registration Failed";
    public static String LOGIN_FAILED = "Sign in Failed";
    public static String STATE_OF_RESIDENCE = "State of Residence";
    public static String Number_FAILED = "Failed";
    public static String PROFILE_UPDATE_FAILED = "Profile Update Failed";
    public static String FORGOT_PASSWORD = "Forgot Password";
    public static String PASSWORD_MISMATCH = "Password Mismatch";
    public static String INTERNET_FAIL = "Internet Failure";
    public static String NO_DATA = "No Data Available";
    public static String NO_RESULT = "No search result found";
    public static CartActivity CART_ACTIVITY = null;
    /*
    DEVICEID, SENT_TOKEN_TO_SERVER, REGISTRATION_COMPLETE, REFRESH_NEEDED,
    MP3, HD, PLAYING_SONG_ID, PLAYING_SONG_TYPE
    */
    public final static String DEVICEID = "deviceId";
    public static final String REFRESH_NEEDED = "refreshNeeded";
    public static final String MP3 = "mp3";
    public static final String HD = "hd";
    public static final String PLAYING_SONG_ID = "playing_song_id";
    public static final String PLAYING_SONG_TYPE = "playing_song_type";
    /*LIMIT is used to fetch the number of results at a time and is manageable from backend through AppConfig API*/
    public static int LIMIT = 20;
    public static int ARTIST_LIMIT = 30;
    public static ArrayList<Activity> ACTIVITIES = new ArrayList<>();
    public static int PLAYING_TIME = 90000;
    public static int KB_TO_MB = 1024;
    public static long REMAINING_TIME;
    public static String song_id;
    public static int pos;
    /*SPECIAL is used when # is selected from the Alphabets List */
    public static String SPECIAL = "special";
    /*ALL is used when ALL is selected from the Alphabets List */
    public static String ALL = "";
    public static String PLAYING_SONG_ALBUM_ID;
    public static int PLAYING_SONG_C_TYPE;
    public static float SEEkTO;
    public static float PERCENTAGE;
    public static boolean IS_PLAYING = false, IS_PAUSED = false, IS_STOPPED = false, IS_PLAYING_WHEN_CALL_RECEIVED = false, IS_PLAYING_WHEN_SONG_PLAYED = false;

    /*TAB1 is used to fetch the list of songs and albums in Download now fragment*/
    public final static String TAB1 = "1";
    /*TAB2 is used to fetch the list of songs and albums in Downloaded fragment*/
    public final static String TAB2 = "2";
    public final static String DOWNLOAD_SIZE_ALERT = "download_size_alert";
    public final static String COUNTRY = "COUNTRY";
    public final static String IP = "IP";
    public final static String STATE = "STATE";
    public final static String CITY_USER = "CITY_USER";
    public final static String MUSIC_APP = "musicapp";
    public final static String METHOD = "get";
    public static String NOTIFICATION_SIZE = "notificationsize";
    public static String DOWNLOAD_SIZE = "downloadsize";
    public static String DOWNLOADED_SIZE = "downloadedSize";
    /*ORDER is used to fetch the popular artistes*/
    public static String ORDER = "top";
    public static int Artist = 3;
    // SONG_LIMIT is used to check if the number of songs in an album is less than 3 than it can't be purchased as an album.
    public static int SONG_LIMIT = 3;
    public final static String COUPON_APPLIED_TEXT = "The Code \nhas been applied";

    /*ID_UP & ID_DOWN are used for showing the Add to Cart Dialog*/
    public static final int ID_UP = 1;
    public static final int ID_DOWN = 2;

    public static String PLAYING_SONG_CURRENCY;
    public static String PLAYING_SONG_PRICE;
    public static String PLAYING_SONG_PRICE_HD;

    public static String UPDATE_DIALOG_TITLE = "App Update Available";
    public static String UPDATE_DIALOG_POSITIVE_BUTTON = "Update";
    public static String UPDATE_DIALOG_NEGATIVE_BUTTON = "Skip";
    public static String APP_NOT_AVAILABLE = "Sorry, unable to find market app.";

    public static String LINK_EXPIRED = "Link Expired";
    public static String RESET_PASSWORD = "Reset Password";

    public static int AVAIL_OFFERS = 0;

    public static String LOGIN_ERROR = "Please try again.";

    public static String[] items = {"All", "#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
            "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public static String[] artist_arr = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
            "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
}
