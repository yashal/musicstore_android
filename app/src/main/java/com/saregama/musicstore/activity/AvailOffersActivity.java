package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.AvailOffersAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.AvailOfferPojo;
import com.saregama.musicstore.pojo.OfferDataPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AvailOffersActivity extends BaseActivity implements View.OnClickListener {

    private Activity ctx = this;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private LinearLayout global_search, no_offers_layout;
    private RelativeLayout cart_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avail_offers);

        setDrawerAndToolbar("Avail Offers");
        SaregamaConstants.ACTIVITIES.add(ctx);

        cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        global_search = (LinearLayout) findViewById(R.id.global_search);

        mRecyclerView = (RecyclerView) findViewById(R.id.avail_offers_recycler_view);
        no_offers_layout = (LinearLayout) findViewById(R.id.no_offers_layout);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(ctx);

        dialog = new SaregamaDialogs(ctx);

        if (getIntent().getStringExtra("pay_cart") != null && getIntent().getStringExtra("pay_cart").equalsIgnoreCase("cart")) {
            cart_layout.setVisibility(View.GONE);
            global_search.setVisibility(View.GONE);
            notification_layout.setVisibility(View.GONE);
        } else {
            cart_layout.setVisibility(View.VISIBLE);
            global_search.setVisibility(View.VISIBLE);
        }
        getAvailOffers();
    }

    public void getAvailOffers() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getavailOffers(new Callback<AvailOfferPojo>() {
                @Override
                public void success(AvailOfferPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            if (basePojo.getData() != null) {
                                if (basePojo.getData().size() > 0) {
                                    SaregamaConstants.AVAIL_OFFERS = basePojo.getData().size();
                                    mAdapter = new AvailOffersAdapter(ctx, basePojo.getData());
                                    mRecyclerView.setAdapter(mAdapter);
                                    no_offers_layout.setVisibility(View.GONE);
                                } else {
                                    SaregamaConstants.AVAIL_OFFERS = 0;
                                    no_offers_layout.setVisibility(View.VISIBLE);
                                }
                            } else {
                                SaregamaConstants.AVAIL_OFFERS = 0;
                                no_offers_layout.setVisibility(View.VISIBLE);
                            }
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    @Override
    public void onClick(View v) {

        OfferDataPojo data = (OfferDataPojo) v.getTag(R.string.data);

        // Show dialog to display Offer Details
        displayOfferDialogOffers(data);
    }

    /* Displays the Detail of the Offer in Dialog */
    private void displayOfferDialogOffers(final OfferDataPojo data) {
        Button apply_offer;
//        final Dialog offerDialog = new Dialog(this, R.style.PauseDialog);
        final Dialog offerDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        offerDialog.setContentView(R.layout.offer_dialog_layout);

        TextView offer_title = (TextView) offerDialog.findViewById(R.id.offer_title);
        TextView offer_sub_title = (TextView) offerDialog.findViewById(R.id.offer_sub_title);
        TextView offer_terms = (TextView) offerDialog.findViewById(R.id.offer_terms);
        TextView offer_validity = (TextView) offerDialog.findViewById(R.id.offer_validity);
        offer_title.setText(data.getTitle());
        offer_sub_title.setText(data.getSub_title());
        if (data.getTerm() != null)
            offer_terms.setText("Terms: " + data.getTerm());
        else
            offer_terms.setVisibility(View.GONE);
        offer_validity.setText("Offer valid till " + data.getExpiry_date());
        apply_offer = (Button) offerDialog.findViewById(R.id.apply_offer);
        ImageView cross = (ImageView) offerDialog.findViewById(R.id.cross);
        EditText coupon_code = (EditText) offerDialog.findViewById(R.id.coupon_code);

        coupon_code.setText(data.getCoupon_code());

        apply_offer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                offerDialog.dismiss();
                saveIntoPrefs(SaregamaConstants.COUPON_CODE, data.getCoupon_code());
//                Toast.makeText(getApplicationContext(), "Coupon code " + data.getCoupon_code() + " copied", Toast.LENGTH_SHORT).show();
                dialog.displayCommonDialogWithHeader("Coupon Applied", getAppConfigJson().getCoupon_offer());

                if (getIntent().getStringExtra("pay_cart") != null && getIntent().getStringExtra("pay_cart").equalsIgnoreCase("cart")) {
                    finish();
                }
            }
        });
        cross.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                offerDialog.dismiss();
            }
        });
        offerDialog.show();
    }
}
