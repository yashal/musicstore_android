package com.saregama.musicstore.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appsflyer.AppsFlyerLib;
import com.flurry.android.FlurryAgent;
import com.saregama.musicstore.R;
import com.saregama.musicstore.listener.NetworkStateReceiver;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.AppConfigPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.saregama.musicstore.util.SaregamaConstants.IMEI;


public class SplashActivity extends BaseActivity implements NotifyAdapterInterface, NetworkStateReceiver.NetworkStateReceiverListener {

    private String identifier = null;
    private SplashActivity ctx = this;
    private ConnectionDetector cd;
    private Map<String, String> articleParams;
    private Handler splashTimeHandler;
    private Runnable finalizer;

    private int delay = 1000;
    private SaregamaDialogs dialog;
    private NetworkStateReceiver networkStateReceiver;
    private boolean network_available = true;

    private String notification_key, content_id, content_type, url;
    PackageInfo info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String packageName = getApplicationContext().getPackageName();

        AppsFlyerLib.getInstance().startTracking(this.getApplication(), getString(R.string.apps_flyer_id));
        AppsFlyerLib.getInstance().setImeiData(identifier);

        try {
            info = getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                System.out.println("hh hash key " + Base64.encodeToString(md.digest(),
                        Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        SaregamaConstants.ACTIVITIES = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(ctx, R.color.splash_header));
        }

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        dialog = new SaregamaDialogs(ctx);

        cd = new ConnectionDetector(ctx);

        articleParams = new HashMap<>();

//        saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
        saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
        getCountryDetails();
        if (getIntent().getStringExtra("notification_key") != null || getIntent().getStringExtra("content_type") != null || getIntent().getStringExtra("content_id") != null || getIntent().getStringExtra("url") != null) {
            notification_key = getIntent().getStringExtra("notification_key");
            content_id = getIntent().getStringExtra("content_id");
            content_type = getIntent().getStringExtra("content_type");
            url = getIntent().getStringExtra("url");

            navigateToPush(url, notification_key);
            finish();

        } else {
            appConfigDetails();
        }

//        if (getFromPrefs(SaregamaConstants.MP3) == null || getFromPrefs(SaregamaConstants.MP3).isEmpty())
//            saveIntoPrefs(SaregamaConstants.MP3, "checked");
//        if (getFromPrefs(SaregamaConstants.HD) == null || getFromPrefs(SaregamaConstants.HD).isEmpty())
//            saveIntoPrefs(SaregamaConstants.HD, "checked");

    }

    private void getImei() {
        if (identifier == null || identifier.length() == 0)
            identifier = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);

        saveIntoPrefs(IMEI, identifier);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */

    @Override
    protected void onPause() {
        try {
            this.unregisterReceiver(networkStateReceiver);
        } catch (IllegalArgumentException e) {
        }

        super.onPause();
    }

    private void appConfigDetails() {
        getImei();
        if (cd.isConnectingToInternet()) {

            RestClient.get().getAppConfigDetails(new Callback<AppConfigPojo>() {
                @Override
                public void success(AppConfigPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
//                            checkCommonURL(response.getUrl());

                            saveAppConfigData(basePojo.getData());
                            SaregamaConstants.BASE_URL_ANALYTICS = basePojo.getData().getAnalytics_log_url();
                            saveIntoPrefs("analyticsUrl", basePojo.getData().getAnalytics_log_url());
//                            SaregamaConstants.PLAYING_TIME = 400 * 1000;
                            SaregamaConstants.PLAYING_TIME = basePojo.getData().getStream_time_second() * 1000;
                            SaregamaConstants.SONG_LIMIT = basePojo.getData().getAlbum_song_limit();
                            getCartDataFirstTime();
                            getAvailOffers();
                            if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
                                getNotificationsSize();
                                getNumberofDownload();
                                getDownloadedCount();
                            }
                            checkAppUpdate();

                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }

            });
        } else {
            if (getAppConfigJson() != null) {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, "Please check your internet connection and try again." + "\n");
            }

        }
    }

    private void getCountryDetails() {
        if (cd.isConnectingToInternet()) {

            RestClient.get().getGeolocation(new Callback<AppConfigPojo>() {
                @Override
                public void success(AppConfigPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            saveIntoPrefs(SaregamaConstants.COUNTRY, basePojo.getData().getCountry());
                            saveIntoPrefs(SaregamaConstants.IP, basePojo.getData().getIp());
                            saveIntoPrefs(SaregamaConstants.STATE, basePojo.getData().getState());
                            saveIntoPrefs(SaregamaConstants.CITY_USER, basePojo.getData().getCity());

                            String first_time = getFromPrefs("firstTime");

                            if (first_time == null || first_time.length() == 0) {
                                saveIntoPrefs("firstTime", "second");

                                // analytics code for source of installation start
                                // checks value of installationSource if null then debugger app install otherwise from play store
                                PackageManager pm = getPackageManager();
                                String installationSource = pm.getInstallerPackageName(getPackageName());
                                if (installationSource == null || installationSource.equals("null")) {
                                    articleParams.put("installation", "Debugger Mode");
                                } else {
                                    articleParams.put("installation", installationSource);
                                }

                                articleParams.put("device_id", identifier);
                                articleParams.put("installation_country", basePojo.getData().getCountry());

                                FlurryAgent.logEvent("installation_source", articleParams);

                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    private void goAhead() {
        splashTimeHandler = new Handler();
        finalizer = new Runnable() {
            public void run() {
                Intent intent = getIntent();
                Uri data = intent.getData();

                if (data != null && data.toString().contains(SaregamaConstants.DOWNLOAD_URL)) {
                    Intent mainIntent = new Intent(ctx, DownloadManagerActivity.class);
                    startActivity(mainIntent);
                } else {
                    Intent mainIntent = new Intent(ctx, HomeActivity.class);
                    startActivity(mainIntent);
                }
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        };
        splashTimeHandler.postDelayed(finalizer, delay);
    }

    public void navigateToPush(String url, String notification_key) {
        if (notification_key.equalsIgnoreCase("home_mp3")) {
            Intent intent = new Intent(ctx, HomeActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", "home_mp3");
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("more_offer")) {
            Intent intent = new Intent(ctx, AvailOffersActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("album_details")) {
            Intent intent = new Intent(ctx, Mp3HindiAlbumDetail.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("c_type", Integer.valueOf(content_type));
            intent.putExtra("album_id", content_id);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("artist_details")) {
            Intent intent = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("content_type", content_type);
            intent.putExtra("from", "");
            intent.putExtra("content_id", content_id);
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("hindi_films_mp3")) {
            Intent intent = new Intent(ctx, CategoryListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("geetmala_all_mp3") || notification_key.equalsIgnoreCase("geetmala_1950") || notification_key.equalsIgnoreCase("geetmala_1960")
                || notification_key.equalsIgnoreCase("geetmala_1970") || notification_key.equalsIgnoreCase("geetmala_1980") || notification_key.equalsIgnoreCase("geetmala_1990")
                || notification_key.equalsIgnoreCase("geetmala_2000")) {
            Intent intent = new Intent(ctx, Mp3HindiLandingActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("regional_home_mp3")) {
            Intent intent = new Intent(ctx, LanguageListActivity.class);
            intent.putExtra("notification_url", url);
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("regional_telugu_mp3") || notification_key.equalsIgnoreCase("regional_marathi_mp3")
                || notification_key.equalsIgnoreCase("regional_kannada_mp3") || notification_key.equalsIgnoreCase("regional_punjabi_mp3")) {
            Intent intent = new Intent(ctx, Mp3RegionalActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("regional_other_mp3")) {
            Intent intent = new Intent(ctx, Regional_Others.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("classical_home_mp3")) {
            Intent intent = new Intent(ctx, ClassicalListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("hindustani_mp3")
                || notification_key.equalsIgnoreCase("carnatic_mp3")
                || notification_key.equalsIgnoreCase("hinduatani_artist") || notification_key.equalsIgnoreCase("carnatic_artist")) {
            Intent intent = new Intent(ctx, ClassicalHindustaniActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("fusion_mp3")
                || notification_key.equalsIgnoreCase("fusion_artist")) {
            Intent intent = new Intent(ctx, ClassicalFusionListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("rabindra_sangeet_mp3")
                || notification_key.equalsIgnoreCase("nazrul_geet_mp3")) {
            Intent intent = new Intent(ctx, RabindraSangeetActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("ghazals_sufi_mp3")) {
            Intent intent = new Intent(ctx, GajalSufiActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "mp3");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("other_mp3")) {
            Intent intent = new Intent(ctx, OtherLandingListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("remix_mp3")) {
            Intent intent = new Intent(ctx, RemixSongsActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("cart")) {
            Intent intent = new Intent(ctx, CartActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("offer_popup_mp3")) {
            Intent intent = new Intent(ctx, HomeActivity.class);
            intent.putExtra("content_id", content_id);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("download")) {
            if (getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
                Intent intent = new Intent(ctx, DownloadManagerActivity.class);
                intent.putExtra("notification_url", url);
                intent.putExtra("from", "notification");
                startActivity(intent);
            } else {
                Intent intent = new Intent(ctx, LoginActivity.class);
                intent.putExtra("from", "notification");
                intent.putExtra("notification_key", notification_key);
                startActivity(intent);
            }
        } else if (notification_key.equalsIgnoreCase("devotional_artist")) {
            Intent intent = new Intent(ctx, Mp3Devotional.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("devotional_home_mp3")) {
            Intent intent = new Intent(ctx, Mp3Devotional.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("ghazals_sufi_artist")) {
            Intent intent = new Intent(ctx, GajalSufiActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("rabindra_sangeet_artist") || notification_key.equalsIgnoreCase("nazrul_geet_artist")) {
            Intent intent = new Intent(ctx, RabindraSangeetActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("album_regional_telugu") || notification_key.equalsIgnoreCase("album_regional_marathi")
                || notification_key.equalsIgnoreCase("album_regional_kannada") || notification_key.equalsIgnoreCase("album_regional_punjabi")) {
            Intent intent = new Intent(ctx, BuyAnyAlbumListing.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("ghazals_sufi")) {
            Intent intent = new Intent(ctx, GajalSufiActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("album_home_mp3")) {
            Intent intent = new Intent(ctx, CategoryListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("hindi_films")) {
            Intent intent = new Intent(ctx, BuyAnyAlbumHindiLanding.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("album_regional_films")) {
            Intent intent = new Intent(ctx, LanguageListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        }
    }

    @Override
    public void notifyAdapter() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (splashTimeHandler != null && finalizer != null)
            splashTimeHandler.removeCallbacks(finalizer);
    }

    @Override
    public void networkAvailable() {
        if (!network_available) {
            network_available = true;
            getCountryDetails();
            if (getIntent().getStringExtra("notification_key") != null || getIntent().getStringExtra("content_type") != null || getIntent().getStringExtra("content_id") != null || getIntent().getStringExtra("url") != null) {
                notification_key = getIntent().getStringExtra("notification_key");
                content_id = getIntent().getStringExtra("content_id");
                content_type = getIntent().getStringExtra("content_type");
                url = getIntent().getStringExtra("url");

                navigateToPush(url, notification_key);
                finish();

            } else {
                appConfigDetails();
            }
        }
    }

    @Override
    public void networkUnavailable() {
        network_available = false;
    }


    public void checkAppUpdate() {
        PackageInfo pInfo;
        String versionName = "";
        int versionCode = 0;
        int version = 0;
        int serverVersion = 0;
        int serverVersionCode = 0;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = pInfo.versionName;
            versionCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        try {
            version = Integer.parseInt(versionName.replace(".", ""));
            serverVersion = Integer.parseInt(getAppConfigJson().getApp_version().getVersion().replace(".", ""));
            serverVersionCode = getAppConfigJson().getApp_version().getVersion_code();
        } catch (NumberFormatException e) {
            // TODO: handle exception
        }
        if (versionCode != 0 && serverVersionCode != 0 && versionCode < serverVersionCode) {
//        if (version != 0 && serverVersion != 0 && version < serverVersion) {
            if (!getAppConfigJson().getApp_version().isMandatory_update()) {
                // show dialog non mandatory update dialog
                showUpdateDialog(true);
            } else {
                showUpdateDialog(false);
            }
        } else {
            goAhead();
        }
    }

    private void showUpdateDialog(boolean isStatus) {
        Button updateButton;
        final Dialog dialogUpdate = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialogUpdate.setContentView(R.layout.custom_dialog);
        if (!dialogUpdate.isShowing()) {
            TextView msg_textView = (TextView) dialogUpdate.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) dialogUpdate.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);

            dialog_header.setText(SaregamaConstants.UPDATE_DIALOG_TITLE);
            updateButton = (Button) dialogUpdate.findViewById(R.id.btn_yes_exit);
            updateButton.setText(SaregamaConstants.UPDATE_DIALOG_POSITIVE_BUTTON);
            ImageView dialog_header_cross = (ImageView) dialogUpdate.findViewById(R.id.dialog_header_cross);
            LinearLayout btn_no_exit_LL = (LinearLayout) dialogUpdate.findViewById(R.id.btn_no_exit_LL);
            Button doLaterButton = (Button) dialogUpdate.findViewById(R.id.btn_no_exit);
            if (isStatus) {
                msg_textView.setText(getAppConfigJson().getApp_version_msg());
                btn_no_exit_LL.setVisibility(View.VISIBLE);
                dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogUpdate.dismiss();
                        goAhead();
                    }
                });
            } else {
                dialog_header.setText("Mandatory Update");
                updateButton.setText("PROCEED");
                msg_textView.setText(getAppConfigJson().getApp_update_compulsorily_msg());
                btn_no_exit_LL.setVisibility(View.GONE);
                dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogUpdate.dismiss();
                        finish();
                    }
                });
            }
            doLaterButton.setText(SaregamaConstants.UPDATE_DIALOG_NEGATIVE_BUTTON);
            updateButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    launchMarket(ctx);
                    finish();
                }
            });

            doLaterButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogUpdate.dismiss();
                    goAhead();
                }
            });

            dialogUpdate.show();
        }


    }
}
