package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appsflyer.AppsFlyerLib;
import com.flurry.android.FlurryAgent;
import com.saregama.musicstore.R;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.LogInPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Yash ji on 9/6/2016.
 */
public class SetPasswordLoginActivity extends BaseActivity {

    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;
    private String email;
    private Map<String, String> articleParams;
    private String notification_key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resetpassword_login);

        articleParams = new HashMap<>();

        if (getIntent().getStringExtra("email") != null) {
            email = getIntent().getStringExtra("email");
        }
        if (getIntent().getStringExtra("notification_key") != null) {
            notification_key = getIntent().getStringExtra("notification_key");
        }

        dialog = new SaregamaDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());

        final ImageView reset_password_login_back = (ImageView) findViewById(R.id.reset_password_login_back);
        reset_password_login_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        TextView tv_rememberpassword = (TextView) findViewById(R.id.tv_remember_password);
        final CheckBox rememberpassword = (CheckBox) findViewById(R.id.remember_password);

        rememberpassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rememberpassword.setButtonDrawable(R.mipmap.check_box_checked);
                } else {
                    rememberpassword.setButtonDrawable(R.mipmap.check_box_unchecked);
                }
            }
        });

        tv_rememberpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rememberpassword.isChecked()) {
                    rememberpassword.setButtonDrawable(R.mipmap.check_box_unchecked);
                    rememberpassword.setChecked(false);
                } else {
                    rememberpassword.setButtonDrawable(R.mipmap.check_box_checked);
                    rememberpassword.setChecked(true);
                }
            }
        });

        final EditText create_password = (EditText) findViewById(R.id.create_password);
        final EditText confirm_password = (EditText) findViewById(R.id.confirm_password);
        final LinearLayout resetpassword_login_ok = (LinearLayout) findViewById(R.id.resetpassword_login_ok);

        resetpassword_login_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String create_passwordText = create_password.getText().toString();
                String confirm_passwordText = confirm_password.getText().toString();
                if (create_passwordText.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getNew_password());
                } else if (create_passwordText.length() < 6) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getShort_length_password_popup());
                } else if (confirm_passwordText.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getConfirm_password());
                } else if (!create_passwordText.equals(confirm_passwordText)) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.PASSWORD_MISMATCH, getAppConfigJson().getPassword_reset_popup());
                } else {
                    saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "normal");
                    hideSoftKeyboard();
                    doLoginWithEmail(email, create_passwordText);
                    if (rememberpassword.isChecked()) {
                        saveIntoPrefs("password", create_passwordText);
                    } else {
                        saveIntoPrefs("password", "");
                    }
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    private void doLoginWithEmail(final String username, String password) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(SetPasswordLoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().newUserLoginEmail(getFromPrefs(SaregamaConstants.IMEI), username, password, "signin", "email", "true", new Callback<LogInPojo>() {
                @Override
                public void success(LogInPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            // analytics code for source of signin start
                            articleParams.put("sign_in_email", username);
                            articleParams.put("User_Device", "Android");
                            articleParams.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            articleParams.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            articleParams.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            articleParams.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));
                            FlurryAgent.logEvent("sign_up_source", articleParams);

                            Map<String, Object> eventValue = new HashMap<String, Object>();
                            eventValue.put("sign_in_email", username);
                            eventValue.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            eventValue.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            eventValue.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            eventValue.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));
                            AppsFlyerLib.getInstance().trackEvent(ctx, "SignUp", eventValue);

                            // analytics code for source of signin End
                            checkCommonURL(response.getUrl());
                            navigateToHomeActivity(basePojo);
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void navigateToHomeActivity(LogInPojo basePojo) {
        if (basePojo.getData().getListener().getId() != null && basePojo.getData().getListener().getSession_id() != null) {
            saveIntoPrefs(SaregamaConstants.EMAIL, basePojo.getData().getListener().getEmail());
            saveIntoPrefs(SaregamaConstants.NAME, basePojo.getData().getListener().getTitle());
            saveIntoPrefs(SaregamaConstants.MOBILE, basePojo.getData().getListener().getMobile());
            saveIntoPrefs(SaregamaConstants.SESSION_ID, basePojo.getData().getListener().getSession_id());
            saveIntoPrefs(SaregamaConstants.ID, basePojo.getData().getListener().getId());
            saveIntoPrefs(SaregamaConstants.CART, basePojo.getData().getCart());
            saveIntoPrefs(SaregamaConstants.DOWNLOAD, basePojo.getData().getDownload());
            saveIntoPrefs(SaregamaConstants.USERIMAGE, basePojo.getData().getListener().getImage());
            getCartDataFirstTime();
            getNotificationsSize();
            getNumberofDownload();
            getDownloadedCount();
            if (getFromPrefs(SaregamaConstants.IMEI) != null && getFromPrefs(SaregamaConstants.IMEI).length() > 0 && getFromPrefs(SaregamaConstants.SESSION_ID) != null)
                sendRegistrationId(getFromPrefs(SaregamaConstants.DEVICEID), getFromPrefs(SaregamaConstants.IMEI));
            saveIntoPrefs(SaregamaConstants.REFRESH_NEEDED, "yes");
            if (notification_key.equals("download")) {
                Intent mIntent = new Intent(ctx, DownloadManagerActivity.class);
                startActivity(mIntent);
            }
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, SaregamaConstants.LOGIN_ERROR);
        }
    }
}

