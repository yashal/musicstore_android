/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.saregama.musicstore.activity;

import android.content.SharedPreferences;
import android.util.Log;

import com.appsflyer.AppsFlyerLib;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.BasePojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);

        AppsFlyerLib.getInstance().updateServerUninstallToken(getApplicationContext(), refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        saveIntoPrefs(SaregamaConstants.DEVICEID, token);
        sendRegistrationId(getFromPrefs(SaregamaConstants.DEVICEID), getFromPrefs(SaregamaConstants.IMEI));
    }

    private void sendRegistrationId(String gcm_id, String deviceId) {
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {

            RestClient.get().sendRegistrationId(getFromPrefs(SaregamaConstants.SESSION_ID), deviceId, "android", gcm_id, new Callback<BasePojo>() {
                @Override
                public void success(BasePojo basePojo, Response response) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    public void saveIntoPrefs(String key, String value) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        return prefs.getString(key, SaregamaConstants.DEFAULT_VALUE);
    }
}
