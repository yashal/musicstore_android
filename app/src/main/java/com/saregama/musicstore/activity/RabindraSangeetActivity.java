package com.saregama.musicstore.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.RabindraSangeetPagerAdapter;
import com.saregama.musicstore.pojo.ClassHindArtistListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class RabindraSangeetActivity extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {

    private LinearLayout songs, artiste, songs_selector, artiste_selector;
    private TextView songs_text, artiste_text;
    private ViewPager viewPager;
    private Activity ctx = this;
    private RabindraSangeetPagerAdapter mAdapter;

    public RecyclerView.Adapter rabindra_sangeet_adapter;
    public ArrayList<MP3HindiSongListPojo> arrdata;

    private CircularSeekBar seekbar;
    private ImageView previous_view;
    private ImageView imageView;
    private MP3HindiSongListPojo data_playing;
    private int fragment_pos = 0;
    private String notification_key;
    private int c_type;
//    private EditText search;
//    private TextView no_result_found;
//    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rabindra_sangeet);

//        String coming_from = getIntent().getStringExtra("coming_from");
        c_type = getIntent().getIntExtra("c_type", 1);
//
//        if (coming_from != null && coming_from.equals("search")) {
//            LinearLayout page_data = (LinearLayout) findViewById(R.id.page_data);
//            page_data.setVisibility(View.GONE);
//
//            String fragment_pos = getIntent().getStringExtra("fragment_pos");
//
//            if (fragment_pos.equalsIgnoreCase("songs")) {
//                setDrawerAndToolbar("Songs");
//                RabindraSangeetSongFragment fragment = new RabindraSangeetSongFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "RabindraSangeetSongFragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("artiste")) {
//                setDrawerAndToolbar("Artistes");
//                RabindraSangeetArtistesFragment fragment = new RabindraSangeetArtistesFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "RabindraSangeetArtistesFragment").commit();
//            }
//        } else {
        if (getIntent().getStringExtra("from") != null) {
            setDrawerAndToolbarWithDropDown(getIntent().getStringExtra("from"));
        } else {
            setDrawerAndToolbarWithDropDown("");
        }

        SaregamaConstants.ACTIVITIES.add(ctx);

        mAdapter = new RabindraSangeetPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.rabindra_sangeet_pager);
        viewPager.setAdapter(mAdapter);
//            viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.setOffscreenPageLimit(2);

        if (getIntent().getStringExtra("notification_key") != null) {
            notification_key = getIntent().getStringExtra("notification_key");
            if (notification_key.equals("rabindra_sangeet_artist") || notification_key.equals("nazrul_geet_artist")) {
                viewPager.setCurrentItem(1);
                setStyle(1);
            }
        }

//            search = (EditText) findViewById(R.id.search_song);
//            no_result_found = (TextView) findViewById(R.id.no_result_found);
//            softKeyboardDoneClickListener(search);
//            search.setHint("Search Songs, Artistes");
//
//            search.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(ctx, HindiFilmsSearchActivity.class);
//                    intent.putExtra("search_from", getIntent().getStringExtra("from"));
//                    intent.putExtra("c_type", c_type);
//                    startActivity(intent);
//                }
//            });

        songs = (LinearLayout) findViewById(R.id.tab1_layout);
        artiste = (LinearLayout) findViewById(R.id.tab2_layout);

        songs_selector = (LinearLayout) findViewById(R.id.tab1_selector);
        artiste_selector = (LinearLayout) findViewById(R.id.tab2_selector);

        songs_text = (TextView) findViewById(R.id.tab1_text);
        artiste_text = (TextView) findViewById(R.id.tab2_text);

        songs_text.setText(getResources().getString(R.string.songs));
        artiste_text.setText(getResources().getString(R.string.artist));

        songs.setOnClickListener(this);
        artiste.setOnClickListener(this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                setStyle(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
//        }

        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab1_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.tab2_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            case R.id.classhindustani_artist_albumLL:
                hideSoftKeyboard();
                Intent intent = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
                ClassHindArtistListPojo data = (ClassHindArtistListPojo) v.getTag(R.string.data);
                intent.putExtra("artist_id", data.getId());
                intent.putExtra("c_type", c_type);
                intent.putExtra("from", getIntent().getStringExtra("from"));
                intent.putExtra("header", getIntent().getStringExtra("from"));
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
                CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);
                QuickAction quickAction = setupQuickAction(data_addtocart.getSong_id(), c_type);
                quickAction.show(addToCart);
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            default:
                break;
        }
    }

    private void setStyle(int position) {
        fragment_pos = position;
        if (position == 1) {
            songs_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artiste_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));

            artiste_selector.setVisibility(View.VISIBLE);
            songs_selector.setVisibility(View.GONE);
        } else if (position == 0) {
            songs_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            artiste_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            artiste_selector.setVisibility(View.GONE);
            songs_selector.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void notifyAdapter() {
        if (rabindra_sangeet_adapter != null)
            rabindra_sangeet_adapter.notifyDataSetChanged();
    }

    private void playMP3Song(ImageView imageView, View v) {
        hideSoftKeyboard();
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        if (data_playing.is_playing()) {
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            for (int i = 0; i < arrdata.size(); i++)
                arrdata.get(i).setIs_playing(false);
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            data_playing.setIs_playing(true);
            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        previous_view = imageView;
        seekbar.setVisibility(View.VISIBLE);
        playPauseButtonClickListener(c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }

    /*public void showListBubble(View v) {
        if (fragment_pos == 0) {
            RabindraSangeetSongFragment rabindra_song_fragment = (RabindraSangeetSongFragment) mAdapter.getFragment(fragment_pos);
            if (rabindra_song_fragment != null)
                rabindra_song_fragment.showListBubble(v);
        } else if (fragment_pos == 1) {
            RabindraSangeetArtistesFragment rabindra_artist_fragment = (RabindraSangeetArtistesFragment) mAdapter.getFragment(fragment_pos);
            if (rabindra_artist_fragment != null)
                rabindra_artist_fragment.showListBubble(v);
        }
    }*/
}
