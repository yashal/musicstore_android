package com.saregama.musicstore.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.fragments.GlobalAlbumsFragment;
import com.saregama.musicstore.fragments.GlobalArtistesFragment;
import com.saregama.musicstore.fragments.GlobalSongsFragment;
import com.saregama.musicstore.pojo.GlobalAlbumListPojo;
import com.saregama.musicstore.pojo.GlobalArtistSearchListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class GlobalSearchTabListActivity extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {

    private Activity ctx = this;
    private CircularSeekBar seekbar;
    private ImageView previous_view;
    private ImageView imageView;
    private MP3HindiSongListPojo data_playing;
    public ArrayList<MP3HindiSongListPojo> arrdata;
    public RecyclerView.Adapter globalSearchSongsAdapter;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_search_tab_list);

        String fragment_pos = getIntent().getStringExtra("fragment_pos");
        if (fragment_pos.equalsIgnoreCase("songs")){
            setDrawerAndToolbar("Songs");
            GlobalSongsFragment fragment = new GlobalSongsFragment();
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "GlobalSongsFragment").commit();
        } else if (fragment_pos.equalsIgnoreCase("album")) {
            setDrawerAndToolbar("Albums");
            GlobalAlbumsFragment fragment = new GlobalAlbumsFragment();
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "GlobalAlbumsFragment").commit();
        } else if (fragment_pos.equalsIgnoreCase("artistes")) {
            setDrawerAndToolbar("Artistes");
            GlobalArtistesFragment fragment = new GlobalArtistesFragment();
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "GlobalArtistesFragment").commit();
        }

        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
//                AddToCartWithoutLogin(getAppConfigJson().getC_type().getSONG()+"",data1.getId());
                CheckBox addToCart = (CheckBox)v.getTag(R.string.addtocart_id);
                QuickAction quickAction =   setupQuickAction(data_addtocart.getId(), getAppConfigJson().getC_type().getSONG());
                quickAction.show(addToCart);
                break;

            case R.id.albumLL:
                hideSoftKeyboard();
                Intent intent = new Intent(GlobalSearchTabListActivity.this, GlobalSearchAlbumDetail.class);
                GlobalAlbumListPojo data = (GlobalAlbumListPojo) v.getTag(R.string.global_data);
                intent.putExtra("album_id", data.getId());
                intent.putExtra("from", data.getName());
                startActivity(intent);
                break;

            case R.id.classhindustani_artist_albumLL:
                hideSoftKeyboard();
                Intent intent1 = new Intent(GlobalSearchTabListActivity.this, ClassicalHindustaniSOngDetail.class);
                GlobalArtistSearchListPojo data2 = (GlobalArtistSearchListPojo) v.getTag(R.string.global_artist);
                intent1.putExtra("artist_id", data2.getId());
                intent1.putExtra("from", "global");
                intent1.putExtra("c_type", getAppConfigJson().getC_type().getSONG());
                intent1.putExtra("header", data2.getName());
                startActivity(intent1);
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.addtocart_global_album:
                GlobalAlbumListPojo data_global_album = (GlobalAlbumListPojo) v.getTag(R.string.global_data);
                CheckBox addToCart_album = (CheckBox) v.getTag(R.string.addtocart_id);
                QuickAction quickActionAlbum = setupQuickActionAlbumLeft(data_global_album.getId(), getAppConfigJson().getC_type().getALBUM(), data_global_album.getCurrency(), data_global_album.getPrice(), data_global_album.getPrice_hd());
                quickActionAlbum.show(addToCart_album);
                break;

            default:
                break;
        }
    }

    @Override
    public void notifyAdapter() {
        if (globalSearchSongsAdapter != null)
            globalSearchSongsAdapter.notifyDataSetChanged();
    }

    private void playMP3Song(ImageView imageView, View v) {
        hideSoftKeyboard();
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            notifyAdapter();
        }
        if (data_playing.is_playing()) {
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            for (int i = 0; i < arrdata.size(); i++)
                arrdata.get(i).setIs_playing(false);
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getId());
            data_playing.setIs_playing(true);
            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        seekbar.setVisibility(View.VISIBLE);
        previous_view = imageView;
        playPauseButtonClickListener(getAppConfigJson().getC_type().getSONG(), imageView, seekbar, data_playing.getIsrc(), data_playing.getId(), data_playing, position);
    }
}