package com.saregama.musicstore.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.BasePojo;
import com.saregama.musicstore.pojo.ForgotPasswordMailPojo;
import com.saregama.musicstore.pojo.ResetPasswordPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ResetPassword extends BaseActivity {

    private ConnectionDetector cd;
    private ResetPassword ctx = this;
    private SaregamaDialogs dialog;
    private EditText temp_password;
    private static ResetPassword inst;
    private EditText email;
    private String listener_id;
    private String email_text;

    public static ResetPassword instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }

    public void updateList(final String smsMessage) {
        temp_password.setText(smsMessage);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        Intent intent = getIntent();
        Uri data = intent.getData();

        final TextView login_msg = (TextView) findViewById(R.id.login_msg);
        email = (EditText) findViewById(R.id.email);
        temp_password = (EditText) findViewById(R.id.temp_password);
        final EditText password = (EditText) findViewById(R.id.passwordReg);
        final EditText confirmPassword = (EditText) findViewById(R.id.confirmPassword);

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new SaregamaDialogs(ctx);

        if (getAppConfigJson().getResetpassword_text() != null && !getAppConfigJson().getResetpassword_text().isEmpty()) {
            login_msg.setText(getAppConfigJson().getResetpassword_text());
        }

        if (data != null) {
            String[] separated = data.toString().split("key=");
            String key = separated[1];
            verifyEMailKey(key);
        }

        TextView submit = (TextView) findViewById(R.id.resetPassword);
        ImageView reset_back = (ImageView) findViewById(R.id.reset_back);

        reset_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email_val = email.getText().toString();
                String password_val = password.getText().toString();
                String confirm_password = confirmPassword.getText().toString();

                hideSoftKeyboard();
                if (email_val.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.RESET_PASSWORD, getAppConfigJson().getBlank_email());
                } else if (!isValidEmail(email_val) && !email_val.matches("[0-9]+")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.RESET_PASSWORD, getAppConfigJson().getValid_email());
                } /*else if (temp_password_val.equals("")) {
                    dialog.displayCommonDialog("Please enter the temporary password");
                }*/ else if (password_val.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.RESET_PASSWORD, getAppConfigJson().getNew_password());
                } else if (password_val.length() < 6) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.RESET_PASSWORD, getAppConfigJson().getShort_length_password_popup());
                } else if (confirm_password.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.RESET_PASSWORD, getAppConfigJson().getConfirm_password());
                } else if (!password_val.equals(confirm_password)) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.PASSWORD_MISMATCH, getAppConfigJson().getPassword_reset_popup());
                } else {
                    resetPassword(email_val, listener_id, password_val);
                }
            }
        });
    }

    private void resetPassword(String email, String listener_id, final String password) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().postResetPassword(getFromPrefs(SaregamaConstants.IMEI), email, listener_id, password, "setpassword", new Callback<ResetPasswordPojo>() {
                @Override
                public void success(ResetPasswordPojo basePojo, Response response) {
                    BasePojo baseVo = basePojo;
                    if (basePojo != null) {
                        if (baseVo.getStatus()) {
                            checkCommonURL(response.getUrl());

                            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
//                            dialog.displayCommonDialogWithHeader("PASSWORD CHANGED", "Your password has been changed successfully. Please use your new password to log in");
                            Intent mIntent = new Intent(ctx, LoginActivity.class);
                            mIntent.putExtra("email", basePojo.getData().getEmail());
                            mIntent.putExtra("password", password);
                            startActivity(mIntent);
                            SaregamaConstants.ACTIVITIES.add(ctx);
                            finish();
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.LINK_EXPIRED, baseVo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.RESET_PASSWORD, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void verifyEMailKey(String key) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().forgotPasswordMail(key, new Callback<ForgotPasswordMailPojo>() {
                @Override
                public void success(ForgotPasswordMailPojo basePojo, Response response) {
                    BasePojo baseVo = basePojo;
                    if (basePojo != null) {
                        if (baseVo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            if (basePojo.getData().getEmail() != null && !basePojo.getData().getEmail().isEmpty()) {
                                email.setText(basePojo.getData().getEmail());
                                email_text = basePojo.getData().getEmail();
                            }
                            if (basePojo.getData().getListener_id() != null && !basePojo.getData().getListener_id().isEmpty()) {
                                listener_id = basePojo.getData().getListener_id();
                            }
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.LINK_EXPIRED, baseVo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.RESET_PASSWORD, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    public void navigateToForgotPassword(){
        Intent intent = new Intent(ctx, LoginActivity.class);
        intent.putExtra("email", email_text);
        startActivity(intent);
        finish();
    }
}
