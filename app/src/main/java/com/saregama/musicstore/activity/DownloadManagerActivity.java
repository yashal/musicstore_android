package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.DownloadManagerAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.DownloadManagerAlbumPojo;
import com.saregama.musicstore.pojo.DownloadManagerSongPojo;
import com.saregama.musicstore.pojo.DownloadSongsPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;
import com.saregama.musicstore.views.CustomTextViewBold;

import java.io.File;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.widget.Toast.makeText;
import static com.saregama.musicstore.util.SaregamaConstants.pos;

public class DownloadManagerActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout download_now, downloaded, download_now_selector, downloaded_selector;
    private TextView download_now_text, downloaded_text;
    private ViewPager viewPager;
    private Activity ctx = this;
    private DownloadManagerAdapter pagerAdapter;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    // for external permission
    private final static int READ_EXTERNAL_STORAGE = 101;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;

    private int fragment_pos = 0;
    public ArrayList<DownloadManagerSongPojo> songList;
    public ArrayList<DownloadManagerSongPojo> songListDownloadNow;
    public ArrayList<DownloadManagerAlbumPojo> albumList;
    public RecyclerView.Adapter mAdapterDownloadNow;
    public CustomTextViewBold songs_count_download;
    public RecyclerView.Adapter mAdapter;
    public LinearLayout songs_layout_download;
    public int songs_count = 0, album_count = 0;
    private String filename = "";
    ArrayList<String> permissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_manager);

        setDrawerAndToolbar("Download Manager");
        SaregamaConstants.ACTIVITIES.add(ctx);

        if (songList == null)
            songList = new ArrayList<>();

        if (songListDownloadNow == null)
            songListDownloadNow = new ArrayList<>();

        if (albumList == null)
            albumList = new ArrayList<>();

        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        pagerAdapter = new DownloadManagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.DownloadMGRpager);

        download_now = (LinearLayout) findViewById(R.id.tab1_layout);
        downloaded = (LinearLayout) findViewById(R.id.tab2_layout);

        download_now_selector = (LinearLayout) findViewById(R.id.tab1_selector);
        downloaded_selector = (LinearLayout) findViewById(R.id.tab2_selector);

        download_now_text = (TextView) findViewById(R.id.tab1_text);
        downloaded_text = (TextView) findViewById(R.id.tab2_text);

        download_now_text.setText(getResources().getString(R.string.download_now));
        downloaded_text.setText(getResources().getString(R.string.downloaded));

        download_now.setOnClickListener(this);
        downloaded.setOnClickListener(this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    songListDownloadNow = new ArrayList<>();
                    albumList = new ArrayList<>();
                    albumList.clear();
                    songListDownloadNow.clear();
                } else if (position == 1) {
                    songList = new ArrayList<>();
                    songList.clear();
                }
                setStyle(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && !getFromPrefs(SaregamaConstants.SESSION_ID).trim().equals("")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permissions = new ArrayList<>();
                permissions.add(WRITE_EXTERNAL_STORAGE);
                permissionsToRequest = findUnAskedPermissions(permissions);
                permissionsRejected = findRejectedPermissions(permissions);
                if (permissionsToRequest.size() > 0 || permissionsRejected.size() > 0)
                    displayDialog();
                else if (fragment_pos != 1 && songs_count == 0 && album_count == 0)
                    goAhead();
            } else {
                if (fragment_pos != 1 && songs_count == 0 && album_count == 0)
                    goAhead();
            }
        } else {
            Intent intent = new Intent(ctx, LoginActivity.class);
            intent.putExtra("from", "sms_download_manager");
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tab1_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.tab2_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            case R.id.mp3hindi_songlist_cart:
                DownloadManagerSongPojo data = (DownloadManagerSongPojo) view.getTag(R.string.data);
                String id = data.getId();
                String encryptedId = base64Encoding(data.getOrder_no());
                String str_songName = data.getSong_name();
                int pos = (int) view.getTag(R.string.key);
                checkIfFileExists(data, str_songName, pos);

                break;

            case R.id.downlaod_albumlist_LL:
                DownloadManagerAlbumPojo data_album = (DownloadManagerAlbumPojo) view.getTag(R.string.data);
                String data_from = (String) view.getTag(R.string.navigate_from);
                Intent mIntent = new Intent(DownloadManagerActivity.this, DownloadMgrAlbumDetails.class);
                mIntent.putExtra("album_name", data_album.getName());
                mIntent.putExtra("album_image", data_album.getImage());
                mIntent.putExtra("album_songs", data_album.getAlbum_songs());
                mIntent.putExtra("navigate_from", data_from);
                startActivity(mIntent);
                break;

            default:
                break;
        }
    }

    private void setStyle(int position) {
        fragment_pos = position;
        if (position == 1) {
            download_now_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            downloaded_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));

            downloaded_selector.setVisibility(View.VISIBLE);
            download_now_selector.setVisibility(View.GONE);
        } else if (position == 0) {
            download_now_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            downloaded_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            downloaded_selector.setVisibility(View.GONE);
            download_now_selector.setVisibility(View.VISIBLE);
        }
    }

    private void getMp3URL(String id, String encryptedId, final DownloadManagerSongPojo data, final int pos) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().downloadSongs(getFromPrefs(SaregamaConstants.SESSION_ID), encryptedId, id, new Callback<DownloadSongsPojo>() {
                @Override
                public void success(DownloadSongsPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            String url_song = basePojo.getData().getUrl();
                            file_download(url_song, filename);
                            makeText(ctx, getAppConfigJson().getDownload_initiated(), Toast.LENGTH_SHORT).show();
//                          Below code is use to update the list view and remove song from the list.
                            int count = Integer.parseInt(data.getDownload_count());

                            count += 1;
                            data.setDownload_count(count + "");
                            if (fragment_pos == 1)
                                mAdapter.notifyDataSetChanged();
                            else if (songListDownloadNow != null && songListDownloadNow.size() > 0 && songListDownloadNow.size() > pos) {
                                songListDownloadNow.remove(pos);
                                mAdapterDownloadNow.notifyDataSetChanged();
                                mAdapterDownloadNow.notifyItemRemoved(pos);
                                if (songListDownloadNow.size() == 0) {
                                    songs_layout_download.setVisibility(View.GONE);
                                } else {
                                    songs_layout_download.setVisibility(View.VISIBLE);
                                }
                                songs_count = songs_count - 1;
                                songs_count_download.setText("" + songs_count);
                                saveIntIntoPrefs(SaregamaConstants.DOWNLOAD_SIZE, songs_count + album_count);
                                saveIntIntoPrefs(SaregamaConstants.DOWNLOADED_SIZE, getIntFromPrefs(SaregamaConstants.DOWNLOADED_SIZE) + 1);
                            }
                        } else {
                            dialog.displayCommonDialogWithHeader("Maximum Limit Exceeded", getAppConfigJson().getDownload_complete());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    // mp3 file download and Notification show

    public void file_download(String uRl, String file_name) {
        File direct = new File(Environment.getExternalStorageDirectory() + "/Temp");

        if (!direct.exists()) {
            direct.mkdirs();
        }
        DownloadManager mgr = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

//        String frm[] = file_name.split("\\.");
//        String new_name = frm[0]+"(sarega.ma)."+frm[1];
        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle(file_name)
                .setDescription("")
                .setDestinationInExternalPublicDir("/Temp", file_name);

        mgr.enqueue(request);


    }

    private void addPermissionDialogMarshMallow() {
        int resultCode = 0;
        resultCode = READ_EXTERNAL_STORAGE;


        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    if (fragment_pos != 1)
                        goAhead();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (permissionsRejected == null)
            permissionsRejected = new ArrayList<>();
        switch (requestCode) {
            case READ_EXTERNAL_STORAGE:
                if (hasPermission(WRITE_EXTERNAL_STORAGE)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    goAhead();
                } else {
                    permissionsRejected.add(WRITE_EXTERNAL_STORAGE);
                    clearMarkAsAsked(WRITE_EXTERNAL_STORAGE);
                    String message = "permission for access external storage was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }

    }

    private void goAhead() {
        viewPager.setAdapter(pagerAdapter);
    }

    private void downloadAgainDialog(final DownloadManagerSongPojo data, final int pos) {
        Button OkButtonLogout;
        final Dialog dialogDownloadAgain = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogDownloadAgain.setContentView(R.layout.custom_dialog);
        if (!dialogDownloadAgain.isShowing()) {
            TextView msg_textView = (TextView) dialogDownloadAgain.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) dialogDownloadAgain.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(getAppConfigJson().getDownload_already());
            dialog_header.setText("Download Manager");
            OkButtonLogout = (Button) dialogDownloadAgain.findViewById(R.id.btn_yes_exit);
            OkButtonLogout.setText("YES");
            Button CancelButtonLogout = (Button) dialogDownloadAgain.findViewById(R.id.btn_no_exit);
            CancelButtonLogout.setText("NO");
            ImageView dialog_header_cross = (ImageView) dialogDownloadAgain.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogDownloadAgain.dismiss();
                }
            });

            OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogDownloadAgain.dismiss();
                    checkDownloadCondition(data, pos);
                }
            });

            CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogDownloadAgain.dismiss();
                }
            });

            dialogDownloadAgain.show();
        }
    }

    public void checkIfFileExists(DownloadManagerSongPojo data, final String str_songName, int pos) {
        if (data.getC_type().equals(getAppConfigJson().getSong_type().getSONGHD() + ""))
            filename = str_songName + ".wav";
        else
            filename = str_songName + ".mp3";

        boolean fileExists = new File(Environment.getExternalStorageDirectory() + "/MusicStore/" + filename).isFile();

        if (!fileExists) {
            checkDownloadCondition(data, pos);
        } else {
            downloadAgainDialog(data, pos);
        }
    }

    private void checkDownloadCondition(DownloadManagerSongPojo data, int pos) {
        if (Integer.parseInt(data.getC_type()) == getAppConfigJson().getC_type().getSONGHD() && (Integer.parseInt(data.getWav_file_size()) / SaregamaConstants.KB_TO_MB) > getAppConfigJson().getMax_download_size() && getFromPrefs(SaregamaConstants.DOWNLOAD_SIZE_ALERT).equals("")) {
            displaySizeAlertDialog(data, (Integer.parseInt(data.getWav_file_size()) / SaregamaConstants.KB_TO_MB));
        } else if (Integer.parseInt(data.getC_type()) == getAppConfigJson().getC_type().getSONG() && (Integer.parseInt(data.getFile_size()) / SaregamaConstants.KB_TO_MB) > getAppConfigJson().getMax_download_size() && getFromPrefs(SaregamaConstants.DOWNLOAD_SIZE_ALERT).equals("")) {
            displaySizeAlertDialog(data, (Integer.parseInt(data.getFile_size()) / SaregamaConstants.KB_TO_MB));
        } else
            proceedWithDownload(data);

    }

    private void proceedWithDownload(DownloadManagerSongPojo data) {
        String id = data.getId();
        String encryptedId = base64Encoding(data.getOrder_no());
        if (data.getDownload_count() != null) {
            if (getFromPrefs(SaregamaConstants.MP3).equals("checked") && checkConnectedNetwork().equals("wifi") && data.getC_type().equals(getAppConfigJson().getSong_type().getSONG() + "")) {
                getMp3URL(id, encryptedId, data, pos);
            } else if ((getFromPrefs(SaregamaConstants.MP3) == null || getFromPrefs(SaregamaConstants.MP3).isEmpty() || getFromPrefs(SaregamaConstants.MP3).equals("unchecked")) && data.getC_type().equals(getAppConfigJson().getSong_type().getSONG() + "")) {
                if (checkConnectedNetwork().equals("wifi"))
                    getMp3URL(id, encryptedId, data, pos);
                else
                    changeSettingsDialog(id, encryptedId, data);
            } else if (getFromPrefs(SaregamaConstants.HD).equals("checked") && checkConnectedNetwork().equals("wifi") && data.getC_type().equals(getAppConfigJson().getSong_type().getSONGHD() + "")) {
                getMp3URL(id, encryptedId, data, pos);
            } else if ((getFromPrefs(SaregamaConstants.HD) == null || getFromPrefs(SaregamaConstants.HD).isEmpty() || getFromPrefs(SaregamaConstants.HD).equals("unchecked")) && data.getC_type().equals(getAppConfigJson().getSong_type().getSONGHD() + "")) {
                if (checkConnectedNetwork().equals("wifi"))
                    getMp3URL(id, encryptedId, data, pos);
                else
                    changeSettingsDialog(id, encryptedId, data);
            } else {
                Toast.makeText(ctx, "Please turn on the wi-fi to download", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void displaySizeAlertDialog(final DownloadManagerSongPojo data, int size) {
        Button OkButton;
        final Dialog commonDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        commonDialog.setContentView(R.layout.custom_dialog);
        if (!commonDialog.isShowing()) {
            TextView msg_textView = (TextView) commonDialog.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) commonDialog.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            LinearLayout do_not_show_layout = (LinearLayout) commonDialog.findViewById(R.id.do_not_show_layout);
            do_not_show_layout.setVisibility(View.VISIBLE);
            final CheckBox check_box = (CheckBox) commonDialog.findViewById(R.id.check_box);
            do_not_show_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (check_box.isChecked()) {
                        check_box.setButtonDrawable(R.mipmap.check_box_artist);
                        check_box.setChecked(false);
                    } else {
                        check_box.setButtonDrawable(R.mipmap.check_box_artist_selected);
                        check_box.setChecked(true);
                    }
                }
            });
            msg_textView.setText(getAppConfigJson().getMax_download().replace("SONG_SIZE", size + ""));
            dialog_header.setText("Download Manager");
            OkButton = (Button) commonDialog.findViewById(R.id.btn_yes_exit);
            OkButton.setText("OK");
            Button CancelButton = (Button) commonDialog.findViewById(R.id.btn_no_exit);
            CancelButton.setText("CANCEL");
            ImageView dialog_header_cross = (ImageView) commonDialog.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    commonDialog.dismiss();
                }
            });

            OkButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    commonDialog.dismiss();
                    if (check_box.isChecked())
                        saveIntoPrefs(SaregamaConstants.DOWNLOAD_SIZE_ALERT, "no");
                    else
                        saveIntoPrefs(SaregamaConstants.DOWNLOAD_SIZE_ALERT, "");
                    proceedWithDownload(data);
                }
            });

            CancelButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    commonDialog.dismiss();
                }
            });

            commonDialog.show();
        }
    }

    private void displayDialog() {
        final Dialog allowDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        allowDialog.setContentView(R.layout.allow_access_dialog);

        LinearLayout allow_access = (LinearLayout) allowDialog.findViewById(R.id.dialog_allow);
        allow_access.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPermissionDialogMarshMallow();
                allowDialog.dismiss();
            }
        });
        allowDialog.setCancelable(false);

        allowDialog.show();
    }

    private void changeSettingsDialog(final String id, final String encryptedId, final DownloadManagerSongPojo data) {
        Button OkButton;
        final Dialog commonDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        commonDialog.setContentView(R.layout.custom_dialog);
        if (!commonDialog.isShowing()) {
            TextView msg_textView = (TextView) commonDialog.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) commonDialog.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(getAppConfigJson().getDownload_setting());
            dialog_header.setText("Download Manager");
            OkButton = (Button) commonDialog.findViewById(R.id.btn_yes_download);
            OkButton.setText("Continue");
            Button CancelButton = (Button) commonDialog.findViewById(R.id.btn_no_download);
            CancelButton.setText("Go to Settings");
            LinearLayout custom_dialog_bottom_layout_settings = (LinearLayout) commonDialog.findViewById(R.id.custom_dialog_bottom_layout_settings);
            LinearLayout custom_dialog_bottom_layout = (LinearLayout) commonDialog.findViewById(R.id.custom_dialog_bottom_layout);
            custom_dialog_bottom_layout.setVisibility(View.GONE);
            custom_dialog_bottom_layout_settings.setVisibility(View.VISIBLE);
            ImageView dialog_header_cross = (ImageView) commonDialog.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    commonDialog.dismiss();
                }
            });

            OkButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    getMp3URL(id, encryptedId, data, pos);
                    commonDialog.dismiss();
                }
            });

            CancelButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    removeActivity(getResources().getString(R.string.package_name) + ".SettingsActivity");
                    Intent intent = new Intent(ctx, SettingsActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                    commonDialog.dismiss();
                }
            });

            commonDialog.show();
        }
    }
}
