package com.saregama.musicstore.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.saregama.musicstore.R;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.LogInPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Yash ji on 9/6/2016.
 */
public class EmailLoginActivity extends BaseActivity {

    private ConnectionDetector cd;
    private EmailLoginActivity ctx = this;
    private SaregamaDialogs dialog;
    private String notification_key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);
        dialog = new SaregamaDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        SaregamaConstants.ACTIVITIES.add(ctx);
        if (getIntent().getStringExtra("notification_key") != null) {
            notification_key = getIntent().getStringExtra("notification_key");
        }
        final ImageView email_login_back = (ImageView) findViewById(R.id.email_login_back);
        final EditText login_email = (EditText) findViewById(R.id.login_email);
        final LinearLayout email_login_next = (LinearLayout) findViewById(R.id.email_login_next);
        email_login_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String login_emailText = login_email.getText().toString();
                if (login_emailText.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getBlank_email());
                } else if (!isValidEmail(login_emailText)) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getValid_email());
                } else {
                    saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "normal");
                    hideSoftKeyboard();
                    doLoginWithEmail(login_emailText);
                }
            }
        });

        email_login_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmailLoginActivity.this, LoginActivity.class);
                String from = getIntent().getStringExtra("from");
                intent.putExtra("from", from);
                startActivity(intent);
                finish();
            }
        });
    }

    private void doLoginWithEmail(final String username) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(EmailLoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().checkEmailExist(username, "signin", "email", new Callback<LogInPojo>() {
                @Override
                public void success(LogInPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {

                            checkCommonURL(response.getUrl());

                            if (basePojo.getData().getListener().isUser_exits()) {
                                Intent mIntent = new Intent(ctx, PasswordLoginActivity.class);
                                String from = getIntent().getStringExtra("from");
                                mIntent.putExtra("from", from);
                                mIntent.putExtra("email", basePojo.getData().getListener().getEmail());
                                mIntent.putExtra("notification_key", notification_key);
                                startActivityForResult(mIntent, 1);
                            } else {
                                Intent mIntent = new Intent(ctx, SetPasswordLoginActivity.class);
                                String from = getIntent().getStringExtra("from");
                                mIntent.putExtra("from", from);
                                mIntent.putExtra("email", basePojo.getData().getListener().getEmail());
                                mIntent.putExtra("notification_key", notification_key);
                                startActivityForResult(mIntent, 1);
                            }
                        }
                        else
                        {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(EmailLoginActivity.this, LoginActivity.class);
        String from = getIntent().getStringExtra("from");
        intent.putExtra("from", from);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                finish();
            }

        }
        if (resultCode == RESULT_CANCELED) {
            //Write your code if there's no result
        }
    }
}
