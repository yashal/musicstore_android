package com.saregama.musicstore.activity;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by arpit on 2/21/2017.
 */

public class CheckDownloadComplete extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0));
            DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            Cursor cursor = manager.query(query);
            if (cursor.moveToFirst()) {
                if (cursor.getCount() > 0) {
                    int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    if (status == DownloadManager.STATUS_SUCCESSFUL) {
                        String file = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
                        File inputPath = new File(Environment.getExternalStorageDirectory() + "/Temp");
                        File outputPath = new File(Environment.getExternalStorageDirectory() + "/MusicStore");
                        String[] name = file.split("/");
                        if (name != null && name.length > 0) {
                            moveFile(inputPath + "/", name[name.length - 1], outputPath + "/", context);
                        }

//                        String[] name = file.split("/");
//                        if (name != null && name.length > 0) {
//                            File from = new File(sdcard, name[name.length - 1]);
////                            String frm = name[name.length - 1].replace(" ","-");/*.split("\\.");*/
//                            String frm = name[name.length - 1].replace("(sarega.ma)","");/*.split("\\.");*/
////                            if(frm != null && frm.length >= 2) {
////                                File to = new File(sdcard, frm[0]+"(sarega.ma)."+frm[1]);
//                                File to = new File(sdcard, frm);
//                                from.renameTo(to);
////                            }
//                        }
                        // So something here on success

                    } else {
                        int message = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                        // So something here on failed.
                    }
                }
            }
        }
    }

    private void moveFile(String inputPath, String inputFile, String outputPath, Context context) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();
        } catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

        Intent intent =
                new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "/MusicStore/"+inputFile)));
        context.sendBroadcast(intent);

    }

}
