package com.saregama.musicstore.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.Mp3HindiAlbumAdapter;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.pojo.Mp3HindiAlbumPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

public class BuyAnyAlbumListing extends BaseActivity implements View.OnClickListener {

    private BuyAnyAlbumListing ctx = this;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
//    private EditText mp3song_search;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private int start = 0;
    private ArrayList<MP3HindiAlbumListPojo> arrdata;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 5, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0;

    private String str_alphbet = "";
    private String asyncTaskUrl;

    /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;

//    private boolean flag_editText = false;
//    private TextView no_result_found;
    private int c_type;
    private String search_word = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_any_album_listing);

        cd = new ConnectionDetector(ctx);
        c_type = getIntent().getIntExtra("c_type", 1);
        dialog = new SaregamaDialogs(this);

        if (getIntent().getStringExtra("notification_key") == null) {
            if (getIntent().getStringExtra("languagename") == null) {
                setDrawerAndToolbarWithDropDown("Ghazals & Sufi");
            } else {
                setDrawerAndToolbarWithDropDown(getIntent().getStringExtra("languagename"));
            }
        } else {
            setDrawerAndToolbarWithDropDown("Regional Album");
        }

        if (ctx.getIntent().getStringExtra("notification_url") != null) {
            asyncTaskUrl = getIntent().getStringExtra("notification_url");
        } else {
            asyncTaskUrl = getIntent().getStringExtra("url");
        }

        SaregamaConstants.ACTIVITIES.add(ctx);

        mRecyclerView = (RecyclerView) findViewById(R.id.mp3songs_recycler_view);
//        mp3song_search = (EditText) findViewById(R.id.search_song);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        arrdata = new ArrayList<>();

//        no_result_found = (TextView) findViewById(R.id.no_result_found);

       /* ****scrollable alphabet list code as per change request**** */
        rowTextView = (TextView) findViewById(R.id.row_text_view);
        alphabetslist = (ListView) findViewById(R.id.mp3_landing_list);

        /* ****scrollable alphabet list code as per change request start**** */
        final AlphabetListAdapter alpha_adapter = new AlphabetListAdapter(ctx, SaregamaConstants.items);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                str_alphbet =  convertAlphabet(SaregamaConstants.items[position].toLowerCase());
                arrdata.clear();
                alpha_adapter.setSelectedIndex(position);
                start = 0;
                new GetSongsList().execute();

                setBubblePosition(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        new GetSongsList().execute();

//        mp3song_search.setHint("Search Albums");

//        softKeyboardDoneClickListener(mp3song_search);

//        mp3song_search.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int starts, int before, int count) {
//                if (mp3song_search.getText().toString().trim().length() == 0 && flag_editText) {
//                    flag_editText = false;
//                    start = 0;
//                    previousTotal = 0;
//                    arrdata.clear();
//                    search_word = "";
//                    new GetSongsList().execute();
//                } else {
//                    if (mp3song_search.getText().toString().trim().length() > 2) {
//                        flag_editText = true;
//                        start = 0;
//                        previousTotal = 0;
//                        arrdata.clear();
//                        search_word = mp3song_search.getText().toString().trim();
//                        new GetSearchSongsList().execute(mp3song_search.getText().toString().trim().replaceAll(" ", "%20"));
//                    }
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
    }

    /* **** scrollable alphabet list code as per change request comment previous code**** */
    /*public void showListBubble(View v) {
        hideSoftKeyboard();
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        arrdata.clear();
        str_alphbet = showListBubbleCommon(v, rowTextView);
//        if (mp3song_search.length() > 0){
//            mp3song_search.setText("");
//        } else {
            new GetSongsList().execute();
//        }
    }*/

    private void setAdapter(ArrayList<MP3HindiAlbumListPojo> arr) {
        mAdapter = new Mp3HindiAlbumAdapter(ctx, arr, "buyanyalbum_gazal", search_word);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
//                        if (mp3song_search.getText().toString().trim().length() > 2) {
//                            new GetSearchSongsList().execute(mp3song_search.getText().toString().trim().replaceAll(" ", "%20"));
//                        } else {
                            new GetSongsList().execute();
//                        }
                        loading = true;
                    }
                }
            }
        });
    }

    // Ghazals & Sufi Albums

    private class GetSearchSongsList extends AsyncTask<String,Void,Void> {
        private Mp3HindiAlbumPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... querry) {

            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            if (getIntent().getStringExtra("languagename") == null) {
                basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getALBUM() + "&query=" + querry[0]  + "&mode=" + "ghazal_sufi"+ "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), Mp3HindiAlbumPojo.class);
            } else {
                basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getALBUM() + "&query=" + querry[0]  + "&mode=" + "regional_films"+ "&lang=" + getIntent().getIntExtra("languageid", 0)+ "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), Mp3HindiAlbumPojo.class);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (arrdata.size() > 0 && start > 0) {
                                arrdata.addAll(basePojo.getData().getList());
                            } else {
                                arrdata.clear();
                                arrdata = basePojo.getData().getList();
                                previousTotal = 0;
                            }
                        }
//                        no_result_found.setVisibility(View.GONE);
                        if (basePojo.getStatus()) {
                            setAdapter(arrdata);
                            mRecyclerView.scrollToPosition(firstVisibleItemPosition);
                            addScrollListner();

                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
//
//                        if (arrdata.size() == 0) {
//                            no_result_found.setText(SaregamaConstants.NO_RESULT);
//                            no_result_found.setVisibility(View.VISIBLE);
//                        } else {
//                            no_result_found.setVisibility(View.GONE);
//                        }
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private Mp3HindiAlbumPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            hideSoftKeyboard();
            if (cd.isConnectingToInternet()) {
                d = SaregamaDialogs.showLoading(BuyAnyAlbumListing.this);
                d.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(asyncTaskUrl + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&alpha=" + str_alphbet), Mp3HindiAlbumPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
//                    no_result_found.setVisibility(View.GONE);
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
//                            mp3song_search.setText("");
                            if (arrdata.size() > 0) {
                                arrdata.addAll(basePojo.getData().getList());
                            } else {
                                arrdata.clear();
                                arrdata = basePojo.getData().getList();
                                setAdapter(arrdata);
                                previousTotal = 0;
                            }
                        }

                        if (basePojo.getStatus()) {
                            mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), arrdata.size() - mAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.albumLL:
                hideSoftKeyboard();
                Intent intent = new Intent(ctx, RegionalMp3AlbumDetail.class);
                MP3HindiAlbumListPojo data = (MP3HindiAlbumListPojo) v.getTag(R.string.data);
                intent.putExtra("album_id", data.getAlbum_id());
                intent.putExtra("c_type", c_type);
                intent.putExtra("languagename", getIntent().getStringExtra("languagename"));
                intent.putExtra("from", "buy_album");
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.album_addtocart:
                MP3HindiAlbumListPojo data_album = (MP3HindiAlbumListPojo) v.getTag(R.string.data);
//                AddToCartWithoutLogin(c_type+"", data_album.getAlbum_id());
                CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);
                QuickAction quickActionAlbum = setupQuickActionAlbumLeft(data_album.getAlbum_id(), c_type, data_album.getCurrency(), data_album.getPrice(), data_album.getPrice_hd());
                quickActionAlbum.show(addToCart);
                break;
        }
    }
}