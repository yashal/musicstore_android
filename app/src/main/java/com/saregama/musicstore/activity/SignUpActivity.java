package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.saregama.musicstore.R;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.BasePojo;
import com.saregama.musicstore.pojo.LogInPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignUpActivity extends BaseActivity {

    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;
    private Map<String, String> articleParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        final EditText name = (EditText) findViewById(R.id.name);
        final EditText email = (EditText) findViewById(R.id.email);
        final EditText mobile = (EditText) findViewById(R.id.contact);
        final EditText password = (EditText) findViewById(R.id.passwordReg);
        final EditText confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        TextView tv_rememberpassword = (TextView) findViewById(R.id.tv_remember_password);

        articleParams = new HashMap<>();

        final ImageView signup_back = (ImageView) findViewById(R.id.signup_back);

        signup_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final CheckBox rememberpassword = (CheckBox) findViewById(R.id.remember_password);

        rememberpassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    rememberpassword.setButtonDrawable(R.mipmap.check_box_checked);

                } else {
                    rememberpassword.setButtonDrawable(R.mipmap.check_box_unchecked);
                }
            }
        });

        tv_rememberpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rememberpassword.isChecked()) {
                    rememberpassword.setButtonDrawable(R.mipmap.check_box_unchecked);
                    rememberpassword.setChecked(false);
                } else {
                    rememberpassword.setButtonDrawable(R.mipmap.check_box_checked);
                    rememberpassword.setChecked(true);
                }
            }
        });

        cd = new ConnectionDetector(getApplicationContext());

        TextView submit = (TextView) findViewById(R.id.registerSubmit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name_val = name.getText().toString().trim();
                String email_val = email.getText().toString();
                String mobile_val = mobile.getText().toString();
                String password_val = password.getText().toString();
                String confirm_password = confirmPassword.getText().toString();

                dialog = new SaregamaDialogs(ctx);

                if (name_val.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.REGISTRATION_FAILED, "Please enter name");
                } else if (email_val.equals("") && mobile_val.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.REGISTRATION_FAILED, "Please enter either email ID or mobile number");
                } else if (!email_val.equals("") && !isValidEmail(email_val)) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.REGISTRATION_FAILED, "Please enter a valid email ID");
                } else if (!mobile_val.equals("") && !mobile_val.matches("[0-9]+")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.REGISTRATION_FAILED, "Please enter a valid mobile number");
                } else if (password_val.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.REGISTRATION_FAILED, "Please enter password");
                } else if (password_val.length() < 6) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.REGISTRATION_FAILED, "Your password should be a minimum of 6 characters");
                } else if (confirm_password.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.REGISTRATION_FAILED, "Please enter confirm password");
                } else if (!password_val.equals(confirm_password)) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.PASSWORD_MISMATCH, "Passwords do not match");
                } else {
                    doSignUp(name_val, email_val, mobile_val, password_val);
                    hideSoftKeyboard();
                    if (rememberpassword.isChecked()) {
                        saveIntoPrefs("password", password_val);
                    } else {
                        saveIntoPrefs("password", "");
                    }
                }
            }
        });
    }

    private void doSignUp(String name, final String email, final String mobile, String password) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().postRegister(getFromPrefs(SaregamaConstants.IMEI), name, email, mobile, password, "signup", new Callback<LogInPojo>() {
                @Override
                public void success(LogInPojo basePojo, Response response) {
                    BasePojo baseVo = basePojo;
                    if (basePojo != null) {
                        if (baseVo.getStatus()) {

                            // analytics code for source of registration start
                            if(email.length() > 0 && mobile.length()== 0)
                            {
                                articleParams.put("sign_up_email", email);
                            }
                            else if(mobile.length() > 0 && email.length()== 0)
                            {
                                articleParams.put("sign_up_mobile", mobile);
                            }
                            else if(mobile.length() > 0 && email.length()> 0) {
                                articleParams.put("sign_up_email", email);
                                articleParams.put("sign_up_mobile", mobile);
                            }
                            articleParams.put("User_Device", "Android");
                            articleParams.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            articleParams.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            articleParams.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            articleParams.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));
                            FlurryAgent.logEvent("sign_up_source", articleParams);
                            // analytics code for source of registration End

                            checkCommonURL(response.getUrl());
                            Intent intent = new Intent();
                            setResult(RESULT_OK, intent);
                            finish();
                            saveIntoPrefs(SaregamaConstants.EMAIL, basePojo.getData().getListener().getEmail());
                            saveIntoPrefs(SaregamaConstants.NAME, basePojo.getData().getListener().getName());
                            saveIntoPrefs(SaregamaConstants.MOBILE, basePojo.getData().getListener().getMobile());
                            saveIntoPrefs(SaregamaConstants.SESSION_ID, basePojo.getData().getListener().getSession_id());
                            saveIntoPrefs(SaregamaConstants.ID, basePojo.getData().getListener().getId());
                            saveIntoPrefs(SaregamaConstants.CART, basePojo.getData().getCart());
                            saveIntoPrefs(SaregamaConstants.DOWNLOAD, basePojo.getData().getDownload());
                            SaregamaConstants.HEADER = basePojo.getData().getListener().getSession_id();
                            saveIntoPrefs(SaregamaConstants.REFRESH_NEEDED, "yes");
                            Toast.makeText(ctx, "Successfully registered", Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.REGISTRATION_FAILED, baseVo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.REGISTRATION_FAILED, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, "Sorry! We could not create your account. Please check your internet connection and try again.");
        }
    }
}
