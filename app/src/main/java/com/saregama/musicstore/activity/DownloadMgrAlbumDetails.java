package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.DownloadAlbumDetailsAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.DownloadManagerAlbumSongs;
import com.saregama.musicstore.pojo.DownloadSongsPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.io.File;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Yash on 5/12/2016.
 */
public class DownloadMgrAlbumDetails extends BaseActivity implements View.OnClickListener {

    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;
    private TextView albumName;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ImageView bannerImage, albumImage;
    private Bitmap largeIcon;
    private int height, songs_count;
    private String str_albumName, str_albumImage;
    private ArrayList<DownloadManagerAlbumSongs> album_songs;
    private String navigate_from;
    private LinearLayout ll_change_album_to_song;
    private String filename = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mp3hindi_album_detail);

        str_albumName = getIntent().getStringExtra("album_name").toString();
        str_albumImage = getIntent().getStringExtra("album_image").toString();
        album_songs = (ArrayList<DownloadManagerAlbumSongs>) getIntent().getSerializableExtra("album_songs");
        setDrawerAndToolbar(str_albumName);
        dialog = new SaregamaDialogs(ctx);

        navigate_from = getIntent().getStringExtra("navigate_from");

        if (album_songs != null)
            songs_count = album_songs.size();
        DisplayMetrics metrics = new DisplayMetrics();
        ctx.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        height = metrics.heightPixels / 3;

        SaregamaConstants.ACTIVITIES.add(ctx);

        ll_change_album_to_song = (LinearLayout) findViewById(R.id.ll_change_album_to_song);
        ll_change_album_to_song.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) findViewById(R.id.mp3hindi_albumdetails_recycler);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(getApplicationContext());

        RelativeLayout header_layout_albumdetail = (RelativeLayout) findViewById(R.id.header_layout_albumdetail);
        header_layout_albumdetail.requestLayout();
        header_layout_albumdetail.getLayoutParams().height = height;

        LinearLayout opacityFilter = (LinearLayout) findViewById(R.id.opacityFilter);
        opacityFilter.requestLayout();
        opacityFilter.getLayoutParams().height = height - 80;

        albumName = (TextView) findViewById(R.id.mp3hindi_albumdetails_albumName);

        albumImage = (ImageView) findViewById(R.id.mp3hindi_albumdetails_albumImage);
        bannerImage = (ImageView) findViewById(R.id.mp3hindi_albumdetails_banner);
        bannerImage.requestLayout();
        bannerImage.getLayoutParams().height = height - 80;

        LinearLayout mp3hindi_albumdetails_albumImageLL = (LinearLayout) findViewById(R.id.mp3hindi_albumdetails_albumImageLL);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins((int) getResources().getDimension(R.dimen.album_image_margin_left), height - getResources().getInteger(R.integer.album_detail), 0, 0);
        mp3hindi_albumdetails_albumImageLL.setLayoutParams(layoutParams);

        setImageInLayout(ctx, (int) getResources().getDimension(R.dimen.album_detail_image), (int) getResources().getDimension(R.dimen.album_detail_image), str_albumImage, albumImage);
        albumImage.setScaleType(ImageView.ScaleType.FIT_XY);

        albumImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlbumImageClick(str_albumImage);
            }
        });

        try {
            largeIcon = new GetBitmapTask().execute(str_albumImage).get();
            int width = largeIcon.getWidth();
            int height = largeIcon.getHeight();
            int newWidth = largeIcon.getWidth();
            int newHeight = 80;

            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;

            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);

            Bitmap resizedBitmap = Bitmap.createBitmap(largeIcon, 0, 0,
                    width, height, matrix, true);

            bannerImage.setImageBitmap(createBitmap_ScriptIntrinsicBlur(resizedBitmap, 100.0f));
        } catch (Exception e) {
            e.printStackTrace();
        }

        albumName.setText(str_albumName);

        if (songs_count > 0) {
            mAdapter = new DownloadAlbumDetailsAdapter(ctx, album_songs, navigate_from, str_albumImage);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.mp3hindi_songlist_cart:
                DownloadManagerAlbumSongs data = (DownloadManagerAlbumSongs) view.getTag(R.string.data);
                String str_songName = data.getSong_name();
                checkIfFileExists(data, str_songName);

                break;
        }
    }

    private void getMp3URL(String id, String encryptedId, final DownloadManagerAlbumSongs data) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().downloadSongs(getFromPrefs(SaregamaConstants.SESSION_ID), encryptedId, id, new Callback<DownloadSongsPojo>() {
                @Override
                public void success(DownloadSongsPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            String url_song = basePojo.getData().getUrl();

                            int count = Integer.parseInt(data.getDownload_count());
                            count += 1;
                            data.setDownload_count(count + "");
                            mAdapter.notifyDataSetChanged();

                            file_download(url_song, filename);
                            Toast.makeText(ctx, getAppConfigJson().getDownload_initiated(), Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.displayCommonDialogWithHeader("Maximum Limit Exceeded", getAppConfigJson().getDownload_complete());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });

        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    public void file_download(String uRl, String file_name) {
        File direct = new File(Environment.getExternalStorageDirectory() + "/Temp");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

//        String frm[] = file_name.split("\\.");
//        String new_name = frm[0]+"(sarega.ma)."+frm[1];
        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle(file_name)
                .setDescription("")
                .setDestinationInExternalPublicDir("/Temp", file_name);

        mgr.enqueue(request);

    }

    private void downloadAgainDialog(final DownloadManagerAlbumSongs data) {
        Button OkButtonLogout;
        final Dialog dialogDownloadAgain = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogDownloadAgain.setContentView(R.layout.custom_dialog);
        if (!dialogDownloadAgain.isShowing()) {
            TextView msg_textView = (TextView) dialogDownloadAgain.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) dialogDownloadAgain.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(getAppConfigJson().getDownload_already());
            dialog_header.setText("Download Manager");
            OkButtonLogout = (Button) dialogDownloadAgain.findViewById(R.id.btn_yes_exit);
            OkButtonLogout.setText("YES");
            Button CancelButtonLogout = (Button) dialogDownloadAgain.findViewById(R.id.btn_no_exit);
            CancelButtonLogout.setText("NO");
            ImageView dialog_header_cross = (ImageView) dialogDownloadAgain.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogDownloadAgain.dismiss();
                }
            });

            OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogDownloadAgain.dismiss();
                    checkDownloadCondition(data);
                }
            });

            CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogDownloadAgain.dismiss();
                }
            });

            dialogDownloadAgain.show();
        }
    }

    public void checkIfFileExists(DownloadManagerAlbumSongs data, final String str_songName) {
        if (data.getC_type().equals(getAppConfigJson().getSong_type().getSONGHD() + ""))
            filename = str_songName + ".wav";
        else
            filename = str_songName + ".mp3";

        boolean fileExists = new File(Environment.getExternalStorageDirectory() + "/MusicStore/" + filename).isFile();

        if (!fileExists) {
            checkDownloadCondition(data);
        } else {
            downloadAgainDialog(data);
        }
    }

    private void checkDownloadCondition(DownloadManagerAlbumSongs data) {
        if (Integer.parseInt(data.getC_type()) == getAppConfigJson().getC_type().getSONGHD() && (Integer.parseInt(data.getWav_file_size()) / SaregamaConstants.KB_TO_MB) > getAppConfigJson().getMax_download_size() && getFromPrefs(SaregamaConstants.DOWNLOAD_SIZE_ALERT).equals("")) {
            displaySizeAlertDialog(data, (Integer.parseInt(data.getWav_file_size()) / SaregamaConstants.KB_TO_MB));
        } else if (Integer.parseInt(data.getC_type()) == getAppConfigJson().getC_type().getSONG() && (Integer.parseInt(data.getFile_size()) / SaregamaConstants.KB_TO_MB) > getAppConfigJson().getMax_download_size() && getFromPrefs(SaregamaConstants.DOWNLOAD_SIZE_ALERT).equals("")) {
            displaySizeAlertDialog(data, (Integer.parseInt(data.getFile_size()) / SaregamaConstants.KB_TO_MB));
        } else
            proceedWithDownload(data);
    }

    private void proceedWithDownload(DownloadManagerAlbumSongs data) {
        String id = data.getId();
        String encryptedId = base64Encoding(data.getOrder_no());
        if (data.getDownload_count() != null) {
            if (getFromPrefs(SaregamaConstants.MP3).equals("checked") && checkConnectedNetwork().equals("wifi") && data.getC_type().equals(getAppConfigJson().getSong_type().getSONG() + "")) {
                getMp3URL(id, encryptedId, data);
            } else if ((getFromPrefs(SaregamaConstants.MP3) == null || getFromPrefs(SaregamaConstants.MP3).isEmpty() || getFromPrefs(SaregamaConstants.MP3).equals("unchecked")) && data.getC_type().equals(getAppConfigJson().getSong_type().getSONG() + "")) {
                if (checkConnectedNetwork().equals("wifi"))
                    getMp3URL(id, encryptedId, data);
                else
                    changeSettingsDialog(id, encryptedId, data);
            } else if (getFromPrefs(SaregamaConstants.HD).equals("checked") && checkConnectedNetwork().equals("wifi") && data.getC_type().equals(getAppConfigJson().getSong_type().getSONGHD() + "")) {
                getMp3URL(id, encryptedId, data);
            } else if ((getFromPrefs(SaregamaConstants.HD) == null || getFromPrefs(SaregamaConstants.HD).isEmpty() || getFromPrefs(SaregamaConstants.HD).equals("unchecked")) && data.getC_type().equals(getAppConfigJson().getSong_type().getSONGHD() + "")) {
                if (checkConnectedNetwork().equals("wifi"))
                    getMp3URL(id, encryptedId, data);
                else
                    changeSettingsDialog(id, encryptedId, data);
            } else {
                Toast.makeText(ctx, "Please turn on the wi-fi to download", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void displaySizeAlertDialog(final DownloadManagerAlbumSongs data, int size) {
        Button OkButtonLogout;
        final Dialog dialogDownloadAgain = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogDownloadAgain.setContentView(R.layout.custom_dialog);
        if (!dialogDownloadAgain.isShowing()) {
            TextView msg_textView = (TextView) dialogDownloadAgain.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) dialogDownloadAgain.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            LinearLayout do_not_show_layout = (LinearLayout) dialogDownloadAgain.findViewById(R.id.do_not_show_layout);
            do_not_show_layout.setVisibility(View.VISIBLE);
            final CheckBox check_box = (CheckBox) dialogDownloadAgain.findViewById(R.id.check_box);
            do_not_show_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (check_box.isChecked()) {
                        check_box.setButtonDrawable(R.mipmap.check_box_artist);
                        check_box.setChecked(false);
                    }
                    else {
                        check_box.setButtonDrawable(R.mipmap.check_box_artist_selected);
                        check_box.setChecked(true);
                    }
                }
            });
            msg_textView.setText(getAppConfigJson().getMax_download().replace("SONG_SIZE", size+""));
            dialog_header.setText("Download Manager");
            OkButtonLogout = (Button) dialogDownloadAgain.findViewById(R.id.btn_yes_exit);
            OkButtonLogout.setText("OK");
            Button CancelButtonLogout = (Button) dialogDownloadAgain.findViewById(R.id.btn_no_exit);
            CancelButtonLogout.setText("CANCEL");
            ImageView dialog_header_cross = (ImageView) dialogDownloadAgain.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogDownloadAgain.dismiss();
                }
            });

            OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogDownloadAgain.dismiss();
                    if(check_box.isChecked())
                        saveIntoPrefs(SaregamaConstants.DOWNLOAD_SIZE_ALERT, "no");
                    else
                        saveIntoPrefs(SaregamaConstants.DOWNLOAD_SIZE_ALERT, "");
                    proceedWithDownload(data);
                }
            });

            CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogDownloadAgain.dismiss();
                }
            });

            dialogDownloadAgain.show();
        }
    }

    private void changeSettingsDialog(final String id, final String encryptedId, final DownloadManagerAlbumSongs data){
        Button OkButton;
        final Dialog commonDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        commonDialog.setContentView(R.layout.custom_dialog);
        if (!commonDialog.isShowing()) {
            TextView msg_textView = (TextView) commonDialog.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) commonDialog.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(getAppConfigJson().getDownload_setting());
            dialog_header.setText("Download Manager");
            OkButton = (Button) commonDialog.findViewById(R.id.btn_yes_download);
            OkButton.setText("Continue");
            Button CancelButton = (Button) commonDialog.findViewById(R.id.btn_no_download);
            CancelButton.setText("Go to Settings");
            LinearLayout custom_dialog_bottom_layout_settings = (LinearLayout) commonDialog.findViewById(R.id.custom_dialog_bottom_layout_settings);
            LinearLayout custom_dialog_bottom_layout = (LinearLayout) commonDialog.findViewById(R.id.custom_dialog_bottom_layout);
            custom_dialog_bottom_layout.setVisibility(View.GONE);
            custom_dialog_bottom_layout_settings.setVisibility(View.VISIBLE);
            ImageView dialog_header_cross = (ImageView) commonDialog.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    commonDialog.dismiss();
                }
            });

            OkButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    getMp3URL(id, encryptedId, data);
                    commonDialog.dismiss();
                }
            });

            CancelButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    removeActivity(getResources().getString(R.string.package_name) + ".SettingsActivity");
                    Intent intent = new Intent(ctx, SettingsActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                    commonDialog.dismiss();
                }
            });

            commonDialog.show();
        }
    }
}
