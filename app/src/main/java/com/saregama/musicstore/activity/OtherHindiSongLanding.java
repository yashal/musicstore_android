package com.saregama.musicstore.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.OtherLandingPagerAdapter;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class OtherHindiSongLanding extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {
    private OtherLandingPagerAdapter mAdapter;
    private ViewPager viewPager;

    private LinearLayout song_layout, artist_layout, song_selector, artist_selector;
    private TextView song_text, artist_text;

    public RecyclerView.Adapter folk_adapter;
    public ArrayList<MP3HindiSongListPojo> arrdata_folk;
    public ArrayList<MP3HindiSongListPojo> arrdata_pop;
    public RecyclerView.Adapter pop_adapter;

    private CircularSeekBar seekbar;
    private ImageView previous_view;
    private ImageView imageView;
    private MP3HindiSongListPojo data_playing;

    private int fragment_pos = 0;
    private int c_type;
    private int language_id;
    private Activity ctx = this;
//    private FragmentManager fragmentManager;
//    private EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hd_hindi_landing);

        SaregamaConstants.ACTIVITIES.add(ctx);
        c_type = getIntent().getIntExtra("c_type", 1);

//        String coming_from = getIntent().getStringExtra("coming_from");

        if (arrdata_folk == null)
            arrdata_folk = new ArrayList<>();

//        if (coming_from != null && coming_from.equals("search")) {
//            LinearLayout page_data = (LinearLayout) findViewById(R.id.page_data);
//            page_data.setVisibility(View.GONE);
//
//            String fragment_pos = getIntent().getStringExtra("fragment_pos");
//            if (fragment_pos.equalsIgnoreCase("pop")) {
//                setDrawerAndToolbar("Pop Songs");
//                this.fragment_pos = 0;
//                PopFragment fragment = new PopFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "PopFragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("folk")) {
//                setDrawerAndToolbar("Folk Songs");
//                this.fragment_pos = 1;
//                FolkFragment fragment = new FolkFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "FolkFragment").commit();
//            }
//        } else {
            setDrawerAndToolbar(getIntent().getStringExtra("languagename"));

            language_id = getIntent().getIntExtra("languageid", 0);

            song_layout = (LinearLayout) findViewById(R.id.tab1_layout);
            artist_layout = (LinearLayout) findViewById(R.id.tab2_layout);

            song_selector = (LinearLayout) findViewById(R.id.tab1_selector);
            artist_selector = (LinearLayout) findViewById(R.id.tab2_selector);

            song_text = (TextView) findViewById(R.id.tab1_text);
            artist_text = (TextView) findViewById(R.id.tab2_text);

//            search = (EditText) findViewById(R.id.search_song);
//            softKeyboardDoneClickListener(search);
//            search.setHint("Search Pop Songs, Folk Songs");
//
//            search.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(ctx, DevotionalSearchActivity.class);
//                    intent.putExtra("search_from", "others");
//                    intent.putExtra("c_type", c_type);
//                    intent.putExtra("languageid", language_id);
//                    startActivity(intent);
//                }
//            });

            song_text.setText("Pop Songs");
            artist_text.setText("Folk Songs");

            mAdapter = new OtherLandingPagerAdapter(getSupportFragmentManager());
            viewPager = (ViewPager) findViewById(R.id.hd_landing_pager);
            viewPager.setAdapter(mAdapter);
//            viewPager.setPageTransformer(true, new DepthPageTransformer());
            viewPager.setOffscreenPageLimit(1);

            song_layout.setOnClickListener(this);
            artist_layout.setOnClickListener(this);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    setStyle(position);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            });
//        }

        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab1_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.tab2_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
//                AddToCartWithoutLogin(c_type + "", data1.getSong_id());
                CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);
                QuickAction quickAction = setupQuickAction(data_addtocart.getSong_id(), c_type);
                quickAction.show(addToCart);
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            default:
                break;
        }
    }

    private void setStyle(int position) {
        fragment_pos = position;

        if (position == 0) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            song_selector.setVisibility(View.VISIBLE);
            artist_selector.setVisibility(View.GONE);
        } else if (position == 1) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));

            song_selector.setVisibility(View.GONE);
            artist_selector.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void notifyAdapter() {
        if (folk_adapter != null)
            folk_adapter.notifyDataSetChanged();
        if (pop_adapter != null)
            pop_adapter.notifyDataSetChanged();
    }

    private void playMP3Song(ImageView imageView, View v) {
        hideSoftKeyboard();
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        if (data_playing.is_playing()) {
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            if (fragment_pos == 1) {
                for (int i = 0; i < arrdata_folk.size(); i++)
                    arrdata_folk.get(i).setIs_playing(false);
            } else if (fragment_pos == 0) {
                for (int i = 0; i < arrdata_pop.size(); i++)
                    arrdata_pop.get(i).setIs_playing(false);
            }

//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            data_playing.setIs_playing(true);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            imageView.setImageResource(R.mipmap.pause_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        previous_view = imageView;
        seekbar.setVisibility(View.VISIBLE);
        playPauseButtonClickListener(c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }

    /*public void showListBubble(View v) {
        if (fragment_pos == 0) {
            PopFragment pop_fragment = (PopFragment) mAdapter.getFragment(fragment_pos);
            if (pop_fragment != null)
                pop_fragment.showListBubble(v);
        } else if (fragment_pos == 1) {
            FolkFragment folk_fragment = (FolkFragment) mAdapter.getFragment(fragment_pos);
            if (folk_fragment != null)
                folk_fragment.showListBubble(v);
        }
    }*/
}
