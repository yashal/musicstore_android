package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.HomePagerAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.AppConfigDataPojo;
import com.saregama.musicstore.pojo.BasePojo;
import com.saregama.musicstore.pojo.HomeBannerPojo;
import com.saregama.musicstore.pojo.OfferPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;
import com.saregama.musicstore.util.SquareType;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeActivity extends BaseActivity {

    private ViewPager viewPager;
    private Activity ctx = this;
    private HomePagerAdapter mAdapter;
    private boolean doubleBackToExitPressedOnce = false;

    /* Variables below this are Used for Showing Fragment Data */
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;

    private LinearLayout llFrame;
    private int height, width;
    private SquareType squareType;

    private int padding = 6;
    private double divide_by = 1;
    private Dialog dialog_incentivisation;

    private String notification_key;
    public AppConfigDataPojo appConfigData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setDrawerAndToolbar("Music");

        TextView header = (TextView) findViewById(R.id.header);
        header.requestLayout();
        LinearLayout.LayoutParams rlp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        header.setLayoutParams(rlp);

        ImageView logo = (ImageView) findViewById(R.id.logo);
        logo.setVisibility(View.VISIBLE);

        appConfigData = getAppConfigJson();

        mAdapter = new HomePagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(mAdapter);

        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);

        System.out.println("hh subtract home is "+getResources().getInteger(R.integer.subtract_home));

        TextView header_light = (TextView) findViewById(R.id.header_light);
        header_light.setVisibility(View.VISIBLE);

        if (getIntent().getStringExtra("notification_key") != null) {
            notification_key = getIntent().getStringExtra("notification_key");
            if (notification_key.equals("home_mp3")) {
                viewPager.setCurrentItem(0);
            } else if (notification_key.equals("offer_popup_mp3")) {
                if (getIntent().getStringExtra("content_id") != null) {
                    viewPager.setCurrentItem(0);

                    String asyncURL = SaregamaConstants.BASE_URL + "offer?coupon_id=" + getIntent().getStringExtra("content_id") + "&banner_type=offer&song_type=" + appConfigData.getSong_type().getSONG();
                    new getOfferDetailTask().execute(asyncURL);
                }
            }
        }
    }

    /* Called from MP3 Fragment and HD Fragment to display the data dynamically*/
    public void displayData(View view, List<HomeBannerPojo> items) {

        llFrame = (LinearLayout) view.findViewById(R.id.ll_frame);

        /*if ((getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) ){
            if (getFromPrefs(SaregamaConstants.MOBILE) == null || getFromPrefs(SaregamaConstants.MOBILE).length() == 0)
                setAlertproblemDialog();
        }*/
        int itemSize = 0;
        int square_item = 0;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getGrid().equals("horizontal")) {
                itemSize += 1;
            } else if (items.get(i).getGrid().equals("square"))
                square_item += 1;
            if (square_item == 2) {
                itemSize += 1;
                square_item = 0;
            }
        }

        /* If data size is less than 5 than divide the screen by item size else divide by 4 */
        if (itemSize < 4 && itemSize > 0) {
            divide_by = itemSize;
            if (items.size() == 1)
                padding = 0;
        } else
            divide_by = 4.3;

        DisplayMetrics metrics = new DisplayMetrics();
        ctx.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        // layout helper....
        LinearLayout outerBox = null;
        GridCreationState creationState = GridCreationState.START_NEW_ROW;
        int counter = 0;

        for (final HomeBannerPojo item : items) {

            /*Code For Testing Single Banner*/
//                height = metrics.heightPixels - getResources().getInteger(R.integer.subtract_home);
//                height = (int) Math.round(height / 1);
//                width = metrics.widthPixels;
//                counter++;
//                if (counter == items.size()) {
//                    padding = 0;
//                } else
//                    padding = 0;
             /*Code For Testing Single Banner*/

            if (itemSize < 4) {
                height = metrics.heightPixels - getResources().getInteger(R.integer.subtract_home);
            } else {
                height = metrics.heightPixels;
            }

            height = (int) Math.round(height / divide_by);
            width = metrics.widthPixels;
            counter++;
            if (counter == items.size()) {
                padding = 0;
            } else
                padding = 6;
            if (creationState == GridCreationState.START_NEW_ROW) {
                if (item.getGrid().equals("horizontal")) {
                    squareType = SquareType.LARGE_ITEM;
                } else {
                    squareType = SquareType.MEDIUM_ITEM;
                }

                // add outer layoutbox...
                outerBox = new LinearLayout(view.getContext());
                outerBox.setOrientation(LinearLayout.HORIZONTAL);

                LinearLayout.LayoutParams boxParams = new LinearLayout.LayoutParams(width, height);
                outerBox.setLayoutParams(boxParams);

                outerBox.setPadding(0, 0, 0, padding);
                llFrame.addView(outerBox);

                switch (squareType) {
                    case LARGE_ITEM: {
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
                        RelativeLayout rl = new RelativeLayout(view.getContext());
                        rl.setBackgroundColor(Color.parseColor("#d8dedf"));
                        rl.setLayoutParams(layoutParams);

                        ImageView iv = initImage(item, width, height);
                        rl.addView(iv);
                        outerBox.addView(rl);

                        rl.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                setClickListener(item);
                            }
                        });

                        creationState = GridCreationState.START_NEW_ROW;
                        break;
                    }
                    case MEDIUM_ITEM: {

                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 2, height);
                        RelativeLayout rl = new RelativeLayout(view.getContext());
                        rl.setBackgroundColor(Color.parseColor("#d8dedf"));
                        rl.setLayoutParams(layoutParams);
                        rl.setPadding(0, 0, padding, 0);
                        width = width / 2;
                        ImageView iv = initImage(item, width, height);
                        rl.addView(iv);

                        outerBox.addView(rl);

                        rl.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                setClickListener(item);
                            }
                        });

                        creationState = GridCreationState.ADDED_MEDIUM;
                        break;
                    }
                }

            } else {
                switch (creationState) {
                    case ADDED_MEDIUM: {
                        // add inner layoutbox...
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 2, height);
                        RelativeLayout rl = new RelativeLayout(view.getContext());
                        rl.setBackgroundColor(Color.parseColor("#d8dedf"));
                        rl.setLayoutParams(layoutParams);
                        width = width / 2;
                        ImageView iv = initImage(item, width, height);
                        rl.addView(iv);

                        outerBox.addView(rl);

                        rl.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                setClickListener(item);
                            }
                        });
                        creationState = GridCreationState.START_NEW_ROW;
                        break;
                    }
                }
            }
        }
    }

    private void setClickListener(HomeBannerPojo item) {
        if (item.getType().equals("content")) {
            if (item.getUrl().contains(SaregamaConstants.BASE_URL + "album?album_id")) {
                Intent intent = new Intent(this, Mp3HindiAlbumDetail.class);
                intent.putExtra("home_url", item.getUrl());
                intent.putExtra("c_type", item.getC_type());
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            } else if (item.getUrl().contains(SaregamaConstants.BASE_URL + "artist?artist_id")) {
                Intent intent = new Intent(this, ClassicalHindustaniSOngDetail.class);
                intent.putExtra("home_url", item.getUrl());
                intent.putExtra("header", "Artiste Detail");
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            } else
                sendToCategoryListPage(item);
        } else if (item.getType().equals("offer") && item.getUrl().equals(SaregamaConstants.BASE_URL + "offer")) {
            Intent intent = new Intent(ctx, AvailOffersActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        } else {
            new getOfferDetailTask().execute(item.getUrl());
        }
    }

    /* Getting Detail of Offer on Clicking the Offer Banner */
    private class getOfferDetailTask extends AsyncTask<String, String, Void> {
        ProgressDialog dialog_p;
        OfferPojo basePojo;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                dialog_p = SaregamaDialogs.showLoading(ctx);
                dialog_p.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(getAsyncTaskData(params[0]), OfferPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (dialog_p != null && dialog_p.isShowing()) {
                dialog_p.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (dialog_p != null && dialog_p.isShowing()) {
                dialog_p.dismiss();
            }
            if (cd.isConnectingToInternet()) {

                if (basePojo != null) {
                    if (basePojo.getStatus()) {
                        // Show dialog to display Offer Details
                        displayOfferDialog(basePojo.getData());
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private void sendToCategoryListPage(HomeBannerPojo item) {
        Intent mIntent = new Intent(ctx, CategoryListActivity.class);
        mIntent.putExtra("url", item.getUrl());
        startActivity(mIntent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    /* Set the Image in the Imageview */
    private ImageView initImage(HomeBannerPojo item, int width, int height) {
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT);
        ImageView iv = new ImageView(ctx);
        iv.setLayoutParams(params);

        setImageInLayoutHome(ctx, width, height, item.getImg_1000(), iv);
//        iv.setScaleType(ImageView.ScaleType.CENTER_CROP);

        return iv;
    }

    public enum GridCreationState {
        ADDED_MEDIUM, START_NEW_ROW
    }

    private void setAlertproblemDialog() {
        dialog_incentivisation = new Dialog(HomeActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog_incentivisation.setContentView(R.layout.dialog_numberadd);
        dialog_incentivisation.setCancelable(true);
        dialog_incentivisation.show();

        ImageView iView_cross = (ImageView) dialog_incentivisation.findViewById(R.id.cross);

        TextView tv_submit = (TextView) dialog_incentivisation.findViewById(R.id.numberdialog_tv_submit);
        TextView tv_cancel = (TextView) dialog_incentivisation.findViewById(R.id.numberdialog_tv_cancel);

        final EditText contact = (EditText) dialog_incentivisation.findViewById(R.id.contact);

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    final ProgressDialog d = SaregamaDialogs.showLoading(HomeActivity.this);
                    d.setCanceledOnTouchOutside(false);

                    RestClient.get().postmobileno(getFromPrefs(SaregamaConstants.SESSION_ID), contact.getText().toString(), new Callback<BasePojo>() {
                        @Override
                        public void success(BasePojo basePojo, Response response) {

                            if (basePojo != null) {
                                if (basePojo.getStatus() == true) {
                                    saveIntoPrefs(SaregamaConstants.MOBILE, contact.getText().toString());
                                    d.dismiss();
                                    dialog_incentivisation.dismiss();
                                } else {
                                    dialog.displayCommonDialogWithHeader(SaregamaConstants.Number_FAILED, basePojo.getError());
                                    d.dismiss();
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            d.dismiss();
                        }
                    });
                }
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_incentivisation.dismiss();

            }
        });

        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_incentivisation.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        TextView notification_quantity = (TextView) findViewById(R.id.notification_quantity);
        if (notification_layout != null) {
            if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
                if (getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE) > 0) {
                    notification_quantity.setVisibility(View.VISIBLE);
                    notification_quantity.setText("" + getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE));
                } else {
                    notification_quantity.setVisibility(View.GONE);
                }
            } else {
                notification_quantity.setVisibility(View.GONE);
            }
        }
    }
}