package com.saregama.musicstore.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.DevotionalPagerAdapter;
import com.saregama.musicstore.pojo.ClassHindArtistListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class Mp3Devotional extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {

    private DevotionalPagerAdapter mAdapter;
    private ViewPager viewPager;

    private LinearLayout song_layout, movies_layout, artist_layout, geetmala_layout,
            song_selector, movies_selector, artist_selector, geetmala_selector;
    private TextView song_text, movies_text, artist_text, geetmala_text;
    private Activity ctx = this;

    public ArrayList<MP3HindiSongListPojo> arrdata_artis;
    public RecyclerView.Adapter adapter_artis;

    public ArrayList<MP3HindiSongListPojo> arrdata_mantras;
    public RecyclerView.Adapter adapter_mantras;

    public ArrayList<MP3HindiSongListPojo> arrdata_bhajans;
    public RecyclerView.Adapter adapter_bhajans;

    private CircularSeekBar seekbar;
    private ImageView previous_view;
    private ImageView imageView;
    private MP3HindiSongListPojo data_playing;

    private int fragment_pos = 0;
    private int c_type;

//    private EditText search;
//    private TextView no_result_found;
//    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp3_devotional);

        c_type = getIntent().getIntExtra("c_type", 1);
//        String coming_from = getIntent().getStringExtra("coming_from");

        if (arrdata_mantras == null)
            arrdata_mantras = new ArrayList<>();

        if (arrdata_artis == null)
            arrdata_artis = new ArrayList<>();

        if (arrdata_bhajans == null)
            arrdata_bhajans = new ArrayList<>();

//        if (coming_from != null && coming_from.equals("search")) {
//            LinearLayout page_data = (LinearLayout) findViewById(R.id.page_data);
//            page_data.setVisibility(View.GONE);
//
//            String fragment_pos = getIntent().getStringExtra("fragment_pos");
//
//            if (fragment_pos.equalsIgnoreCase("bhajans")) {
//                setDrawerAndToolbar("Bhajans");
//                this.fragment_pos = 0;
//                Devi_Devta_Devotional_Fragment fragment = new Devi_Devta_Devotional_Fragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "Devi_Devta_Devotional_Fragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("mantras")) {
//                setDrawerAndToolbar("Mantras");
//                this.fragment_pos = 1;
//                Mantras_Devotional_Fragment fragment = new Mantras_Devotional_Fragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "Mantras_Devotional_Fragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("artiste")) {
//                setDrawerAndToolbar("Artistes");
//                this.fragment_pos = 3;
//                Artistes_Devotional_Fragment fragment = new Artistes_Devotional_Fragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "Artistes_Devotional_Fragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("aartis")) {
//                setDrawerAndToolbar("Aartis");
//                this.fragment_pos = 2;
//                Aartis_Devotional_Fragment fragment = new Aartis_Devotional_Fragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "Aartis_Devotional_Fragment").commit();
//            }
//        } else {
        setDrawerAndToolbarWithDropDown("Devotional");
        SaregamaConstants.ACTIVITIES.add(ctx);

//            search = (EditText) findViewById(R.id.search_song);
//            no_result_found = (TextView) findViewById(R.id.no_result_found);
//            softKeyboardDoneClickListener(search);
//            search.setHint("Search Bhajans, Mantras, Aartis, Artistes");
//
//            search.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(ctx, DevotionalSearchActivity.class);
//                    intent.putExtra("search_from", "devotional");
//                    intent.putExtra("c_type", c_type);
//                    startActivity(intent);
//                }
//            });

        song_layout = (LinearLayout) findViewById(R.id.song_layout);
        movies_layout = (LinearLayout) findViewById(R.id.movies_layout);
        artist_layout = (LinearLayout) findViewById(R.id.artist_layout);
        geetmala_layout = (LinearLayout) findViewById(R.id.geetmala_layout);

        song_selector = (LinearLayout) findViewById(R.id.song_selector);
        movies_selector = (LinearLayout) findViewById(R.id.movies_selector);
        artist_selector = (LinearLayout) findViewById(R.id.artist_selector);
        geetmala_selector = (LinearLayout) findViewById(R.id.geetmala_selector);

        song_text = (TextView) findViewById(R.id.song_text);
        movies_text = (TextView) findViewById(R.id.movies_text);
        artist_text = (TextView) findViewById(R.id.artist_text);
        geetmala_text = (TextView) findViewById(R.id.geetmala_text);

        song_text.setText("Bhajans");
        movies_text.setText("Mantras");
        artist_text.setText("Aartis");
        geetmala_text.setText("Artistes");

        mAdapter = new DevotionalPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.mp3_landing_pager);
        viewPager.setAdapter(mAdapter);
//            viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.setOffscreenPageLimit(3);

        song_layout.setOnClickListener(this);
        movies_layout.setOnClickListener(this);
        artist_layout.setOnClickListener(this);
        geetmala_layout.setOnClickListener(this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                setStyle(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        if (getIntent().getStringExtra("notification_key") != null && getIntent().getStringExtra("notification_key").equals("devotional_artist")) {
            viewPager.setCurrentItem(3, false);
            fragment_pos = 3;
            setStyle(3);
        }
//        }
        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.song_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.movies_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            case R.id.artist_layout:
                if (viewPager.getCurrentItem() != 2) {
                    viewPager.setCurrentItem(2);
                    setStyle(2);
                }
                break;

            case R.id.geetmala_layout:
                if (viewPager.getCurrentItem() != 3) {
                    viewPager.setCurrentItem(3);
                    setStyle(3);
                }
                break;

            case R.id.classhindustani_artist_albumLL:
                hideSoftKeyboard();
                Intent intent = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
                ClassHindArtistListPojo data = (ClassHindArtistListPojo) v.getTag(R.string.data);
                intent.putExtra("artist_id", data.getId());
                intent.putExtra("from", "devotional");
                intent.putExtra("c_type", c_type);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
                CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);

                QuickAction quickAction = setupQuickAction(data_addtocart.getSong_id(), c_type);
                quickAction.show(addToCart);
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            default:
                break;
        }
    }

    private void setStyle(int position) {
        fragment_pos = position;
        if (position == 1) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            song_selector.setVisibility(View.GONE);
            movies_selector.setVisibility(View.VISIBLE);
            artist_selector.setVisibility(View.GONE);
            geetmala_selector.setVisibility(View.GONE);
        } else if (position == 0) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            song_selector.setVisibility(View.VISIBLE);
            movies_selector.setVisibility(View.GONE);
            artist_selector.setVisibility(View.GONE);
            geetmala_selector.setVisibility(View.GONE);
        } else if (position == 2) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            song_selector.setVisibility(View.GONE);
            movies_selector.setVisibility(View.GONE);
            artist_selector.setVisibility(View.VISIBLE);
            geetmala_selector.setVisibility(View.GONE);
        } else if (position == 3) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));

            song_selector.setVisibility(View.GONE);
            movies_selector.setVisibility(View.GONE);
            artist_selector.setVisibility(View.GONE);
            geetmala_selector.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void notifyAdapter() {
        if (adapter_artis != null)
            adapter_artis.notifyDataSetChanged();
        if (adapter_mantras != null)
            adapter_mantras.notifyDataSetChanged();
        if (adapter_bhajans != null)
            adapter_bhajans.notifyDataSetChanged();
    }

    private void playMP3Song(ImageView imageView, View v) {
        hideSoftKeyboard();
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        if (data_playing.is_playing()) {
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            if (arrdata_artis != null && arrdata_artis.size() > 0) {
                for (int i = 0; i < arrdata_artis.size(); i++)
                    arrdata_artis.get(i).setIs_playing(false);
            }
            if (arrdata_mantras != null && arrdata_mantras.size() > 0) {
                for (int i = 0; i < arrdata_mantras.size(); i++)
                    arrdata_mantras.get(i).setIs_playing(false);
            }
            if (arrdata_bhajans != null && arrdata_bhajans.size() > 0) {
                for (int i = 0; i < arrdata_bhajans.size(); i++)
                    arrdata_bhajans.get(i).setIs_playing(false);
            }
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");

            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            data_playing.setIs_playing(true);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        seekbar.setVisibility(View.VISIBLE);
        previous_view = imageView;
        playPauseButtonClickListener(c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }

    /*public void showListBubble(View v) {
        if (fragment_pos == 0) {
            Devi_Devta_Devotional_Fragment devidevta_fragment = (Devi_Devta_Devotional_Fragment) mAdapter.getFragment(fragment_pos);
            if (devidevta_fragment != null)
                devidevta_fragment.showListBubble(v);
        } else if (fragment_pos == 1) {
            Mantras_Devotional_Fragment mantra_fragment = (Mantras_Devotional_Fragment) mAdapter.getFragment(fragment_pos);
            if (mantra_fragment != null)
                mantra_fragment.showListBubble(v);
        } else if (fragment_pos == 2) {
            Aartis_Devotional_Fragment artis_fragment = (Aartis_Devotional_Fragment) mAdapter.getFragment(fragment_pos);
            if (artis_fragment != null)
                artis_fragment.showListBubble(v);
        } else if (fragment_pos == 3) {
            Artistes_Devotional_Fragment artistes_fragment = (Artistes_Devotional_Fragment) mAdapter.getFragment(fragment_pos);
            if (artistes_fragment != null)
                artistes_fragment.showListBubble(v);
        }
    }*/
}

