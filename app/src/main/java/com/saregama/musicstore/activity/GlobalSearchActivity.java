package com.saregama.musicstore.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.GlobalSearchAlbumAdapter;
import com.saregama.musicstore.adapter.GlobalSearchArtistAdapter;
import com.saregama.musicstore.adapter.GlobalSearchSongsAdapter;
import com.saregama.musicstore.pojo.GlobalArtistPojo;
import com.saregama.musicstore.pojo.GlobalSearchAutoSuggestsDataPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class GlobalSearchActivity extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {

    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private ListView songs_list__layout_global_search, album_list__layout_global_search, artist_list__layout_global_search;
    private GlobalSearchArtistAdapter globalSearchArtistAdapter_ob;
    private GlobalSearchSongsAdapter globalSearchSongsAdapter_ob;
    private GlobalSearchAlbumAdapter globalSearchAlbumAdapter_ob;
    private ArrayList<GlobalArtistPojo> album_arrlistlist_layout_global_search, artist_arrlistlist_layout_global_search;
    private ArrayList<MP3HindiSongListPojo> songs_arrlistlist_layout_global_search;
    private EditText global_search_main;
    private LinearLayout songs_layout_global_search, album_layout_global_search, artist_layout_global_search;
    private LinearLayout global_search_layout;
    private ImageView logo;
    private GlobalSearchActivity ctx = this;
    private String search_keyword = "";
    private String str = "";
    private TextView header_text_first_global, header_text_second_global, header_text_third_global;
    private TextView songs_viewall, album_viewall, artistes_viewall;
    private ImageView global_search_cross;
    private Timer timer;
    private ImageView imageView;
    private CircularSeekBar seekbar;
    private MP3HindiSongListPojo data_playing;
    private ImageView previous_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_search);

        init();

        //preparing Toolbar
        setDrawerAndToolbar("");
        global_search_main = (EditText) findViewById(R.id.global_search_main);
        global_search_main.setVisibility(View.VISIBLE);
        TextView header = (TextView) findViewById(R.id.header);
        header.setVisibility(View.GONE);

        SaregamaConstants.ACTIVITIES.add(ctx);

        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        cart_layout.setVisibility(View.VISIBLE);

        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        notification_layout.setVisibility(View.GONE);

        // view all list
        songs_viewall = (TextView) findViewById(R.id.songs_view_all);
        album_viewall = (TextView) findViewById(R.id.album_view_all);
        artistes_viewall = (TextView) findViewById(R.id.artistes_view_all);

        songs_viewall.setPaintFlags(songs_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        album_viewall.setPaintFlags(album_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        artistes_viewall.setPaintFlags(artistes_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        header_text_first_global = (TextView) findViewById(R.id.header_text_first_global);
        header_text_second_global = (TextView) findViewById(R.id.header_text_second_global);
        header_text_third_global = (TextView) findViewById(R.id.header_text_third_global);

        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);
        global_search.setVisibility(View.GONE);
        global_search_cross = (ImageView) findViewById(R.id.global_search_cross);
        global_search_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global_search_main.setText("");
                if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
                showSoftKeyboard();
            }
        });

        global_search_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
            }
        });

        global_search_main.addTextChangedListener(new TextWatcher() {

            private Timer timer = new Timer();

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (global_search_main.getText().toString().trim().length() < 1) {
                    global_search_cross.setVisibility(View.GONE);
                } else {
                    global_search_cross.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (timer != null)
                    timer.cancel();

                if (global_search_main.getText().toString().trim().length() > 2) {
                    search_keyword = global_search_main.getText().toString().trim();
                    search_keyword = search_keyword.replaceAll(" ", "%20");
                    timer = new Timer();
                    timer.schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    new GetGlobalSearchList().execute();
                                }
                            }, 700
                    );
                } else {
                    songs_arrlistlist_layout_global_search = new ArrayList<>();
                    album_arrlistlist_layout_global_search = new ArrayList<>();
                    artist_arrlistlist_layout_global_search = new ArrayList<>();
                    globalSearchSongsAdapter_ob = new GlobalSearchSongsAdapter(ctx, songs_arrlistlist_layout_global_search, search_keyword);
                    globalSearchAlbumAdapter_ob = new GlobalSearchAlbumAdapter(ctx, album_arrlistlist_layout_global_search, search_keyword);
                    globalSearchArtistAdapter_ob = new GlobalSearchArtistAdapter(ctx, artist_arrlistlist_layout_global_search, search_keyword);
                    songs_list__layout_global_search.setAdapter(globalSearchSongsAdapter_ob);
                    album_list__layout_global_search.setAdapter(globalSearchAlbumAdapter_ob);
                    artist_list__layout_global_search.setAdapter(globalSearchArtistAdapter_ob);
                    songs_layout_global_search.setVisibility(View.GONE);
                    album_layout_global_search.setVisibility(View.GONE);
                    artist_layout_global_search.setVisibility(View.GONE);
                }
            }

        });

        if (getIntent().getStringExtra("search_text") != null)
            global_search_main.setText(getIntent().getStringExtra("search_text"));

        global_search_main.setSelection(global_search_main.getText().length());

        global_search_main.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    hideSoftKeyboard();
                }
                return false;
            }
        });

        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    private class GetGlobalSearchList extends AsyncTask<Void, Void, Void> {
        private GlobalSearchAutoSuggestsDataPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            str = getAsyncTaskDataGlobal(SaregamaConstants.BASE_URL + "search?type=autosuggest&query=" + search_keyword);

            JsonReader reader = new JsonReader(new StringReader(str));
            reader.setLenient(true);
            basePojo = gsonObj.fromJson(reader, GlobalSearchAutoSuggestsDataPojo.class);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {

                    songs_arrlistlist_layout_global_search = new ArrayList<>();
                    album_arrlistlist_layout_global_search = new ArrayList<>();
                    artist_arrlistlist_layout_global_search = new ArrayList<>();

                    int artist_count = 0;
                    int album_count = 0;
                    int songs_count = 0;
                    int max, min;
                    boolean artist = false, songs_bool = false, albums = false;

                    if (str.contains("\"artist\":"))
                        artist_count = str.indexOf("\"artist\":");
                    if (str.contains("\"song\":"))
                        songs_count = str.indexOf("\"song\":");
                    if (str.contains("\"album\":"))
                        album_count = str.indexOf("\"album\":");

                    if (artist_count >= album_count)
                        if (artist_count >= songs_count) {
                            max = artist_count;
                            if (album_count >= songs_count)
                                min = songs_count;
                            else
                                min = album_count;
                        } else {
                            max = songs_count;
                            min = album_count;
                        }
                    else if (album_count >= songs_count) {
                        max = album_count;
                        if (artist_count >= songs_count) min = songs_count;
                        else min = artist_count;
                    } else {
                        max = songs_count;
                        if (artist_count >= album_count) min = album_count;
                        else min = artist_count;
                    }

                    if (max == artist_count && artist_count > 0) {
                        artist = true;
                        artist_layout_global_search.setVisibility(View.VISIBLE);
                        header_text_third_global.setText(getResources().getString(R.string.artist));
                        setArtistesAdapter(basePojo, artist_list__layout_global_search, artistes_viewall);
                    } else if (max == songs_count && songs_count > 0) {
                        songs_bool = true;
                        artist_layout_global_search.setVisibility(View.VISIBLE);
                        header_text_third_global.setText(getResources().getString(R.string.songs));
                        setSongsAdapter(basePojo, artist_list__layout_global_search, artistes_viewall);
                    } else if (max == album_count && album_count > 0) {
                        albums = true;
                        artist_layout_global_search.setVisibility(View.VISIBLE);
                        header_text_third_global.setText(getResources().getString(R.string.album_text));
                        setAlbumAdapter(basePojo, artist_list__layout_global_search, artistes_viewall);
                    } else {
                        artist_layout_global_search.setVisibility(View.GONE);
                    }
                    if (min > 0) {
                        if (min == artist_count && artist_count > 0) {
                            artist = true;
                            header_text_first_global.setText(getResources().getString(R.string.artist));
                            songs_layout_global_search.setVisibility(View.VISIBLE);
                            setArtistesAdapter(basePojo, songs_list__layout_global_search, songs_viewall);
                        } else if (min == songs_count && songs_count > 0) {
                            songs_bool = true;
                            header_text_first_global.setText(getResources().getString(R.string.songs));
                            songs_layout_global_search.setVisibility(View.VISIBLE);
                            setSongsAdapter(basePojo, songs_list__layout_global_search, songs_viewall);
                        } else if (min == album_count && album_count > 0) {
                            albums = true;
                            header_text_first_global.setText(getResources().getString(R.string.album_text));
                            songs_layout_global_search.setVisibility(View.VISIBLE);
                            setAlbumAdapter(basePojo, songs_list__layout_global_search, songs_viewall);
                        } else {
                            songs_layout_global_search.setVisibility(View.GONE);
                        }
                    } else {
                        songs_layout_global_search.setVisibility(View.GONE);
                    }

                    if (!artist && artist_count > 0) {
                        header_text_second_global.setText(getResources().getString(R.string.artist));
                        album_layout_global_search.setVisibility(View.VISIBLE);
                        setArtistesAdapter(basePojo, album_list__layout_global_search, album_viewall);
                    } else if (!songs_bool && songs_count > 0) {
                        header_text_second_global.setText(getResources().getString(R.string.songs));
                        album_layout_global_search.setVisibility(View.VISIBLE);
                        setSongsAdapter(basePojo, album_list__layout_global_search, album_viewall);
                    } else if (!albums && album_count > 0) {
                        header_text_second_global.setText(getResources().getString(R.string.album_text));
                        album_layout_global_search.setVisibility(View.VISIBLE);
                        setAlbumAdapter(basePojo, album_list__layout_global_search, album_viewall);
                    } else {
                        album_layout_global_search.setVisibility(View.GONE);
                    }
                    if(album_count == 0 & songs_count == 0 & artist_count == 0)
                        dialog.displayCommonDialogWithHeader("Search", "No results found for '"+search_keyword+"' Please check if you have entered the correct spelling or try a different keyword.");
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            }
        }
    }

    private void navigateToTabListActivity(String fragment_pos) {
        Intent intent = new Intent(ctx, GlobalSearchTabListActivity.class);
        intent.putExtra("search_text", global_search_main.getText().toString().trim());
        intent.putExtra("fragment_pos", fragment_pos);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private void setAlbumAdapter(GlobalSearchAutoSuggestsDataPojo basePojo, ListView list, TextView viewAll) {
        if(basePojo.getData() != null && basePojo.getData().getList() != null) {
            album_arrlistlist_layout_global_search = basePojo.getData().getList().getAlbum();
            globalSearchAlbumAdapter_ob = new GlobalSearchAlbumAdapter(ctx, album_arrlistlist_layout_global_search, search_keyword);
            if (album_arrlistlist_layout_global_search != null && album_arrlistlist_layout_global_search.size() > 0) {
                list.setAdapter(globalSearchAlbumAdapter_ob);
                ListUtils.setDynamicHeight(list);
            }
            globalSearchAlbumAdapter_ob.notifyDataSetChanged();
            viewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToTabListActivity("album");
                }
            });
        }
    }

    private void setArtistesAdapter(GlobalSearchAutoSuggestsDataPojo basePojo, ListView list, TextView viewAll) {
        if(basePojo.getData() != null && basePojo.getData().getList() != null) {
            artist_arrlistlist_layout_global_search = basePojo.getData().getList().getArtist();
            globalSearchArtistAdapter_ob = new GlobalSearchArtistAdapter(ctx, artist_arrlistlist_layout_global_search, search_keyword);
            if (artist_arrlistlist_layout_global_search != null && artist_arrlistlist_layout_global_search.size() > 0) {
                list.setAdapter(globalSearchArtistAdapter_ob);
                ListUtils.setDynamicHeight(list);
            }
            globalSearchArtistAdapter_ob.notifyDataSetChanged();
            viewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToTabListActivity("artistes");
                }
            });
        }
    }

    private void setSongsAdapter(GlobalSearchAutoSuggestsDataPojo basePojo, ListView list, TextView viewAll) {
        if(basePojo.getData() != null && basePojo.getData().getList() != null) {
            songs_arrlistlist_layout_global_search = basePojo.getData().getList().getSong();
            globalSearchSongsAdapter_ob = new GlobalSearchSongsAdapter(ctx, songs_arrlistlist_layout_global_search, search_keyword);
            if (songs_arrlistlist_layout_global_search != null && songs_arrlistlist_layout_global_search.size() > 0) {
                list.setAdapter(globalSearchSongsAdapter_ob);
                ListUtils.setDynamicHeight(list);
            }
            globalSearchSongsAdapter_ob.notifyDataSetChanged();
            viewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToTabListActivity("songs");
                }
            });
        }
    }

    private void init() {
        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);
        songs_list__layout_global_search = (ListView) findViewById(R.id.songs_list__layout_global_search);
        album_list__layout_global_search = (ListView) findViewById(R.id.album_list__layout_global_search);
        artist_list__layout_global_search = (ListView) findViewById(R.id.artist_list__layout_global_search);

        songs_arrlistlist_layout_global_search = new ArrayList<>();
        album_arrlistlist_layout_global_search = new ArrayList<>();
        artist_arrlistlist_layout_global_search = new ArrayList<>();

        global_search_layout = (LinearLayout) findViewById(R.id.global_search_layout);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width * 3 / 4 + getResources().getInteger(R.integer.appbar_search_add), ViewGroup.LayoutParams.WRAP_CONTENT);
        global_search_layout.setLayoutParams(params);
        global_search_layout.setVisibility(View.VISIBLE);
        logo = (ImageView) findViewById(R.id.logo);
        logo.setVisibility(View.GONE);

        songs_layout_global_search = (LinearLayout) findViewById(R.id.songs_layout_global_search);
        album_layout_global_search = (LinearLayout) findViewById(R.id.album_layout_global_search);
        artist_layout_global_search = (LinearLayout) findViewById(R.id.artist_layout_global_search);
        songs_layout_global_search.setVisibility(View.GONE);
        album_layout_global_search.setVisibility(View.GONE);
        artist_layout_global_search.setVisibility(View.GONE);

        /*globalSearchArtistAdapter_ob = new GlobalSearchArtistAdapter(this, songs_arrlistlist_layout_global_search);
        songs_list__layout_global_search.setAdapter(globalSearchArtistAdapter_ob);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.slider_transparent_album:
                hideSoftKeyboard();
                for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
                    if (SaregamaConstants.ACTIVITIES.get(i) != null && SaregamaConstants.ACTIVITIES.get(i).toString().contains(getResources().getString(R.string.package_name) + ".GlobalSearchAlbumDetail")) {
                        SaregamaConstants.ACTIVITIES.get(i).finish();
                        SaregamaConstants.ACTIVITIES.remove(i);
                        break;
                    }
                }
                Intent intent = new Intent(ctx, GlobalSearchAlbumDetail.class);
                GlobalArtistPojo data = (GlobalArtistPojo) v.getTag(R.string.data);
                intent.putExtra("query", global_search_main.getText().toString().trim());
                intent.putExtra("album_id", "" + data.getContent_id());
                intent.putExtra("from", data.getName());
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.slider_transparent_artist:
                hideSoftKeyboard();
                for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
                    if (SaregamaConstants.ACTIVITIES.get(i) != null && SaregamaConstants.ACTIVITIES.get(i).toString().contains(getResources().getString(R.string.package_name) + ".ClassicalHindustaniSOngDetail")) {
                        SaregamaConstants.ACTIVITIES.get(i).finish();
                        SaregamaConstants.ACTIVITIES.remove(i);
                        break;
                    }
                }
                Intent mIntent = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
                GlobalArtistPojo mData = (GlobalArtistPojo) v.getTag(R.string.data);
                mIntent.putExtra("artist_id", "" + mData.getContent_id());
                mIntent.putExtra("query", global_search_main.getText().toString().trim());
                mIntent.putExtra("from", "global");
                mIntent.putExtra("header", mData.getName());
                startActivity(mIntent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.playIcon:
                hideSoftKeyboard();
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                hideSoftKeyboard();
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                hideSoftKeyboard();
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            default:
                break;
        }
    }

    private void playMP3Song(ImageView imageView, View v) {
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            notifyAdapter();
        }
        if (data_playing.is_playing()) {
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            notifyAdapter();
        } else {
            for (int i = 0; i < songs_arrlistlist_layout_global_search.size(); i++)
                songs_arrlistlist_layout_global_search.get(i).setIs_playing(false);
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            data_playing.setIs_playing(true);
            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            notifyAdapter();
        }
        seekbar.setVisibility(View.VISIBLE);
        previous_view = imageView;
        playPauseButtonClickListener(getAppConfigJson().getSong_type().getSONG(), imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }

    @Override
    public void notifyAdapter() {
        if (globalSearchSongsAdapter_ob != null)
            globalSearchSongsAdapter_ob.notifyDataSetChanged();
    }

}