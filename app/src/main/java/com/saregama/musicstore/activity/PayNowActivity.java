package com.saregama.musicstore.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appsflyer.AppsFlyerLib;
import com.flurry.android.FlurryAgent;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.LocationAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.AppConfigDataPojo;
import com.saregama.musicstore.pojo.ApplyCouponPojo;
import com.saregama.musicstore.pojo.GetSatePojo;
import com.saregama.musicstore.pojo.GetpaymentlogPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.PaymentPojo;
import com.saregama.musicstore.pojo.ProfilePojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Yash ji on 4/19/2016.
 */
public class PayNowActivity extends BaseActivity implements View.OnClickListener {

    private EditText couponCodeEdit;
    private ImageView couponCodeClear, couponCodeRemove;
    private TextView couponCodeDone, songAmount, couponcode_song, song_discountAmount, song_discountPercent, songTotal, error_message;
    private TextView albumAmount, albumTotal, grandTotal, getcouponcode, got_a_coupon;
    private LinearLayout albumLL, songLL, pay_now, songDiscountLL, songs_total_layout, song_amount_layout;
    private float songs_total, albums_total, grand_total, original_total;

    private final static int PAYMENT_STATUS = 100;
    private PayNowActivity ctx = this;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private int coupon_status = 0;
    private ProgressDialog d;
    private Map<String, String> articleParams;
    private String order_no;
    private String songmp3_mapping = "";
    private String album_mapping = "";
    private String albumhd_mapping = "";
    private String songhd_mapping = "";
    private String content_mapping = "";
    private LinearLayout offers_cart_layout;
    private String coupon_type = "-1";

    // location change functionality for gst
    private TextView changeLocation, residentLocationText;
    private String residance = "";
    private AppConfigDataPojo appConfigData;
    private LinearLayout residence_location;
    private Dialog location_dialog;
    private RecyclerView mRecyclerView;
    private LocationAdapter adapter;
    private ArrayList<String> locationArr;
    private String locationName_str="";
    private ImageView locationSelect;
    private GetSatePojo getStatePojo;
    private LinearLayout locationSelectLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paynow);

        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);
        appConfigData = getAppConfigJson();

        if (appConfigData.getState_name() != null && appConfigData.getCountry_name() != null) {
            residance = appConfigData.getState_name() + ", " + appConfigData.getCountry_name() + ".";
        }
        System.out.println("hh yahsal " + residance);

//        FrameLayout seekbar_root = (FrameLayout) findViewById(R.id.seekbar_root);
//        CoordinatorLayout.LayoutParams buttonLayoutParams = new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.WRAP_CONTENT, CoordinatorLayout.LayoutParams.WRAP_CONTENT);
//        buttonLayoutParams.setMargins(0, 0, 0, 0);
//        buttonLayoutParams.gravity = Gravity.BOTTOM | Gravity.END;
//        seekbar_root.setLayoutParams(buttonLayoutParams);

        couponCodeEdit = (EditText) findViewById(R.id.couponCodeEdit);
        couponCodeClear = (ImageView) findViewById(R.id.couponCodeClear);
        couponCodeRemove = (ImageView) findViewById(R.id.couponCodeRemove);
        couponCodeDone = (TextView) findViewById(R.id.couponCodeDone);
        error_message = (TextView) findViewById(R.id.no_result_found);
        offers_cart_layout = (LinearLayout) findViewById(R.id.offers_cart_layout);
        songs_total_layout = (LinearLayout) findViewById(R.id.songs_total_layout);
        song_amount_layout = (LinearLayout) findViewById(R.id.song_amount_layout);

        ImageView paynow_back = (ImageView) findViewById(R.id.paynow_back);
        paynow_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            }
        });
        songAmount = (TextView) findViewById(R.id.couponCode_songAmount);
        couponcode_song = (TextView) findViewById(R.id.couponcode_song);
        song_discountAmount = (TextView) findViewById(R.id.song_discountAmount);
        song_discountPercent = (TextView) findViewById(R.id.song_discountPercent);
        songTotal = (TextView) findViewById(R.id.couponCode_songTotal);

        albumAmount = (TextView) findViewById(R.id.couponCode_albumAmount);
        albumTotal = (TextView) findViewById(R.id.couponCode_albumTotal);
        grandTotal = (TextView) findViewById(R.id.couponCode_grandTotal);
        getcouponcode = (TextView) findViewById(R.id.getcouponCode);

        if (getAppConfigJson().getCountry_code() != null && getAppConfigJson().getCountry_code().equalsIgnoreCase("IN") && SaregamaConstants.AVAIL_OFFERS > 0)
            getcouponcode.setVisibility(View.VISIBLE);
        else
            getcouponcode.setVisibility(View.GONE);


        got_a_coupon = (TextView) findViewById(R.id.got_a_coupon);

        songLL = (LinearLayout) findViewById(R.id.paynowSongLL);
        albumLL = (LinearLayout) findViewById(R.id.paynowAlbumLL);
        pay_now = (LinearLayout) findViewById(R.id.pay_now);
        songDiscountLL = (LinearLayout) findViewById(R.id.couponcode_songLL);

        locationSelect = (ImageView) findViewById(R.id.locationSelect);
        locationSelectLL = (LinearLayout) findViewById(R.id.locationSelectLL);
        changeLocation = (TextView) findViewById(R.id.changeLocation);
        residentLocationText = (TextView) findViewById(R.id.residentLocationText);
        residence_location = (LinearLayout) findViewById(R.id.residence_location);

        songs_total = getIntent().getFloatExtra("songs_total", 0);
        albums_total = getIntent().getFloatExtra("albums_total", 0);
        grand_total = getIntent().getFloatExtra("grand_total", 0);
        original_total = getIntent().getFloatExtra("grand_total", 0);
        final String currency = getIntent().getStringExtra("currency");

        setValues(songs_total, albums_total, grand_total, 0, currency);
        if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
            residence_location.setVisibility(View.VISIBLE);
//            residentLocationText.setText(getResources().getString(R.string.location_str) + " " + residance);
            getStateData();

            locationSelectLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getStatePojo != null) {
                        System.out.println("hh yashal Status "+getStatePojo.getData().isStatus());
                        if (getStatePojo.getData().isStatus()) {
                            locationSelect.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.untick));
                            getStatePojo.getData().setStatus(false);
//                            showLocationDialog(ctx);
                        } else {
                            locationSelect.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.tick));
                            getStatePojo.getData().setStatus(true);
                        }
                    }

                }
            });
        }


        articleParams = new HashMap<>();

        getcouponcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PayNowActivity.this, AvailOffersActivity.class);
                intent.putExtra("pay_cart", "cart");
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
                    String[] arrStrLoc = residentLocationText.getText().toString().split(": ");
                    if (arrStrLoc != null && arrStrLoc.length > 0 ){
                        locationName_str = arrStrLoc[1];
                    }

                    System.out.println("xxxxxxxxxxxxx yashal "+locationName_str);
                    if (getStatePojo != null) {
                        System.out.println("hh yashal Status " + getStatePojo.getData().isStatus());
                        if (getStatePojo.getData().isStatus()) {
                            if (coupon_status == 0 && !couponCodeEdit.getText().toString().equals(""))
                                dialog.displayCommonDialog(getAppConfigJson().getWrong_coupon());
                            else
                                getPaymentDetails();
                        }
                        else
                        {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.STATE_OF_RESIDENCE, getAppConfigJson().getState_of_residence());
                        }
                    }

                } else {
                    Intent intent = new Intent(ctx, LoginActivity.class);
                    intent.putExtra("from", "cart");
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                }



            }
        });

        couponCodeEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                error_message.setVisibility(View.GONE);
                if (!couponCodeEdit.getText().equals("") && couponCodeEdit.getText().length() > 0)
                    couponCodeClear.setVisibility(View.VISIBLE);
                else {
                    couponCodeClear.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                String result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    couponCodeEdit.setText(result);
                    couponCodeEdit.setSelection(result.length());
                    if (result.length() == 0)
                        couponCodeClear.setVisibility(View.GONE);
                }
            }
        });

        couponCodeRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                couponCodeClear.performClick();
            }
        });

        couponCodeClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                songDiscountLL.setVisibility(View.GONE);
                couponCodeEdit.setEnabled(true);
                couponCodeDone.setEnabled(true);
                couponCodeEdit.setText("");
                coupon_status = 0;
                couponCodeDone.setText("Apply");
                couponCodeClear.setVisibility(View.GONE);
                error_message.setVisibility(View.GONE);
                got_a_coupon.setVisibility(View.VISIBLE);
                getcouponcode.setText(getResources().getString(R.string.view_offers));
                setValues(songs_total, albums_total, original_total, 0, currency);
            }
        });

        couponCodeDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!couponCodeEdit.getText().equals("") && couponCodeEdit.getText().length() > 0) {
                    hideSoftKeyboard();
                    error_message.setVisibility(View.GONE);
                    applyCoupon(couponCodeEdit.getText().toString());
                } else {
                    error_message.setText("Please enter coupon code.");
                    error_message.setVisibility(View.VISIBLE);
                }
            }
        });

        changeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showLocationDialog(ctx);

            }
        });
    }

    public void showLocationDialog(Context ctx) {
        location_dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        location_dialog.setContentView(R.layout.select_location_dialog);

        mRecyclerView = (RecyclerView) location_dialog.findViewById(R.id.change_location_recycler);
        ImageView close_dialog = (ImageView) location_dialog.findViewById(R.id.close_dialog);
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                location_dialog.dismiss();
            }
        });

        locationArr = new ArrayList<>();

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

        locationArr = appConfigData.getIndian_states().getState_name();
        if (locationArr != null && locationArr.size() > 0) {
            adapter = new LocationAdapter(ctx, locationArr, "PayNow");
            mRecyclerView.setAdapter(adapter);
        }

        location_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        location_dialog.show();
    }

    private void applyCoupon(final String coupon_code) {
        if (cd.isConnectingToInternet()) {

            if (d != null && !d.isShowing()) {
                d = SaregamaDialogs.showLoading(ctx);
                d.setCanceledOnTouchOutside(false);
            }

            RestClient.get().applyCoupon(getFromPrefs(SaregamaConstants.SESSION_ID), coupon_code, getFromPrefs(SaregamaConstants.IMEI), getFromPrefs(SaregamaConstants.ID), getAppConfigJson().getCountry_code(), new Callback<ApplyCouponPojo>() {
                @Override
                public void success(final ApplyCouponPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            coupon_status = 1;
                            saveIntoPrefs((SaregamaConstants.COUPON_CODE), coupon_code);
                            couponCodeDone.setText("Applied");
                            getcouponcode.setText(getResources().getString(R.string.change_coupon_code));
                            couponCodeDone.setEnabled(false);
                            couponCodeEdit.setEnabled(false);
                            coupon_type = basePojo.getData().getCoupon_type();
                            songDiscountLL.setVisibility(View.VISIBLE);
                            setValues(songs_total, albums_total, basePojo.getData().getNetpayment(), basePojo.getData().getDiscount(), basePojo.getData().getCurrency());
                            couponcode_song.setText(basePojo.getData().getCoupon());
                            error_message.setVisibility(View.GONE);
                            got_a_coupon.setVisibility(View.GONE);
                        } else {
                            error_message.setText(basePojo.getError());
                            error_message.setVisibility(View.VISIBLE);
                            got_a_coupon.setVisibility(View.GONE);
                            songDiscountLL.setVisibility(View.GONE);
                            couponCodeDone.setText("Apply");
                            coupon_type = "-1";
                            coupon_status = 0;
                            getcouponcode.setText(getResources().getString(R.string.change_coupon_code));
                            couponCodeDone.setEnabled(true);
                            couponCodeEdit.setEnabled(true);
                            setValues(songs_total, albums_total, grand_total, 0, getIntent().getStringExtra("currency"));
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    if (d != null && d.isShowing())
                        d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    if (d != null && d.isShowing())
                        d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void getPaymentDetails() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getPaymentDetails(getFromPrefs(SaregamaConstants.SESSION_ID), "", getFromPrefs(SaregamaConstants.IMEI), getFromPrefs(SaregamaConstants.ID), original_total, grand_total, coupon_status, couponCodeEdit.getText().toString(), getAppConfigJson().getCountry_code(), new Callback<PaymentPojo>() {
                @Override
                public void success(final PaymentPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            order_no = String.valueOf(basePojo.getData().getOrder_id());
                            if (order_no != null && !order_no.isEmpty()) {
                                setStateData(locationName_str, order_no);
                            }
                            if (SaregamaConstants.IS_PLAYING || SaregamaConstants.IS_PAUSED)
                                stopPlayer();
                            Intent intent = new Intent(PayNowActivity.this, PaymentActivity.class);
                            intent.putExtra("coupon_status", coupon_status);
                            intent.putExtra("coupon_code", couponCodeEdit.getText().toString());
                            intent.putExtra("grand_total", grand_total);
                            intent.putExtra("coupon_type", coupon_type);
                            intent.putExtra("order_id", basePojo.getData().getOrder_id());
                            startActivityForResult(intent, PAYMENT_STATUS);
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void setValues(final float songTotalAmt, float AlbumTotalAmt, float grandTotalAmt, final float discountAmt, String currency) {
        float percentage = (discountAmt / songs_total) * 100;
        String str = String.format("%2.0f", percentage);
        NumberFormat nf = NumberFormat.getInstance(); // get instance
        nf.setMaximumFractionDigits(2);

        if (currency != null && currency.equals("Rs.")) {
            currency = getResources().getString(R.string.Rs);
            song_discountAmount.setText("-" + currency + " " + Math.round(discountAmt) + "");
            if (songs_total > 0) {
                songLL.setVisibility(View.VISIBLE);
                songAmount.setText(currency + " " + Math.round(songs_total));
                if (!coupon_type.equals("0"))
                    songTotal.setText(currency + " " + Math.round((songs_total - discountAmt)));
                else
                    songTotal.setText(currency + " " + Math.round(songs_total));
            } else {
                songLL.setVisibility(View.GONE);
            }

            if (albums_total > 0) {
                albumLL.setVisibility(View.VISIBLE);
                albumAmount.setText(currency + " " + Math.round(AlbumTotalAmt));
                albumTotal.setText(currency + " " + Math.round(AlbumTotalAmt));
            } else {
                albumLL.setVisibility(View.GONE);
            }
            grand_total = grandTotalAmt;
            grandTotal.setText(currency + " " + Math.round(grandTotalAmt));
        } else {
            song_discountAmount.setText("-" + currency + " " + discountAmt + "");
            if (songs_total > 0) {
                songLL.setVisibility(View.VISIBLE);
                songAmount.setText(currency + " " + String.format("%.2f", songs_total));
                if (!coupon_type.equals("0"))
                    songTotal.setText(currency + " " + String.format("%.2f", songs_total - discountAmt));
                else
                    songTotal.setText(currency + " " + String.format("%.2f", songs_total));

            } else {
                songLL.setVisibility(View.GONE);
            }

            if (albums_total > 0) {
                albumLL.setVisibility(View.VISIBLE);
                albumAmount.setText(currency + " " + String.format("%.2f", AlbumTotalAmt));
                albumTotal.setText(currency + " " + String.format("%.2f", AlbumTotalAmt));
            } else {
                albumLL.setVisibility(View.GONE);
            }
            grand_total = grandTotalAmt;
            grandTotal.setText(currency + " " + String.format("%.2f", grandTotalAmt));
        }
        if (grand_total == 0.0 && coupon_type.equals("0") && coupon_status == 1) {
            pay_now.setBackground(ContextCompat.getDrawable(ctx, R.mipmap.proceed));
        } else {
            pay_now.setBackground(ContextCompat.getDrawable(ctx, R.mipmap.paynow));
        }
        if (coupon_status == 1) {
            songLL.setVisibility(View.VISIBLE);
            if ((songTotalAmt == 0 || songTotalAmt == 0.0) && coupon_type.equals("0")) {
                songs_total_layout.setVisibility(View.GONE);
                song_amount_layout.setVisibility(View.GONE);
            }

        }
        song_discountPercent.setText(str + "%  Discount");
        song_discountPercent.setVisibility(View.GONE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_STATUS) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                if (data.getStringExtra("result").equals("success")) {
                    emptyCart();
                    for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
                        if (SaregamaConstants.ACTIVITIES.get(i) != null && SaregamaConstants.ACTIVITIES.get(i).toString().contains(getResources().getString(R.string.package_name) + ".CartActivity")) {
                            SaregamaConstants.ACTIVITIES.get(i).finish();
                            SaregamaConstants.ACTIVITIES.remove(i);
                            break;
                        }
                    }

                    getPaymentLog();
                } else {
                    dialog.displayCommonDialogWithHeader("Payment Failed", getAppConfigJson().getCommon_payment_failed());
                }
            } else {
//                dialog.displayCommonDialogWithHeader("Transaction Failed", getAppConfigJson().getTransaction_failed_popup());
            }
        }
    }

    private void getPaymentLog() {
        RestClient.get().getpaymentlog(getFromPrefs(SaregamaConstants.SESSION_ID), order_no, new Callback<GetpaymentlogPojo>() {
            @Override
            public void success(GetpaymentlogPojo basepojo, Response response) {

                if (basepojo != null) {
                    if (basepojo.getStatus()) {
                        String order_no = basepojo.getData().getOrder_no();
                        String coupon_id = basepojo.getData().getCoupon_id();
                        String paid_price = basepojo.getData().getPaid_price();
                        String order_price = basepojo.getData().getOrder_price();
                        String payment_gateway = basepojo.getData().getPayment_gateway();
                        String currency = basepojo.getData().getCurrency();
                        String country = basepojo.getData().getCurrency();
                        String transaction_date = basepojo.getData().getTransaction_date();

                                    /* Albums Mapping */
                        if (basepojo.getData().getAlbums() != null && basepojo.getData().getAlbums().size() > 0) {
                            for (int i = 0; i < basepojo.getData().getAlbums().size(); i++) {
                                album_mapping += "A|" + basepojo.getData().getAlbums().get(i).getAlbum_id() + ",";
                            }
                        }
                                    /* MP3 Songs Mapping */
                        if (basepojo.getData().getSong_mp3() != null && basepojo.getData().getSong_mp3().size() > 0) {
                            for (int j = 0; j < basepojo.getData().getSong_mp3().size(); j++) {
                                songmp3_mapping += "S|" + basepojo.getData().getSong_mp3().get(j).getSong_id() + "|" + basepojo.getData().getSong_mp3().get(j).getIsrc() + ",";
                            }
                        }
                                    /* HD Songs Mapping */
                        if (basepojo.getData().getSong_hd() != null && basepojo.getData().getSong_hd().size() > 0) {
                            for (int k = 0; k < basepojo.getData().getSong_hd().size(); k++) {
                                songhd_mapping += "H|" + basepojo.getData().getSong_hd().get(k).getSong_id() + "|" + basepojo.getData().getSong_hd().get(k).getIsrc() + ",";
                            }
                        }

                                    /* HD Albums Mapping */
                        if (basepojo.getData().getAlbums_hd() != null && basepojo.getData().getAlbums_hd().size() > 0) {
                            for (int i = 0; i < basepojo.getData().getAlbums_hd().size(); i++) {
                                albumhd_mapping += "AH|" + basepojo.getData().getAlbums_hd().get(i).getAlbum_id() + ",";
                            }
                        }

                        if (album_mapping.length() > 0) {
                            album_mapping = album_mapping.substring(0, album_mapping.length() - 1);
                            content_mapping += album_mapping + ";";
                        }
                        if (songmp3_mapping.length() > 0) {
                            songmp3_mapping = songmp3_mapping.substring(0, songmp3_mapping.length() - 1);
                            content_mapping += songmp3_mapping + ";";
                        }
                        if (songhd_mapping.length() > 0) {
                            songhd_mapping = songhd_mapping.substring(0, songhd_mapping.length() - 1);
                            content_mapping += songhd_mapping + ";";
                        }
                        if (albumhd_mapping.length() > 0) {
                            albumhd_mapping = albumhd_mapping.substring(0, albumhd_mapping.length() - 1);
                            content_mapping += albumhd_mapping + ";";
                        }

                        if (content_mapping.length() > 0)
                            content_mapping = content_mapping.substring(0, content_mapping.length() - 1);

                        articleParams.put("email", getFromPrefs(SaregamaConstants.EMAIL));
                        articleParams.put("order_id", order_no);
                        articleParams.put("transaction_date", transaction_date);
                        articleParams.put("order_price", order_price);
                        articleParams.put("paid_price", paid_price);
                        articleParams.put("country", country);
                        articleParams.put("coupon_code", couponCodeEdit.getText().toString());
                        articleParams.put("payment_gateway", payment_gateway);
                        articleParams.put("content_mapping", content_mapping);

                        FlurryAgent.logEvent("transaction_details", articleParams);

                        Map<String, Object> eventValue = new HashMap<String, Object>();
                        eventValue.put("email", getFromPrefs(SaregamaConstants.EMAIL));
                        eventValue.put("order_id", order_no);
                        eventValue.put("transaction_date", transaction_date);
                        eventValue.put("order_price", order_price);
                        eventValue.put("paid_price", paid_price);
                        eventValue.put("country", country);
                        eventValue.put("coupon_code", couponCodeEdit.getText().toString());
                        eventValue.put("payment_gateway", payment_gateway);
                        eventValue.put("content_mapping", content_mapping);
                        AppsFlyerLib.getInstance().trackEvent(ctx, "transaction_details", eventValue);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

        Intent intent = new Intent(ctx, DownloadManagerActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getFromPrefs(SaregamaConstants.COUPON_CODE) != null && getFromPrefs(SaregamaConstants.COUPON_CODE).length() > 0) {
            couponCodeEdit.setText("" + getFromPrefs(SaregamaConstants.COUPON_CODE));
            applyCoupon(getFromPrefs(SaregamaConstants.COUPON_CODE));
            couponCodeEdit.setEnabled(false);
            error_message.setVisibility(View.GONE);
        }
    }

    private void getStateData() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getState(getFromPrefs(SaregamaConstants.SESSION_ID), new Callback<GetSatePojo>() {
                @Override
                public void success(GetSatePojo basePojo, Response response) {

                    if (basePojo != null) {
                        getStatePojo = basePojo;
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            if (basePojo.getData().getState() != null && !basePojo.getData().getState().isEmpty()) {
                                residentLocationText.setText(getResources().getString(R.string.location_str) + " " + basePojo.getData().getState());
                            } else {
                                residentLocationText.setText(getResources().getString(R.string.location_str) + " " + residance);
                            }

                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                            residentLocationText.setText(getResources().getString(R.string.location_str) + " " + residance);
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                        residentLocationText.setText(getResources().getString(R.string.location_str) + " " + residance);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                    residentLocationText.setText(getResources().getString(R.string.location_str) + " " + residance);

                }
            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            residentLocationText.setText(getResources().getString(R.string.location_str) + " " + residance);
        }
    }

    @Override
    public void onClick(View view) {
        location_dialog.dismiss();
        String locationName = (String) view.getTag(R.string.key);
        residentLocationText.setText(getResources().getString(R.string.location_str) + " " + locationName);
    }

    private void setStateData(String location_name, String order_id) {
        if (cd.isConnectingToInternet()) {

            RestClient.get().setState(getFromPrefs(SaregamaConstants.SESSION_ID), location_name, order_id, new Callback<GetSatePojo>() {
                @Override
                public void success(GetSatePojo basePojo, Response response) {

                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }
}
