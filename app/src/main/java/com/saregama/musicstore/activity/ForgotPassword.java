package com.saregama.musicstore.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.ForgotPasswordPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ForgotPassword extends BaseActivity {

    private ConnectionDetector cd;
    private ForgotPassword ctx = this;
    private SaregamaDialogs dialog;
    private String email;
    private String email_str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        final EditText emailEdit = (EditText) findViewById(R.id.forgotpassword_email);
        final TextView sendPassword = (TextView) findViewById(R.id.forgotPassword_send);
        ImageView forgot_back = (ImageView) findViewById(R.id.forgot_back);
        TextView forgot_password_text1 = (TextView) findViewById(R.id.forgot_password_text1);
        TextView forgot_password_text2 = (TextView) findViewById(R.id.forgot_password_text2);

        forgot_password_text1.setText(getAppConfigJson().getForgotpassword1());
        forgot_password_text2.setText(getAppConfigJson().getForgotpassword2());

        forgot_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ForgotPassword.this, PasswordLoginActivity.class);
                intent.putExtra("email", email);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            }
        });

        dialog = new SaregamaDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        SaregamaConstants.ACTIVITIES.add(ctx);
        if (getIntent().getStringExtra("email") != null) {
            email = getIntent().getStringExtra("email");
            emailEdit.setText(email);
        }

        sendPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 email_str = emailEdit.getText().toString();
                if (email_str.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.FORGOT_PASSWORD, getAppConfigJson().getForgotpassword1());
                } else if (!isValidEmail(email_str)) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.FORGOT_PASSWORD, getAppConfigJson().getValid_email());
                } else {
                    doForgotPassword(email_str);
                }
            }
        });
    }

    private void doForgotPassword(final String email) {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = SaregamaDialogs.showLoading(ForgotPassword.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().postForgotPassword(getFromPrefs(SaregamaConstants.IMEI), email, "verifyemail", new Callback<ForgotPasswordPojo>() {
                @Override
                public void success(ForgotPasswordPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
//                            dialog.displayCommonDialogWithHeader("PASSWORD SENT", "Temporary password has been sent on mail successfully.");
                            checkCommonURL(response.getUrl());
                            if (basePojo.getData().getMsg() != null && !basePojo.getData().getMsg().isEmpty())
                                emailDialog(basePojo.getData().getMsg());
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.FORGOT_PASSWORD, basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.FORGOT_PASSWORD, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
        }
    }


    public boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ForgotPassword.this, PasswordLoginActivity.class);
        intent.putExtra("email", email);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    /*private void emailDialog(String msg) {
        Button OkButtonEmail;
        final Dialog dialogEmail = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialogEmail.setContentView(R.layout.custom_dialog);
            if (!dialogEmail.isShowing()) {
                TextView msg_textView = (TextView) dialogEmail.findViewById(R.id.text_exit);
                TextView dialog_header = (TextView) dialogEmail.findViewById(R.id.dialog_header);
                dialog_header.setVisibility(View.VISIBLE);
                msg_textView.setText(msg);
                dialog_header.setText("Email Sent");
                OkButtonEmail = (Button) dialogEmail.findViewById(R.id.btn_yes_exit);
                OkButtonEmail.setText("OK");
                Button ResendButtonEmail = (Button) dialogEmail.findViewById(R.id.btn_no_exit);
                ResendButtonEmail.setText("Resend");
                ImageView dialog_header_cross = (ImageView) dialogEmail.findViewById(R.id.dialog_header_cross);
                dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogEmail.dismiss();
                    }
                });

                OkButtonEmail.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialogEmail.dismiss();
                    }
                });

                ResendButtonEmail.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialogEmail.dismiss();
                        doForgotPassword(email_str);
                    }
                });

                dialogEmail.show();
            }

    }*/

    private void emailDialog(String msg) {
        Button OkButtonEmail;
        final Dialog dialogEmail = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialogEmail.setContentView(R.layout.email_dialog_small);
            if (!dialogEmail.isShowing()) {
                TextView msg_textView = (TextView) dialogEmail.findViewById(R.id.text_exit);
                TextView dialog_header = (TextView) dialogEmail.findViewById(R.id.dialog_header);
                dialog_header.setVisibility(View.VISIBLE);
                msg_textView.setText(msg);
                dialog_header.setText("Email Sent");
                OkButtonEmail = (Button) dialogEmail.findViewById(R.id.btn_yes_exit);
                OkButtonEmail.setText("OK");
                TextView text_resend = (TextView) dialogEmail.findViewById(R.id.text_resend);
                SpannableString ss = new SpannableString(getResources().getString(R.string.forgor_password_resend_text));
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        dialogEmail.dismiss();
                        doForgotPassword(email_str);
                    }
                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                ss.setSpan(clickableSpan, 44, text_resend.getText().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                text_resend.setText(ss);
                text_resend.setMovementMethod(LinkMovementMethod.getInstance());
                text_resend.setHighlightColor(Color.parseColor(ctx.getResources().getString(R.string.highlightedText)));

                ImageView dialog_header_cross = (ImageView) dialogEmail.findViewById(R.id.dialog_header_cross);
                dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogEmail.dismiss();
                    }
                });

                OkButtonEmail.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialogEmail.dismiss();
                    }
                });

                dialogEmail.show();
            }

    }
}