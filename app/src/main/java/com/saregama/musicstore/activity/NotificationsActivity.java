package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.NotificationAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.NotificationDataPojo;
import com.saregama.musicstore.pojo.NotificationPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NotificationsActivity extends BaseActivity implements View.OnClickListener {

    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private LinearLayout no_notification_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        setDrawerAndToolbar("Notifications");

        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        notification_layout.setVisibility(View.GONE);
        dialog = new SaregamaDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());

        SaregamaConstants.ACTIVITIES.add(ctx);
        mRecyclerView = (RecyclerView) findViewById(R.id.notification_recycler);
        no_notification_layout = (LinearLayout) findViewById(R.id.no_notification_layout);
        mRecyclerView.setHasFixedSize(true);
        getNotifications();
    }


    @Override
    public void onClick(View view) {

        if (view != null) {
            NotificationDataPojo data = (NotificationDataPojo) view.getTag(R.string.data);
            if (data != null) {
                navigateToPush(data.getUrl(), data.getNotification_key(), data.getContent_type(), data.getContent_id());
            }
        }
    }

    private void getNotifications() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);


            RestClient.get().getNotificationList(getFromPrefs(SaregamaConstants.SESSION_ID), getFromPrefs(SaregamaConstants.IMEI), 1, new Callback<NotificationPojo>() {
                @Override
                public void success(NotificationPojo basePojo, Response response) {

                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            mRecyclerView.setLayoutManager(mLayoutManager);
                            mAdapter = new NotificationAdapter(ctx, basePojo.getData());
                            mRecyclerView.setAdapter(mAdapter);
                            saveIntIntoPrefs(SaregamaConstants.NOTIFICATION_SIZE, 0);
                            if (basePojo.getData().size() == 0)
                                no_notification_layout.setVisibility(View.VISIBLE);
                            else
                                no_notification_layout.setVisibility(View.GONE);
                        } else {
                            if (basePojo.getData().size() == 0)
                                no_notification_layout.setVisibility(View.VISIBLE);
                            else
                                no_notification_layout.setVisibility(View.GONE);
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
        }
    }

    public void navigateToPush(String url, String notification_key, String content_type, String content_id) {
        if (notification_key.equalsIgnoreCase("home_mp3")) {
            Intent intent = new Intent(ctx, HomeActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", "home_mp3");
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("more_offer")) {
            Intent intent = new Intent(ctx, AvailOffersActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("album_details")) {
            removeActivity(getResources().getString(R.string.package_name) + ".Mp3HindiAlbumDetail");
            Intent intent = new Intent(ctx, Mp3HindiAlbumDetail.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("c_type", Integer.valueOf(content_type));
            intent.putExtra("album_id", content_id);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("artist_details")) {
            Intent intent = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("content_type", content_type);
            intent.putExtra("from", "");
            intent.putExtra("content_id", content_id);
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("hindi_films_mp3")) {
            Intent intent = new Intent(ctx, CategoryListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("geetmala_all_mp3") || notification_key.equalsIgnoreCase("geetmala_1950") || notification_key.equalsIgnoreCase("geetmala_1960")
                || notification_key.equalsIgnoreCase("geetmala_1970") || notification_key.equalsIgnoreCase("geetmala_1980") || notification_key.equalsIgnoreCase("geetmala_1990")
                || notification_key.equalsIgnoreCase("geetmala_2000")) {
            Intent intent = new Intent(ctx, Mp3HindiLandingActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("regional_home_mp3")) {
            Intent intent = new Intent(ctx, LanguageListActivity.class);
            intent.putExtra("notification_url", url);
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("regional_telugu_mp3") || notification_key.equalsIgnoreCase("regional_marathi_mp3")
                || notification_key.equalsIgnoreCase("regional_kannada_mp3") || notification_key.equalsIgnoreCase("regional_punjabi_mp3")) {
            Intent intent = new Intent(ctx, Mp3RegionalActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("regional_other_mp3")) {
            Intent intent = new Intent(ctx, Regional_Others.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("classical_home_mp3")) {
            Intent intent = new Intent(ctx, ClassicalListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("hindustani_mp3")
                || notification_key.equalsIgnoreCase("carnatic_mp3")
                || notification_key.equalsIgnoreCase("hinduatani_artist") || notification_key.equalsIgnoreCase("carnatic_artist")) {
            Intent intent = new Intent(ctx, ClassicalHindustaniActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("fusion_mp3")
                || notification_key.equalsIgnoreCase("fusion_artist")) {
            Intent intent = new Intent(ctx, ClassicalFusionListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("devotional_home_mp3")) {
            Intent intent = new Intent(ctx, Mp3Devotional.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("rabindra_sangeet_mp3")
                || notification_key.equalsIgnoreCase("nazrul_geet_mp3")) {
            Intent intent = new Intent(ctx, RabindraSangeetActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("ghazals_sufi_mp3")) {
            Intent intent = new Intent(ctx, GajalSufiActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "mp3");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("other_mp3")) {
            Intent intent = new Intent(ctx, OtherLandingListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("remix_mp3")) {
            Intent intent = new Intent(ctx, RemixSongsActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("cart")) {
            Intent intent = new Intent(ctx, CartActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("offer_popup_mp3")) {
            Intent intent = new Intent(ctx, HomeActivity.class);
            intent.putExtra("content_id", content_id);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("download")) {
            if (getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
                Intent intent = new Intent(ctx, DownloadManagerActivity.class);
                intent.putExtra("notification_url", url);
                intent.putExtra("from", "notification");
                startActivity(intent);
            } else {
                Intent intent = new Intent(ctx, LoginActivity.class);
                intent.putExtra("from", "notification");
                intent.putExtra("notification_key", notification_key);
                startActivity(intent);
            }
        } else if (notification_key.equalsIgnoreCase("devotional_artist")) {
            Intent intent = new Intent(ctx, Mp3Devotional.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("devotional_home_mp3")) {
            Intent intent = new Intent(ctx, Mp3Devotional.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("ghazals_sufi_artist")) {
            Intent intent = new Intent(ctx, GajalSufiActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("rabindra_sangeet_artist") || notification_key.equalsIgnoreCase("nazrul_geet_artist")) {
            Intent intent = new Intent(ctx, RabindraSangeetActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("album_regional_telugu") || notification_key.equalsIgnoreCase("album_regional_marathi")
                || notification_key.equalsIgnoreCase("album_regional_kannada") || notification_key.equalsIgnoreCase("album_regional_punjabi")) {
            Intent intent = new Intent(ctx, BuyAnyAlbumListing.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("ghazals_sufi")) {
            Intent intent = new Intent(ctx, GajalSufiActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("notification_key", notification_key);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("album_home_mp3")) {
            Intent intent = new Intent(ctx, CategoryListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("hindi_films")) {
            Intent intent = new Intent(ctx, BuyAnyAlbumHindiLanding.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        } else if (notification_key.equalsIgnoreCase("album_regional_films")) {
            Intent intent = new Intent(ctx, LanguageListActivity.class);
            intent.putExtra("notification_url", url);
            intent.putExtra("from", "notification");
            startActivity(intent);
        }
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        overridePendingTransition(R.anim.flip_in, R.anim.flip_out);
//    }
}
