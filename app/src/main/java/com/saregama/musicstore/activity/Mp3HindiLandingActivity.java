package com.saregama.musicstore.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.Mp3HindiPagerAdapter;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class Mp3HindiLandingActivity extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {

    private Mp3HindiPagerAdapter mAdapter;
    private ViewPager viewPager;

    private LinearLayout song_layout, movies_layout, artist_layout, geetmala_layout,
            song_selector, movies_selector, artist_selector, geetmala_selector;
    private TextView song_text, movies_text, artist_text, geetmala_text;

    private Activity ctx = this;

    public ArrayList<MP3HindiSongListPojo> arrdata;
    public ArrayList<MP3HindiSongListPojo> arrdata_mp3songs;
    public RecyclerView.Adapter mp3SongAdapter;

    private CircularSeekBar seekbar;
    private ImageView previous_view;
    private ImageView imageView;
    private MP3HindiSongListPojo data_playing;
    private String notification_key;
    private int fragment_pos = 0;
    private int c_type;
//    private EditText search;
//    private TextView no_result_found;
//    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp3_hindi_landing);

        c_type = getIntent().getIntExtra("c_type", 1);
//        String coming_from = getIntent().getStringExtra("coming_from");

//        if (coming_from != null && coming_from.equals("search")) {
//            LinearLayout page_data = (LinearLayout) findViewById(R.id.page_data);
//            page_data.setVisibility(View.GONE);
//
//            String fragment_pos = getIntent().getStringExtra("fragment_pos");
//
//            if (fragment_pos.equalsIgnoreCase("songs")) {
//                setDrawerAndToolbar("Songs");
//                Mp3HindiSongFragment fragment = new Mp3HindiSongFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "Mp3HindiSongFragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("album")) {
//                setDrawerAndToolbar("Albums");
//                Mp3HindiAlbumFragment fragment = new Mp3HindiAlbumFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "Mp3HindiAlbumFragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("artiste")) {
//                setDrawerAndToolbar("Artistes");
//                Mp3HindiArtistFragment fragment = new Mp3HindiArtistFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "Mp3HindiArtistFragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("geetmala")) {
//                setDrawerAndToolbar("Geetmala");
//                Mp3HindiGeetmalaFragment fragment = new Mp3HindiGeetmalaFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "Mp3HindiGeetmalaFragment").commit();
//            }
//        } else {
        setDrawerAndToolbarWithDropDown("Hindi Films");

            SaregamaConstants.ACTIVITIES.add(ctx);

            song_layout = (LinearLayout) findViewById(R.id.song_layout);
            movies_layout = (LinearLayout) findViewById(R.id.movies_layout);
            artist_layout = (LinearLayout) findViewById(R.id.artist_layout);
            geetmala_layout = (LinearLayout) findViewById(R.id.geetmala_layout);

            song_selector = (LinearLayout) findViewById(R.id.song_selector);
            movies_selector = (LinearLayout) findViewById(R.id.movies_selector);
            artist_selector = (LinearLayout) findViewById(R.id.artist_selector);
            geetmala_selector = (LinearLayout) findViewById(R.id.geetmala_selector);

//            search = (EditText) findViewById(R.id.search_song);
//            no_result_found = (TextView) findViewById(R.id.no_result_found);
//            softKeyboardDoneClickListener(search);
//            search.setHint("Search Songs, Albums, Artistes, Geetmala");
//
//            search.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(ctx, HindiFilmsSearchActivity.class);
//                    intent.putExtra("search_from", "hindi");
//                    intent.putExtra("c_type", c_type);
//                    startActivity(intent);
//                }
//            });

            song_text = (TextView) findViewById(R.id.song_text);
            movies_text = (TextView) findViewById(R.id.movies_text);
            artist_text = (TextView) findViewById(R.id.artist_text);
            geetmala_text = (TextView) findViewById(R.id.geetmala_text);

            song_text.setText(getResources().getString(R.string.songs));
            movies_text.setText(getResources().getString(R.string.album));
            artist_text.setText(getResources().getString(R.string.artist));
            geetmala_text.setText(getResources().getString(R.string.geetmala));

            mAdapter = new Mp3HindiPagerAdapter(getSupportFragmentManager());
            viewPager = (ViewPager) findViewById(R.id.mp3_landing_pager);
            viewPager.setAdapter(mAdapter);
//            viewPager.setPageTransformer(true, new DepthPageTransformer());

            viewPager.setOffscreenPageLimit(3);

            song_layout.setOnClickListener(this);
            movies_layout.setOnClickListener(this);
            artist_layout.setOnClickListener(this);
            geetmala_layout.setOnClickListener(this);

            if (getIntent().getStringExtra("notification_key") != null) {
                notification_key = getIntent().getStringExtra("notification_key");
                if (notification_key.equals("geetmala_all_mp3") || notification_key.equals("geetmala_1950") || notification_key.equals("geetmala_1960") ||
                        notification_key.equals("geetmala_1970") || notification_key.equals("geetmala_1980") || notification_key.equals("geetmala_1990") ||
                        notification_key.equals("geetmala_2000")) {
                    viewPager.setCurrentItem(3);
                    setStyle(3);
                }
            }

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    setStyle(position);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            });
//        }

        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.song_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.movies_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            case R.id.artist_layout:
                if (viewPager.getCurrentItem() != 2) {
                    viewPager.setCurrentItem(2);
                    setStyle(2);
                }
                break;

            case R.id.geetmala_layout:
                if (viewPager.getCurrentItem() != 3) {
                    viewPager.setCurrentItem(3);
                    setStyle(3);
                }
                break;

            case R.id.albumLL:
                hideSoftKeyboard();
                Intent intent = new Intent(ctx, Mp3HindiAlbumDetail.class);
                MP3HindiAlbumListPojo data = (MP3HindiAlbumListPojo) v.getTag(R.string.data);
                String from = (String) v.getTag(R.string.from);
                intent.putExtra("album_id", data.getAlbum_id());
                intent.putExtra("c_type", c_type);
                intent.putExtra("from", from);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
                CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);
                QuickAction quickAction = setupQuickAction(data_addtocart.getSong_id(), c_type);
                quickAction.show(addToCart);
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            default:
                break;
        }
    }

    private void setStyle(int position) {
        fragment_pos = position;
        if (position == 1) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            song_selector.setVisibility(View.GONE);
            movies_selector.setVisibility(View.VISIBLE);
            artist_selector.setVisibility(View.GONE);
            geetmala_selector.setVisibility(View.GONE);
        } else if (position == 0) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            song_selector.setVisibility(View.VISIBLE);
            movies_selector.setVisibility(View.GONE);
            artist_selector.setVisibility(View.GONE);
            geetmala_selector.setVisibility(View.GONE);
        } else if (position == 2) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            song_selector.setVisibility(View.GONE);
            movies_selector.setVisibility(View.GONE);
            artist_selector.setVisibility(View.VISIBLE);
            geetmala_selector.setVisibility(View.GONE);
        } else if (position == 3) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));

            song_selector.setVisibility(View.GONE);
            movies_selector.setVisibility(View.GONE);
            artist_selector.setVisibility(View.GONE);
            geetmala_selector.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void notifyAdapter() {
        if (mp3SongAdapter != null)
            mp3SongAdapter.notifyDataSetChanged();
    }

    private void playMP3Song(ImageView imageView, View v) {
        hideSoftKeyboard();
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        if (data_playing.is_playing()) {
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            for (int i = 0; i < arrdata_mp3songs.size(); i++)
                arrdata_mp3songs.get(i).setIs_playing(false);
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            data_playing.setIs_playing(true);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        seekbar.setVisibility(View.VISIBLE);
        previous_view = imageView;
        playPauseButtonClickListener(c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }

    /*public void showListBubble(View v) {
       *//* if (fragment_pos == 0) {
            Mp3HindiSongFragment song_fragment = (Mp3HindiSongFragment) mAdapter.getFragment(fragment_pos);
            if (song_fragment != null)
                song_fragment.showListBubble(v);
        } else if (fragment_pos == 1) {
            Mp3HindiAlbumFragment album_fragment = (Mp3HindiAlbumFragment) mAdapter.getFragment(fragment_pos);
            if (album_fragment != null)
                album_fragment.showListBubble(v);
        }*//*
    }*/
}