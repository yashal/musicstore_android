package com.saregama.musicstore.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.ClassicalHindustaniPagerAdapter;
import com.saregama.musicstore.pojo.ClassHindArtistListPojo;
import com.saregama.musicstore.pojo.ClassHindInstrumentListPojo;
import com.saregama.musicstore.util.SaregamaConstants;

/**
 * Created by navneet on 14/3/2016.
 */
public class ClassicalHindustaniActivity extends BaseActivity implements View.OnClickListener {

    private ClassicalHindustaniPagerAdapter mAdapter;
    private Activity ctx = this;
    private ViewPager viewPager;
    private LinearLayout artist_layout, instruments_layout, raagas_layout, artist_selector, instruments_selector, raagas_selector;
    private TextView artist_text, instruments_text, raagas_text;
    private int fragment_pos = 0;
    private int c_type;
//    private int d_type;
//    private EditText search;

//    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp3_regional);

        SaregamaConstants.ACTIVITIES.add(ctx);
        c_type = getIntent().getIntExtra("c_type", 1);

//        String coming_from = getIntent().getStringExtra("coming_from");

//        d_type = getIntent().getIntExtra("d_type", 19);

//        if (coming_from != null && coming_from.equals("search")) {
//            LinearLayout page_data = (LinearLayout) findViewById(R.id.page_data);
//            page_data.setVisibility(View.GONE);
//
//            String fragment_pos = getIntent().getStringExtra("fragment_pos");
//
//            if (fragment_pos.equalsIgnoreCase("artiste")) {
//                setDrawerAndToolbar("Artistes");
//                HindustaniArtistFragment fragment = new HindustaniArtistFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "HindustaniArtistFragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("instruments")) {
//                setDrawerAndToolbar("Instruments");
//                HindustaniInstrumentsFragment fragment = new HindustaniInstrumentsFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "HindustaniInstrumentsFragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("raagas")) {
//                setDrawerAndToolbar("Raagas");
//                HindustaniRaagasFragment fragment = new HindustaniRaagasFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "HindustaniRaagasFragment").commit();
//            }
//        } else {
        setDrawerAndToolbar(getIntent().getStringExtra("from"));
        artist_layout = (LinearLayout) findViewById(R.id.regional_song_layout);
        instruments_layout = (LinearLayout) findViewById(R.id.regional_movies_layout);
        raagas_layout = (LinearLayout) findViewById(R.id.regional_artist_layout);

        artist_selector = (LinearLayout) findViewById(R.id.regional_song_selector);
        instruments_selector = (LinearLayout) findViewById(R.id.regional_movies_selector);
        raagas_selector = (LinearLayout) findViewById(R.id.regional_artist_selector);

        artist_text = (TextView) findViewById(R.id.regional_song_text);
        instruments_text = (TextView) findViewById(R.id.regional_movies_text);
        raagas_text = (TextView) findViewById(R.id.regional_artist_text);

        artist_text.setText(getResources().getString(R.string.artist));
        instruments_text.setText(getResources().getString(R.string.instruments));
        raagas_text.setText(getResources().getString(R.string.raagas));

        artist_layout.setOnClickListener(this);
        instruments_layout.setOnClickListener(this);
        raagas_layout.setOnClickListener(this);

        mAdapter = new ClassicalHindustaniPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.regional_mp3_landing_pager);
        viewPager.setAdapter(mAdapter);
//        viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.setOffscreenPageLimit(2);

//            search = (EditText) findViewById(R.id.search_song);
//            softKeyboardDoneClickListener(search);
//            search.setHint("Search Artistes, Instruments, Raagas");
//
//            search.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent intent = new Intent(ctx, DevotionalSearchActivity.class);
//                    intent.putExtra("search_from", "classical_hindustani");
//                    intent.putExtra("from", getIntent().getStringExtra("from"));
//                    intent.putExtra("c_type", c_type);
//                    intent.putExtra("d_type", d_type);
//                    startActivity(intent);
//                }
//            });

        mAdapter = new ClassicalHindustaniPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.regional_mp3_landing_pager);
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                setStyle(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
//        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regional_song_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.regional_movies_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            case R.id.regional_artist_layout:
                if (viewPager.getCurrentItem() != 2) {
                    viewPager.setCurrentItem(2);
                    setStyle(2);
                }
                break;

            case R.id.classhindustani_artist_albumLL:
                hideSoftKeyboard();
                Intent intent = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
                ClassHindArtistListPojo data = (ClassHindArtistListPojo) v.getTag(R.string.data);
                intent.putExtra("artist_id", data.getId());
                intent.putExtra("from", "Artist");
                intent.putExtra("c_type", c_type);
                intent.putExtra("header", getIntent().getStringExtra("from"));
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.classhindustani_instrument_albumLL:
                hideSoftKeyboard();
                Intent intent_instru = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
                ClassHindInstrumentListPojo data_instru = (ClassHindInstrumentListPojo) v.getTag(R.string.data);
                intent_instru.putExtra("artist_id", data_instru.getTag_id());
                intent_instru.putExtra("from", "Instrument");
                intent_instru.putExtra("c_type", c_type);
                intent_instru.putExtra("header", getIntent().getStringExtra("from"));
                startActivity(intent_instru);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            default:
                break;
        }
    }

    private void setStyle(int position) {
        fragment_pos = position;
        if (position == 1) {
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            instruments_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            raagas_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            artist_selector.setVisibility(View.GONE);
            instruments_selector.setVisibility(View.VISIBLE);
            raagas_selector.setVisibility(View.GONE);
        } else if (position == 0) {
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            instruments_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            raagas_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            artist_selector.setVisibility(View.VISIBLE);
            instruments_selector.setVisibility(View.GONE);
            raagas_selector.setVisibility(View.GONE);
        } else if (position == 2) {
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            instruments_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            raagas_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));

            artist_selector.setVisibility(View.GONE);
            instruments_selector.setVisibility(View.GONE);
            raagas_selector.setVisibility(View.VISIBLE);
        }
    }

    /*public void showListBubble(View v) {
        if (fragment_pos == 0) {
            HindustaniArtistFragment hind_artist_fragment = (HindustaniArtistFragment) mAdapter.getFragment(fragment_pos);
            if (hind_artist_fragment != null)
                hind_artist_fragment.showListBubble(v);
        } else if (fragment_pos == 1) {
            HindustaniInstrumentsFragment hind_instru_fragment = (HindustaniInstrumentsFragment) mAdapter.getFragment(fragment_pos);
            if (hind_instru_fragment != null)
                hind_instru_fragment.showListBubble(v);
        } else if (fragment_pos == 2) {
            HindustaniRaagasFragment hind_raagas_fragment = (HindustaniRaagasFragment) mAdapter.getFragment(fragment_pos);
            if (hind_raagas_fragment != null)
                hind_raagas_fragment.showListBubble(v);
        }
    }*/
}