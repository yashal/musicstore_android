package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.Mp3HindiAlbumDetailAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.Mp3HindiAlbumDetailPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.QuickActionAlbumTop;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegionalMp3AlbumDetail extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {
    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;
    private TextView albumName, directorName;
    private TextView add_album_to_cart;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ImageView bannerImage, albumImage;
    private Bitmap largeIcon;
    private String album_id;
    private LinearLayout add_album_layout, ll_change_album_to_song;
    private int height;
    private CircularSeekBar seekbar;
    private MP3HindiSongListPojo data_playing;
    private ImageView previous_view;
    private ImageView imageView;
    private int c_type;
    private TextView change_album_song_text, to_buy_text, album_price;
    private ArrayList<MP3HindiSongListPojo> arrdata;
    private String album_image;
    private String currency, price, price_hd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mp3hindi_album_detail);

        DisplayMetrics metrics = new DisplayMetrics();
        ctx.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        height = metrics.heightPixels / 3;
        c_type = getIntent().getIntExtra("c_type", 1);

        if (getIntent().getStringExtra("from").equals("regional")) {
            setDrawerAndToolbar(getIntent().getStringExtra("languagename"));
        } else if (getIntent().getStringExtra("from").equals("gajal_sufi")) {
            setDrawerAndToolbar("Ghazals & Sufi");
        } else if (getIntent().getStringExtra("from").equals("buy_album")) {
            if (getIntent().getStringExtra("languagename") != null)
                setDrawerAndToolbar(getIntent().getStringExtra("languagename"));
            else
                setDrawerAndToolbar("Ghazals & Sufi");
        } else
            setDrawerAndToolbar("");
        dialog = new SaregamaDialogs(ctx);

        album_id = getIntent().getStringExtra("album_id");

        mRecyclerView = (RecyclerView) findViewById(R.id.mp3hindi_albumdetails_recycler);
        album_price = (TextView) findViewById(R.id.album_price);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(getApplicationContext());

        SaregamaConstants.ACTIVITIES.add(ctx);

        RelativeLayout header_layout_albumdetail = (RelativeLayout) findViewById(R.id.header_layout_albumdetail);
        header_layout_albumdetail.requestLayout();
        header_layout_albumdetail.getLayoutParams().height = height;

        LinearLayout opacityFilter = (LinearLayout) findViewById(R.id.opacityFilter);
        opacityFilter.requestLayout();
        opacityFilter.getLayoutParams().height = height - 80;

        albumName = (TextView) findViewById(R.id.mp3hindi_albumdetails_albumName);
        directorName = (TextView) findViewById(R.id.mp3hindi_albumdetails_dirName);
        add_album_to_cart = (TextView) findViewById(R.id.add_album_to_cart);

        ll_change_album_to_song = (LinearLayout) findViewById(R.id.ll_change_album_to_song);

        add_album_layout = (LinearLayout) findViewById(R.id.add_album_layout);
        albumImage = (ImageView) findViewById(R.id.mp3hindi_albumdetails_albumImage);
        bannerImage = (ImageView) findViewById(R.id.mp3hindi_albumdetails_banner);
        bannerImage.requestLayout();
        bannerImage.getLayoutParams().height = height - 80;

        LinearLayout mp3hindi_albumdetails_albumImageLL = (LinearLayout) findViewById(R.id.mp3hindi_albumdetails_albumImageLL);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins((int) getResources().getDimension(R.dimen.album_image_margin_left), height - getResources().getInteger(R.integer.album_detail), 0, 0);
        mp3hindi_albumdetails_albumImageLL.setLayoutParams(layoutParams);

        arrdata = new ArrayList<>();
        getAlbumSongs();

        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);

        change_album_song_text = (TextView) findViewById(R.id.change_album_song_text);
        to_buy_text = (TextView) findViewById(R.id.to_buy_text);
        change_album_song_text.setPaintFlags(change_album_song_text.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if(c_type == getAppConfigJson().getC_type().getALBUM())
            to_buy_text.setText(getResources().getString(R.string.buy_songs));
        else
            to_buy_text.setText(getResources().getString(R.string.buy_album));

        change_album_song_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* if(c_type == getAppConfigJson().getC_type().getALBUM()) {
                    to_buy_text.setText(getResources().getString(R.string.buy_album));
                    c_type = getAppConfigJson().getC_type().getSONG();
                    add_album_layout.setVisibility(View.GONE);
                }
                else {
                    to_buy_text.setText(getResources().getString(R.string.buy_songs));
                    c_type = getAppConfigJson().getC_type().getALBUM();
                    add_album_layout.setVisibility(View.VISIBLE);
                }
                mAdapter = new Mp3HindiAlbumDetailAdapter(ctx, arrdata, "mp3_regional", c_type, album_image);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();*/

                if (c_type == getAppConfigJson().getC_type().getALBUM()) {
                    to_buy_text.setText(getResources().getString(R.string.buy_album));
                    c_type = getAppConfigJson().getC_type().getSONG();
                    add_album_layout.setVisibility(View.INVISIBLE);
                } else {
                    to_buy_text.setText(getResources().getString(R.string.buy_songs));
                    c_type = getAppConfigJson().getC_type().getALBUM();
                    add_album_layout.setVisibility(View.VISIBLE);
                }
                mAdapter = new Mp3HindiAlbumDetailAdapter(ctx, arrdata, "mp3_regional", c_type, album_image);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    private void getAlbumSongs() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(RegionalMp3AlbumDetail.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getMp3HindiAlbumDetails(album_id, new Callback<Mp3HindiAlbumDetailPojo>() {
                @Override
                public void success(final Mp3HindiAlbumDetailPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            if (basePojo.getData().getCurrency()!=null)
                            {
                                currency = basePojo.getData().getCurrency();
                            }
                            if (basePojo.getData().getPrice()!=null)
                            {
                                price = basePojo.getData().getPrice();
                            }
                            if (basePojo.getData().getPrice_hd()!=null)
                            {
                                price_hd = basePojo.getData().getPrice_hd();
                            }

                            if (basePojo.getData().getCurrency().equals("Rs.")) {
                                album_price.setText("MP3 " + getResources().getString(R.string.Rs) + " " + price + "  |  HD " + getResources().getString(R.string.Rs) + " " + price_hd);
                            } else {
                                album_price.setText("MP3 " + basePojo.getData().getCurrency() + " " + price + "  |  HD " + basePojo.getData().getCurrency() + " " + price_hd);
                            }
                            setImageInLayout(ctx, (int) getResources().getDimension(R.dimen.album_detail_image), (int) getResources().getDimension(R.dimen.album_detail_image), basePojo.getData().getAlbum_img(), albumImage);
                            albumImage.setScaleType(ImageView.ScaleType.FIT_XY);

                            albumImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    setAlbumImageClick(basePojo.getData().getAlbum_img());
                                }
                            });
                            try {
                                largeIcon = new GetBitmapTask().execute(basePojo.getData().getAlbum_img()).get();
                                int width = largeIcon.getWidth();
                                int height = largeIcon.getHeight();
                                int newWidth = largeIcon.getWidth();
                                int newHeight = 80;

                                float scaleWidth = ((float) newWidth) / width;
                                float scaleHeight = ((float) newHeight) / height;

                                Matrix matrix = new Matrix();
                                matrix.postScale(scaleWidth, scaleHeight);

                                Bitmap resizedBitmap = Bitmap.createBitmap(largeIcon, 0, 0,
                                        width, height, matrix, true);

                                bannerImage.setImageBitmap(createBitmap_ScriptIntrinsicBlur(resizedBitmap, 100.0f));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            albumName.setText(basePojo.getData().getAlbum_name());
                            directorName.setText("Album by " + basePojo.getData().getMusic_director());

                            if (c_type == getAppConfigJson().getC_type().getALBUM()) {
                                add_album_layout.setVisibility(View.VISIBLE);
                            }
                            add_album_to_cart.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
//                                    AddToCartWithoutLogin(c_type+"", basePojo.getData().getAlbum_id());
                                    QuickActionAlbumTop quickAction = setupQuickActionAlbum(basePojo.getData().getAlbum_id(), c_type,  basePojo.getData().getCurrency(), basePojo.getData().getPrice(), basePojo.getData().getPrice_hd());
                                    quickAction.show(add_album_to_cart);
                                }
                            });

                            add_album_to_cart.setText(getResources().getString(R.string.add_album));

//                            if (basePojo.getData().getCurrency().equals("Rs."))
//                                add_album_to_cart.setText(getResources().getString(R.string.add_album) + " " + getResources().getString(R.string.Rs) + " " + basePojo.getData().getPrice());
//                            else
//                                add_album_to_cart.setText(getResources().getString(R.string.add_album) + " " + basePojo.getData().getCurrency() + " " + basePojo.getData().getPrice());

                            if (basePojo.getData().getList().size() <= SaregamaConstants.SONG_LIMIT) {
                                c_type = getAppConfigJson().getC_type().getSONG();
                                add_album_layout.setVisibility(View.GONE);
                                ll_change_album_to_song.setVisibility(View.GONE);
                            } else {
                                ll_change_album_to_song.setVisibility(View.VISIBLE);
                            }
                            mAdapter = new Mp3HindiAlbumDetailAdapter(ctx, basePojo.getData().getList(), "mp3_regional", c_type, basePojo.getData().getAlbum_img());
                            mRecyclerView.setAdapter(mAdapter);
                            arrdata = basePojo.getData().getList();
                            album_image = basePojo.getData().getAlbum_img();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });

        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
//                AddToCartWithoutLogin(c_type+"", data2.getSong_id());
                CheckBox addToCart = (CheckBox)v.getTag(R.string.addtocart_id);

                QuickAction quickAction =   setupQuickAction(data_addtocart.getSong_id(), c_type);
                quickAction.show(addToCart);
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;
        }
    }

    @Override
    public void notifyAdapter() {
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    private void playMP3Song(ImageView imageView, View v) {
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);
        SaregamaConstants.PLAYING_SONG_ALBUM_ID = album_id;
        SaregamaConstants.PLAYING_SONG_CURRENCY = currency;
        SaregamaConstants.PLAYING_SONG_PRICE = price;
        SaregamaConstants.PLAYING_SONG_PRICE_HD = price_hd;

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        if (data_playing.is_playing()) {
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            for (int i = 0; i < arrdata.size(); i++)
                arrdata.get(i).setIs_playing(false);
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            data_playing.setIs_playing(true);
            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        previous_view = imageView;
        seekbar.setVisibility(View.VISIBLE);
        playPauseButtonClickListener(c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }
}



