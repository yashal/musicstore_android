package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.ClassHindArtistDetailAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.ClassHindArtistDetailsPojo;
import com.saregama.musicstore.pojo.ClassHindInstrumentDetailsPojo;
import com.saregama.musicstore.pojo.LanguageBasePojo;
import com.saregama.musicstore.pojo.LanguageListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;
import com.saregama.musicstore.views.CircularImageView;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by navneet on 8/3/2016.
 */
public class ClassicalHindustaniSOngDetail extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {

    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;
    private TextView artistName;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private CircularImageView atistImage;
    private String artist_id, from = "";

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0, start = 0;

    private ArrayList<MP3HindiSongListPojo> arrdata;

    private CircularSeekBar seekbar;
    private MP3HindiSongListPojo data_playing;
    private ImageView previous_view;
    private ImageView imageView;
    private String asyncTaskUrl;
    private int c_type;
    private int lang_id = 0;

    // set Filter by language
    private TextView filterby_language_text;
    private PopupWindow pwindo;
    private View layout;
    protected Drawable mBackground = null;
    private ListView language_filter_list;
    private Typeface face_normal, face_bold;
    private String selected_text;
    private ArrayList<LanguageListPojo> artiste_lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.classhind_song_detail);

        from = getIntent().getStringExtra("from");
        c_type = getIntent().getIntExtra("c_type", 1);

        if (from == null)
            from = "";

        setHeader();
        SaregamaConstants.ACTIVITIES.add(ctx);

        dialog = new SaregamaDialogs(ctx);

        artist_id = getIntent().getStringExtra("artist_id");
        lang_id = getIntent().getIntExtra("lang_id", 0);

        // set Filter by language
        filterby_language_text = (TextView) findViewById(R.id.filterby_language_text);
        face_normal = Typeface.createFromAsset(ctx.getAssets(), "fonts/SourceSansPro_Regular.otf");
        face_bold = Typeface.createFromAsset(ctx.getAssets(), "fonts/SourceSansPro_Semibold.otf");

        mRecyclerView = (RecyclerView) findViewById(R.id.classhind_song_recycler);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(getApplicationContext());

        DisplayMetrics metrics = new DisplayMetrics();
        ctx.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels / 4;
        LinearLayout header_background = (LinearLayout) findViewById(R.id.header_background);

        header_background.requestLayout();
        header_background.getLayoutParams().height = height;

        artistName = (TextView) findViewById(R.id.classhind_song_artistName);

        atistImage = (CircularImageView) findViewById(R.id.classhind_song_imgArtist);

        arrdata = new ArrayList<>();
        artiste_lang = new ArrayList<>();

        if (from.equals("Instrument")) {
            new GetInstrumentListList().execute();
            filterby_language_text.setVisibility(View.GONE);
        } else {
            if (getIntent().getStringExtra("notification_url") != null) {
                asyncTaskUrl = SaregamaConstants.BASE_URL + "artist?artist_id=" + getIntent().getStringExtra("content_id") + "&c_type=" + getIntent().getStringExtra("content_type") + "&lang=" + lang_id;
            } else if (getIntent().getStringExtra("home_url") != null) {
                asyncTaskUrl = getIntent().getStringExtra("home_url") + "&lang=" + lang_id;
            } else if (getIntent().getStringExtra("from") != null && getIntent().getStringExtra("from").equals("global")) {
                asyncTaskUrl = SaregamaConstants.BASE_URL + "artist?artist_id=" + artist_id + "&c_type=" + c_type + "&lang=" + lang_id;
            } else {
                asyncTaskUrl = SaregamaConstants.BASE_URL + "artist?artist_id=" + artist_id + "&c_type=" + c_type + "&d_type=" + getIntFromPrefs(SaregamaConstants.D_TYPE) + "&lang=" + lang_id;
            }
            if (from.equals("global")) {
                getArtisteLanguage();
                filterby_language_text.setVisibility(View.VISIBLE);
            }
            new GetArtistSongsList().execute();
        }

        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);

        filterby_language_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiatePopupWindow();
                filterby_language_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.drop_up, 0);
                filterby_language_text.setBackgroundResource(R.drawable.square_new_box_button);

            }
        });

    }

    private void setHeader() {
        if (from.equals("devotional")) {
            setDrawerAndToolbar("Devotional");
        } else if (from.equals("regional") || from.equals("hdRegional")) {
            setDrawerAndToolbar(getIntent().getStringExtra("languagename"));
        } else if (from.equals("RabindraSangeet_Artist")) {
            setDrawerAndToolbar("Rabindra Sangeet");
        } else if (from.equals("gajal_sufi")) {
            setDrawerAndToolbar("Ghazals & Sufi");
        } else if (from.equals("Nazrul_Geet")) {
            setDrawerAndToolbar("Nazrul Geet");
        } else {
            setDrawerAndToolbar(getIntent().getStringExtra("header"));
        }
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;

                        if (from.equals("Instrument")) {
                            new GetInstrumentListList().execute();
                        } else {
                            new GetArtistSongsList().execute();
                        }
                        loading = true;
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
                if (getIntent().getStringExtra("from") != null && !getIntent().getStringExtra("from").equals("global")) {
                    CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);
                    QuickAction quickAction = setupQuickAction(data_addtocart.getSong_id(), c_type);
                    quickAction.show(addToCart);
                } else {
                    CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);
                    QuickAction quickAction = setupQuickAction(data_addtocart.getSong_id(), getAppConfigJson().getSong_type().getSONG());
                    quickAction.show(addToCart);
                }
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;
        }
    }

    @Override
    public void notifyAdapter() {
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    private class GetArtistSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private ClassHindArtistDetailsPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(ClassicalHindustaniSOngDetail.this);
                d.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            if (getIntent().getStringExtra("from") != null && getIntent().getStringExtra("from").equals("global"))
                basePojo = gsonObj.fromJson(getAsyncTaskData(asyncTaskUrl + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), ClassHindArtistDetailsPojo.class);
            else
                basePojo = gsonObj.fromJson(getAsyncTaskData(asyncTaskUrl + "&song_type=" + getAppConfigJson().getSong_type().getSONG() + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), ClassHindArtistDetailsPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            if (cd.isConnectingToInternet()) {

                super.onPostExecute(result);
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getStatus()) {
                        setImageInLayout(ctx, (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image), basePojo.getData().getArtist_image(), atistImage);
                        atistImage.setScaleType(ImageView.ScaleType.FIT_XY);
                        artistName.setText(basePojo.getData().getArtist_name());
                    }
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (arrdata.size() > 0) {
                                arrdata.addAll(basePojo.getData().getList());
                            } else {
                                arrdata.clear();
                                arrdata = basePojo.getData().getList();
                                mAdapter = new ClassHindArtistDetailAdapter(ctx, arrdata, "regional_art");
                                mRecyclerView.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();
                                previousTotal = 0;
                            }
                        }

                        if (basePojo.getStatus()) {
                            mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), arrdata.size() - mAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            }
        }
    }

    private class GetInstrumentListList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private ClassHindInstrumentDetailsPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(ClassicalHindustaniSOngDetail.this);
                d.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(getAsyncTaskData(SaregamaConstants.BASE_URL + "song?t_id=" + artist_id + "&d_type=" + getIntFromPrefs(SaregamaConstants.D_TYPE) + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), ClassHindInstrumentDetailsPojo.class);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (d != null && d.isShowing()) {
                d.dismiss();
            }

            if (basePojo != null) {
                if (basePojo.getData().getCount() > 0) {
                    if (basePojo.getData().getList() != null) {
                        if (arrdata.size() > 0) {
                            arrdata.addAll(basePojo.getData().getList());
                        } else {
                            arrdata.clear();
                            arrdata = basePojo.getData().getList();
                            mAdapter = new ClassHindArtistDetailAdapter(ctx, arrdata, "regional_art");
                            mRecyclerView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            previousTotal = 0;
                        }
                    }

                    if (basePojo.getStatus()) {
                        setImageInLayout(ctx, (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image), basePojo.getData().getInstrument_image(), atistImage);
                        atistImage.setScaleType(ImageView.ScaleType.FIT_XY);
                        artistName.setText(basePojo.getData().getInstrument_name());
                        mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), arrdata.size() - mAdapter.getItemCount());
                        addScrollListner();
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    start = 0;
                    firstVisibleItemPosition = 0;
                    mRecyclerView.clearOnScrollListeners();
                    mRecyclerView.scrollToPosition(0);
                    Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                }
            } else {
                dialog.displayCommonDialog(getAppConfigJson().getServer_error());
            }
        }
    }

    private void playMP3Song(ImageView imageView, View v) {
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        if (data_playing.is_playing()) {
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            for (int i = 0; i < arrdata.size(); i++)
                arrdata.get(i).setIs_playing(false);
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            data_playing.setIs_playing(true);
            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        seekbar.setVisibility(View.VISIBLE);
        previous_view = imageView;
        playPauseButtonClickListener(c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }

    private void getArtisteLanguage() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getArtisteLanguage(artist_id, new Callback<LanguageBasePojo>() {
                @Override
                public void success(LanguageBasePojo basePojo, Response response) {
                    if (basePojo != null) {
//                        if (basePojo.getStatus()) {
                        checkCommonURL(response.getUrl());
                        artiste_lang = basePojo.getData().getList();
                        if (artiste_lang != null && artiste_lang.size() > 1)
                            fetchLanguageList();
                        else
                            filterby_language_text.setVisibility(View.GONE);
//                        } else {
//                            dialog.displayCommonDialog(basePojo.getError());
//                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void initiatePopupWindow() {
        try {
            pwindo.showAsDropDown(filterby_language_text, getResources().getInteger(R.integer.popup_art_window_x_offset), getResources().getInteger(R.integer.popup_art_window_y_offset));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set Filter by language
    public void fetchLanguageList() {

        LayoutInflater inflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.language_list,
                (ViewGroup) ctx.findViewById(R.id.popup_element));
        layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        pwindo = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true);

        pwindo.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                pwindo.dismiss();
                filterby_language_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.drop_down, 0);
                filterby_language_text.setBackgroundResource(R.drawable.square_box_button);
                // end may TODO anything else
            }
        });

        pwindo.setWidth(getResources().getInteger(R.integer.popup_art_window_width));
        if (mBackground == null)
            pwindo.setBackgroundDrawable(new BitmapDrawable());
        else
            pwindo.setBackgroundDrawable(mBackground);
        pwindo.setOutsideTouchable(true);
        pwindo.setFocusable(true);
        pwindo.setTouchable(true);
        language_filter_list = (ListView) layout.findViewById(R.id.language_list_filter);
        language_filter_list.setAdapter(new Filter_Artist_Language_List_Adapter(ctx, artiste_lang));
    }

    private class Filter_Artist_Language_List_Adapter extends BaseAdapter {
        ArrayList<LanguageListPojo> arr;
        Context context;
        private LayoutInflater inflater = null;

        public Filter_Artist_Language_List_Adapter(Context context, ArrayList<LanguageListPojo> arr) {
            this.context = context;
            this.arr = arr;
            inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (arr != null)
                return arr.size();
            else return 0;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class Holder {
            private TextView language_name;
            private LinearLayout layout_filter_bg;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final Filter_Artist_Language_List_Adapter.Holder holder = new Filter_Artist_Language_List_Adapter.Holder();
            View rowView;
            rowView = inflater.inflate(R.layout.filter_lang_art_list, null);
            holder.language_name = (TextView) rowView.findViewById(R.id.language_name);
            holder.layout_filter_bg = (LinearLayout) rowView.findViewById(R.id.layout_filter_bg);

            holder.layout_filter_bg.setTag(R.string.data, arr.get(position));

            if (arr.get(position).getIsCheck()) {
                holder.language_name.setTextColor(ContextCompat.getColor(context, R.color.white));
                holder.layout_filter_bg.setBackgroundColor(ContextCompat.getColor(ctx, R.color.selected_tab_color));
            } else {
                holder.layout_filter_bg.setBackgroundColor(ContextCompat.getColor(ctx, R.color.white));
                holder.language_name.setTextColor(ContextCompat.getColor(context, R.color.black));
            }

            holder.language_name.setText(arr.get(position).getName());
            holder.layout_filter_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LanguageListPojo data = (LanguageListPojo) v.getTag(R.string.data);
                    for (int i = 0; i < arr.size(); i++) {
                        if (arr.get(i).getIsCheck()) {
                            arr.get(i).setIsCheck(false);
                            holder.layout_filter_bg.setBackgroundColor(ContextCompat.getColor(ctx, R.color.white));
                            holder.language_name.setTextColor(ContextCompat.getColor(context, R.color.black));
                            break;
                        }
                    }

                    notifyDataSetChanged();

                    holder.language_name.setTextColor(ContextCompat.getColor(context, R.color.white));
                    holder.layout_filter_bg.setBackgroundColor(ContextCompat.getColor(ctx, R.color.selected_tab_color));
                    data.setIsCheck(true);

                    lang_id = Integer.parseInt(data.getId());

                    if (getIntent().getStringExtra("notification_url") != null) {
                        asyncTaskUrl = SaregamaConstants.BASE_URL + "artist?artist_id=" + getIntent().getStringExtra("content_id") + "&c_type=" + getIntent().getStringExtra("content_type") + "&lang=" + lang_id;
                    } else if (getIntent().getStringExtra("home_url") != null) {
                        asyncTaskUrl = getIntent().getStringExtra("home_url") + "&lang=" + lang_id;
                    } else if (getIntent().getStringExtra("from") != null && getIntent().getStringExtra("from").equals("global")) {
                        asyncTaskUrl = SaregamaConstants.BASE_URL + "artist?artist_id=" + artist_id + "&c_type=" + c_type + "&lang=" + lang_id;
                    } else {
                        asyncTaskUrl = SaregamaConstants.BASE_URL + "artist?artist_id=" + artist_id + "&c_type=" + c_type + "&d_type=" + getIntFromPrefs(SaregamaConstants.D_TYPE) + "&lang=" + lang_id;
                    }

                    start = 0;
                    previousTotal = 0;
                    arrdata.clear();
                    new GetArtistSongsList().execute();

                    selected_text = (data.getName());
                    filterby_language_text.setText(selected_text);
                    pwindo.dismiss();
                }
            });

            return rowView;
        }
    }

}
