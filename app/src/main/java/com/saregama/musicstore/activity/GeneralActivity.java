package com.saregama.musicstore.activity;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.saregama.musicstore.R;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;
import java.util.List;

public class GeneralActivity extends BaseActivity {

    private WebView webview;
    private GeneralActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general);

        setDrawerAndToolbar("General");

        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);
        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        cart_layout.setVisibility(View.GONE);
        global_search.setVisibility(View.GONE);
        notification_layout.setVisibility(View.GONE);

        SaregamaConstants.ACTIVITIES.add(ctx);

        final ProgressDialog progressDialog = SaregamaDialogs.showLoading(ctx);

        webview = (WebView) findViewById(R.id.webview_general);

        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 85) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }
        });

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
                    if (SaregamaConstants.ACTIVITIES.get(i) != null && SaregamaConstants.ACTIVITIES.get(i).toString().contains(getResources().getString(R.string.package_name) + ".AboutUsActivity")) {
                        SaregamaConstants.ACTIVITIES.get(i).finish();
                        SaregamaConstants.ACTIVITIES.remove(i);
                        break;
                    }
                }
                if (url.toLowerCase().startsWith("http") || url.toLowerCase().startsWith("https") || url.toLowerCase().startsWith("file")) {
                    if (url.toLowerCase().contains("share")) {
//                        onShareClick(view, getAppConfigJson().getApp_share());
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, getAppConfigJson().getApp_share());
                        startActivity(Intent.createChooser(sharingIntent, "Share using"));
                    } else {
                        Intent intent = new Intent(GeneralActivity.this, AboutUsActivity.class);
                        intent.putExtra("url", url);
                        startActivity(intent);
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                    }
                } else {
                    try {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.d("JSLogs", "Webview Error:" + e.getMessage());
                    }
                }
                return (true);
            }
        });

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        SaregamaDialogs dialog = new SaregamaDialogs(ctx);
        if (cd.isConnectingToInternet()) {
            openURL(getAppConfigJson().getGeneral());
        } else {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void openURL(String url) {
        webview.loadUrl(url);
        webview.requestFocus();
    }

    public void onShareClick(View v, String sharing_text) {
        Resources resources = getResources();

        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);
        // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
        emailIntent.putExtra(Intent.EXTRA_TEXT, sharing_text);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, sharing_text);
        emailIntent.setType("message/rfc822");

        PackageManager pm = getPackageManager();
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");


        Intent openInChooser = Intent.createChooser(emailIntent, "Choose app to share");

        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
        for (int i = 0; i < resInfo.size(); i++) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if (packageName.contains("android.email")) {
                emailIntent.setPackage(packageName);
            } else if (packageName.contains("twitter") || packageName.contains("facebook") || packageName.contains("mms") || packageName.contains("android.gm") || packageName.contains("com.whatsapp")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                if (packageName.contains("twitter")) {
                    intent.putExtra(Intent.EXTRA_TEXT, sharing_text);
                } else if (packageName.contains("facebook")) {
                    // Warning: Facebook IGNORES our text. They say "These fields are intended for users to express themselves. Pre-filling these fields erodes the authenticity of the user voice."
                    // One workaround is to use the Facebook SDK to post, but that doesn't allow the user to choose how they want to share. We can also make a custom landing page, and the link
                    // will show the <meta content ="..."> text from that page with our link in Facebook.
                    intent.putExtra(Intent.EXTRA_TEXT, sharing_text);
                } else if (packageName.contains("mms")) {
                    intent.putExtra(Intent.EXTRA_TEXT, sharing_text);
                } else if (packageName.contains("com.whatsapp")) {
                    intent.putExtra(Intent.EXTRA_TEXT, sharing_text);
                } else if (packageName.contains("android.gm")) { // If Gmail shows up twice, try removing this else-if clause and the reference to "android.gm" above
                    intent.putExtra(Intent.EXTRA_TEXT, sharing_text);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "MusicStore App URL");
                    intent.setType("message/rfc822");
                }

                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }

        // convert intentList to array
        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        startActivity(openInChooser);
    }
}