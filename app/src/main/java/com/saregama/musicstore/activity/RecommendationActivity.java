package com.saregama.musicstore.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.RecommendadedAlbumAdapter;
import com.saregama.musicstore.adapter.RecommendadedSongAdapter;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class RecommendationActivity extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {
    private RecommendationActivity ctx = this;
    public DrawerLayout mDrawerLayout;
    LinearLayout recommend_close, recommend_continueLL;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<MP3HindiSongListPojo> listRecommendationSong;
    private ArrayList<MP3HindiAlbumListPojo> listRecommendationAlbum;
    private String c_type;
    public ArrayList<MP3HindiSongListPojo> arrdata_mp3songs;
    private CircularSeekBar seekbar;
    private ImageView previous_view;
    private ImageView imageView;
    private MP3HindiSongListPojo data_playing;
    private int int_c_type;
    private RecommendadedSongAdapter recommendationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommendation);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        recommend_close = (LinearLayout) findViewById(R.id.recommend_close);
        recommend_continueLL = (LinearLayout) findViewById(R.id.recommend_continueLL);
        mRecyclerView = (RecyclerView) findViewById(R.id.recommend_recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

        arrdata_mp3songs = new ArrayList<>();

        if (getIntent().getStringExtra("type").equals("song")) {
            c_type = getIntent().getStringExtra("cType");
            int_c_type = Integer.parseInt(c_type);
            listRecommendationSong = (ArrayList<MP3HindiSongListPojo>) getIntent().getSerializableExtra("songs_list");
            recommendationAdapter = new RecommendadedSongAdapter(ctx, listRecommendationSong, c_type);
            mRecyclerView.setAdapter(recommendationAdapter);
            recommendationAdapter.notifyDataSetChanged();
        } else if (getIntent().getStringExtra("type").equals("album")) {
            c_type = getIntent().getStringExtra("cType");
            int_c_type = Integer.parseInt(c_type);
            listRecommendationAlbum = (ArrayList<MP3HindiAlbumListPojo>) getIntent().getSerializableExtra("songs_list");
            RecommendadedAlbumAdapter recommendationAdapterAlbum = new RecommendadedAlbumAdapter(ctx, listRecommendationAlbum, c_type);
            mRecyclerView.setAdapter(recommendationAdapterAlbum);
            recommendationAdapterAlbum.notifyDataSetChanged();
        }
        recommend_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            }
        });

        recommend_continueLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            }
        });
        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addtocart_album:
                MP3HindiAlbumListPojo data_addtocart = (MP3HindiAlbumListPojo) view.getTag(R.string.data);
                String str_ctype = (String) view.getTag(R.string.c_type);
                CheckBox addToCart = (CheckBox) view.getTag(R.string.addtocart_id);
                QuickAction quickAction = setupQuickActionAlbumLeftRecommendation(data_addtocart.getAlbum_id(), getAppConfigJson().getCurrency(), data_addtocart.getPrice(), data_addtocart.getPrice_hd());
                quickAction.showRecommendDialogAlbum(addToCart);
                break;

            case R.id.addtocart_song:
                MP3HindiSongListPojo song_addtocart = (MP3HindiSongListPojo) view.getTag(R.string.data);
                CheckBox addToCart_song = (CheckBox) view.getTag(R.string.addtocart_id);
                QuickAction quickAction_song = setupQuickActionRecommendation(song_addtocart.getSong_id());
                quickAction_song.showRecommendDialog(addToCart_song);
                break;

            case R.id.playIcon:
                imageView = (ImageView) view;
                playMP3Song(imageView, view);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) view.getTag(R.string.seekbar_id);
                playMP3Song(imageView, view);
                break;

            case R.id.albumLL:
                hideSoftKeyboard();
                Intent intent = new Intent(ctx, Mp3HindiAlbumDetail.class);
                MP3HindiAlbumListPojo data = (MP3HindiAlbumListPojo) view.getTag(R.string.data);
                String from = (String) view.getTag(R.string.from);
                intent.putExtra("album_id", data.getAlbum_id());
                intent.putExtra("c_type", int_c_type);
                intent.putExtra("from", from);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
        }
    }

    @Override
    public void notifyAdapter() {
        if (recommendationAdapter != null)
            recommendationAdapter.notifyDataSetChanged();
    }

    private void playMP3Song(ImageView imageView, View v) {
        hideSoftKeyboard();
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        if (data_playing.is_playing()) {
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            for (int i = 0; i < arrdata_mp3songs.size(); i++)
                arrdata_mp3songs.get(i).setIs_playing(false);
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            data_playing.setIs_playing(true);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        seekbar.setVisibility(View.VISIBLE);
        previous_view = imageView;
        playPauseButtonClickListener(int_c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }
}