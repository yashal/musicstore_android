package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.CategoryListAdapter;
import com.saregama.musicstore.pojo.AppConfigDataPojo;
import com.saregama.musicstore.pojo.HomeBannerPojo;
import com.saregama.musicstore.pojo.HomeDataPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;


public class CategoryListActivity extends BaseActivity implements View.OnClickListener {

    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    private String asyncTaskUrl;
    private AppConfigDataPojo appConfigData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        setDrawerAndToolbar("Music");
        TextView header = (TextView) findViewById(R.id.header);
        header.requestLayout();
        LinearLayout.LayoutParams rlp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        header.setLayoutParams(rlp);

        TextView header_light = (TextView) findViewById(R.id.header_light);
        header_light.setVisibility(View.VISIBLE);

        appConfigData = getAppConfigJson();

        dialog = new SaregamaDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());

        SaregamaConstants.ACTIVITIES.add(ctx);

        ImageView logo = (ImageView) findViewById(R.id.logo);
        logo.setVisibility(View.VISIBLE);

        mRecyclerView = (RecyclerView) findViewById(R.id.categorylist_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        if (getIntent().getStringExtra("notification_url") != null) {
            asyncTaskUrl = getIntent().getStringExtra("notification_url");
        } else {
            asyncTaskUrl = getIntent().getStringExtra("url");
        }
        new getCategoryListData().execute();
    }

    private class getCategoryListData extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog_p;
        HomeDataPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                dialog_p = SaregamaDialogs.showLoading(ctx);
                dialog_p.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(getAsyncTaskData(asyncTaskUrl), HomeDataPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (dialog_p != null && dialog_p.isShowing()) {
                dialog_p.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (cd.isConnectingToInternet()) {

                if (basePojo != null) {
                    if (basePojo.getStatus()) {
                        mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        mRecyclerView.setLayoutManager(mLayoutManager);
                        mAdapter = new CategoryListAdapter(ctx, basePojo.getData(), "category");
                        saveCategoryJsonObject(basePojo.getData());
                        mRecyclerView.setAdapter(mAdapter);
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
                if (dialog_p != null && dialog_p.isShowing() && !ctx.isFinishing()) {
                    dialog_p.dismiss();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {

        HomeBannerPojo data = (HomeBannerPojo) v.getTag(R.string.data);
        int dtypecount = data.getD_type();

        saveIntIntoPrefs(SaregamaConstants.D_TYPE, data.getD_type());

        if (data.getC_type() == appConfigData.getC_type().getSONG()) {
            if (dtypecount == appConfigData.getD_type().getHINDI_FILMS()) {
                Intent i = new Intent(getApplicationContext(), Mp3HindiLandingActivity.class);
                i.putExtra("from", "mp3");
                i.putExtra("c_type", data.getC_type());
                i.putExtra("link", data.getUrl());
                startActivity(i);
            } else if (dtypecount == appConfigData.getD_type().getREGIONAL_FILMS()) {
                Intent i = new Intent(getApplicationContext(), LanguageListActivity.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("d_type", data.getD_type());
                i.putExtra("c_type", data.getC_type());
                startActivity(i);
            } else if (dtypecount == appConfigData.getD_type().getDEVOTIONAL()) {
                Intent i = new Intent(getApplicationContext(), Mp3Devotional.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("c_type", data.getC_type());
                i.putExtra("from", "mp3");
                startActivity(i);
            } else if (dtypecount == appConfigData.getD_type().getCLASSICAL()) {
                Intent i = new Intent(getApplicationContext(), ClassicalListActivity.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("c_type", data.getC_type());
                startActivity(i);
            } else if (dtypecount == appConfigData.getD_type().getRABINDRA_SANGEET()) {
                Intent i = new Intent(getApplicationContext(), RabindraSangeetActivity.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("c_type", data.getC_type());
                i.putExtra("from", "Rabindra Sangeet");
                startActivity(i);
            } else if (dtypecount == appConfigData.getD_type().getOTHERS()) {
                Intent i = new Intent(getApplicationContext(), OtherLandingListActivity.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("c_type", data.getC_type());
                startActivity(i);
            } else if (dtypecount == appConfigData.getD_type().getNAZRUL_GEET()) {
                Intent i = new Intent(getApplicationContext(), RabindraSangeetActivity.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("c_type", data.getC_type());
                i.putExtra("from", "Nazrul Geet");
                startActivity(i);
            } else if (dtypecount == appConfigData.getD_type().getREMIX()) {
                Intent i = new Intent(getApplicationContext(), RemixSongsActivity.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("c_type", data.getC_type());
                startActivity(i);
            } else if (dtypecount == appConfigData.getD_type().getGHAZALS_SUFI()) {
                Intent i = new Intent(getApplicationContext(), GajalSufiActivity.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("c_type", data.getC_type());
                i.putExtra("from", "mp3");
                startActivity(i);
            }
        } else if (data.getC_type() == appConfigData.getC_type().getALBUM()) {
            if (data.getD_type() == appConfigData.getD_type().getHINDI_FILMS()) {
                Intent i = new Intent(getApplicationContext(), BuyAnyAlbumHindiLanding.class);
                i.putExtra("c_type", data.getC_type());
                i.putExtra("url", data.getUrl());
                startActivity(i);
            } else if (data.getD_type() == appConfigData.getD_type().getGHAZALS_SUFI()) {
                Intent i = new Intent(getApplicationContext(), BuyAnyAlbumListing.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("c_type", data.getC_type());
                startActivity(i);
                //gazal song
            } else if (data.getD_type() == appConfigData.getD_type().getREGIONAL_FILMS()) {
                //regional listing
                Intent i = new Intent(getApplicationContext(), LanguageListActivity.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("c_type", data.getC_type());
                startActivity(i);
            }
        }
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }
}