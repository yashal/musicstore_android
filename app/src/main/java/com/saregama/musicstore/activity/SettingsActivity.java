package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.LocationAdapter;
import com.saregama.musicstore.model.FileUploadSer;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.model.ServiceGenerator;
import com.saregama.musicstore.pojo.AppConfigDataPojo;
import com.saregama.musicstore.pojo.GetSatePojo;
import com.saregama.musicstore.pojo.ProfilePojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.ImageLoadingUtils;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class SettingsActivity extends BaseActivity implements View.OnClickListener {

    private ConnectionDetector cd;
    private ImageView user_image;
    private EditText user_name;
    private ToggleButton toggle_mp3, toggle_hd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;
    private final int SELECT_PHOTO = 457;
    private final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 456;
    private String mCurrentPhotoPath;
    private String photoPath = "";
    private TextView user_city;
    private static final String CAMERA_DIR = "/dcim/";

    // for external permission
    private final static int READ_EXTERNAL_STORAGE = 101;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;

    private Dialog location_dialog;
    private RecyclerView mRecyclerView;
    private LocationAdapter adapter;
    private ArrayList<String> locationArr;
    private AppConfigDataPojo appConfigData;
    private TextView changeLocation;
    private String residance = "";
    private String login_from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        appConfigData = getAppConfigJson();
        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);
        LinearLayout appbar_doneLL = (LinearLayout) findViewById(R.id.appbar_doneLL);
        changeLocation = (TextView) findViewById(R.id.changeLocation);
        cart_layout.setVisibility(View.GONE);
        global_search.setVisibility(View.GONE);
        notification_layout.setVisibility(View.GONE);
        appbar_doneLL.setVisibility(View.VISIBLE);

        SaregamaConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("Settings");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (getFromPrefs(SaregamaConstants.LOGGEDINFROM).equals("fbkit"))
        {
            login_from =  "mobile";
        }
        else
        {
            login_from =  "email";
        }


//        FrameLayout seekbar_root = (FrameLayout) findViewById(R.id.seekbar_root);
//        CoordinatorLayout.LayoutParams buttonLayoutParams = new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.WRAP_CONTENT, CoordinatorLayout.LayoutParams.WRAP_CONTENT);
//        buttonLayoutParams.setMargins(0, 0, 0, 0);
//        buttonLayoutParams.gravity = Gravity.BOTTOM | Gravity.END;
//        seekbar_root.setLayoutParams(buttonLayoutParams);

        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);

        if (appConfigData.getState_name() != null && appConfigData.getCountry_name() != null) {
            residance = appConfigData.getState_name();
        }
        System.out.println("hh yahsal " + residance);

        user_image = (ImageView) findViewById(R.id.activity_setting_imgUser);
        user_name = (EditText) findViewById(R.id.setting_userName);
        user_city = (TextView) findViewById(R.id.setting_userLocation);
        toggle_mp3 = (ToggleButton) findViewById(R.id.toggle_mp3);
        toggle_hd = (ToggleButton) findViewById(R.id.toggle_hd);
        TextView change_pic_text = (TextView) findViewById(R.id.change_pic_text);

        //        user_city.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        user_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        user_name.setKeyListener(null);

        change_pic_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    addPermissionDialogMarshMallow();
                } else {
                    showChooserDialog();
                }
            }
        });

        user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    addPermissionDialogMarshMallow();
                } else {
                    showChooserDialog();
                }
            }
        });

        appbar_doneLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hideSoftKeyboard();

                String str_name = user_name.getText().toString().trim();
                String str_city = user_city.getText().toString().trim();

                if (str_name.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.PROFILE_UPDATE_FAILED, getAppConfigJson().getProfile_update_name());
                } else if (str_city.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.PROFILE_UPDATE_FAILED, getAppConfigJson().getProfile_update_location());
                } else {
                    updateDetails(str_name, str_city);
                }
            }
        });

        if (getFromPrefs(SaregamaConstants.MP3) != null && getFromPrefs(SaregamaConstants.MP3).equals("checked"))
            toggle_mp3.setChecked(false);
        else
            toggle_mp3.setChecked(true);

        if (getFromPrefs(SaregamaConstants.HD) != null && getFromPrefs(SaregamaConstants.HD).equals("checked"))
            toggle_hd.setChecked(false);
        else
            toggle_hd.setChecked(true);

        toggle_mp3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    saveIntoPrefs(SaregamaConstants.MP3, "unchecked");
                else
                    saveIntoPrefs(SaregamaConstants.MP3, "checked");
            }
        });

        toggle_hd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    saveIntoPrefs(SaregamaConstants.HD, "unchecked");
                else
                    saveIntoPrefs(SaregamaConstants.HD, "checked");
            }
        });

        getProfile();
        changeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLocationDialog(ctx);
            }
        });
    }

    private void getProfile() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getProfileData(getFromPrefs(SaregamaConstants.SESSION_ID), login_from, new Callback<ProfilePojo>() {
                @Override
                public void success(ProfilePojo basePojo, Response response) {

                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            if (basePojo.getData().getImage() != null && basePojo.getData().getImage().length() > 0) {
                                ((BaseActivity) ctx).setProfileImageInLayout(ctx, (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image), basePojo.getData().getImage(), user_image);
                            }
                            if (basePojo.getData().getName() != null && basePojo.getData().getName().length() > 0) {
                                user_name.setText(basePojo.getData().getName());
                            }

                            if (basePojo.getData().getState() != null && basePojo.getData().getState().length() > 0) {
                                user_city.setText(basePojo.getData().getState());
                            }

                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    public void updateDetails(String name, String city) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            FileUploadSer service = ServiceGenerator.createService(FileUploadSer.class, FileUploadSer.BASE_URL);
            TypedFile typedFile = null;
            if (photoPath.length() > 0)
                typedFile = new TypedFile("multipart/form-data", new File(photoPath));

            service.updateProfile(getFromPrefs(SaregamaConstants.SESSION_ID), typedFile, name, getFromPrefs(SaregamaConstants.ID),  city, new Callback<ProfilePojo>() {
                @Override
                public void success(ProfilePojo basePojo, Response response) {
//                    System.out.println("retrofit url " + response.getUrl());
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            Toast.makeText(SettingsActivity.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                            if (basePojo.getData().getImage() != null && basePojo.getData().getImage().length() > 0) {
                                ((BaseActivity) ctx).setProfileImageInLayout(ctx, (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image), basePojo.getData().getImage(), user_image);
                                saveIntoPrefs(SaregamaConstants.USERIMAGE, basePojo.getData().getImage());
                            }
                            if (basePojo.getData().getName() != null && basePojo.getData().getName().length() > 0) {
                                user_name.setText(basePojo.getData().getName());
                                saveIntoPrefs(SaregamaConstants.NAME, basePojo.getData().getName());
                            }
                            if (basePojo.getData().getCity() != null && basePojo.getData().getCity().length() > 0) {
                                user_city.setText(basePojo.getData().getCity());
                                saveIntoPrefs(SaregamaConstants.CITY, basePojo.getData().getCity());
                            }
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    public void showChooserDialog() {
        final Dialog dialogr = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogr.setContentView(R.layout.custom_imageupload_dialog);
        dialogr.setCancelable(true);

        // set the custom dialog components - text, image and button
        ImageView gallery = (ImageView) dialogr.findViewById(R.id.galleryimage);
        gallery.setImageResource(R.mipmap.gallery_icon);
        ImageView camera = (ImageView) dialogr.findViewById(R.id.cameraimage);
        camera.setImageResource(R.mipmap.cmeraicon);

        gallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, SELECT_PHOTO);
                dialogr.dismiss();
                dialogr.hide();
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // give the image a name so we can store it in the phone's
                // default location
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//				File f = null;
                try {
//					f = setUpPhotoFile();

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "bmh" + timeStamp + "_";
                    File albumF = getAlbumDir();
                    File imageF = File.createTempFile(imageFileName, "bmh", albumF);


                    mCurrentPhotoPath = imageF.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageF));
                } catch (IOException e) {
                    e.printStackTrace();
//					f = null;
//					mCurrentPhotoPath = null;

                }

                startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                dialogr.dismiss();
                dialogr.hide();
            }
        });
        dialogr.show();
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraPicture");
            } else {
                storageDir = new File(Environment.getExternalStorageDirectory() + CAMERA_DIR + "CameraPicture");
            }

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
//		Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
//		Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }


    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            System.out.println("hh data null");
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imagePath1 = cursor.getString(columnIndex);

//                File f =new File(compressImage(imagePath1, 1));
                File f = new File(compressImage(imagePath1, 1));
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                user_image.setImageBitmap(myBitmap);
                photoPath = f.getAbsolutePath();

            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

                File f = new File(compressImage(mCurrentPhotoPath, 0));
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                user_image.setImageBitmap(myBitmap);
                photoPath = f.getAbsolutePath();
            }
        }
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = this.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }

    }

    private void addPermissionDialogMarshMallow() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(WRITE_EXTERNAL_STORAGE);
        resultCode = READ_EXTERNAL_STORAGE;


        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    showChooserDialog();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case READ_EXTERNAL_STORAGE:
                if (hasPermission(WRITE_EXTERNAL_STORAGE)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    showChooserDialog();
                } else {
                    permissionsRejected.add(WRITE_EXTERNAL_STORAGE);
                    clearMarkAsAsked(WRITE_EXTERNAL_STORAGE);
                    String message = "permission for access external storage was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }

    }

    public String compressImage(String imageUri, int flag) {
        String filePath;
        if (flag == 1) {
            filePath = getRealPathFromURI(imageUri);
        }

        filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        ImageLoadingUtils utils = new ImageLoadingUtils(this);
        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
//        options.inPurgeable = true;
//        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public void showLocationDialog(Context ctx) {
        location_dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        location_dialog.setContentView(R.layout.select_location_dialog);

        mRecyclerView = (RecyclerView) location_dialog.findViewById(R.id.change_location_recycler);
        ImageView close_dialog = (ImageView) location_dialog.findViewById(R.id.close_dialog);
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                location_dialog.dismiss();
            }
        });

        locationArr = new ArrayList<>();

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

        locationArr = appConfigData.getIndian_states().getState_name();
        if (locationArr != null && locationArr.size() > 0) {
            adapter = new LocationAdapter(ctx, locationArr, "Settings");
            mRecyclerView.setAdapter(adapter);
        }

        location_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        location_dialog.show();
    }

    @Override
    public void onClick(View view) {
        location_dialog.dismiss();
        String locationName_str = (String) view.getTag(R.string.key);
        user_city.setText(locationName_str);
//        user_city.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    }
}
