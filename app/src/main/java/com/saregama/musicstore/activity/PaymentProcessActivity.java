package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

public class PaymentProcessActivity extends BaseActivity {

    private WebView webview;
    private PaymentProcessActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_process);

        setDrawerAndToolbar("Payment");

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);
        LinearLayout drawerButton = (LinearLayout) findViewById(R.id.drawerButton);
        cart_layout.setVisibility(View.GONE);
        global_search.setVisibility(View.GONE);
        drawerButton.setVisibility(View.GONE);
        notification_layout.setVisibility(View.GONE);

        SaregamaConstants.ACTIVITIES.add(ctx);

        final ProgressDialog alertDialog = SaregamaDialogs.showLoading(ctx);

        webview = (WebView) findViewById(R.id.webview);

        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 85) {
                    if (alertDialog != null && alertDialog.isShowing())
                        alertDialog.dismiss();
                }
            }
        });

        webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//                displayDialog(handler);
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                AlertDialog alertDialog = builder.create();
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }

                message = getAppConfigJson().getPayment_ssl_alert();
                alertDialog.setTitle("");
                alertDialog.setMessage(message);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Ignore SSL certificate errors
                        handler.proceed();
                    }
                });

                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                alertDialog.show();
            }

            public boolean shouldOverrideUrlLoading(WebView view, final String url) {
                if (url.toLowerCase().startsWith("http") || url.toLowerCase().startsWith("https") || url.toLowerCase().startsWith("file")) {
                    webview.loadUrl(url);
                    if (url.contains(getAppConfigJson().getFail()) || url.contains(getAppConfigJson().getSuccess())) {
                        final ProgressDialog progressDialog = SaregamaDialogs.showLoading(ctx);
                        progressDialog.setTitle("Please Wait");
                        progressDialog.setMessage("Redirecting back to the app.");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Intent returnIntent = new Intent();
                                if (url.contains(SaregamaConstants.BASE_URL + "fail")) {
                                    returnIntent.putExtra("result", "fail");
                                } else if (url.contains(SaregamaConstants.BASE_URL + "success")) {
                                    returnIntent.putExtra("result", "success");
                                }
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }
                        }, 1000);
                    } else if (url.equals("http://sapi.saregama.com/") || url.equals("http://api.saregama.com/") || url.equals("http://www.saregama.com/") || url.equals("https://www.saregama.com/")) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", "fail");
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                }
                return (true);
            }
        });

        openURL(getIntent().getStringExtra("url"));
    }

    private void openURL(String url) {
        webview.loadUrl(url);
        webview.requestFocus();
    }

    @Override
    public void onBackPressed() {
        Button OkButtonLogout;
        final Dialog cancelPayment = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        cancelPayment.setContentView(R.layout.custom_dialog);
        if (!cancelPayment.isShowing()) {
            TextView msg_textView = (TextView) cancelPayment.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) cancelPayment.findViewById(R.id.dialog_header);
            ImageView dialog_header_cross = (ImageView) cancelPayment.findViewById(R.id.dialog_header_cross);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(getAppConfigJson().getCancel_transaction_popup());
            dialog_header.setText("Warning");
            OkButtonLogout = (Button) cancelPayment.findViewById(R.id.btn_yes_exit);
            OkButtonLogout.setText("YES");
            Button CancelButtonLogout = (Button) cancelPayment.findViewById(R.id.btn_no_exit);
            CancelButtonLogout.setText("NO");
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelPayment.dismiss();
                }
            });

            OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelPayment.dismiss();
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_CANCELED, returnIntent);
                    finish();
                }
            });
            CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelPayment.dismiss();
                }
            });
            cancelPayment.show();
        }
    }

    public void displayDialog(final SslErrorHandler handler) {
        Button okButton;
        final Dialog dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.custom_dialog);
        TextView msg_textView = (TextView) dialog.findViewById(R.id.text_exit);
        TextView dialog_header = (TextView) dialog.findViewById(R.id.dialog_header);
        dialog_header.setText("");
        msg_textView.setText(getAppConfigJson().getPayment_ssl_alert());
        okButton = (Button) dialog.findViewById(R.id.btn_yes_exit);
        okButton.setText("OK");
        Button cancelButton = (Button) dialog.findViewById(R.id.btn_no_exit);
        cancelButton.setText("CANCEL");
        ImageView dialog_header_cross = (ImageView) findViewById(R.id.dialog_header_cross);
        dialog_header_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.cancel();
                dialog.dismiss();
            }
        });
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.proceed();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.cancel();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
