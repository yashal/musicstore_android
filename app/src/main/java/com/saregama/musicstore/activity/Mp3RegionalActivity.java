package com.saregama.musicstore.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.RegionalMp3songPagerAdapter;
import com.saregama.musicstore.pojo.ClassHindArtistListPojo;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class Mp3RegionalActivity extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {
    private RegionalMp3songPagerAdapter mAdapter;
    private ViewPager viewPager;
    private LinearLayout song_layout, movies_layout, artist_layout, song_selector, movies_selector, artist_selector;
    private TextView song_text, movies_text, artist_text;
    private Activity ctx = this;
    private CircularSeekBar seekbar;
    private ImageView previous_view;
    private ImageView imageView;
    private MP3HindiSongListPojo data_playing;
    public ArrayList<MP3HindiSongListPojo> arrdata;
    public RecyclerView.Adapter regionalSongsFragmentAdapter;
    private int fragment_pos = 0;
    private String notification_key;
    private int c_type;
//    private EditText search;
//    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp3_regional);

        c_type = getIntent().getIntExtra("c_type", 1);
//        String coming_from = getIntent().getStringExtra("coming_from");

        if (arrdata == null)
            arrdata = new ArrayList<>();

//        if (coming_from != null && coming_from.equals("search")) {
//            LinearLayout page_data = (LinearLayout) findViewById(R.id.page_data);
//            page_data.setVisibility(View.GONE);
//
//            String fragment_pos = getIntent().getStringExtra("fragment_pos");
//
//            if (fragment_pos.equalsIgnoreCase("songs")) {
//                setDrawerAndToolbar("Songs");
//                RegionalMp3SongFragment fragment = new RegionalMp3SongFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "RegionalMp3SongFragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("album")) {
//                setDrawerAndToolbar("Albums");
//                RegionalMoviesAlbumFragment fragment = new RegionalMoviesAlbumFragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "RegionalMoviesAlbumFragment").commit();
//            } else if (fragment_pos.equalsIgnoreCase("artiste")) {
//                setDrawerAndToolbar("Artistes");
//                RegionalArtistfragment fragment = new RegionalArtistfragment();
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().add(R.id.global_search_songs, fragment, "RegionalArtistfragment").commit();
//            }
//        } else {
            if (getIntent().getStringExtra("languagename") != null) {
                setDrawerAndToolbar(getIntent().getStringExtra("languagename"));
            } else {
                setDrawerAndToolbar("");
            }
            SaregamaConstants.ACTIVITIES.add(ctx);

//            search = (EditText) findViewById(R.id.search_song);
//            softKeyboardDoneClickListener(search);
//            search.setHint("Search Songs, Albums, Artistes");
//
//            search.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(ctx, HindiFilmsSearchActivity.class);
//                    intent.putExtra("search_from", "regional");
//                    intent.putExtra("c_type", c_type);
//                    intent.putExtra("lang_id", getIntent().getIntExtra("languageid", 0));
//                    intent.putExtra("languagename", getIntent().getStringExtra("languagename"));
//                    startActivity(intent);
//                }
//            });

            song_layout = (LinearLayout) findViewById(R.id.regional_song_layout);
            movies_layout = (LinearLayout) findViewById(R.id.regional_movies_layout);
            artist_layout = (LinearLayout) findViewById(R.id.regional_artist_layout);

            song_selector = (LinearLayout) findViewById(R.id.regional_song_selector);
            movies_selector = (LinearLayout) findViewById(R.id.regional_movies_selector);
            artist_selector = (LinearLayout) findViewById(R.id.regional_artist_selector);

            song_text = (TextView) findViewById(R.id.regional_song_text);
            movies_text = (TextView) findViewById(R.id.regional_movies_text);
            artist_text = (TextView) findViewById(R.id.regional_artist_text);

            song_text.setText(getResources().getString(R.string.songs));
            movies_text.setText(getResources().getString(R.string.album));
            artist_text.setText(getResources().getString(R.string.artist));

            song_layout.setOnClickListener(this);
            movies_layout.setOnClickListener(this);
            artist_layout.setOnClickListener(this);

            mAdapter = new RegionalMp3songPagerAdapter(getSupportFragmentManager());
            viewPager = (ViewPager) findViewById(R.id.regional_mp3_landing_pager);
            viewPager.setAdapter(mAdapter);
//            viewPager.setPageTransformer(true, new DepthPageTransformer());
            viewPager.setOffscreenPageLimit(2);

            if (getIntent().getStringExtra("notification_key") != null) {
                notification_key = getIntent().getStringExtra("notification_key");
                if (notification_key.equals("album_regional_telugu")) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
            }

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    setStyle(position);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            });
//        }

        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regional_song_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;
            case R.id.regional_movies_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;
            case R.id.regional_artist_layout:
                if (viewPager.getCurrentItem() != 2) {
                    viewPager.setCurrentItem(2);
                    setStyle(2);
                }
                break;

            case R.id.albumLL:
                hideSoftKeyboard();
                Intent intent = new Intent(ctx, RegionalMp3AlbumDetail.class);
                MP3HindiAlbumListPojo data = (MP3HindiAlbumListPojo) v.getTag(R.string.data);
                intent.putExtra("album_id", data.getAlbum_id());
                intent.putExtra("from", "regional");
                intent.putExtra("c_type", c_type);
                intent.putExtra("languagename", getIntent().getStringExtra("languagename"));
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.classhindustani_artist_albumLL:
                hideSoftKeyboard();
                Intent intent_artiste = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
                ClassHindArtistListPojo data_artiste = (ClassHindArtistListPojo) v.getTag(R.string.data);
                intent_artiste.putExtra("artist_id", data_artiste.getId());
                intent_artiste.putExtra("from", "regional");
                intent_artiste.putExtra("lang_id", getIntent().getIntExtra("languageid", 0));
                intent_artiste.putExtra("c_type", c_type);
                intent_artiste.putExtra("languagename", getIntent().getStringExtra("languagename"));
                startActivity(intent_artiste);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
                CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);

                QuickAction quickAction = setupQuickAction(data_addtocart.getSong_id(), c_type);
                quickAction.show(addToCart);
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            default:
                break;
        }
    }

    private void setStyle(int position) {
        fragment_pos = position;
        if (position == 1) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            song_selector.setVisibility(View.GONE);
            movies_selector.setVisibility(View.VISIBLE);
            artist_selector.setVisibility(View.GONE);
        } else if (position == 0) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            song_selector.setVisibility(View.VISIBLE);
            movies_selector.setVisibility(View.GONE);
            artist_selector.setVisibility(View.GONE);
        } else if (position == 2) {
            song_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            movies_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            artist_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));

            song_selector.setVisibility(View.GONE);
            movies_selector.setVisibility(View.GONE);
            artist_selector.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void notifyAdapter() {
        if (regionalSongsFragmentAdapter != null)
            regionalSongsFragmentAdapter.notifyDataSetChanged();
    }

    private void playMP3Song(ImageView imageView, View v) {
        hideSoftKeyboard();
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        if (data_playing.is_playing()) {
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            imageView.setImageResource(R.mipmap.play_icon);
//            seekbar.setBackgroundResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            for (int i = 0; i < arrdata.size(); i++)
                arrdata.get(i).setIs_playing(false);
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            data_playing.setIs_playing(true);
            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        previous_view = imageView;
        seekbar.setVisibility(View.VISIBLE);
        playPauseButtonClickListener(c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }

//    public void showListBubble(View v) {
//        if (fragment_pos == 0) {
//            RegionalMp3SongFragment reg_song_fragment = (RegionalMp3SongFragment) mAdapter.getFragment(fragment_pos);
//            if (reg_song_fragment != null)
//                reg_song_fragment.showListBubble(v);
//        } else if (fragment_pos == 1) {
//            RegionalMoviesAlbumFragment reg_album_fragment = (RegionalMoviesAlbumFragment) mAdapter.getFragment(fragment_pos);
//            if (reg_album_fragment != null)
//                reg_album_fragment.showListBubble(v);
//        } else if (fragment_pos == 2) {
//            RegionalArtistfragment reg_artist_fragment = (RegionalArtistfragment) mAdapter.getFragment(fragment_pos);
//            if (reg_artist_fragment != null)
//                reg_artist_fragment.showListBubble(v);
//        }
//    }
}
