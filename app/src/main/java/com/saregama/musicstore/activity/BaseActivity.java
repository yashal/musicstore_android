package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.FilterCategoryListAdapter;
import com.saregama.musicstore.adapter.LocationAdapter;
import com.saregama.musicstore.fragments.DrawerFragment;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.model.RestClientAnalytics;
import com.saregama.musicstore.pojo.AddToCartPojo;
import com.saregama.musicstore.pojo.AnalyticsBasePojo;
import com.saregama.musicstore.pojo.AppConfigDataPojo;
import com.saregama.musicstore.pojo.AvailOfferPojo;
import com.saregama.musicstore.pojo.BasePojo;
import com.saregama.musicstore.pojo.CartItemPojo;
import com.saregama.musicstore.pojo.CartPojo;
import com.saregama.musicstore.pojo.CommonUrlPOjo;
import com.saregama.musicstore.pojo.DownloadManagerPojo;
import com.saregama.musicstore.pojo.HomeBannerPojo;
import com.saregama.musicstore.pojo.ItemsAddedInCartPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.NotificationDataPojo;
import com.saregama.musicstore.pojo.NotificationPojo;
import com.saregama.musicstore.pojo.OfferDataPojo;
import com.saregama.musicstore.pojo.RecommendationsAlbumPojo;
import com.saregama.musicstore.pojo.RecommendationsPojo;
import com.saregama.musicstore.services.PlayerService;
import com.saregama.musicstore.util.ActionItem;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.QuickActionAlbumTop;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;
import com.squareup.picasso.Picasso;

import org.apache.http.client.ClientProtocolException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.tankery.lib.circularseekbar.CircularSeekBar;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by Arpit on 15-Feb-16.
 */
public class BaseActivity extends AppCompatActivity implements NotifyAdapterInterface {

    public DrawerLayout mDrawerLayout;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private TextView cart_counter;
    private ArrayList<ItemsAddedInCartPojo> arr;
    private MP3HindiSongListPojo data_playing;

    private NotifyAdapterInterface notifyAdapterInterface;
    private ArrayList<CommonUrlPOjo> obj_commonUrl;
    private String version;
    private RelativeLayout notification_layout;
    private CircularSeekBar seekBar;
    private LinearLayout seek_barLL;
    boolean is_playing, is_stopped, is_paused;

    private Map<String, String> articleParams;

    private Messenger serviceMessenger;
    private FrameLayout seekbar_root;
    private TextView seekbar_songName, seekbar_albumName, seekbar_remainingtime;
    private CheckBox seekbar_addtocart;
    private LinearLayout seekbar_close, miniplayer_bg_layout;
    private ServiceConnection serviceConnection;
    private String add_cart_typeId = "";
    private PhoneStateListener phoneStateListener;
    QuickAction quickAction;

    ActionItem nextItem_album, prevItem_album, prevItem, nextItem;

    private SharedPreferences sharedPreferences;

    private Handler splashTimeHandler;
    private Runnable finalizer;

    // set Filter by Category
    private TextView header_list;
    private PopupWindow pwindo;
    private View layout;
    protected Drawable mBackground = null;
    private ListView language_filter_list;
    private Typeface face_normal, face_bold;
    private String selected_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            switch (intent.getAction()) {
                case PlayerService.ACTION_IS_PLAYING:
                    is_playing = intent.getBooleanExtra("is_playing", false);
                    is_stopped = intent.getBooleanExtra("is_stopped", false);
                    is_paused = intent.getBooleanExtra("is_paused", false);
                    System.out.println("xxxxxxxxxxxxxxxxxx hello is_playing " + is_playing);
                    System.out.println("xxxxxxxxxxxxxxxxxx hello is_stopped " + is_stopped);
                    System.out.println("xxxxxxxxxxxxxxxxxx hello is_paused " + is_paused);
                    SaregamaConstants.IS_PLAYING = is_playing;
                    SaregamaConstants.IS_PAUSED = is_paused;
                    SaregamaConstants.IS_STOPPED = is_stopped;
                    if (!is_paused && !is_playing && !is_stopped) {
                        saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
                        notifyAdapterInterface = BaseActivity.this;
                        notifyAdapterInterface.notifyAdapter();
                        removeSeekbarClick();
                        if (seekBar != null && seekbar_root != null) {
                            seekbar_root.setVisibility(View.GONE);
                        }
                    } else if (is_paused && is_playing && !is_stopped) {
                        notifyAdapterInterface = BaseActivity.this;
                        notifyAdapterInterface.notifyAdapter();
                    }
                    if (is_stopped) {
                        removeSeekbarClick();
                        saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
                        saveJsonObjectSong(new MP3HindiSongListPojo());
                        if (getTelephonyManager()) {
                            TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                            if (mgr != null) {
                                mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
                            }
                        }
                        notifyAdapterInterface = BaseActivity.this;
                        notifyAdapterInterface.notifyAdapter();
                        if (seekBar != null && seekbar_root != null) {
                            slideOut();
                            seekbar_root.setVisibility(View.GONE);
                        }
                    } else {
                        notifyAdapterInterface = BaseActivity.this;
                        notifyAdapterInterface.notifyAdapter();
                        if (is_playing) {
                            seekbarLLClick();
                            if (seekBar != null && seekbar_root != null && !is_paused) {
                                if (seekbar_root.getVisibility() == View.GONE)
                                    seekbar_root.setVisibility(View.VISIBLE);
                                seekBar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
                                if (getTelephonyManager()) {
                                    TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                                    if (mgr != null) {
                                        mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
                                    }
                                }
                            }
                            if (is_paused && seekBar != null) {
                                seekBar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
//                                slideOutPause();
                            }
                        } else {
                            removeSeekbarClick();
                            if (seekBar != null && seekbar_root != null) {
                                seekbar_root.setVisibility(View.GONE);
                            }
                        }
                    }
                    break;

                case PlayerService.ACTION_SEEK_POSITION:
                    if (seekBar != null) {
                        seekBar.setProgress((((float) intent.getIntExtra("current_position", 0) / intent.getIntExtra("duration", 0)) * 100)); // This math construction give a percentage of "was playing"/"song length"
                        SaregamaConstants.SEEkTO = (((float) intent.getIntExtra("current_position", 0) / intent.getIntExtra("duration", 0)) * 100);
                        float percentage = (((float) SaregamaConstants.PLAYING_TIME) / intent.getIntExtra("duration", 0)) * 100;
                        seekBar.setMax(percentage);
                        SaregamaConstants.PERCENTAGE = percentage;
                        SaregamaConstants.REMAINING_TIME = (intent.getLongExtra("millis_remaining_time", 0)) / 1000;
                        setNameText();
                        showRemainingTime();
                    }
                    break;
            }
        }
    };

    public Toolbar setDrawerAndToolbar(String name) {
        Toolbar appbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(appbar);
        getSupportActionBar().setTitle("");
        final LinearLayout drawer_button = (LinearLayout) findViewById(R.id.drawerButton);
        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        TextView notification_quantity = (TextView) findViewById(R.id.notification_quantity);
        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);


        cart_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeActivity(getResources().getString(R.string.package_name) + ".CartActivity");
                Intent cartActivity = new Intent(BaseActivity.this, CartActivity.class);
                startActivity(cartActivity);
                hideSoftKeyboard();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
            if (getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE) > 0) {
                notification_quantity.setVisibility(View.VISIBLE);
                notification_quantity.setText("" + getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE));
            } else {
                notification_quantity.setVisibility(View.GONE);
            }
        } else {
            notification_quantity.setVisibility(View.GONE);
        }

        notification_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeActivity(getResources().getString(R.string.package_name) + ".NotificationsActivity");
                Intent cartActivity = new Intent(BaseActivity.this, NotificationsActivity.class);
                startActivity(cartActivity);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        global_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeActivity(getResources().getString(R.string.package_name) + ".GlobalSearchActivity");
                Intent cartActivity = new Intent(BaseActivity.this, GlobalSearchActivity.class);
                startActivity(cartActivity);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        TextView header = (TextView) findViewById(R.id.header);
        cart_counter = (TextView) findViewById(R.id.cart_quantity);
        showCartCounter("normal");

        header.setText(name);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                if (mDrawerLayout != null && !mDrawerLayout.isDrawerOpen(Gravity.LEFT))
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                else if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
            }
        });
        if (mDrawerLayout != null) {
            DrawerFragment fragment = (DrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_drawer);
            fragment.setUp(mDrawerLayout);
        }

        initCommonComponent();

        if (seekBar != null) {
            seekBar.setClickable(false);
            seekBar.setFocusable(false);
            seekBar.setEnabled(false);
        }
        return appbar;
    }

    public Toolbar setDrawerAndToolbarWithDropDown(String name) {
        Toolbar appbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(appbar);
        getSupportActionBar().setTitle("");
        final LinearLayout drawer_button = (LinearLayout) findViewById(R.id.drawerButton);
        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        TextView notification_quantity = (TextView) findViewById(R.id.notification_quantity);
        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);

        cart_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeActivity(getResources().getString(R.string.package_name) + ".CartActivity");
                Intent cartActivity = new Intent(BaseActivity.this, CartActivity.class);
                startActivity(cartActivity);
                hideSoftKeyboard();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
            if (getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE) > 0) {
                notification_quantity.setVisibility(View.VISIBLE);
                notification_quantity.setText("" + getIntFromPrefs(SaregamaConstants.NOTIFICATION_SIZE));
            } else {
                notification_quantity.setVisibility(View.GONE);
            }
        } else {
            notification_quantity.setVisibility(View.GONE);
        }

        notification_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeActivity(getResources().getString(R.string.package_name) + ".NotificationsActivity");
                Intent cartActivity = new Intent(BaseActivity.this, NotificationsActivity.class);
                startActivity(cartActivity);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        global_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeActivity(getResources().getString(R.string.package_name) + ".GlobalSearchActivity");
                Intent cartActivity = new Intent(BaseActivity.this, GlobalSearchActivity.class);
                startActivity(cartActivity);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        TextView header = (TextView) findViewById(R.id.header);

        // set Filter by Category
        header_list = (TextView) findViewById(R.id.header_list);
        face_normal = Typeface.createFromAsset(this.getAssets(), "fonts/SourceSansPro_Regular.otf");
        face_bold = Typeface.createFromAsset(this.getAssets(), "fonts/SourceSansPro_Semibold.otf");
        fetchCategoryDropdownList();
        cart_counter = (TextView) findViewById(R.id.cart_quantity);
        showCartCounter("normal");

        header.setVisibility(View.GONE);
        header_list.setVisibility(View.VISIBLE);
        header_list.setText(name);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                if (mDrawerLayout != null && !mDrawerLayout.isDrawerOpen(Gravity.LEFT))
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                else if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
            }
        });
        if (mDrawerLayout != null) {
            DrawerFragment fragment = (DrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_drawer);
            fragment.setUp(mDrawerLayout);
        }

        initCommonComponent();

        if (seekBar != null) {
            seekBar.setClickable(false);
            seekBar.setFocusable(false);
            seekBar.setEnabled(false);
        }

        // set Filter by Category
        header_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiateDropDownPopupWindow();
                header_list.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.drop_up_header, 0);

            }
        });

        pwindo.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                pwindo.dismiss();
                header_list.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.drop_down_header, 0);
            }
        });
        return appbar;
    }


    public void setDrawerAndToolbarTitle(String title) {

        LinearLayout drawer = (LinearLayout) findViewById(R.id.drawerButton);
        TextView header = (TextView) findViewById(R.id.header);

        header.setText(title);
        final DrawerLayout mDrawerLayout;
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final DrawerFragment fragment = (DrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_drawer);
        fragment.setUp(mDrawerLayout);
        showCartCounter("normal");
        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });

        initCommonComponent();

    }

    public void removeActivity(String activity) {
        for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
            if (SaregamaConstants.ACTIVITIES.get(i) != null && SaregamaConstants.ACTIVITIES.get(i).toString().contains(activity)) {
                SaregamaConstants.ACTIVITIES.get(i).finish();
                SaregamaConstants.ACTIVITIES.remove(i);
                break;
            }
        }
    }

    public boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void saveIntoPrefs(String key, String value) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public void saveIntIntoPrefs(String key, int value) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public void saveDoubleIntoPrefs(String key, double value) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putLong(key, Double.doubleToRawLongBits(value));
        edit.commit();
    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        return prefs.getString(key, SaregamaConstants.DEFAULT_VALUE);
    }

    public int getIntFromPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        return prefs.getInt(key, 0);
    }

    public double getDoubleFromPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        return Double.longBitsToDouble(prefs.getLong(key, Double.doubleToLongBits(0)));
    }


    public void saveBooleanIntoPrefs(String key, boolean value) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(key, SaregamaConstants.STATUS);
        edit.commit();

    }

    public boolean getBooleanFromPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        return prefs.getBoolean(key, SaregamaConstants.STATUS);
    }

    public void clearPrefs() {
        SharedPreferences prefs = getSharedPreferences(SaregamaConstants.PREF_NAME, MODE_PRIVATE);
        prefs.edit().clear().commit();
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void showSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            inputMethodManager.toggleSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    public void softKeyboardDoneClickListener(EditText edittext) {
        edittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    hideSoftKeyboard();
                    return true;
                }
                return false;
            }
        });
    }

    public void setImageInLayout(Context ctx, int width, int height, String url, ImageView image) {
        Picasso.with(ctx).load(url).resize(width, height).placeholder(R.mipmap.fallback_image).error(R.mipmap.fallback_image).into(image);
    }

    public void setImageInLayoutHome(Context ctx, int width, int height, String url, ImageView image) {
        Picasso.with(ctx).load(url).fit().centerCrop().placeholder(R.mipmap.fallback_image).error(R.mipmap.fallback_image).into(image);
    }

    public void setProfileImageInLayout(Context ctx, int width, int height, String url, ImageView image) {
        Picasso.with(ctx).load(url).placeholder(R.mipmap.fallback_image).error(R.mipmap.fallback_image).into(image);
    }

    public JsonReader getAsyncTaskData(String url) {
        InputStream is = null;
        String json = "";

        // Making HTTP request
        try {
            // defaultHttpClient
            URL urll = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urll.openConnection();
            connection.setDoInput(true);
            connection.connect();
            is = connection.getInputStream();

            System.out.println("xxxx " + url);
            checkCommonURL(url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        System.out.println("xxxx json response " + json);
        // return JSON String

        JsonReader reader = new JsonReader(new StringReader(json));
        reader.setLenient(true);

        return reader;

    }

    public JsonReader getAsyncTaskDataArtisteSearch(String url) {
        InputStream is = null;
        String json = "";

        // Making HTTP request
        try {
            // defaultHttpClient
            URL urll = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urll.openConnection();
            connection.setDoInput(true);
            connection.connect();
            is = connection.getInputStream();

            System.out.println("xxxx " + url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        System.out.println("xxxx json response " + json);
        // return JSON String

        JsonReader reader = new JsonReader(new StringReader(json));
        reader.setLenient(true);

        return reader;

    }

    public String getAsyncTaskDataGlobal(String url) {
        InputStream is = null;
        String json = "";

        // Making HTTP request
        try {
            // defaultHttpClient
            URL urll = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urll.openConnection();
            connection.setDoInput(true);
            connection.connect();
            is = connection.getInputStream();

//            System.out.println("xxxx " + url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // return JSON String

        return json;

//        JsonReader reader = new JsonReader(new StringReader(json));
//        reader.setLenient(true);
//
//        return reader;

    }

    /* Displays the Detail of the Offer in Dialog */
    public void displayOfferDialog(final OfferDataPojo data) {
        Button apply_offer;
//        final Dialog offerDialog = new Dialog(this, R.style.PauseDialog);
        final Dialog offerDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        offerDialog.setContentView(R.layout.offer_dialog_layout);

        TextView offer_title = (TextView) offerDialog.findViewById(R.id.offer_title);
        TextView offer_sub_title = (TextView) offerDialog.findViewById(R.id.offer_sub_title);
        TextView offer_terms = (TextView) offerDialog.findViewById(R.id.offer_terms);
        TextView offer_validity = (TextView) offerDialog.findViewById(R.id.offer_validity);
        offer_title.setText(data.getTitle());
        offer_sub_title.setText(data.getSub_title());
        offer_terms.setText("Terms: " + data.getTerm());
        offer_validity.setText("Offer valid till " + data.getExpiry_date());
        apply_offer = (Button) offerDialog.findViewById(R.id.apply_offer);
        ImageView cross = (ImageView) offerDialog.findViewById(R.id.cross);
        EditText coupon_code = (EditText) offerDialog.findViewById(R.id.coupon_code);

        coupon_code.setText(data.getCoupon_code());

        apply_offer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                offerDialog.dismiss();
                saveIntoPrefs(SaregamaConstants.COUPON_CODE, data.getCoupon_code());
                Toast.makeText(getApplicationContext(), getAppConfigJson().getCoupon_offer(), Toast.LENGTH_SHORT).show();
            }
        });
        cross.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                offerDialog.dismiss();
            }
        });
        offerDialog.show();
    }

    public void loginRequiredAlert(String message) {
        Button OkButtonLogout;
        final Dialog cancelPayment = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        cancelPayment.setContentView(R.layout.custom_dialog);
        if (!cancelPayment.isShowing()) {

            TextView msg_textView = (TextView) cancelPayment.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) cancelPayment.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(message);
            dialog_header.setText("Sign in");
            OkButtonLogout = (Button) cancelPayment.findViewById(R.id.btn_yes_exit);
            OkButtonLogout.setText("SIGN IN");
            Button CancelButtonLogout = (Button) cancelPayment.findViewById(R.id.btn_no_exit);
            CancelButtonLogout.setText("SKIP");

            ImageView dialog_header_cross = (ImageView) cancelPayment.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelPayment.dismiss();
                }
            });

            OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelPayment.dismiss();
                    Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                }
            });
            CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelPayment.dismiss();
                }
            });
            cancelPayment.show();
        }
    }

    @Override
    public void notifyAdapter() {

    }

    public class GetBitmapTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... arg0) {
            Bitmap bitmap = getBitmapFromURL(arg0[0]);
            return bitmap;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            // sendBigPictureStyleNotification();
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Bitmap createBitmap_ScriptIntrinsicBlur(Bitmap src, float r) {

        //Radius range (0 < r <= 25)
        if (r <= 0) {
            r = 0.1f;
        } else if (r > 25) {
            r = 25.0f;
        }

        Bitmap bitmap = Bitmap.createBitmap(
                src.getWidth(), src.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(this);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, src);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(r);
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();
        return bitmap;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }
    }

//    public void AddToCartWithoutLogin(final String crt) {
//        dialog = new SaregamaDialogs(this);
//        hideSoftKeyboard();
//        if (!checkCart(crt)) {
//            cd = new ConnectionDetector(getApplicationContext());
//
//            String song_id = getFromPrefs(SaregamaConstants.TYPE_ID) + "|" + crt;
//            if (cd.isConnectingToInternet()) {
//                final ProgressDialog d = SaregamaDialogs.showLoading(this);
//                d.setCanceledOnTouchOutside(false);
//                if (getFromPrefs(SaregamaConstants.TYPE_ID).equals(getAppConfigJson().getC_type().getALBUM() + ""))
//                    d.setMessage("Adding album to cart...");
//                else
//                    d.setMessage("Adding song to cart...");
//
//                RestClient.get().addToCartWithoutLogin(getFromPrefs(SaregamaConstants.SESSION_ID), getFromPrefs(SaregamaConstants.ID), song_id, getFromPrefs(SaregamaConstants.IMEI), new Callback<AddToCartPojo>() {
//                    @Override
//                    public void success(AddToCartPojo basePojo, Response response) {
//                        if (basePojo != null) {
//                            if (basePojo.getStatus()) {
//
//                                if (basePojo.getData() != null) {
//                                    if (getFromPrefs(SaregamaConstants.TYPE_ID).equals(getAppConfigJson().getC_type().getALBUM() + ""))
//                                        Toast.makeText(getApplicationContext(), "Album added to cart", Toast.LENGTH_SHORT).show();
//                                    else
//                                        Toast.makeText(getApplicationContext(), "Song added to cart", Toast.LENGTH_SHORT).show();
//
//                                    ItemsAddedInCartPojo items = new ItemsAddedInCartPojo();
//                                    items.setId(crt);
//                                    if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0)
//                                        items.setLoggedIn(true);
//                                    else
//                                        items.setLoggedIn(false);
//
//                                    items.setType(getFromPrefs(SaregamaConstants.TYPE_ID));
//
//                                    ArrayList<ItemsAddedInCartPojo> arr = getJsonObject();
//
//                                    if (arr != null)
//                                        arr.add(items);
//                                    else {
//                                        arr = new ArrayList<>();
//                                        arr.add(items);
//                                    }
//                                    saveJsonObject(arr);
//                                    showCartCounter("normal");
//                                }
//
//                            } else {
//                                dialog.displayCommonDialog(basePojo.getError());
//                            }
//                        } else {
//                            dialog.displayCommonDialog(getAppConfigJson().getServer_error());
//                        }
//                        d.dismiss();
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        d.dismiss();
//                    }
//
//                });
//            } else {
//                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
//            }
//        } else {
//            if (getFromPrefs(SaregamaConstants.TYPE_ID).equals(getAppConfigJson().getC_type().getALBUM() + ""))
//                dialog.displayCommonDialogWithHeader("CART STATUS", "This Album is already added in the cart.");
//            else
//                dialog.displayCommonDialogWithHeader("CART STATUS", "This Song is already added in the cart.");
//        }
//    }

    public void AddToCartWithoutLogin(final String type_id, final String crt) {
        dialog = new SaregamaDialogs(this);
        hideSoftKeyboard();
        if (type_id.equals("3"))
            add_cart_typeId = getAppConfigJson().getSong_type().getSONGHD() + "";
        else
            add_cart_typeId = type_id;
        if (!checkCart(crt)) {
            cd = new ConnectionDetector(getApplicationContext());
            String song_id = add_cart_typeId + "|" + crt;
            if (crt != null)
                if (cd.isConnectingToInternet()) {
                    final ProgressDialog d = SaregamaDialogs.showLoading(this);
                    d.setCanceledOnTouchOutside(false);

                    if (add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUM() + "") || add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUMHD() + ""))
                        d.setMessage("Adding album to cart...");
                    else
                        d.setMessage("Adding song to cart...");

                    RestClient.get().addToCartWithoutLogin(getFromPrefs(SaregamaConstants.SESSION_ID), getFromPrefs(SaregamaConstants.ID), song_id, getFromPrefs(SaregamaConstants.IMEI), "", new Callback<AddToCartPojo>() {
                        @Override
                        public void success(AddToCartPojo basePojo, Response response) {
                            if (basePojo != null) {
                                if (basePojo.getStatus()) {
                                    if (basePojo.getData() != null) {
                                        if (add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUM() + "") || add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUMHD() + "")) {
                                            Toast.makeText(getApplicationContext(), "Album added to cart", Toast.LENGTH_SHORT).show();
                                            if (getIntFromPrefs(SaregamaConstants.COUNTER) < 3)
                                                getRecommendationsAlbum(add_cart_typeId, crt);
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Song added to cart", Toast.LENGTH_SHORT).show();
                                            if (getIntFromPrefs(SaregamaConstants.COUNTER) < 3)
                                                getRecommendationsSong(add_cart_typeId, crt);
                                        }

                                        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                                        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                                        if (cn.getShortClassName().equalsIgnoreCase(".activity.CartActivity")) {
                                            SaregamaConstants.CART_ACTIVITY.getCartData();
                                        }
                                        ItemsAddedInCartPojo items = new ItemsAddedInCartPojo();
                                        items.setId(crt);
                                        if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0)
                                            items.setLoggedIn(true);
                                        else
                                            items.setLoggedIn(false);

                                        items.setType(add_cart_typeId + "");

                                        ArrayList<ItemsAddedInCartPojo> arr = getJsonObject();

                                        if (arr != null)
                                            arr.add(items);
                                        else {
                                            arr = new ArrayList<>();
                                            arr.add(items);
                                        }
                                        saveJsonObject(arr);
                                        showCartCounter("normal");
                                    }

                                } else {
                                    Toast.makeText(getApplicationContext(), basePojo.getError(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                            }
                            d.dismiss();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            d.dismiss();
                        }

                    });
                } else {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
                }
        } else {
            if (add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUM() + "") || add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUMHD() + ""))
                Toast.makeText(getApplicationContext(), getAppConfigJson().getAlbum_exists_cart(), Toast.LENGTH_LONG).show();
            else
                Toast.makeText(getApplicationContext(), getAppConfigJson().getSong_exists_cart(), Toast.LENGTH_LONG).show();
        }
    }

    public void AddToCartRecommendation(final String type_id, final String crt) {
        dialog = new SaregamaDialogs(this);
        hideSoftKeyboard();
        if (type_id.equals("3"))
            add_cart_typeId = getAppConfigJson().getSong_type().getSONGHD() + "";
        else
            add_cart_typeId = type_id;
        if (!checkCart(crt)) {
            cd = new ConnectionDetector(getApplicationContext());
            String song_id = add_cart_typeId + "|" + crt;
            if (crt != null)
                if (cd.isConnectingToInternet()) {
                    final ProgressDialog d = SaregamaDialogs.showLoading(this);
                    d.setCanceledOnTouchOutside(false);

                    if (add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUM() + "") || add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUMHD() + ""))
                        d.setMessage("Adding album to cart...");
                    else
                        d.setMessage("Adding song to cart...");

                    RestClient.get().addToCartWithoutLogin(getFromPrefs(SaregamaConstants.SESSION_ID), getFromPrefs(SaregamaConstants.ID), song_id, getFromPrefs(SaregamaConstants.IMEI), "yes", new Callback<AddToCartPojo>() {
                        @Override
                        public void success(AddToCartPojo basePojo, Response response) {
                            if (basePojo != null) {
                                if (basePojo.getStatus()) {
                                    if (basePojo.getData() != null) {
                                        if (add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUM() + "") || add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUMHD() + "")) {
                                            Toast.makeText(getApplicationContext(), "Album added to cart", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Song added to cart", Toast.LENGTH_SHORT).show();
                                        }

                                        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                                        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                                        if (cn.getShortClassName().equalsIgnoreCase(".activity.CartActivity")) {
                                            SaregamaConstants.CART_ACTIVITY.getCartData();
                                        }
                                        ItemsAddedInCartPojo items = new ItemsAddedInCartPojo();
                                        items.setId(crt);
                                        if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0)
                                            items.setLoggedIn(true);
                                        else
                                            items.setLoggedIn(false);

                                        items.setType(add_cart_typeId + "");

                                        ArrayList<ItemsAddedInCartPojo> arr = getJsonObject();

                                        if (arr != null)
                                            arr.add(items);
                                        else {
                                            arr = new ArrayList<>();
                                            arr.add(items);
                                        }
                                        saveJsonObject(arr);
                                        showCartCounter("normal");
                                    }

                                } else {
                                    Toast.makeText(getApplicationContext(), basePojo.getError(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                            }
                            d.dismiss();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            d.dismiss();
                        }

                    });
                } else {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
                }
        } else {
            if (add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUM() + "") || add_cart_typeId.equals(getAppConfigJson().getC_type().getALBUMHD() + ""))
                Toast.makeText(getApplicationContext(), getAppConfigJson().getAlbum_exists_cart(), Toast.LENGTH_LONG).show();
            else
                Toast.makeText(getApplicationContext(), getAppConfigJson().getSong_exists_cart(), Toast.LENGTH_LONG).show();
        }
    }

    private void getRecommendationsSong(final String cType, String cid) {

        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {

            RestClient.get().recommendation(getFromPrefs(SaregamaConstants.IMEI), getFromPrefs(SaregamaConstants.ID), cType, cid, new Callback<RecommendationsPojo>() {
                @Override
                public void success(RecommendationsPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            if (basePojo.getData().getList() != null && basePojo.getData().getList().size() > 0) {
                                saveIntIntoPrefs(SaregamaConstants.COUNTER, getIntFromPrefs(SaregamaConstants.COUNTER) + 1);
//                                SaregamaDialogs dialogRecommendation = new SaregamaDialogs(BaseActivity.this);
//                                dialogRecommendation.displayRecommendedDialog(basePojo.getData().getList(), cType);

                                Intent mIntet = new Intent(BaseActivity.this, RecommendationActivity.class);
                                mIntet.putExtra("songs_list", basePojo.getData().getList());
                                mIntet.putExtra("cType", cType);
                                mIntet.putExtra("type", "song");
                                startActivity(mIntet);
                                delayHandler();
                            }
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }

            });
        } else {
//            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    public void delayHandler() {
        splashTimeHandler = new Handler();
        finalizer = new Runnable() {
            public void run() {
                if (seekbar_root != null)
                    seekbar_root.setVisibility(View.GONE);
            }
        };
        splashTimeHandler.postDelayed(finalizer, 1000);
    }

    private void getRecommendationsAlbum(final String cType, String cid) {

        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {

            RestClient.get().recommendationAlbum(getFromPrefs(SaregamaConstants.IMEI), getFromPrefs(SaregamaConstants.ID), cType, cid, new Callback<RecommendationsAlbumPojo>() {
                @Override
                public void success(RecommendationsAlbumPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            if (basePojo.getData().getList() != null && basePojo.getData().getList().size() > 0) {
                                saveIntIntoPrefs(SaregamaConstants.COUNTER, getIntFromPrefs(SaregamaConstants.COUNTER) + 1);
//                                SaregamaDialogs dialogRecommendation = new SaregamaDialogs(BaseActivity.this);
//                                dialogRecommendation.displayRecommendedDialogAlbum(basePojo.getData().getList(), cType);

                                Intent mIntet = new Intent(BaseActivity.this, RecommendationActivity.class);
                                mIntet.putExtra("songs_list", basePojo.getData().getList());
                                mIntet.putExtra("cType", cType);
                                mIntet.putExtra("type", "album");
                                startActivity(mIntet);
                                delayHandler();
                            }
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }

            });
        } else {
//            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    public boolean checkCart(String id) {
        boolean isAdded = false;
        ArrayList<ItemsAddedInCartPojo> obj = getJsonObject();
        if (obj != null && add_cart_typeId != null && id != null) {
            for (int i = 0; i < obj.size(); i++) {
                if (add_cart_typeId.equals(obj.get(i).getType()) && id.equals(obj.get(i).getId())) {
                    isAdded = true;
                    break;
                }
            }
        }
        return isAdded;
    }

    public void saveCategoryJsonObject(ArrayList<HomeBannerPojo> cart_object) {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(cart_object);
        prefsEditor.putString("MyCategoryObject", json);
        prefsEditor.commit();
    }

    public ArrayList<HomeBannerPojo> getCategoryJsonObject() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        final Gson gson = new Gson();
        String json = sharedPreferences.getString("MyCategoryObject", "");
        Type type = new TypeToken<List<HomeBannerPojo>>() {
        }.getType();
        ArrayList<HomeBannerPojo> obj = gson.fromJson(json, type);
        return obj;
    }

    public void saveJsonObject(ArrayList<ItemsAddedInCartPojo> cart_object) {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(cart_object);
        prefsEditor.putString("MyCartObject", json);
        prefsEditor.commit();
    }

    public ArrayList<ItemsAddedInCartPojo> getJsonObject() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        final Gson gson = new Gson();
        String json = sharedPreferences.getString("MyCartObject", "");
        Type type = new TypeToken<List<ItemsAddedInCartPojo>>() {
        }.getType();
        ArrayList<ItemsAddedInCartPojo> obj = gson.fromJson(json, type);
        return obj;
    }

    public void saveCommonUrl(ArrayList<CommonUrlPOjo> commonURL) {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(commonURL);
        prefsEditor.putString("CommonURLObject", json);
        prefsEditor.commit();
    }

    public ArrayList<CommonUrlPOjo> getCommonUrl() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        final Gson gson = new Gson();
        String json = sharedPreferences.getString("CommonURLObject", "");
        Type type = new TypeToken<List<CommonUrlPOjo>>() {
        }.getType();
        ArrayList<CommonUrlPOjo> obj = gson.fromJson(json, type);
        return obj;
    }

    public void checkCommonURL(String url) {
        obj_commonUrl = getCommonUrl();
        if (obj_commonUrl == null)
            obj_commonUrl = new ArrayList<>();
        CommonUrlPOjo pojo = new CommonUrlPOjo();
        pojo.setUrl(url);
        obj_commonUrl.add(pojo);
        saveCommonUrl(obj_commonUrl);

        if (obj_commonUrl.size() >= 10) {
            saveCommonUrl(new ArrayList<CommonUrlPOjo>());
            getAnalytics(obj_commonUrl);
        }
    }

    public void showCartCounter(String from) {
        final ArrayList<ItemsAddedInCartPojo> obj = getJsonObject();
        if (from.equals("logout")) {
            if (obj != null) {
                for (int i = obj.size() - 1; i >= 0; i--) {
                    if (obj.get(i).isLoggedIn()) {
                        obj.remove(i);
                    }
                }
                saveJsonObject(obj);
                showCartCounter("");
            }
        } else {
            if (cart_counter != null) {
                if (obj != null) {
                    cart_counter.setVisibility(View.VISIBLE);
                    if (obj.size() == 0)
                        cart_counter.setVisibility(View.GONE);
                    cart_counter.setText("" + obj.size());
                } else {
                    cart_counter.setVisibility(View.GONE);
                    cart_counter.setText("" + 0);
                }
            }
        }
    }

    public void emptyCart() {
        ArrayList<ItemsAddedInCartPojo> obj = getJsonObject();
        obj.clear();
        saveJsonObject(obj);
    }

    public void getCartDataFirstTime() {
        cd = new ConnectionDetector(BaseActivity.this);
        if (cd.isConnectingToInternet()) {

            RestClient.get().getCartData(getFromPrefs(SaregamaConstants.SESSION_ID), getFromPrefs(SaregamaConstants.IMEI), new Callback<CartPojo>() {
                @Override
                public void success(CartPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus() && basePojo.getData() != null) {
                            arr = getJsonObject();
                            if (arr != null) {
                                for (int i = arr.size() - 1; i >= 0; i--) {
                                    if (arr.get(i).isLoggedIn()) {
                                        arr.remove(i);
                                    }
                                }
                                saveJsonObject(arr);
                            }
                            ArrayList<CartItemPojo> hdSongs = new ArrayList<>();
                            ArrayList<CartItemPojo> mp3Songs = new ArrayList<>();
                            ArrayList<CartItemPojo> albums = new ArrayList<>();
                            if (basePojo.getData().getCart() != null && basePojo.getData().getCart().getHd() != null)
                                hdSongs = basePojo.getData().getCart().getHd();
                            if (basePojo.getData().getCart() != null && basePojo.getData().getCart().getMp3() != null)
                                mp3Songs = basePojo.getData().getCart().getMp3();
                            if (basePojo.getData().getCart() != null && basePojo.getData().getCart().getAlbum() != null)
                                albums = basePojo.getData().getCart().getAlbum();

                            if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
                                if (arr != null) {
                                    if (hdSongs != null && hdSongs.size() > 0) {
                                        addCartItemsInLocalData(hdSongs, getAppConfigJson().getC_type().getSONGHD() + "");
                                    }
                                    if (mp3Songs != null && mp3Songs.size() > 0) {
                                        addCartItemsInLocalData(mp3Songs, getAppConfigJson().getC_type().getSONG() + "");
                                    }
                                    if (albums != null && albums.size() > 0) {
                                        addCartItemsInLocalData(albums, getAppConfigJson().getC_type().getALBUM() + "");
                                    }
                                } else {
                                    arr = new ArrayList<>();
                                    if (hdSongs != null && hdSongs.size() > 0) {
                                        addCartItemsInLocalDataFirstTime(hdSongs, getAppConfigJson().getC_type().getSONGHD() + "");
                                    }
                                    if (mp3Songs != null && mp3Songs.size() > 0) {
                                        addCartItemsInLocalDataFirstTime(mp3Songs, getAppConfigJson().getC_type().getSONG() + "");
                                    }
                                    if (albums != null && albums.size() > 0) {
                                        addCartItemsInLocalDataFirstTime(albums, getAppConfigJson().getC_type().getALBUM() + "");
                                    }
                                }
                            } else {
                                arr = new ArrayList<>();
                                if (hdSongs != null && hdSongs.size() > 0) {
                                    addCartItemsInLocalDataWithoutLogin(hdSongs, getAppConfigJson().getC_type().getSONGHD() + "");
                                }
                                if (mp3Songs != null && mp3Songs.size() > 0) {
                                    addCartItemsInLocalDataWithoutLogin(mp3Songs, getAppConfigJson().getC_type().getSONG() + "");
                                }
                                if (albums != null && albums.size() > 0) {
                                    addCartItemsInLocalDataWithoutLogin(albums, getAppConfigJson().getC_type().getALBUM() + "");
                                }
                            }
                            saveJsonObject(arr);
                            showCartCounter("");
                        } else {
                            saveJsonObject(new ArrayList<ItemsAddedInCartPojo>());
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                }

            });
        }
    }

    public void getAvailOffers() {
        if (cd.isConnectingToInternet()) {
            RestClient.get().getavailOffers(new Callback<AvailOfferPojo>() {
                @Override
                public void success(AvailOfferPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            checkCommonURL(response.getUrl());
                            if (basePojo.getData() != null) {
                                if (basePojo.getData().size() > 0) {
                                    SaregamaConstants.AVAIL_OFFERS = basePojo.getData().size();
                                } else {
                                    SaregamaConstants.AVAIL_OFFERS = 0;
                                }
                            } else {
                                SaregamaConstants.AVAIL_OFFERS = 0;
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }

            });
        }
    }

    private void addCartItemsInLocalData(ArrayList<CartItemPojo> cartArr, String type) {
        for (int i = 0; i < cartArr.size(); i++) {
            boolean contains = false;
            for (int j = 0; j < arr.size(); j++) {
                if (cartArr.get(i) != null && arr.get(j).getId().equals(cartArr.get(i).getId()) && arr.get(j).getType().equals(cartArr.get(i).getCtype())) {
                    contains = true;
                    break;
                }
            }
            if (!contains && cartArr.get(i) != null) {
                ItemsAddedInCartPojo items = new ItemsAddedInCartPojo();
                items.setLoggedIn(true);
                items.setId(cartArr.get(i).getId());
                items.setType(cartArr.get(i).getCtype());
                arr.add(items);
            }
        }
        showCartCounter("");
    }

    private void addCartItemsInLocalDataFirstTime(ArrayList<CartItemPojo> cartArr, String type) {
        for (int i = 0; i < cartArr.size(); i++) {
            if (cartArr.get(i) != null) {
                ItemsAddedInCartPojo items = new ItemsAddedInCartPojo();
                items.setLoggedIn(true);
                items.setId(cartArr.get(i).getId());
                items.setType(cartArr.get(i).getCtype());
                arr.add(items);
            }
        }
    }

    private void addCartItemsInLocalDataWithoutLogin(ArrayList<CartItemPojo> cartArr, String type) {
        for (int i = 0; i < cartArr.size(); i++) {
            if (cartArr.get(i) != null) {
                ItemsAddedInCartPojo items = new ItemsAddedInCartPojo();
                items.setLoggedIn(false);
                items.setId(cartArr.get(i).getId());
                items.setType(cartArr.get(i).getCtype());
                arr.add(items);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        articleParams = new HashMap<>();
        initCommonComponent();

        data_playing = getJsonObjectSong();

        if (seekBar != null && seekbar_root != null) {
            if (SaregamaConstants.IS_PLAYING && !SaregamaConstants.IS_PAUSED && !SaregamaConstants.IS_STOPPED) {
                showRemainingTime();
                seekBar.setVisibility(View.VISIBLE);
                seekbar_root.setVisibility(View.VISIBLE);
                setNameText();
                seekBar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
                seekBar.setMax(SaregamaConstants.PERCENTAGE);
                seekBar.setProgress(SaregamaConstants.SEEkTO);
            } else if (SaregamaConstants.IS_PLAYING && SaregamaConstants.IS_PAUSED && !SaregamaConstants.IS_STOPPED) {
                seekBar.setVisibility(View.VISIBLE);
                seekbar_root.setVisibility(View.VISIBLE);
                showRemainingTime();
                setNameText();
                seekBar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
                seekBar.setMax(SaregamaConstants.PERCENTAGE);
                seekBar.setProgress(SaregamaConstants.SEEkTO);
            } else {
                seekbar_root.setVisibility(View.GONE);
            }

            seekBar.setClickable(false);
            seekBar.setFocusable(false);
            seekBar.setEnabled(false);
        }

        seekbarAddToCartClick();

        if (seekbar_close != null) {
            seekbar_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stopPlayer();
                }
            });
        }

        cart_counter = (TextView) findViewById(R.id.cart_quantity);
        showCartCounter("normal");

        Intent intent = new Intent(this, PlayerService.class);
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                serviceMessenger = new Messenger(service);
                Message message = new Message();
                message.what = PlayerService.IS_PLAYING;
                try {
                    serviceMessenger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };
        startService(intent);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE | BIND_ADJUST_WITH_ACTIVITY);

        IntentFilter filter = new IntentFilter();
        filter.addAction(PlayerService.ACTION_IS_PLAYING);
        filter.addAction(PlayerService.ACTION_SEEK_POSITION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    public void stopPlayer() {
        if (seekBar != null && seekbar_root != null) {
            seekBar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            seekbar_root.setVisibility(View.GONE);
            Message message = new Message();
            message.what = PlayerService.STOP;
            slideOut();
            try {
                serviceMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private void seekbarAddToCartClick() {
        if (seekbar_addtocart != null) {
            seekbar_addtocart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (data_playing != null) {
                        if (SaregamaConstants.PLAYING_SONG_C_TYPE == getAppConfigJson().getC_type().getALBUM()) {
//                            AddToCartWithoutLogin(SaregamaConstants.PLAYING_SONG_C_TYPE + "", SaregamaConstants.PLAYING_SONG_ALBUM_ID);
                            QuickAction quickActionAlbum = setupQuickActionAlbumLeft(SaregamaConstants.PLAYING_SONG_ALBUM_ID, SaregamaConstants.PLAYING_SONG_C_TYPE, getAppConfigJson().getCurrency(), SaregamaConstants.PLAYING_SONG_PRICE, SaregamaConstants.PLAYING_SONG_PRICE_HD);
                            quickActionAlbum.showSeekBar(seekbar_addtocart);
                        } else if (data_playing != null && data_playing.getSong_id() != null) {
//                            AddToCartWithoutLogin(SaregamaConstants.PLAYING_SONG_C_TYPE + "", data_playing.getSong_id());
                            quickAction = setupQuickActionSeekBar(data_playing.getSong_id(), SaregamaConstants.PLAYING_SONG_C_TYPE);
                            quickAction.showSeekBar(seekbar_addtocart);
                        } else if (data_playing != null) {
//                            AddToCartWithoutLogin(SaregamaConstants.PLAYING_SONG_C_TYPE + "", data_playing.getId());
                            quickAction = setupQuickActionSeekBar(data_playing.getId(), SaregamaConstants.PLAYING_SONG_C_TYPE);
                            quickAction.showSeekBar(seekbar_addtocart);
                        }

                    }
                }
            });
        }
    }

    private void seekbarLLClick() {
        if (seek_barLL != null)
            seek_barLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    seekBarPlayPauseClickListener(seekBar, data_playing);
                }
            });
    }

    private void removeSeekbarClick() {
        if (seek_barLL != null)
            seek_barLL.setOnClickListener(null);
    }

    public void setNameText() {
        seekbar_songName.setText("");
        seekbar_albumName.setText("");
        if (data_playing.getSong_name() != null) {
            seekbar_songName.setText(data_playing.getSong_name());
        } else if (data_playing.getName() != null) {
            seekbar_songName.setText(data_playing.getName());
        }
        if (data_playing.getAlbum_name() != null) {
            seekbar_albumName.setText(data_playing.getAlbum_name());
        }
//        if (SaregamaConstants.PLAYING_SONG_C_TYPE == getAppConfigJson().getC_type().getSONG()) {
//            add_text_seekbar.setText("Add Song");
//        } else if (SaregamaConstants.PLAYING_SONG_C_TYPE == getAppConfigJson().getC_type().getALBUM()) {
//            add_text_seekbar.setText("Add Album");
//        }
    }

    public void playPauseButtonClickListener(int c_type, ImageView list_seekbar, final CircularSeekBar seekbar, String isrc, String song_id, MP3HindiSongListPojo data, final int position) {
        data_playing = data;
        this.seekBar = seekbar;
        SaregamaConstants.PLAYING_SONG_C_TYPE = c_type;
        setNameText();

        if (!is_playing) {
            slideIn();
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, song_id);
            data_playing.setIs_playing(true);
            seekbar_remainingtime.setText("" + SaregamaConstants.PLAYING_TIME / 1000);
            if (seekBar != null)
                seekBar.setProgress(0);
            Message message = new Message();
            message.what = PlayerService.START;
            Bundle bundle = new Bundle();
            bundle.putString("isrc", isrc);
            bundle.putString("song_id", song_id);
            message.setData(bundle);
            try {
                serviceMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            articleParams.put("isrc", isrc);
            articleParams.put("song_id", song_id);
            articleParams.put("song_name", data_playing.getSong_name());
            articleParams.put("album_name", data_playing.getAlbum_name());
            FlurryAgent.logEvent("song_streaming_tracking", articleParams);
        } else {
            final Message message = new Message();
            if (SaregamaConstants.pos == position && song_id != null && getFromPrefs(SaregamaConstants.PLAYING_SONG_ID).equals(song_id)) {
                if (!is_paused) {
//                    saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
                    message.what = PlayerService.PAUSE;
                    try {
                        serviceMessenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    data_playing.setIs_playing(false);
                } else {
                    data_playing.setIs_playing(true);
//                    slideInPause();
                    message.what = PlayerService.PLAY;
                    try {
                        serviceMessenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                slideOut();
                saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, song_id);
                SaregamaConstants.IS_PLAYING = false;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        slideIn();
                    }
                }, 800);

                SaregamaConstants.REMAINING_TIME = SaregamaConstants.PLAYING_TIME / 1000;
                showRemainingTime();
                if (seekBar != null)
                    seekBar.setProgress(0);
                message.what = PlayerService.START;
                Bundle bundle = new Bundle();
                bundle.putString("isrc", isrc);
                bundle.putString("song_id", song_id);
                message.setData(bundle);

                try {
                    serviceMessenger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                data_playing.setIs_playing(true);
                articleParams.put("isrc", isrc);
                articleParams.put("song_id", song_id);
                articleParams.put("song_name", data_playing.getSong_name());
                articleParams.put("album_name", data_playing.getAlbum_name());
                FlurryAgent.logEvent("song_streaming_tracking", articleParams);
            }
        }
        saveJsonObjectSong(data_playing);
        SaregamaConstants.pos = position;
    }

    private void saveJsonObjectSong(MP3HindiSongListPojo songData) {
        SharedPreferences sharedPreferences = getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(songData);
        prefsEditor.putString("MySongObject", json);
        prefsEditor.commit();
    }

    private MP3HindiSongListPojo getJsonObjectSong() {
        SharedPreferences sharedPreferences = getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        final Gson gson = new Gson();
        String json = sharedPreferences.getString("MySongObject", "");
        MP3HindiSongListPojo obj = gson.fromJson(json, MP3HindiSongListPojo.class);

        if (obj != null) {
            return obj;
        }
        return null;
    }

    public void saveAppConfigData(AppConfigDataPojo appConfigData) {
        SharedPreferences sharedPreferences = getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(appConfigData);
        prefsEditor.putString("AppConfigObject", json);
        prefsEditor.commit();
    }

    public AppConfigDataPojo getAppConfigJson() {
        SharedPreferences sharedPreferences = getSharedPreferences(SaregamaConstants.PREF_NAME, Activity.MODE_PRIVATE);
        final Gson gson = new Gson();
        String json = sharedPreferences.getString("AppConfigObject", "");
        AppConfigDataPojo obj = gson.fromJson(json, AppConfigDataPojo.class);

        if (obj != null) {
            return obj;
        }
        return null;
    }

    public void seekBarPlayPauseClickListener(CircularSeekBar seekbar, MP3HindiSongListPojo data) {
        data_playing = getJsonObjectSong();
        this.seekBar = seekbar;
        Message message = new Message();

        if (data_playing != null) {
            if (is_playing && !is_paused) {
                data_playing.setIs_playing(false);
//                saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
                message.what = PlayerService.PAUSE;
                try {
                    serviceMessenger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } else {
                if (data_playing.getSong_id() != null)
                    saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
                else
                    saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getId());
                data_playing.setIs_playing(true);
                message.what = PlayerService.PLAY;
                try {
                    serviceMessenger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            saveJsonObjectSong(data_playing);
        }
    }

    public String base64Encoding(String str) {
        String result = "";
        str = new StringBuilder(str).reverse().toString();

        byte[] encodeValue = Base64.encode(str.getBytes(), Base64.DEFAULT);
        result = new String(encodeValue);

        String encodedString = URLEncoder.encode(result);

        return encodedString;
    }

    public String base64EncodingWithoutUrl(String str) {
        String result = "";
        str = new StringBuilder(str).reverse().toString();

        byte[] encodeValue = Base64.encode(str.getBytes(), Base64.DEFAULT);
        result = new String(encodeValue);

//        byte[] data = Base64.decode(result, Base64.DEFAULT);
//        String text = new String(data);

        return result;
    }
    // dynamically permission added

    /**
     * method that will return whether the permission is accepted. By default it is true if the user is using a device below
     * version 23
     *
     * @param permission
     * @return
     */

    public boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canMakeSmores()) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     *
     * @param permission
     * @return
     */
    public boolean shouldWeAsk(String permission) {
        return (sharedPreferences.getBoolean(permission, true));
    }

    /**
     * we will save that we have already asked the user
     *
     * @param permission
     */
    public void markAsAsked(String permission, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, false).apply();
    }

    /**
     * We may want to ask the user again at their request.. Let's clear the
     * marked as seen preference for that permission.
     *
     * @param permission
     */
    public void clearMarkAsAsked(String permission) {
        sharedPreferences.edit().putBoolean(permission, true).apply();
    }


    /**
     * This method is used to determine the permissions we do not have accepted yet and ones that we have not already
     * bugged the user about.  This comes in handle when you are asking for multiple permissions at once.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * this will return us all the permissions we have previously asked for but
     * currently do not have permission to use. This may be because they declined us
     * or later revoked our permission. This becomes useful when you want to tell the user
     * what permissions they declined and why they cannot use a feature.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findRejectedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && !shouldWeAsk(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * Just a check to see if we have marshmallows (version 23)
     *
     * @return
     */
    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * a method that will centralize the showing of a snackbar
     */
    public void makePostRequestSnack(String message, int size) {

        Toast.makeText(getApplicationContext(), String.valueOf(size) + " " + message, Toast.LENGTH_SHORT).show();

        finish();
    }

    private void getAnalytics(ArrayList<CommonUrlPOjo> urllist) {
        cd = new ConnectionDetector(BaseActivity.this);
        SaregamaConstants.BASE_URL_ANALYTICS = getFromPrefs("analyticsUrl");
        if (cd.isConnectingToInternet()) {
            String device_id = getFromPrefs(SaregamaConstants.IMEI);
            String listner_id = getFromPrefs(SaregamaConstants.ID);
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                version = pInfo.versionName;
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = "";
            for (int i = 0; i < urllist.size(); i++) {
                url += urllist.get(i).getUrl() + "||";
            }
            url = url.substring(0, url.length() - 2);
            RestClientAnalytics.get().postAnalytics(version, SaregamaConstants.MUSIC_APP, SaregamaConstants.METHOD, device_id, listner_id, url, new Callback<AnalyticsBasePojo>() {
                @Override
                public void success(AnalyticsBasePojo basePojo, Response response) {

                    if (basePojo != null) {

                    }
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(BaseActivity.this, getResources().getString(R.string.flurry_id));
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (seekbar_root != null)
            seekbar_root.setVisibility(View.GONE);
        FlurryAgent.onEndSession(BaseActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        if (serviceConnection != null) {
            unbindService(serviceConnection);
        }
    }

    public void getNotificationsSize() {
        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
            RestClient.get().getNotificationList(getFromPrefs(SaregamaConstants.SESSION_ID), getFromPrefs(SaregamaConstants.IMEI), 0, new Callback<NotificationPojo>() {
                @Override
                public void success(NotificationPojo basePojo, Response response) {

                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            ArrayList<NotificationDataPojo> arr_notification = new ArrayList<>();
                            for (int i = 0; i < basePojo.getData().size(); i++) {
                                if (basePojo.getData().get(i).is_read() == 0)
                                    arr_notification.add(basePojo.getData().get(i));
                            }
                            saveIntIntoPrefs(SaregamaConstants.NOTIFICATION_SIZE, arr_notification.size());
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    public void getNumberofDownload() {
        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
            RestClient.get().downloadAllItems(getFromPrefs(SaregamaConstants.SESSION_ID), SaregamaConstants.TAB1, "0", "" + SaregamaConstants.LIMIT, new Callback<DownloadManagerPojo>() {
                @Override
                public void success(DownloadManagerPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            if (basePojo.getData().getSongCount() != null && basePojo.getData().getAlbumCount() != null) {
                                int numberOfDownloads = basePojo.getData().getAlbumCount() + basePojo.getData().getSongCount();
                                saveIntIntoPrefs(SaregamaConstants.DOWNLOAD_SIZE, numberOfDownloads);
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    public void getDownloadedCount() {
        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
            RestClient.get().downloadAllItems(getFromPrefs(SaregamaConstants.SESSION_ID), SaregamaConstants.TAB2, "0", "" + SaregamaConstants.LIMIT, new Callback<DownloadManagerPojo>() {
                @Override
                public void success(DownloadManagerPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            if (basePojo.getData().getSongCount() != null && basePojo.getData().getAlbumCount() != null) {
                                int numberOfDownloads = basePojo.getData().getAlbumCount() + basePojo.getData().getSongCount();
                                saveIntIntoPrefs(SaregamaConstants.DOWNLOADED_SIZE, numberOfDownloads);
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    public String checkConnectedNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return "wifi";
//                Toast.makeText(_context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return "mobile";
//                Toast.makeText(_context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
            }
        }
        return "no network";
    }


    // abcd click listener
    public String showListBubbleCommon(View v, TextView rowTextView) {
        //find the index of the separator row view
        LinearLayout alpha_layout = (LinearLayout) findViewById(R.id.alpha_layout);
        rowTextView = (TextView) findViewById(R.id.row_text_view);
        int count = alpha_layout.getChildCount();
        String alphabet = "";
        for (int i = 0; i < count; i++) {
            View inner_layout = alpha_layout.getChildAt(i);
            View view = ((ViewGroup) inner_layout).getChildAt(0);

            if (view.isPressed()) {
                rowTextView.setText(view.getTag() + "");
                alphabet = (String) view.getTag();
                rowTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.alphabetic_text_size));
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(this, R.drawable.pressed));
                view.setBackgroundResource(R.drawable.pressed);
            } else {
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(this, R.drawable.normal));
                view.setBackgroundResource(R.drawable.normal);
            }
        }
        if (alphabet.equals("#"))
            alphabet = SaregamaConstants.SPECIAL;
        else if (alphabet.equalsIgnoreCase("ALL"))
            alphabet = SaregamaConstants.ALL;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, v.getTop() - 5, 0, 0);
        rowTextView.setLayoutParams(layoutParams);
        rowTextView.setPadding(0, 0, 6, 0);
        rowTextView.setVisibility(View.VISIBLE);
        return alphabet;
    }

    private void setMiniPlayerSize(LinearLayout linearLayout) {
        DisplayMetrics metrics = new DisplayMetrics();
        BaseActivity.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
//        width = Math.round(width * 2 / 3);
        FrameLayout.LayoutParams boxParams = new FrameLayout.LayoutParams(width, FrameLayout.LayoutParams.WRAP_CONTENT);
        boxParams.setMargins(0, getResources().getInteger(R.integer.seekbar_mainbg_margin), 0, 0);
        linearLayout.setLayoutParams(boxParams);
    }

    private void showRemainingTime() {
        if (SaregamaConstants.REMAINING_TIME >= 0) {
            seekbar_remainingtime.setText("" + SaregamaConstants.REMAINING_TIME);
        }
    }

    private void initCommonComponent() {
        seekBar = (CircularSeekBar) findViewById(R.id.seek_bar);
        seekbar_root = (FrameLayout) findViewById(R.id.seekbar_root);
        seek_barLL = (LinearLayout) findViewById(R.id.seek_barLL);
        seekbar_addtocart = (CheckBox) findViewById(R.id.seekbar_addtocart);
//        add_text_seekbar = (TextView) findViewById(add_text_seekbar);
        seekbar_albumName = (TextView) findViewById(R.id.seekbar_albumName);
        seekbar_songName = (TextView) findViewById(R.id.seekbar_songName);
        seekbar_remainingtime = (TextView) findViewById(R.id.seekbar_remainingtime);
        TextView preview_size = (TextView) findViewById(R.id.preview_size);
        seekbar_close = (LinearLayout) findViewById(R.id.seekbar_close);
        miniplayer_bg_layout = (LinearLayout) findViewById(R.id.miniplayer_bg_layout);
        if (getTelephonyManager()) {
            phoneStateListener = new PhoneStateListener() {
                @Override
                public void onCallStateChanged(int state, String incomingNumber) {
                    if (state == TelephonyManager.CALL_STATE_RINGING) {
                        //Incoming call: Pause music
                        if (SaregamaConstants.IS_PLAYING && !SaregamaConstants.IS_STOPPED) {
                            if (SaregamaConstants.IS_PAUSED)
                                SaregamaConstants.IS_PLAYING_WHEN_CALL_RECEIVED = false;
                            else
                                SaregamaConstants.IS_PLAYING_WHEN_CALL_RECEIVED = true;
                            Message message = new Message();
                            message.what = PlayerService.PAUSE;
                            try {
                                serviceMessenger.send(message);
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                            data_playing.setIs_playing(false);
                        }
                    } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                        //Not in call: Play music
                        if (SaregamaConstants.IS_PLAYING && SaregamaConstants.IS_PAUSED && !SaregamaConstants.IS_STOPPED) {
                            if (SaregamaConstants.IS_PLAYING_WHEN_CALL_RECEIVED) {
                                Message message = new Message();
                                message.what = PlayerService.PLAY;
                                try {
                                    serviceMessenger.send(message);
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                                data_playing.setIs_playing(true);
                            }
                        }

                    } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                        if (SaregamaConstants.IS_PLAYING && !SaregamaConstants.IS_STOPPED) {
                            Message message = new Message();
                            message.what = PlayerService.PAUSE;
                            try {
                                serviceMessenger.send(message);
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                            data_playing.setIs_playing(false);
                        }
                        //A call is dialing, active or on hold
                    }
                    super.onCallStateChanged(state, incomingNumber);
                }
            };
        }
        if (getAppConfigJson() != null && preview_size != null)
            preview_size.setText(" " + getAppConfigJson().getSong_preview_text() + " ");
        if (miniplayer_bg_layout != null) {
            setMiniPlayerSize(miniplayer_bg_layout);
        }
    }

    private void slideOut() {
        seekbar_root.setVisibility(View.GONE);
//        TranslateAnimation animate = new TranslateAnimation(0, seekbar_root.getWidth(), 0, 0);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, 500);
        animate.setDuration(800);
        animate.setFillAfter(false);
        seekbar_root.startAnimation(animate);
    }

    private void slideIn() {
        seekbar_root.setVisibility(View.VISIBLE);
//        TranslateAnimation animate = new TranslateAnimation(seekbar_root.getWidth(), 0, 0, 0);
        TranslateAnimation animate = new TranslateAnimation(0, 0, 500, 0);
        animate.setDuration(800);
        animate.setFillAfter(false);
        seekbar_root.startAnimation(animate);
    }

    private String addSpaceAfterPrice(String price) {
        if (price.length() == 1)
            price = "" + price;
        String finalPrice = price;

        for (int i = price.length(); i <= 4; i++)
            finalPrice += " ";
        return finalPrice;
    }

    private void showQuickAction(String price, String hdPrice) {

    }

    public QuickAction setupQuickAction(final String songId, final int c_type) {
        if (getAppConfigJson().getCurrency().equals("Rs.")) {
            nextItem = new ActionItem(SaregamaConstants.ID_DOWN, "MP3   " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(getAppConfigJson().getMp3_price()), null);
            prevItem = new ActionItem(SaregamaConstants.ID_UP, "HD    " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(getAppConfigJson().getHp_price()), null);
        } else {
            nextItem = new ActionItem(SaregamaConstants.ID_DOWN, "MP3 " + getAppConfigJson().getCurrency() + " " + addSpaceAfterPrice(getAppConfigJson().getMp3_price()), null);
            prevItem = new ActionItem(SaregamaConstants.ID_UP, "HD  " + getAppConfigJson().getCurrency() + " " + addSpaceAfterPrice(getAppConfigJson().getHp_price()), null);
        }

        prevItem.setSticky(true);
        nextItem.setSticky(true);

        final QuickAction quickAction = new QuickAction(this, QuickAction.VERTICAL);

        //add action items into QuickAction
        quickAction.addActionItem(nextItem);
        quickAction.addActionItem(prevItem);

        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = quickAction.getActionItem(pos);

                //here we can filter which action item was clicked with pos or actionId parameter
//                Toast.makeText(getApplicationContext(), actionItem.getTitle() + " selected", Toast.LENGTH_SHORT).show();

                if (pos == 0) {
                    AddToCartWithoutLogin(c_type + "", songId);

                } else if (pos == 1) {
                    AddToCartWithoutLogin(getResources().getString(R.string.hd_c_type), songId);
                }

                quickAction.dismiss();
            }
        });

        quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });

        return quickAction;
    }

    public QuickAction setupQuickActionSeekBar(final String songId, final int c_type) {
        if (getAppConfigJson().getCurrency().equals("Rs.")) {
            nextItem = new ActionItem(SaregamaConstants.ID_DOWN, "MP3   " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(getAppConfigJson().getMp3_price()) + "    ", null);
            prevItem = new ActionItem(SaregamaConstants.ID_UP, "HD    " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(getAppConfigJson().getHp_price()) + "    ", null);
        } else {
            nextItem = new ActionItem(SaregamaConstants.ID_DOWN, "MP3 " + getAppConfigJson().getCurrency() + " " + getAppConfigJson().getMp3_price(), null);
            prevItem = new ActionItem(SaregamaConstants.ID_UP, "HD  " + getAppConfigJson().getCurrency() + " " + getAppConfigJson().getHp_price(), null);
        }

        prevItem.setSticky(true);
        nextItem.setSticky(true);

        final QuickAction quickAction = new QuickAction(this, QuickAction.VERTICAL);

        //add action items into QuickAction
        quickAction.addActionItem(nextItem);
        quickAction.addActionItem(prevItem);

        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = quickAction.getActionItem(pos);

                //here we can filter which action item was clicked with pos or actionId parameter
//                Toast.makeText(getApplicationContext(), actionItem.getTitle() + " selected", Toast.LENGTH_SHORT).show();

                if (pos == 0) {
                    AddToCartWithoutLogin(c_type + "", songId);

                } else if (pos == 1) {
                    AddToCartWithoutLogin(getResources().getString(R.string.hd_c_type), songId);
                }

                quickAction.dismiss();
            }
        });

        quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });

        return quickAction;
    }

    public QuickAction setupQuickActionAlbumLeft(final String songId, final int c_type, String currency, String price, String price_hd) {
        if (currency.equals("Rs.")) {
            nextItem_album = new ActionItem(SaregamaConstants.ID_DOWN, "MP3  " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(price), null);
            prevItem_album = new ActionItem(SaregamaConstants.ID_UP, "HD   " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(price_hd), null);

        } else {
            nextItem_album = new ActionItem(SaregamaConstants.ID_DOWN, "MP3 " + currency + " " + price, null);
            prevItem_album = new ActionItem(SaregamaConstants.ID_UP, "HD  " + currency + " " + price_hd, null);
        }
        nextItem_album.setSticky(true);
        prevItem_album.setSticky(true);

        final QuickAction QuickAction = new QuickAction(this, com.saregama.musicstore.util.QuickAction.VERTICAL);

        //add action items into QuickAction
        QuickAction.addActionItem(nextItem_album);
        QuickAction.addActionItem(prevItem_album);

        QuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = QuickAction.getActionItem(pos);

                //here we can filter which action item was clicked with pos or actionId parameter
//                Toast.makeText(getApplicationContext(), actionItem.getTitle() + " selected", Toast.LENGTH_SHORT).show();
                if (pos == 0) {
                    AddToCartWithoutLogin(c_type + "", songId);

                } else if (pos == 1) {
                    AddToCartWithoutLogin(getAppConfigJson().getC_type().getALBUMHD() + "", songId);
                }

                QuickAction.dismiss();
            }
        });

        QuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });

        return QuickAction;
    }

    // no need to change
    public QuickActionAlbumTop setupQuickActionAlbum(final String songId, final int c_type, String currency, String price, String price_hd) {
        if (currency.equals("Rs.")) {
            nextItem_album = new ActionItem(SaregamaConstants.ID_DOWN, "MP3   " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(price), null);
            prevItem_album = new ActionItem(SaregamaConstants.ID_UP, "HD    " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(price_hd), null);
        } else {
            nextItem_album = new ActionItem(SaregamaConstants.ID_DOWN, "MP3   " + currency + " " + price, null);
            prevItem_album = new ActionItem(SaregamaConstants.ID_UP, "HD    " + currency + " " + price_hd, null);
        }

        nextItem_album.setSticky(true);
        prevItem_album.setSticky(true);

        final QuickActionAlbumTop quickActionAlbumTop = new QuickActionAlbumTop(this, QuickActionAlbumTop.VERTICAL);

        //add action items into QuickAction
        quickActionAlbumTop.addActionItem(nextItem_album);
        quickActionAlbumTop.addActionItem(prevItem_album);

        quickActionAlbumTop.setOnActionItemClickListener(new QuickActionAlbumTop.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickActionAlbumTop source, int pos, int actionId) {
                ActionItem actionItem = quickActionAlbumTop.getActionItem(pos);

                //here we can filter which action item was clicked with pos or actionId parameter
//                Toast.makeText(getApplicationContext(), actionItem.getTitle() + " selected", Toast.LENGTH_SHORT).show();

                if (pos == 0) {
                    AddToCartWithoutLogin(c_type + "", songId);

                } else if (pos == 1) {
                    AddToCartWithoutLogin(getAppConfigJson().getC_type().getALBUMHD() + "", songId);
                }

                quickActionAlbumTop.dismiss();
            }
        });

        quickActionAlbumTop.setOnDismissListener(new QuickActionAlbumTop.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });

        return quickActionAlbumTop;
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

    public QuickAction setupQuickActionAlbumGlobal(final String songId, final int c_type, String currency, String price, String price_hd) {
        if (currency.equals("Rs.")) {
            nextItem_album = new ActionItem(SaregamaConstants.ID_DOWN, "MP3  " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(price) + "    ", null);
            prevItem_album = new ActionItem(SaregamaConstants.ID_UP, "HD     " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(price_hd) + "    ", null);

        } else {
            nextItem_album = new ActionItem(SaregamaConstants.ID_DOWN, "MP3   " + currency + " " + price, null);
            prevItem_album = new ActionItem(SaregamaConstants.ID_UP, "HD    " + currency + " " + price_hd, null);
        }
        nextItem_album.setSticky(true);
        prevItem_album.setSticky(true);

        final QuickAction QuickAction = new QuickAction(this, com.saregama.musicstore.util.QuickAction.VERTICAL);

        //add action items into QuickAction
        QuickAction.addActionItem(nextItem_album);
        QuickAction.addActionItem(prevItem_album);

        QuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = QuickAction.getActionItem(pos);

                //here we can filter which action item was clicked with pos or actionId parameter
//                Toast.makeText(getApplicationContext(), actionItem.getTitle() + " selected", Toast.LENGTH_SHORT).show();

                if (pos == 0) {
                    AddToCartWithoutLogin(c_type + "", songId);

                } else if (pos == 1) {
                    AddToCartWithoutLogin(getAppConfigJson().getC_type().getALBUMHD() + "", songId);
                }

                QuickAction.dismiss();
            }
        });

        QuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });

        return QuickAction;
    }

    public static void launchMarket(Activity activity) {
        Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            activity.startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {

            Toast.makeText(activity, SaregamaConstants.APP_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
        }
    }

    public String convertAlphabet(String alphabet) {
        if (alphabet.equals("#"))
            alphabet = SaregamaConstants.SPECIAL;
        else if (alphabet.equalsIgnoreCase("ALL"))
            alphabet = SaregamaConstants.ALL;

        return alphabet;
    }

    public void setBubblePosition(View view, TextView rowTextView, int position) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, view.getTop() - getResources().getInteger(R.integer.row_text_margin_top), 0, 0);
        rowTextView.setLayoutParams(layoutParams);

        rowTextView.setVisibility(View.VISIBLE);
        rowTextView.setText(SaregamaConstants.items[position]);
    }

    public void setBubblePositionforArtiste(View view, TextView rowTextView, int position) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, view.getTop() - getResources().getInteger(R.integer.row_text_margin_top), 0, 0);
        rowTextView.setLayoutParams(layoutParams);

        rowTextView.setVisibility(View.VISIBLE);
        rowTextView.setText(SaregamaConstants.artist_arr[position]);
    }
//    private void slideOutPause() {
//        TranslateAnimation animate = new TranslateAnimation(0, seekbar_root.getWidth() - ((int)getResources().getDimension(R.dimen.seekbar_height)-28), 0, 0);
//        animate.setDuration(800);
//        animate.setFillAfter(true);
//        seekbar_root.startAnimation(animate);
//        seekbar_addtocart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                slideInPause();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        seek_barLL.performClick();
//                    }
//                }, 800);
//            }
//        });
//        seekbar_root.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                slideInPause();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        seek_barLL.performClick();
//                    }
//                }, 800);
//            }
//        });
//    }
//
//    private void slideInPause() {
//        TranslateAnimation animate = new TranslateAnimation(seekbar_root.getWidth() - getResources().getDimension(R.dimen.seekbar_height), 0, 0, 0);
//        animate.setDuration(800);
//        animate.setFillAfter(false);
//        seekbar_root.startAnimation(animate);
//        seekbarAddToCartClick();
//        seekbarLLClick();
//        seekbar_root.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//    }

    public QuickAction setupQuickActionAlbumLeftRecommendation(final String songId, String currency, String price, String price_hd) {
        if (currency.equals("Rs.")) {
            nextItem_album = new ActionItem(SaregamaConstants.ID_DOWN, "MP3  " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(price), null);
            prevItem_album = new ActionItem(SaregamaConstants.ID_UP, "HD   " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(price_hd), null);
//              nextItem_album = new ActionItem(SaregamaConstants.ID_DOWN, "MP3   " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice("21"), null);
//              prevItem_album = new ActionItem(SaregamaConstants.ID_UP, "HD    " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice("35"), null);
        } else {
            nextItem_album = new ActionItem(SaregamaConstants.ID_DOWN, "MP3 " + currency + " " + price, null);
            prevItem_album = new ActionItem(SaregamaConstants.ID_UP, "HD  " + currency + " " + price_hd, null);
        }
        nextItem_album.setSticky(true);
        prevItem_album.setSticky(true);

        final QuickAction QuickAction = new QuickAction(this, com.saregama.musicstore.util.QuickAction.VERTICAL);

        //add action items into QuickAction
        QuickAction.addActionItem(nextItem_album);
        QuickAction.addActionItem(prevItem_album);

        QuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                //here we can filter which action item was clicked with pos or actionId parameter
//                Toast.makeText(getApplicationContext(), actionItem.getTitle() + " selected", Toast.LENGTH_SHORT).show();

                if (pos == 0) {
                    AddToCartRecommendation(getAppConfigJson().getC_type().getALBUM() + "", songId);
                } else if (pos == 1) {
                    AddToCartRecommendation(getAppConfigJson().getC_type().getALBUMHD() + "", songId);
                }

                QuickAction.dismiss();
            }
        });

        QuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });

        return QuickAction;
    }

    public QuickAction setupQuickActionRecommendation(final String songId) {
        if (getAppConfigJson().getCurrency().equals("Rs.")) {
            nextItem = new ActionItem(SaregamaConstants.ID_DOWN, "MP3   " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(getAppConfigJson().getMp3_price()), null);
            prevItem = new ActionItem(SaregamaConstants.ID_UP, "HD    " + getResources().getString(R.string.Rs) + " " + addSpaceAfterPrice(getAppConfigJson().getHp_price()), null);
        } else {
            nextItem = new ActionItem(SaregamaConstants.ID_DOWN, "MP3 " + getAppConfigJson().getCurrency() + " " + addSpaceAfterPrice(getAppConfigJson().getMp3_price()), null);
            prevItem = new ActionItem(SaregamaConstants.ID_UP, "HD  " + getAppConfigJson().getCurrency() + " " + addSpaceAfterPrice(getAppConfigJson().getHp_price()), null);
        }

        prevItem.setSticky(true);
        nextItem.setSticky(true);

        final QuickAction quickAction = new QuickAction(this, QuickAction.VERTICAL);

        //add action items into QuickAction
        quickAction.addActionItem(nextItem);
        quickAction.addActionItem(prevItem);

        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                //here we can filter which action item was clicked with pos or actionId parameter
//                Toast.makeText(getApplicationContext(), actionItem.getTitle() + " selected", Toast.LENGTH_SHORT).show();
                if (pos == 0) {
                    AddToCartRecommendation(getAppConfigJson().getC_type().getSONG() + "", songId);
                } else if (pos == 1) {
                    AddToCartRecommendation(getResources().getString(R.string.hd_c_type), songId);
                }

                quickAction.dismiss();
            }
        });

        quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });

        return quickAction;
    }

    public void setAlbumImageClick(String url) {
        if (url != null) {
            final Dialog showAlbumImage = new Dialog(BaseActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
            showAlbumImage.setContentView(R.layout.custom_dialog);

            LinearLayout custom_dialog_root = (LinearLayout) showAlbumImage.findViewById(R.id.custom_dialog_root);
            LinearLayout parent_layout = (LinearLayout) showAlbumImage.findViewById(R.id.parent_layout);
            custom_dialog_root.setVisibility(View.GONE);

            parent_layout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    showAlbumImage.dismiss();
                    return false;
                }
            });

            ImageView album_art_image = (ImageView) showAlbumImage.findViewById(R.id.album_art_image);
            album_art_image.setVisibility(View.VISIBLE);

            album_art_image.setOnClickListener(null);

            setImageInLayout(BaseActivity.this, (int) getResources().getDimension(R.dimen.album_detail_big_image), (int) getResources().getDimension(R.dimen.album_detail_big_image), url, album_art_image);
            album_art_image.setScaleType(ImageView.ScaleType.FIT_XY);

            showAlbumImage.setCanceledOnTouchOutside(true);

            showAlbumImage.show();
        }

    }

    public void sendRegistrationId(String gcm_id, String deviceId) {
        if (cd.isConnectingToInternet()) {

            RestClient.get().sendRegistrationId(getFromPrefs(SaregamaConstants.SESSION_ID), deviceId, "android", gcm_id, new Callback<BasePojo>() {
                @Override
                public void success(BasePojo basePojo, Response response) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    public boolean getTelephonyManager() {
        PackageManager pm = this.getPackageManager();
        boolean hasTelephony = pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
        return hasTelephony;
    }


    private void initiateDropDownPopupWindow() {
        try {
            pwindo.showAsDropDown(header_list, getResources().getInteger(R.integer.popup_category_window_x_offset), getResources().getInteger(R.integer.popup_category_window_y_offset));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set Filter by language
    private void fetchCategoryDropdownList() {

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.category_list,
                (ViewGroup) this.findViewById(R.id.popup_element));
        layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        if (getCategoryJsonObject().size() >= 5)
            pwindo = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT,
                    getResources().getInteger(R.integer.popup_category_window_height), true);
        else
            pwindo = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, true);

        pwindo.setWidth(getResources().getInteger(R.integer.popup_category_window_width));
        if (mBackground == null)
            pwindo.setBackgroundDrawable(new BitmapDrawable());
        else
            pwindo.setBackgroundDrawable(mBackground);
        pwindo.setOutsideTouchable(true);
        pwindo.setFocusable(true);
        pwindo.setTouchable(true);
        language_filter_list = (ListView) layout.findViewById(R.id.language_list_filter);
        language_filter_list.setAdapter(new FilterCategoryListAdapter(this, getCategoryJsonObject(), header_list, pwindo));
    }

}