package com.saregama.musicstore.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.SearchAlbumAdapter;
import com.saregama.musicstore.adapter.SearchArtisteAdapter;
import com.saregama.musicstore.adapter.SearchSongsAdapter;
import com.saregama.musicstore.pojo.ClassHindArtistListPojo;
import com.saregama.musicstore.pojo.HindiFilmsSearchPojo;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.io.StringReader;
import java.util.ArrayList;

public class HindiFilmsSearchActivity extends BaseActivity implements View.OnClickListener {

    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private ListView songs_list__layout_global_search, album_list__layout_global_search, artist_list__layout_global_search, geetmala_list__layout_global_search;
    private SearchArtisteAdapter globalSearchArtistAdapter_ob;
    private SearchSongsAdapter globalSearchSongsAdapter_ob;
    private SearchAlbumAdapter globalSearchAlbumAdapter_ob;
    private SearchAlbumAdapter globalSearchGeetmalaAdapter_ob;
    private ArrayList<MP3HindiSongListPojo> songs_arrlistlist_layout_global_search;
    private ArrayList<MP3HindiAlbumListPojo> album_arrlistlist_layout_global_search;
    private ArrayList<ClassHindArtistListPojo> artist_arrlistlist_layout_global_search;
    private ArrayList<MP3HindiAlbumListPojo> geetmala_arrlistlist_layout_global_search;
    private EditText global_search_main;
    private LinearLayout songs_layout_global_search, album_layout_global_search, artist_layout_global_search, geetmala_layout_global_search;
    private LinearLayout global_search_layout;
    private ImageView logo;
    private HindiFilmsSearchActivity ctx = this;
    private String search_keyword = "";
    private String str = "";
    private TextView header_text_first_global, header_text_second_global, header_text_third_global, header_text_fourth_global;
    private TextView songs_viewall, album_viewall, artistes_viewall, geetmala_viewall;
    private String search_from;
    private int c_type;
    private Intent intent;
    private String search_url;
    private ImageView global_search_cross;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_search);

        init();
        search_from = getIntent().getStringExtra("search_from");
        c_type = getIntent().getIntExtra("c_type", 1);

        //preparing Toolbar
        setDrawerAndToolbar("");
        global_search_main = (EditText) findViewById(R.id.global_search_main);
        global_search_main.setVisibility(View.VISIBLE);
        TextView header = (TextView) findViewById(R.id.header);
        header.setVisibility(View.GONE);

        SaregamaConstants.ACTIVITIES.add(ctx);

        if (search_from.equals("hindi")) {
            intent = new Intent(ctx, Mp3HindiLandingActivity.class);
            global_search_main.setHint("Search Songs, Albums, Artistes, Geetmala");
        } else if (search_from.equals("regional")) {
            intent = new Intent(ctx, Mp3RegionalActivity.class);
            global_search_main.setHint("Search Songs, Albums, Artistes");
        } else if (search_from.equals("Rabindra Sangeet")) {
            intent = new Intent(ctx, RabindraSangeetActivity.class);
            global_search_main.setHint("Search Songs, Artistes");
        } else if (search_from.equals("Nazrul Geet")) {
            intent = new Intent(ctx, RabindraSangeetActivity.class);
            global_search_main.setHint("Search Songs, Artistes");
        } else if (search_from.equals("gazal")) {
            intent = new Intent(ctx, GajalSufiActivity.class);
            global_search_main.setHint("Search Songs, Albums, Artistes");
        }

        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
//        cart_layout.setVisibility(View.GONE);

        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        notification_layout.setVisibility(View.GONE);

        // view all list
        songs_viewall = (TextView) findViewById(R.id.songs_view_all);
        album_viewall = (TextView) findViewById(R.id.album_view_all);
        artistes_viewall = (TextView) findViewById(R.id.artistes_view_all);
        geetmala_viewall = (TextView) findViewById(R.id.geetmala_view_all);

        songs_viewall.setPaintFlags(songs_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        album_viewall.setPaintFlags(album_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        artistes_viewall.setPaintFlags(artistes_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        geetmala_viewall.setPaintFlags(geetmala_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        header_text_first_global = (TextView) findViewById(R.id.header_text_first_global);
        header_text_second_global = (TextView) findViewById(R.id.header_text_second_global);
        header_text_third_global = (TextView) findViewById(R.id.header_text_third_global);
        header_text_fourth_global = (TextView) findViewById(R.id.header_text_fourth_global);

        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);
        global_search.setVisibility(View.GONE);
        global_search_cross = (ImageView) findViewById(R.id.global_search_cross);
        global_search_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global_search_main.setText("");
                if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
                showSoftKeyboard();
            }
        });

        global_search_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
            }
        });

        global_search_main.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (global_search_main.getText().toString().trim().length() < 1) {

                    global_search_cross.setVisibility(View.GONE);
                }
                else
                {
                    global_search_cross.setVisibility(View.VISIBLE);
                }
                if (global_search_main.getText().toString().trim().length() > 2) {
                    search_keyword = global_search_main.getText().toString().trim();
                    search_keyword = search_keyword.replaceAll(" ", "%20");
                    new GetGlobalSearchList().execute();
                } else {
                    songs_arrlistlist_layout_global_search = new ArrayList<>();
                    album_arrlistlist_layout_global_search = new ArrayList<>();
                    artist_arrlistlist_layout_global_search = new ArrayList<>();
                    geetmala_arrlistlist_layout_global_search = new ArrayList<>();
                    globalSearchSongsAdapter_ob = new SearchSongsAdapter(ctx, songs_arrlistlist_layout_global_search, search_keyword, search_from);
                    globalSearchAlbumAdapter_ob = new SearchAlbumAdapter(ctx, album_arrlistlist_layout_global_search, search_keyword, search_from);
                    globalSearchGeetmalaAdapter_ob = new SearchAlbumAdapter(ctx, geetmala_arrlistlist_layout_global_search, search_keyword, search_from);
                    globalSearchArtistAdapter_ob = new SearchArtisteAdapter(ctx, artist_arrlistlist_layout_global_search, search_keyword, search_from);
                    songs_list__layout_global_search.setAdapter(globalSearchSongsAdapter_ob);
                    album_list__layout_global_search.setAdapter(globalSearchAlbumAdapter_ob);
                    artist_list__layout_global_search.setAdapter(globalSearchArtistAdapter_ob);
                    geetmala_list__layout_global_search.setAdapter(globalSearchGeetmalaAdapter_ob);
                    songs_layout_global_search.setVisibility(View.GONE);
                    album_layout_global_search.setVisibility(View.GONE);
                    artist_layout_global_search.setVisibility(View.GONE);
                    geetmala_layout_global_search.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        global_search_main.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    hideSoftKeyboard();
                }
                return false;
            }
        });
    }

    private class GetGlobalSearchList extends AsyncTask<Void, Void, Void> {
        private HindiFilmsSearchPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            if (search_from.equals("hindi")) {
                search_url = SaregamaConstants.BASE_URL + "category_search?type=search&query=" + search_keyword + "&mode=hindi_films";
            } else if (search_from.equals("regional")) {
                search_url = SaregamaConstants.BASE_URL + "category_search?type=search&query=" + search_keyword + "&mode=regional_films&lang=" + getIntent().getIntExtra("lang_id", 0);
            } else if (search_from.equals("Rabindra Sangeet")) {
                search_url = SaregamaConstants.BASE_URL + "category_search?type=search&query=" + search_keyword + "&mode=rabindra_sangeet";
            } else if (search_from.equals("Nazrul Geet")) {
                search_url = SaregamaConstants.BASE_URL + "category_search?type=search&query=" + search_keyword + "&mode=nazrul_geet";
            } else if (search_from.equals("gazal")) {
                search_url = SaregamaConstants.BASE_URL + "category_search?type=search&query=" + search_keyword + "&mode=ghazal_sufi";
            }
            str = getAsyncTaskDataGlobal(search_url);
            JsonReader reader = new JsonReader(new StringReader(str));
            reader.setLenient(true);
            basePojo = gsonObj.fromJson(reader, HindiFilmsSearchPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    songs_arrlistlist_layout_global_search = new ArrayList<>();
                    album_arrlistlist_layout_global_search = new ArrayList<>();
                    artist_arrlistlist_layout_global_search = new ArrayList<>();
                    geetmala_arrlistlist_layout_global_search = new ArrayList<>();

                    if (basePojo.getData().getSong() != null && basePojo.getData().getSong().getList() != null && basePojo.getData().getSong().getList().size() > 0) {
                        header_text_first_global.setText(getResources().getString(R.string.songs));
                        songs_layout_global_search.setVisibility(View.VISIBLE);
                        setSongsAdapter(basePojo, songs_list__layout_global_search, songs_viewall);
                    } else {
                        songs_layout_global_search.setVisibility(View.GONE);
                    }

                    if (basePojo.getData().getAlbum() != null && basePojo.getData().getAlbum().getList() != null && basePojo.getData().getAlbum().getList().size() > 0) {
                        header_text_second_global.setText(getResources().getString(R.string.album_text));
                        album_layout_global_search.setVisibility(View.VISIBLE);
                        setAlbumAdapter(basePojo, album_list__layout_global_search, album_viewall);
                    } else {
                        album_layout_global_search.setVisibility(View.GONE);
                    }

                    if (basePojo.getData().getArtist() != null && basePojo.getData().getArtist().getList() != null && basePojo.getData().getArtist().getList().size() > 0) {
                        artist_layout_global_search.setVisibility(View.VISIBLE);
                        header_text_third_global.setText(getResources().getString(R.string.artist));
                        setArtistesAdapter(basePojo, artist_list__layout_global_search, artistes_viewall);
                    } else {
                        artist_layout_global_search.setVisibility(View.GONE);
                    }

                    if (basePojo.getData().getGeetmala() != null && basePojo.getData().getGeetmala().getList() != null && basePojo.getData().getGeetmala().getList().size() > 0) {
                        geetmala_layout_global_search.setVisibility(View.VISIBLE);
                        header_text_fourth_global.setText(getResources().getString(R.string.geetmala));
                        setGeetmalaAdapter(basePojo, geetmala_list__layout_global_search, geetmala_viewall);
                    } else {
                        geetmala_layout_global_search.setVisibility(View.GONE);
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }
    }

    private void navigateToTabListActivity(String fragment_pos) {
        intent.putExtra("search_text", global_search_main.getText().toString().trim().replace(" ", "%20"));
        intent.putExtra("languageid", getIntent().getIntExtra("lang_id", 0));
        intent.putExtra("fragment_pos", fragment_pos);
        intent.putExtra("from", search_from);
        intent.putExtra("c_type", c_type);
        intent.putExtra("languagename", getIntent().getStringExtra("languagename"));
        intent.putExtra("coming_from", "search");
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private void setAlbumAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        album_arrlistlist_layout_global_search = basePojo.getData().getAlbum().getList();
        globalSearchAlbumAdapter_ob = new SearchAlbumAdapter(ctx, album_arrlistlist_layout_global_search, search_keyword, search_from);
        if (album_arrlistlist_layout_global_search != null && album_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchAlbumAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchAlbumAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("album");
            }
        });
    }

    private void setGeetmalaAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        geetmala_arrlistlist_layout_global_search = basePojo.getData().getGeetmala().getList();
        globalSearchGeetmalaAdapter_ob = new SearchAlbumAdapter(ctx, geetmala_arrlistlist_layout_global_search, search_keyword, "geetmala");
        if (geetmala_arrlistlist_layout_global_search != null && geetmala_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchGeetmalaAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchGeetmalaAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("geetmala");
            }
        });
    }

    private void setArtistesAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        artist_arrlistlist_layout_global_search = basePojo.getData().getArtist().getList();
        globalSearchArtistAdapter_ob = new SearchArtisteAdapter(ctx, artist_arrlistlist_layout_global_search, search_keyword, search_from);
        if (artist_arrlistlist_layout_global_search != null && artist_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchArtistAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchArtistAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("artiste");
            }
        });
    }

    private void setSongsAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        songs_arrlistlist_layout_global_search = basePojo.getData().getSong().getList();
        globalSearchSongsAdapter_ob = new SearchSongsAdapter(ctx, songs_arrlistlist_layout_global_search, search_keyword, search_from);
        if (songs_arrlistlist_layout_global_search != null && songs_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchSongsAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchSongsAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("songs");
            }
        });
    }

    private void init() {
        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);
        songs_list__layout_global_search = (ListView) findViewById(R.id.songs_list__layout_global_search);
        album_list__layout_global_search = (ListView) findViewById(R.id.album_list__layout_global_search);
        artist_list__layout_global_search = (ListView) findViewById(R.id.artist_list__layout_global_search);
        geetmala_list__layout_global_search = (ListView) findViewById(R.id.geetmala_list__layout_global_search);

        songs_arrlistlist_layout_global_search = new ArrayList<>();
        album_arrlistlist_layout_global_search = new ArrayList<>();
        artist_arrlistlist_layout_global_search = new ArrayList<>();
        geetmala_arrlistlist_layout_global_search = new ArrayList<>();

        global_search_layout = (LinearLayout) findViewById(R.id.global_search_layout);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width*3/4+getResources().getInteger(R.integer.appbar_search_add), ViewGroup.LayoutParams.WRAP_CONTENT);
        global_search_layout.setLayoutParams(params);
        global_search_layout.setVisibility(View.VISIBLE);
        logo = (ImageView) findViewById(R.id.logo);
        logo.setVisibility(View.GONE);

        songs_layout_global_search = (LinearLayout) findViewById(R.id.songs_layout_global_search);
        album_layout_global_search = (LinearLayout) findViewById(R.id.album_layout_global_search);
        artist_layout_global_search = (LinearLayout) findViewById(R.id.artist_layout_global_search);
        geetmala_layout_global_search = (LinearLayout) findViewById(R.id.geetmala_layout_global_search);
        songs_layout_global_search.setVisibility(View.GONE);
        album_layout_global_search.setVisibility(View.GONE);
        artist_layout_global_search.setVisibility(View.GONE);
        geetmala_layout_global_search.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.albumLL:
                hideSoftKeyboard();
                Intent intent = null;
                if (search_from.equals("hindi")) {
                    for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
                        if (SaregamaConstants.ACTIVITIES.get(i) != null && SaregamaConstants.ACTIVITIES.get(i).toString().contains(getResources().getString(R.string.package_name)+".Mp3HindiAlbumDetail")) {
                            SaregamaConstants.ACTIVITIES.get(i).finish();
                            SaregamaConstants.ACTIVITIES.remove(i);
                            break;
                        }
                    }
                    intent = new Intent(ctx, Mp3HindiAlbumDetail.class);
                } else if (search_from.equals("regional") || search_from.equals("gazal")) {
                    for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
                        if (SaregamaConstants.ACTIVITIES.get(i) != null && SaregamaConstants.ACTIVITIES.get(i).toString().contains(getResources().getString(R.string.package_name)+".RegionalMp3AlbumDetail")) {
                            SaregamaConstants.ACTIVITIES.get(i).finish();
                            SaregamaConstants.ACTIVITIES.remove(i);
                            break;
                        }
                    }
                    intent = new Intent(ctx, RegionalMp3AlbumDetail.class);
                }
                MP3HindiAlbumListPojo data = (MP3HindiAlbumListPojo) v.getTag(R.string.data);
                intent.putExtra("album_id", data.getAlbum_id());
                if (search_from.equals("gazal"))
                    intent.putExtra("from", "gajal_sufi");
                else if (search_from.equals("regional"))
                    intent.putExtra("from", "regional");
                if(v.getTag(R.string.from) != null && v.getTag(R.string.from).toString().equals("geetmala"))
                    intent.putExtra("from", "geetmala");
                intent.putExtra("c_type", c_type);
                intent.putExtra("languagename", getIntent().getStringExtra("languagename"));
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.classhindustani_artist_albumLL:
                hideSoftKeyboard();
                Intent intent_artiste = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
                ClassHindArtistListPojo dataa = (ClassHindArtistListPojo) v.getTag(R.string.data);
                intent_artiste.putExtra("artist_id", dataa.getId());
                if (search_from.equals("regional"))
                    intent_artiste.putExtra("from", "regional");
                else if (search_from.equals("Rabindra Sangeet") || search_from.equals("Nazrul Geet"))
                    intent_artiste.putExtra("from", search_from);
                else if (search_from.equals("gazal"))
                    intent_artiste.putExtra("from", "gajal_sufi");
                intent_artiste.putExtra("header", search_from);
                intent_artiste.putExtra("c_type", c_type);
                intent_artiste.putExtra("languagename", getIntent().getStringExtra("languagename"));
                startActivity(intent_artiste);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            default:
                break;
        }
    }
}
