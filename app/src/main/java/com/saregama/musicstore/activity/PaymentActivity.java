package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

public class PaymentActivity extends BaseActivity {

    private WebView webview;
    private PaymentActivity ctx = this;
    private String base64EncodResult;
    private final static int PAYMENT_STATUS = 100;
    private ProgressDialog progressDialog;
    private String coupon_type;
    private float grand_total;
    private String loaded_url="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        String str = "SRGM_AUTH=" + getFromPrefs(SaregamaConstants.SESSION_ID) + "|order_id=" + getIntent().getIntExtra("order_id", 0) + "|coupon_status=" + getIntent().getIntExtra("coupon_status", 0) + "|coupon_code=" + getIntent().getStringExtra("coupon_code");
        base64EncodResult = base64EncodingWithoutUrl(str);

        setDrawerAndToolbar("Payment");

        coupon_type = getIntent().getStringExtra("coupon_type");
        grand_total = getIntent().getFloatExtra("grand_total", 0);

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);
        LinearLayout drawerButton = (LinearLayout) findViewById(R.id.drawerButton);
        cart_layout.setVisibility(View.GONE);
        global_search.setVisibility(View.GONE);
        drawerButton.setVisibility(View.GONE);
        notification_layout.setVisibility(View.GONE);

        SaregamaConstants.ACTIVITIES.add(ctx);

        progressDialog = SaregamaDialogs.showLoading(ctx);

        webview = (WebView) findViewById(R.id.webview);

        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 95) {
                    if ((grand_total == 0 || grand_total == 0.0) && coupon_type.equals("0") && !loaded_url.equals(SaregamaConstants.PAYMENT_URL+"guest_id=" + getFromPrefs(SaregamaConstants.IMEI))) {
                        openURL(SaregamaConstants.PAYMENT_URL+"guest_id=" + getFromPrefs(SaregamaConstants.IMEI));
                    }
                    else if (!ctx.isFinishing() && progressDialog != null && progressDialog.isShowing() && !(grand_total == 0 || grand_total == 0.0))
                        progressDialog.dismiss();
                }
            }
        });

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                AlertDialog alertDialog = builder.create();
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }

                message = getAppConfigJson().getPayment_ssl_alert();
                alertDialog.setTitle("");
                alertDialog.setMessage(message);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Ignore SSL certificate errors
                        handler.proceed();
                    }
                });

                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                alertDialog.show();
            }

            public boolean shouldOverrideUrlLoading(WebView view, final String url) {
                if (!ctx.isFinishing() && progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (url.toLowerCase().startsWith("http") || url.toLowerCase().startsWith("https") || url.toLowerCase().startsWith("file")) {
                    if (url.contains(getAppConfigJson().getFail()) || url.contains(getAppConfigJson().getSuccess())) {
                        progressDialog = SaregamaDialogs.showLoading(ctx);
                        progressDialog.setTitle("Please Wait");
                        progressDialog.setMessage("Redirecting back to the app.");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Intent returnIntent = new Intent();
                                if (url.contains(SaregamaConstants.BASE_URL + "fail")) {
                                    returnIntent.putExtra("result", "fail");
                                } else if (url.contains(SaregamaConstants.BASE_URL + "success")) {
                                    returnIntent.putExtra("result", "success");
                                }
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }
                        }, 1000);
                    } else if (url.equals("http://sapi.saregama.com/") || url.equals("http://api.saregama.com/") || url.equals("http://www.saregama.com/") || url.equals("https://www.saregama.com/")) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", "fail");
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        Intent intent = new Intent(ctx, PaymentProcessActivity.class);
                        intent.putExtra("url", url);
                        startActivityForResult(intent, PAYMENT_STATUS);
                    }
                }
                return (true);
            }
        });
        String url = SaregamaConstants.BASE_URL + "payment?country_code="+getAppConfigJson().getCountry_code()+"&guest_id=" + getFromPrefs(SaregamaConstants.IMEI) + "&str=" + base64EncodResult;
        openURL(url);
    }

    private void openURL(String url) {
        loaded_url = url;
        webview.loadUrl(url);
        webview.requestFocus();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_STATUS) {
            if (resultCode == RESULT_OK) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", data.getStringExtra("result"));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        }
    }

    public void displayDialog(final SslErrorHandler handler) {
        Button okButton;
        final Dialog dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.custom_dialog);
        TextView msg_textView = (TextView) dialog.findViewById(R.id.text_exit);
        TextView dialog_header = (TextView) dialog.findViewById(R.id.dialog_header);
        dialog_header.setText("");
        msg_textView.setText(getAppConfigJson().getPayment_ssl_alert());
        okButton = (Button) dialog.findViewById(R.id.btn_yes_exit);
        okButton.setText("OK");
        Button cancelButton = (Button) dialog.findViewById(R.id.btn_no_exit);
        cancelButton.setText("CANCEL");
        ImageView dialog_header_cross = (ImageView) findViewById(R.id.dialog_header_cross);
        dialog_header_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.cancel();
                dialog.dismiss();
            }
        });
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.proceed();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.cancel();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}