package com.saregama.musicstore.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.CartAdapter;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.Cart;
import com.saregama.musicstore.pojo.CartItemPojo;
import com.saregama.musicstore.pojo.CartPojo;
import com.saregama.musicstore.pojo.ItemsAddedInCartPojo;
import com.saregama.musicstore.pojo.RemoveCartPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CartActivity extends BaseActivity implements View.OnClickListener {

    private CartActivity ctx = this;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private CartAdapter mAdapter;
    private ArrayList<ItemsAddedInCartPojo> obj;

    private LinearLayout place_order, place_order_layout;
    private float songs_total = 0;
    private float albums_total = 0;
    private float grand_total = 0;
    private String currency;
    private RecyclerView recyclerView;
    private ArrayList<CartItemPojo> songList;
    private ArrayList<CartItemPojo> albumList;
    private Cart song_cart, album_cart;
    private List<Cart> cart_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        setDrawerAndToolbar("Music Cart");

        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);

//        FrameLayout seekbar_root = (FrameLayout) findViewById(R.id.seekbar_root);
//        CoordinatorLayout.LayoutParams buttonLayoutParams = new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.WRAP_CONTENT, CoordinatorLayout.LayoutParams.WRAP_CONTENT);
//        buttonLayoutParams.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen.seekbar_margin_bottom));
//        buttonLayoutParams.gravity = Gravity.BOTTOM | Gravity.END;
//        seekbar_root.setLayoutParams(buttonLayoutParams);

        SaregamaConstants.ACTIVITIES.add(ctx);

        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);
        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        cart_layout.setVisibility(View.GONE);
        global_search.setVisibility(View.GONE);
        notification_layout.setVisibility(View.GONE);

        song_cart = new Cart();
        album_cart = new Cart();

        SaregamaConstants.CART_ACTIVITY = ctx;

        place_order = (LinearLayout) findViewById(R.id.place_order);
        place_order_layout = (LinearLayout) findViewById(R.id.place_order_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_cart);

        place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                grand_total = songs_total + albums_total;
                if(grand_total > 0) {
                    Intent mIntent = new Intent(CartActivity.this, PayNowActivity.class);
                    mIntent.putExtra("songs_total", songs_total);
                    mIntent.putExtra("albums_total", albums_total);
                    mIntent.putExtra("grand_total", grand_total);
                    mIntent.putExtra("currency", getAppConfigJson().getCurrency());
                    startActivity(mIntent);
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                }

            }
        });

        if (getJsonObject() != null && getJsonObject().size() > 0) {
            getCartData();
        } else {
            continueShopping();
        }

    }

    public void getCartData() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getCartData(getFromPrefs(SaregamaConstants.SESSION_ID), getFromPrefs(SaregamaConstants.IMEI), new Callback<CartPojo>() {
                @Override
                public void success(CartPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus() && basePojo.getData() != null && basePojo.getData().getCart() != null) {
                            obj = getJsonObject();
                            if (obj != null) {
                                for (int i = obj.size() - 1; i >= 0; i--) {
                                    if (obj.get(i).isLoggedIn()) {
                                        obj.remove(i);
                                    }
                                }
                                saveJsonObject(obj);
                            }

                            checkCommonURL(response.getUrl());

                            saveIntoPrefs(SaregamaConstants.REFRESH_NEEDED, "no");

                            songs_total = basePojo.getData().getSongs_total();
                            albums_total = basePojo.getData().getAlbums_total();
                            grand_total = basePojo.getData().getGrand_total();
                            currency = basePojo.getData().getCurrency();

                            if(grand_total == 0)
                                place_order_layout.setVisibility(View.GONE);
                            else
                                place_order_layout.setVisibility(View.VISIBLE);

                            if (getFromPrefs(SaregamaConstants.SESSION_ID) != null && getFromPrefs(SaregamaConstants.SESSION_ID).length() > 0) {
                                if (obj != null) {
                                    if (basePojo.getData().getCart().getHd() != null) {
                                        addCartItemsInLocalData(basePojo.getData().getCart().getHd(), getAppConfigJson().getC_type().getSONGHD() + "");
                                    }
                                    if (basePojo.getData().getCart().getMp3() != null) {
                                        addCartItemsInLocalData(basePojo.getData().getCart().getMp3(), getAppConfigJson().getC_type().getSONG() + "");
                                    }
                                    if (basePojo.getData().getCart().getAlbum() != null) {
                                        addCartItemsInLocalData(basePojo.getData().getCart().getAlbum(), getAppConfigJson().getC_type().getALBUM() + "");
                                    }
                                } else {
                                    obj = new ArrayList<>();
                                    if (basePojo.getData().getCart().getHd() != null) {
                                        addCartItemsInLocalDataFirstTime(basePojo.getData().getCart().getHd(), getAppConfigJson().getC_type().getSONGHD() + "");
                                    }
                                    if (basePojo.getData().getCart().getMp3() != null) {
                                        addCartItemsInLocalDataFirstTime(basePojo.getData().getCart().getMp3(), getAppConfigJson().getC_type().getSONG() + "");
                                    }
                                    if (basePojo.getData().getCart().getAlbum() != null) {
                                        addCartItemsInLocalDataFirstTime(basePojo.getData().getCart().getAlbum(), getAppConfigJson().getC_type().getALBUM() + "");
                                    }
                                }
                            }
                            saveJsonObject(obj);

                            ArrayList<CartItemPojo> hdSongs = new ArrayList<>();

                            if (basePojo.getData().getCart() != null) {
                                albumList = basePojo.getData().getCart().getAlbum();
                                songList = basePojo.getData().getCart().getMp3();
                                hdSongs = basePojo.getData().getCart().getHd();
                            }

                            int songs_count = 0;
                            int albumCount = 0;
                            if (songList != null && basePojo.getData().getCart() != null)
                                songs_count = basePojo.getData().getCart().getMp3().size();
                            if (hdSongs != null)
                                songs_count += hdSongs.size();

                            if (albumList != null)
                                albumCount = albumList.size();

                            if (songs_count > 0) {
                                if (basePojo.getData().getCart().getHd() != null && basePojo.getData().getCart().getHd().size() > 0 && songList != null) {
                                    songList.addAll(basePojo.getData().getCart().getHd());
                                } else if (basePojo.getData().getCart().getHd() != null && basePojo.getData().getCart().getHd().size() > 0) {
                                    songList = hdSongs;
                                }
                            } else if (basePojo.getData().getCart().getHd() != null) {
                                songList = basePojo.getData().getCart().getHd();
                            }

                            if (songs_count > 0)
                                song_cart = new Cart("Song(s)"/*, songs_count*/, songList);
                            if (albumCount > 0)
                                album_cart = new Cart("Album(s)"/*,albumCount*/, albumList);
                            if (songs_count == 0) {
                                cart_list = Arrays.asList(album_cart);
                            } else if (albumCount == 0) {
                                cart_list = Arrays.asList(song_cart);
                            } else if (albumCount > 0 && songs_count > 0) {
                                cart_list = Arrays.asList(song_cart, album_cart);
                            }

                            mAdapter = new CartAdapter(ctx, CartActivity.this, cart_list, songs_count, albumCount);
                            mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
                                @Override
                                public void onListItemExpanded(int position) {

                                }

                                @Override
                                public void onListItemCollapsed(int position) {

                                }
                            });

                            recyclerView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            recyclerView.setLayoutManager(new LinearLayoutManager(CartActivity.this));

                        } else {
                            place_order_layout.setVisibility(View.GONE);
                            LinearLayout empty_cart_layout = (LinearLayout) findViewById(R.id.empty_cart_layout);
                            empty_cart_layout.setVisibility(View.VISIBLE);
                            LinearLayout continue_shop = (LinearLayout) findViewById(R.id.continue_shop);
                            continue_shop.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(ctx, HomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                                }
                            });
                            saveJsonObject(new ArrayList<ItemsAddedInCartPojo>());
                        }
                    } else {
                        place_order_layout.setVisibility(View.GONE);
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            place_order_layout.setVisibility(View.GONE);
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mAdapter != null)
            mAdapter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (mAdapter != null)
            mAdapter.onRestoreInstanceState(savedInstanceState);
    }

    private void removeFromCartArray(CartItemPojo pojo, int pos, String from) {
        ArrayList<ItemsAddedInCartPojo> arr = getJsonObject();
        if (arr != null) {
            for (int i = 0; i < arr.size(); i++) {
                if (pojo.getId().equals(arr.get(i).getId()) && pojo.getCtype().equals(arr.get(i).getType())) {
                    arr.remove(i);
                    if (from.equals("songs")) {
                        if (songList.size() > 0)
                            songList.remove(pos - 1);
                    }
                    saveJsonObject(arr);
                    break;
                }
            }
        }
        if (arr != null && arr.size() == 0) {
            LinearLayout empty_cart_layout = (LinearLayout) findViewById(R.id.empty_cart_layout);
            empty_cart_layout.setVisibility(View.VISIBLE);
            LinearLayout continue_shop = (LinearLayout) findViewById(R.id.continue_shop);
            continue_shop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ctx, HomeActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                }
            });
        }
    }

    private void getTotal(ArrayList<CartItemPojo> arr, String from) {
        float total = 0;
        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i).getStatus() == 1)
                total = total + arr.get(i).getPrice();
        }
        if (from.equals("songs"))
            songs_total = total;
        else
            albums_total = total;

        if((int)(songs_total + albums_total) > 0)
            place_order_layout.setVisibility(View.VISIBLE);
        else
            place_order_layout.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getFromPrefs(SaregamaConstants.REFRESH_NEEDED).equals("yes")) {
            getCartData();
        }
    }

    private void addCartItemsInLocalData(ArrayList<CartItemPojo> cartArr, String type) {
        for (int i = 0; i < cartArr.size(); i++) {
            boolean contains = false;
            for (int j = 0; j < obj.size(); j++) {
                if (cartArr.get(i) != null && obj.get(j).getId().equals(cartArr.get(i).getId()) && (obj.get(j).getType().equals(cartArr.get(i).getCtype()))) {
                    contains = true;
                    break;
                }
            }
            if (!contains && cartArr.get(i) != null) {
                ItemsAddedInCartPojo items = new ItemsAddedInCartPojo();
                items.setLoggedIn(true);
                items.setId(cartArr.get(i).getId());
                items.setType(cartArr.get(i).getCtype());
                obj.add(items);
            }
        }
    }

    private void addCartItemsInLocalDataFirstTime(ArrayList<CartItemPojo> cartArr, String type) {
        for (int i = 0; i < cartArr.size(); i++) {
            if (cartArr.get(i) != null) {
                ItemsAddedInCartPojo items = new ItemsAddedInCartPojo();
                items.setLoggedIn(true);
                items.setId(cartArr.get(i).getId());
                items.setType(cartArr.get(i).getCtype());
                obj.add(items);
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.remove_album_fron_cart:
                final CartItemPojo pojo = (CartItemPojo) v.getTag(R.string.data);
                final int pos = (int) v.getTag(R.string.key);
                final String from = (String) v.getTag(R.string.from);
                final String removable_id = pojo.getId();
                String id = pojo.getCtype() + "|" + pojo.getId();

                if (cd.isConnectingToInternet()) {
                    final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
                    d.setCanceledOnTouchOutside(false);

                    RestClient.get().removeCartData(getFromPrefs(SaregamaConstants.SESSION_ID), id, getFromPrefs(SaregamaConstants.IMEI), new Callback<RemoveCartPojo>() {
                        @Override
                        public void success(RemoveCartPojo basePojo, Response response) {
                            if (basePojo != null) {
                                if (basePojo.getStatus()) {
                                    removeFromCartArray(pojo, pos, from);
                                    if (from.equals("songs")) {
                                        if (songList != null && songList.size() == 0 && albumList != null && albumList.size() > 0) {
                                            cart_list = Arrays.asList(album_cart);
                                        } else if (albumList != null && albumList.size() == 0 && songList != null && songList.size() > 0) {
                                            cart_list = Arrays.asList(song_cart);
                                        } else if (albumList != null && albumList.size() > 0 && songList.size() > 0) {
                                            cart_list = Arrays.asList(song_cart, album_cart);
                                        } else if (albumList != null && albumList.size() == 0 && songList.size() == 0) {
                                            continueShopping();
                                        }

                                        if (albumList != null){
                                            mAdapter = new CartAdapter(ctx, CartActivity.this, cart_list, songList.size(), albumList.size());
                                        }
                                        else {
                                            mAdapter = new CartAdapter(ctx, CartActivity.this, cart_list, songList.size(), 0);
                                        }

                                        recyclerView.setAdapter(mAdapter);
                                        mAdapter.notifyDataSetChanged();
                                        recyclerView.clearOnScrollListeners();
                                        recyclerView.scrollToPosition(pos-2);
                                        Toast toast = Toast.makeText(ctx, "Song removed from cart", Toast.LENGTH_SHORT);
                                        toast.show();

                                        getTotal(songList, "songs");
                                    } else if (from.equals("album")) {
//                                        Iterator<CartItemPojo> iter = albumList.iterator();
//                                        while (iter.hasNext()) {
//
//                                            if (iter.next().getId() == removable_id && iter.next().getCtype() == pojo.getCtype()) {
//                                                iter.remove();
//                                            }
//                                        }

                                        if (albumList != null) {
                                            for (int i = 0; i < albumList.size(); i++) {
                                                if (albumList.get(i).getId() == removable_id && albumList.get(i).getCtype() == pojo.getCtype()) {
                                                    albumList.remove(i);
                                                    break;
                                                }
                                            }
                                        }

                                        if (songList != null && songList.size() == 0 && albumList != null && albumList.size() > 0) {
                                            cart_list = Arrays.asList(album_cart);
                                        } else if (albumList != null && albumList.size() == 0 && songList != null && songList.size() > 0) {
                                            cart_list = Arrays.asList(song_cart);
                                        } else if (songList != null && albumList.size() > 0 && songList.size() > 0) {
                                            cart_list = Arrays.asList(song_cart, album_cart);
                                        } else if (songList != null && albumList.size() == 0 && songList.size() == 0) {
                                            continueShopping();
                                        }

                                        if (songList != null) {
                                            mAdapter = new CartAdapter(ctx, CartActivity.this, cart_list, songList.size(), albumList.size());
                                        }
                                        else {
                                            mAdapter = new CartAdapter(ctx, CartActivity.this, cart_list, 0, albumList.size());
                                        }
                                        recyclerView.setAdapter(mAdapter);
                                        mAdapter.notifyDataSetChanged();
                                        recyclerView.clearOnScrollListeners();
                                        recyclerView.scrollToPosition(pos-2);

                                        Toast toast = Toast.makeText(ctx, "Album removed from cart", Toast.LENGTH_SHORT);
                                        toast.show();

                                        getTotal(albumList, "albums");
                                    }
                                } else {
                                    dialog.displayCommonDialog(basePojo.getError());
                                }
                            } else {
                                dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                            }
                            d.dismiss();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            d.dismiss();
                        }

                    });
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getInternat_fail() + "\n");
                }

                break;

            case R.id.cart_song_row_root:
                final CartItemPojo pojo_detail = (CartItemPojo) v.getTag(R.string.data);
                Intent mIntent = new Intent(CartActivity.this, Mp3HindiAlbumDetail.class);
                mIntent.putExtra("album_id", pojo_detail.getId());
                mIntent.putExtra("cart_album", "cart_album");
                mIntent.putExtra("album_name", pojo_detail.getTitle());
                mIntent.putExtra("c_type", getAppConfigJson().getC_type().getALBUM());
                startActivity(mIntent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.buy_again:
                final CartItemPojo item_detail = (CartItemPojo) v.getTag(R.string.data);
                buyAgain(item_detail.getCtype(), item_detail.getId());
                break;

        }

    }

    private void buyAgain(final String cType, final String cid) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().buyAgain(getFromPrefs(SaregamaConstants.SESSION_ID), cType, cid, new Callback<CartPojo>() {
                @Override
                public void success(CartPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            if (String.valueOf(getAppConfigJson().getC_type().getSONG()).equals(cType) || String.valueOf(getAppConfigJson().getC_type().getSONGHD()).equals(cType)) {
                                for (int i = 0; i < songList.size(); i++) {
                                    if (songList.get(i).getId().equals(cid) && songList.get(i).getCtype().equals(cType)) {
                                        songList.get(i).setStatus(1);
                                        break;
                                    }
                                }
                                getTotal(songList, "songs");
                            } else {
                                for (int i = 0; i < albumList.size(); i++) {
                                    if (albumList.get(i).getId().equals(cid) && albumList.get(i).getCtype().equals(cType)) {
                                        albumList.get(i).setStatus(1);
                                        break;
                                    }
                                }
                                getTotal(albumList, "album");
                            }

                            int count_song = 0;
                            int count_album = 0;
                            if (songList != null && songList.size() > 0) {
                                count_song = songList.size();
                                song_cart = new Cart("Song(s)"/*, songs_count*/, songList);
                            }
                            if (albumList != null && albumList.size() > 0) {
                                count_album = albumList.size();
                                album_cart = new Cart("Album(s)"/*,albumCount*/, albumList);
                            }
                            if (count_song == 0) {
                                cart_list = Arrays.asList(album_cart);
                            } else if (count_album == 0) {
                                cart_list = Arrays.asList(song_cart);
                            } else if (count_album > 0 && count_song > 0) {
                                cart_list = Arrays.asList(song_cart, album_cart);
                            }
                            mAdapter.notifyDataSetChanged();
                        }
                    } else {
                        dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void continueShopping() {
        LinearLayout empty_cart_layout = (LinearLayout) findViewById(R.id.empty_cart_layout);
        empty_cart_layout.setVisibility(View.VISIBLE);
        LinearLayout continue_shop = (LinearLayout) findViewById(R.id.continue_shop);
        continue_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, HomeActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            }
        });
    }
}