package com.saregama.musicstore.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.SearchArtisteAdapter;
import com.saregama.musicstore.adapter.SearchInstrumentAdapter;
import com.saregama.musicstore.adapter.SearchSongsAdapter;
import com.saregama.musicstore.pojo.ClassHindArtistListPojo;
import com.saregama.musicstore.pojo.ClassHindInstrumentListPojo;
import com.saregama.musicstore.pojo.HindiFilmsSearchPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by Administrator on 29-Jul-16.
 */
public class DevotionalSearchActivity extends BaseActivity implements View.OnClickListener {

    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
    private ListView bhajans_list_layout_global_search, mantra_list_layout_global_search, artist_list_layout_global_search, aartis_list_layout_global_search;
    private SearchArtisteAdapter globalSearchArtistAdapter_ob;
    private SearchSongsAdapter globalSearchBhajansAdapter_ob;
    private SearchSongsAdapter globalSearchAartisAdapter_ob;
    private SearchSongsAdapter globalSearchMantrasAdapter_ob;
    private ArrayList<MP3HindiSongListPojo> bhajans_arrlistlist_layout_global_search;
    private ArrayList<MP3HindiSongListPojo> aartis_arrlistlist_layout_global_search;
    private ArrayList<ClassHindArtistListPojo> artist_arrlistlist_layout_global_search;
    private ArrayList<ClassHindInstrumentListPojo> instrument_arrlistlist_layout_global_search;
    private ArrayList<ClassHindInstrumentListPojo> raagas_arrlistlist_layout_global_search;
    private ArrayList<MP3HindiSongListPojo> mantras_arrlistlist_layout_global_search;
    private SearchInstrumentAdapter globalSearchInstrumentsAdapter_ob;
    private SearchInstrumentAdapter globalSearchRaagasAdapter_ob;
    ;
    private EditText global_search_main;
    private LinearLayout songs_layout_global_search, album_layout_global_search, artist_layout_global_search, geetmala_layout_global_search;
    private LinearLayout global_search_layout;
    private ImageView logo;
    private DevotionalSearchActivity ctx = this;
    private String search_keyword = "";
    private String str = "";
    private TextView header_text_first_global, header_text_second_global, header_text_third_global, header_text_fourth_global;
    private TextView songs_viewall, album_viewall, artistes_viewall, geetmala_viewall;
    private String search_from;
    private int c_type, language_id, d_type;
    private String from;
    private ImageView global_search_cross;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_search);

        init();
        search_from = getIntent().getStringExtra("search_from");
        c_type = getIntent().getIntExtra("c_type", 1);
        d_type = getIntent().getIntExtra("d_type", 19);
        if (getIntent().getStringExtra("from") != null) {
            from = getIntent().getStringExtra("from");
        }

        language_id = getIntent().getIntExtra("languageid", 0);

        //preparing Toolbar
        setDrawerAndToolbar("");
        global_search_main = (EditText) findViewById(R.id.global_search_main);
        global_search_main.setVisibility(View.VISIBLE);
        TextView header = (TextView) findViewById(R.id.header);
        header.setVisibility(View.GONE);

        SaregamaConstants.ACTIVITIES.add(ctx);

        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        cart_layout.setVisibility(View.VISIBLE);

        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        notification_layout.setVisibility(View.GONE);

        // view all list
        songs_viewall = (TextView) findViewById(R.id.songs_view_all);
        album_viewall = (TextView) findViewById(R.id.album_view_all);
        artistes_viewall = (TextView) findViewById(R.id.artistes_view_all);
        geetmala_viewall = (TextView) findViewById(R.id.geetmala_view_all);

        songs_viewall.setPaintFlags(songs_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        album_viewall.setPaintFlags(album_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        artistes_viewall.setPaintFlags(artistes_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        geetmala_viewall.setPaintFlags(geetmala_viewall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        header_text_first_global = (TextView) findViewById(R.id.header_text_first_global);
        header_text_second_global = (TextView) findViewById(R.id.header_text_second_global);
        header_text_third_global = (TextView) findViewById(R.id.header_text_third_global);
        header_text_fourth_global = (TextView) findViewById(R.id.header_text_fourth_global);

        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);
        global_search.setVisibility(View.GONE);
        global_search_cross = (ImageView) findViewById(R.id.global_search_cross);
        global_search_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global_search_main.setText("");
                if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
                showSoftKeyboard();
            }
        });

        global_search_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
            }
        });

        if (search_from.equals("others")) {
            global_search_main.setHint("Search Pop Songs, Folk Songs");
        } else if (search_from.equals("classical_hindustani")) {
            global_search_main.setHint("Search Artistes, Instruments, Raagas");
        } else {
            global_search_main.setHint("Search Bhajans, Mantras, Aartis, Artistes");
        }


        global_search_main.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (global_search_main.getText().toString().trim().length() < 1) {
                    global_search_cross.setVisibility(View.GONE);
                } else {
                    global_search_cross.setVisibility(View.VISIBLE);
                }
                if (global_search_main.getText().toString().trim().length() > 2) {
                    search_keyword = global_search_main.getText().toString().trim();
                    search_keyword = search_keyword.replaceAll(" ", "%20");
                    new GetGlobalSearchList().execute();
                } else {
                    bhajans_arrlistlist_layout_global_search = new ArrayList<>();
                    aartis_arrlistlist_layout_global_search = new ArrayList<>();
                    artist_arrlistlist_layout_global_search = new ArrayList<>();
                    mantras_arrlistlist_layout_global_search = new ArrayList<>();
                    instrument_arrlistlist_layout_global_search = new ArrayList<>();
                    raagas_arrlistlist_layout_global_search = new ArrayList<>();
                    globalSearchBhajansAdapter_ob = new SearchSongsAdapter(ctx, bhajans_arrlistlist_layout_global_search, search_keyword, search_from);
                    globalSearchAartisAdapter_ob = new SearchSongsAdapter(ctx, aartis_arrlistlist_layout_global_search, search_keyword, search_from);
                    globalSearchMantrasAdapter_ob = new SearchSongsAdapter(ctx, mantras_arrlistlist_layout_global_search, search_keyword, search_from);
                    globalSearchArtistAdapter_ob = new SearchArtisteAdapter(ctx, artist_arrlistlist_layout_global_search, search_keyword, search_from);
                    globalSearchInstrumentsAdapter_ob = new SearchInstrumentAdapter(ctx, instrument_arrlistlist_layout_global_search, search_keyword, search_from);
                    globalSearchRaagasAdapter_ob = new SearchInstrumentAdapter(ctx, raagas_arrlistlist_layout_global_search, search_keyword, search_from);

                    if (search_from.equals("classical_hindustani")) {
                        bhajans_list_layout_global_search.setAdapter(globalSearchInstrumentsAdapter_ob);
                        aartis_list_layout_global_search.setAdapter(globalSearchRaagasAdapter_ob);
                    } else {
                        bhajans_list_layout_global_search.setAdapter(globalSearchBhajansAdapter_ob);
                        aartis_list_layout_global_search.setAdapter(globalSearchAartisAdapter_ob);
                    }
                    artist_list_layout_global_search.setAdapter(globalSearchArtistAdapter_ob);
                    mantra_list_layout_global_search.setAdapter(globalSearchMantrasAdapter_ob);
                    songs_layout_global_search.setVisibility(View.GONE);
                    album_layout_global_search.setVisibility(View.GONE);
                    artist_layout_global_search.setVisibility(View.GONE);
                    geetmala_layout_global_search.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        global_search_main.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    hideSoftKeyboard();
                }
                return false;
            }
        });
    }

    private class GetGlobalSearchList extends AsyncTask<Void, Void, Void> {
        private HindiFilmsSearchPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            if (search_from.equals("others")) {
                str = getAsyncTaskDataGlobal(SaregamaConstants.BASE_URL + "category_search?type=search&query=" + search_keyword + "&mode=others" + "&lang=" + language_id);
            } else if (search_from.equals("classical_hindustani")) {
                str = getAsyncTaskDataGlobal(SaregamaConstants.BASE_URL + "category_search?type=search&query=" + search_keyword + "&mode=classical");
            } else {
                str = getAsyncTaskDataGlobal(SaregamaConstants.BASE_URL + "category_search?type=search&query=" + search_keyword + "&mode=devotional");
            }

            JsonReader reader = new JsonReader(new StringReader(str));
            reader.setLenient(true);
            basePojo = gsonObj.fromJson(reader, HindiFilmsSearchPojo.class);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    bhajans_arrlistlist_layout_global_search = new ArrayList<>();
                    aartis_arrlistlist_layout_global_search = new ArrayList<>();
                    artist_arrlistlist_layout_global_search = new ArrayList<>();
                    mantras_arrlistlist_layout_global_search = new ArrayList<>();
                    instrument_arrlistlist_layout_global_search = new ArrayList<>();
                    raagas_arrlistlist_layout_global_search = new ArrayList<>();

                    if (basePojo.getData().getBhajans() != null && basePojo.getData().getBhajans().getList() != null && basePojo.getData().getBhajans().getList().size() > 0) {
                        header_text_first_global.setText(getResources().getString(R.string.bhajans));
                        songs_layout_global_search.setVisibility(View.VISIBLE);
                        setBhajansAdapter(basePojo, bhajans_list_layout_global_search, songs_viewall);
                    } else {
                        songs_layout_global_search.setVisibility(View.GONE);
                    }

                    if (basePojo.getData().getMantras() != null && basePojo.getData().getMantras().getList() != null && basePojo.getData().getMantras().getList().size() > 0) {
                        header_text_second_global.setText(getResources().getString(R.string.mantras));
                        album_layout_global_search.setVisibility(View.VISIBLE);
                        setMantrasAdapter(basePojo, mantra_list_layout_global_search, album_viewall);
                    } else {
                        album_layout_global_search.setVisibility(View.GONE);
                    }

                    if (basePojo.getData().getAartis() != null && basePojo.getData().getAartis().getList() != null && basePojo.getData().getAartis().getList().size() > 0) {
                        artist_layout_global_search.setVisibility(View.VISIBLE);
                        header_text_third_global.setText(getResources().getString(R.string.aartis));
                        setAartisAdapter(basePojo, aartis_list_layout_global_search, artistes_viewall);
                    } else {
                        artist_layout_global_search.setVisibility(View.GONE);
                    }

                    if (basePojo.getData().getArtist() != null && basePojo.getData().getArtist().getList() != null && basePojo.getData().getArtist().getList().size() > 0) {
                        geetmala_layout_global_search.setVisibility(View.VISIBLE);
                        header_text_fourth_global.setText(getResources().getString(R.string.artist));
                        setArtistesAdapter(basePojo, artist_list_layout_global_search, geetmala_viewall);
                    } else {
                        geetmala_layout_global_search.setVisibility(View.GONE);
                    }

                    if (search_from.equals("others")) {
                        boolean pop = false, folk = false;
                        if (basePojo.getData().getPop() != null && basePojo.getData().getPop().getList() != null && basePojo.getData().getPop().getList().size() > 0) {
                            header_text_first_global.setText(getResources().getString(R.string.pop));
                            songs_layout_global_search.setVisibility(View.VISIBLE);
                            setPopAdapter(basePojo, bhajans_list_layout_global_search, songs_viewall);
                            pop = true;
                        } else {
                            pop = false;
                            songs_layout_global_search.setVisibility(View.GONE);
                        }

                        if (basePojo.getData().getFolk() != null && basePojo.getData().getFolk().getList() != null && basePojo.getData().getFolk().getList().size() > 0) {
                            header_text_second_global.setText(getResources().getString(R.string.folk));
                            album_layout_global_search.setVisibility(View.VISIBLE);
                            setFolkAdapter(basePojo, mantra_list_layout_global_search, album_viewall);
                            folk = true;
                        } else {
                            folk = false;
                            album_layout_global_search.setVisibility(View.GONE);
                        }

                        if(!pop && !folk){
                            Toast toast = Toast.makeText(ctx, SaregamaConstants.NO_RESULT, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    }
                    if (search_from.equals("classical_hindustani")) {
                        if (basePojo.getData().getArtist() != null && basePojo.getData().getArtist().getList() != null && basePojo.getData().getArtist().getList().size() > 0) {
                            geetmala_layout_global_search.setVisibility(View.VISIBLE);
                            header_text_fourth_global.setText(getResources().getString(R.string.artist));
                            setArtistesAdapter(basePojo, artist_list_layout_global_search, geetmala_viewall);
                        } else {
                            geetmala_layout_global_search.setVisibility(View.GONE);
                        }

                        if (basePojo.getData().getInstruments() != null && basePojo.getData().getInstruments().getList() != null && basePojo.getData().getInstruments().getList().size() > 0) {
                            header_text_first_global.setText(getResources().getString(R.string.instruments));
                            songs_layout_global_search.setVisibility(View.VISIBLE);
                            setInstrumentsAdapter(basePojo, bhajans_list_layout_global_search, songs_viewall);
                        } else {
                            songs_layout_global_search.setVisibility(View.GONE);
                        }

                        if (basePojo.getData().getRaagas() != null && basePojo.getData().getRaagas().getList() != null && basePojo.getData().getRaagas().getList().size() > 0) {
                            artist_layout_global_search.setVisibility(View.VISIBLE);
                            header_text_third_global.setText(getResources().getString(R.string.raagas));
                            setRaagasAdapter(basePojo, aartis_list_layout_global_search, artistes_viewall);
                        } else {
                            artist_layout_global_search.setVisibility(View.GONE);
                        }

                    }

                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            }
        }
    }

    private void navigateToTabListActivity(String fragment_pos) {
        Intent intent = null;

        if (search_from.equals("others")) {
            intent = new Intent(ctx, OtherHindiSongLanding.class);
            intent.putExtra("languageid", getIntent().getIntExtra("languageid", 0));
            intent.putExtra("from", "Devotional");
        } else if (search_from.equals("classical_hindustani") && d_type == ctx.getAppConfigJson().getD_type().getHINDUSTANI()) {
            intent = new Intent(ctx, ClassicalHindustaniActivity.class);
            intent.putExtra("from", "Hindustani");
        } else if (search_from.equals("classical_hindustani") && d_type == ctx.getAppConfigJson().getD_type().getCARNATIC()) {
            intent = new Intent(ctx, ClassicalHindustaniActivity.class);
            intent.putExtra("from", "Carnatic");
        } else {
            intent = new Intent(ctx, Mp3Devotional.class);
            intent.putExtra("languageid", getIntent().getIntExtra("lang_id", 0));
            intent.putExtra("from", "Devotional");
        }
        intent.putExtra("search_text", global_search_main.getText().toString().trim().replace(" ", "%20"));
        intent.putExtra("fragment_pos", fragment_pos);
        intent.putExtra("c_type", c_type);
        intent.putExtra("coming_from", "search");
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private void setAartisAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        aartis_arrlistlist_layout_global_search = basePojo.getData().getAartis().getList();
        globalSearchAartisAdapter_ob = new SearchSongsAdapter(ctx, aartis_arrlistlist_layout_global_search, search_keyword, search_from);
        if (aartis_arrlistlist_layout_global_search != null && aartis_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchAartisAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchAartisAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("aartis");
            }
        });
    }

    private void setMantrasAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        mantras_arrlistlist_layout_global_search = basePojo.getData().getMantras().getList();
        globalSearchMantrasAdapter_ob = new SearchSongsAdapter(ctx, mantras_arrlistlist_layout_global_search, search_keyword, search_from);
        if (mantras_arrlistlist_layout_global_search != null && mantras_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchMantrasAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchMantrasAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("mantras");
            }
        });
    }

    private void setArtistesAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        artist_arrlistlist_layout_global_search = basePojo.getData().getArtist().getList();
        globalSearchArtistAdapter_ob = new SearchArtisteAdapter(ctx, artist_arrlistlist_layout_global_search, search_keyword, search_from);
        if (artist_arrlistlist_layout_global_search != null && artist_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchArtistAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchArtistAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("artiste");
            }
        });
    }

    private void setBhajansAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        bhajans_arrlistlist_layout_global_search = basePojo.getData().getBhajans().getList();
        globalSearchBhajansAdapter_ob = new SearchSongsAdapter(ctx, bhajans_arrlistlist_layout_global_search, search_keyword, search_from);
        if (bhajans_arrlistlist_layout_global_search != null && bhajans_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchBhajansAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchBhajansAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("bhajans");
            }
        });
    }

    private void setPopAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        bhajans_arrlistlist_layout_global_search = basePojo.getData().getPop().getList();
        globalSearchBhajansAdapter_ob = new SearchSongsAdapter(ctx, bhajans_arrlistlist_layout_global_search, search_keyword, search_from);
        if (bhajans_arrlistlist_layout_global_search != null && bhajans_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchBhajansAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchBhajansAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("pop");
            }
        });
    }

    private void setFolkAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        bhajans_arrlistlist_layout_global_search = basePojo.getData().getFolk().getList();
        globalSearchBhajansAdapter_ob = new SearchSongsAdapter(ctx, bhajans_arrlistlist_layout_global_search, search_keyword, search_from);
        if (bhajans_arrlistlist_layout_global_search != null && bhajans_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchBhajansAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchBhajansAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("folk");
            }
        });
    }

    private void setInstrumentsAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        instrument_arrlistlist_layout_global_search = basePojo.getData().getInstruments().getList();
        globalSearchInstrumentsAdapter_ob = new SearchInstrumentAdapter(ctx, instrument_arrlistlist_layout_global_search, search_keyword, search_from);
        if (instrument_arrlistlist_layout_global_search != null && instrument_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchInstrumentsAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchInstrumentsAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("instruments");
            }
        });
    }

    private void setRaagasAdapter(HindiFilmsSearchPojo basePojo, ListView list, TextView viewAll) {
        raagas_arrlistlist_layout_global_search = basePojo.getData().getRaagas().getList();
        globalSearchRaagasAdapter_ob = new SearchInstrumentAdapter(ctx, raagas_arrlistlist_layout_global_search, search_keyword, search_from);
        if (raagas_arrlistlist_layout_global_search != null && raagas_arrlistlist_layout_global_search.size() > 0) {
            list.setAdapter(globalSearchRaagasAdapter_ob);
            ListUtils.setDynamicHeight(list);
        }
        globalSearchRaagasAdapter_ob.notifyDataSetChanged();
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTabListActivity("raagas");
            }
        });
    }

    private void init() {
        cd = new ConnectionDetector(ctx);
        dialog = new SaregamaDialogs(ctx);
        bhajans_list_layout_global_search = (ListView) findViewById(R.id.songs_list__layout_global_search);
        mantra_list_layout_global_search = (ListView) findViewById(R.id.album_list__layout_global_search);
        artist_list_layout_global_search = (ListView) findViewById(R.id.geetmala_list__layout_global_search);
        aartis_list_layout_global_search = (ListView) findViewById(R.id.artist_list__layout_global_search);

        bhajans_arrlistlist_layout_global_search = new ArrayList<>();
        aartis_arrlistlist_layout_global_search = new ArrayList<>();
        artist_arrlistlist_layout_global_search = new ArrayList<>();
        mantras_arrlistlist_layout_global_search = new ArrayList<>();

        global_search_layout = (LinearLayout) findViewById(R.id.global_search_layout);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width * 3 / 4 + getResources().getInteger(R.integer.appbar_search_add), ViewGroup.LayoutParams.WRAP_CONTENT);
        global_search_layout.setLayoutParams(params);
        global_search_layout.setVisibility(View.VISIBLE);
        logo = (ImageView) findViewById(R.id.logo);
        logo.setVisibility(View.GONE);

        songs_layout_global_search = (LinearLayout) findViewById(R.id.songs_layout_global_search);
        album_layout_global_search = (LinearLayout) findViewById(R.id.album_layout_global_search);
        artist_layout_global_search = (LinearLayout) findViewById(R.id.artist_layout_global_search);
        geetmala_layout_global_search = (LinearLayout) findViewById(R.id.geetmala_layout_global_search);
        songs_layout_global_search.setVisibility(View.GONE);
        album_layout_global_search.setVisibility(View.GONE);
        artist_layout_global_search.setVisibility(View.GONE);
        geetmala_layout_global_search.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.classhindustani_artist_albumLL:
                hideSoftKeyboard();
                Intent intent_artiste = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
                ClassHindArtistListPojo data = (ClassHindArtistListPojo) v.getTag(R.string.data);
                intent_artiste.putExtra("artist_id", data.getId());
                if (search_from.equals("classical_hindustani")) {
                    intent_artiste.putExtra("header", getIntent().getStringExtra("from"));
                } else {
                    intent_artiste.putExtra("from", "devotional");
                }

                intent_artiste.putExtra("c_type", c_type);
                startActivity(intent_artiste);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;


            case R.id.classhindustani_instrument_albumLL:
                hideSoftKeyboard();
                Intent intent_instrument = new Intent(ctx, ClassicalHindustaniSOngDetail.class);
                ClassHindInstrumentListPojo data_instru = (ClassHindInstrumentListPojo) v.getTag(R.string.data);
                intent_instrument.putExtra("artist_id", data_instru.getTag_id());
                intent_instrument.putExtra("from", "Instrument");
                intent_instrument.putExtra("c_type", c_type);
                intent_instrument.putExtra("header", getIntent().getStringExtra("from"));
                startActivity(intent_instrument);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            default:
                break;
        }
    }
}
