package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.appsflyer.AppsFlyerLib;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.saregama.musicstore.R;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.LogInPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.Manifest.permission.GET_ACCOUNTS;
import static android.Manifest.permission.RECEIVE_SMS;

public class LoginActivity extends BaseActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;
    CallbackManager callbackManager;

    // for external permission
    private final static int RECEIVE_SMS_RESULT = 202;
    private final static int GET_ACCOUNTS_RESULT = 102;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;
    private String notification_key = "";
    private Map<String, String> articleParams;

    public static int APP_REQUEST_CODE = 99;

    // for google plus
    private String googleId, token;
    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked = false;
    private ConnectionResult mConnectionResult;
    private String user_email, user_password;
    private String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AccountKit.initialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        dialog = new SaregamaDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());

        LinearLayout phone_accountKit = (LinearLayout) findViewById(R.id.phone_accountKit);
        LinearLayout skip = (LinearLayout) findViewById(R.id.skip);
        LinearLayout email_login = (LinearLayout) findViewById(R.id.email_login);
        LinearLayout gmail = (LinearLayout) findViewById(R.id.gmaillogin);
        LinearLayout fb = (LinearLayout) findViewById(R.id.fblogin);

        from = getIntent().getStringExtra("from");

        if (from != null && (from.equals("cart") || from.equals("sms_download_manager")))
            skip.setVisibility(View.GONE);

        articleParams = new HashMap<>();

        if (getIntent().getStringExtra("notification_key") != null) {
            notification_key = getIntent().getStringExtra("notification_key");
        }

        if (getIntent().getStringExtra("email") != null && getIntent().getStringExtra("password") != null) {
            user_email = getIntent().getStringExtra("email");
            user_password = getIntent().getStringExtra("password");
            doLoginWithEmail(user_email, user_password);
        }

        email_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailLoginIntent = new Intent(ctx, EmailLoginActivity.class);
                emailLoginIntent.putExtra("notification_key", notification_key);
                emailLoginIntent.putExtra("from", from);
                startActivity(emailLoginIntent);
                finish();
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //facebook integration starts
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "fb");
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
                } else {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail());
                }
            }
        });

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                String token = loginResult.getAccessToken().getToken();
                String uid = loginResult.getAccessToken().getUserId();
                doLoginWithSocialMedia("fb", token, uid);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });

        //facebook integration ends

        //Gmail integration starts

        gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        addPermissionDialogMarshMallow();
                    } else {
                        saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "gmail");
                        signInWithGplus();
                    }
                } else {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail());
                }
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        mGoogleApiClient.connect();


        phone_accountKit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    addPermissionDialogMarshMallowForSMS();
                } else {
                    saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "fbkit");
                    goToLogin(true);
                }

            }
        });

//        AccessToken accessToken = AccountKit.getCurrentAccessToken();
//
//        if (accessToken != null) {
//            goToMyLoggedInActivity();
//        }
    }

    private void doLoginWithEmail(final String username, String password) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(LoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().postLoginEmail(getFromPrefs(SaregamaConstants.IMEI), username, password, "signin", new Callback<LogInPojo>() {
                @Override
                public void success(LogInPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            // analytics code for source of signin start
                            articleParams.put("sign_in_email", username);
                            articleParams.put("User_Device", "Android");
                            articleParams.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            articleParams.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            articleParams.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            articleParams.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));
                            FlurryAgent.logEvent("sign_in_source", articleParams);

                            Map<String, Object> eventValue = new HashMap<String, Object>();
                            eventValue.put("sign_in_email", username);
                            eventValue.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            eventValue.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            eventValue.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            eventValue.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));
                            AppsFlyerLib.getInstance().trackEvent(ctx, "Login", eventValue);

                            // analytics code for source of signin End
                            checkCommonURL(response.getUrl());
                            navigateToHomeActivity(basePojo);
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void doLoginWithSocialMedia(String source, String token, String socialId) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(LoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().postLoginSocialMedia(getFromPrefs(SaregamaConstants.IMEI), source, token, socialId, "signin", new Callback<LogInPojo>() {
                @Override
                public void success(LogInPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            // analytics code for source of registration start
                            articleParams.put("User_Device", "Android");
                            articleParams.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            articleParams.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            articleParams.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            articleParams.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));
                            Map<String, Object> eventValue = new HashMap<String, Object>();
                            // checks value of LOGGEDINFROM if fb then registration through Facebook otherwise registration through Google Plus
                            if (getFromPrefs(SaregamaConstants.LOGGEDINFROM).equals("fb")) {
                                articleParams.put("sign_in_fb", basePojo.getData().getListener().getEmail());
                                eventValue.put("sign_in_fb", basePojo.getData().getListener().getEmail());
                            } else if (getFromPrefs(SaregamaConstants.LOGGEDINFROM).equals("gmail")) {
                                articleParams.put("sign_in_gplus", basePojo.getData().getListener().getEmail());
                                eventValue.put("sign_in_gplus", basePojo.getData().getListener().getEmail());
                            }

                            eventValue.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            eventValue.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            eventValue.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            eventValue.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));

                            if (basePojo.getData().getListener().isReturning_user()) {
                                FlurryAgent.logEvent("sign_in_source", articleParams);
                                AppsFlyerLib.getInstance().trackEvent(ctx, "Login", eventValue);
                            } else {
                                FlurryAgent.logEvent("sign_up_source", articleParams);
                                AppsFlyerLib.getInstance().trackEvent(ctx, "SignUp", eventValue);
                            }
                            // analytics code for source of registration end

                            checkCommonURL(response.getUrl());
                            navigateToHomeActivity(basePojo);
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getServer_error());
                    }
                    if(!ctx.isFinishing() && d.isShowing())
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void navigateToHomeActivity(LogInPojo basePojo) {
        if (basePojo.getData().getListener().getId() != null && basePojo.getData().getListener().getSession_id() != null) {
            saveIntoPrefs(SaregamaConstants.EMAIL, basePojo.getData().getListener().getEmail());
            saveIntoPrefs(SaregamaConstants.NAME, basePojo.getData().getListener().getTitle());
            saveIntoPrefs(SaregamaConstants.MOBILE, basePojo.getData().getListener().getMobile());
            saveIntoPrefs(SaregamaConstants.SESSION_ID, basePojo.getData().getListener().getSession_id());
            saveIntoPrefs(SaregamaConstants.ID, basePojo.getData().getListener().getId());
            saveIntoPrefs(SaregamaConstants.CART, basePojo.getData().getCart());
            saveIntoPrefs(SaregamaConstants.DOWNLOAD, basePojo.getData().getDownload());
            saveIntoPrefs(SaregamaConstants.USERIMAGE, basePojo.getData().getListener().getImage());
            getCartDataFirstTime();
            getNotificationsSize();
            getNumberofDownload();
            getDownloadedCount();
            if (getFromPrefs(SaregamaConstants.IMEI) != null && getFromPrefs(SaregamaConstants.IMEI).length() > 0 && getFromPrefs(SaregamaConstants.SESSION_ID) != null)
                sendRegistrationId(getFromPrefs(SaregamaConstants.DEVICEID), getFromPrefs(SaregamaConstants.IMEI));
            saveIntoPrefs(SaregamaConstants.REFRESH_NEEDED, "yes");
            if (from != null && (from.equals("cart") || from.equals("sms_download_manager")))
                finish();
            else {
                if (notification_key.equals("download")) {
                    Intent mIntent = new Intent(ctx, DownloadManagerActivity.class);
                    startActivity(mIntent);
                } else {
                    boolean openHome = false;
                    for (int i = 0; i < SaregamaConstants.ACTIVITIES.size(); i++) {
                        if (SaregamaConstants.ACTIVITIES.get(i) != null && SaregamaConstants.ACTIVITIES.get(i).toString().contains(getResources().getString(R.string.package_name) + ".HomeActivity")) {
                            openHome = true;
                            break;
                        }
                    }
                    if (!openHome) {
                        Intent intent = new Intent(ctx, HomeActivity.class);
                        startActivity(intent);
                    }
                }
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, SaregamaConstants.LOGIN_ERROR);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == 1) {
            if (responseCode == RESULT_OK) {
                Intent mIntent = new Intent(ctx, HomeActivity.class);
                startActivity(mIntent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        } else if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }
            mIntentInProgress = false;
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }

        } else if (requestCode == 125 && responseCode == RESULT_OK) {
            Bundle extra = intent.getExtras();
            token = extra.getString("authtoken");
            getProfileInformation();
        } else if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = intent.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
                showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                } else {
                    toastMessage = String.format(
                            "Success:%s...",
                            loginResult.getAuthorizationCode().substring(0, 10));
                }

                goToMyLoggedInActivity();
            }
        } else {
            callbackManager.onActivityResult(requestCode, responseCode, intent);
        }
    }

    private void addPermissionDialogMarshMallow() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(GET_ACCOUNTS);
        resultCode = GET_ACCOUNTS_RESULT;


        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "gmail");
                    signInWithGplus();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    private void addPermissionDialogMarshMallowForSMS() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(RECEIVE_SMS);
        resultCode = RECEIVE_SMS_RESULT;


        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "fbkit");
                    goToLogin(true);
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if(permissionsRejected == null)
            permissionsRejected = new ArrayList<>();

        switch (requestCode) {
            case GET_ACCOUNTS_RESULT:
                if (hasPermission(GET_ACCOUNTS)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "gmail");
                    signInWithGplus();
                } else {
                    permissionsRejected.add(GET_ACCOUNTS);
                    clearMarkAsAsked(GET_ACCOUNTS);
                    String message = "permission for contact access was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;

            case RECEIVE_SMS_RESULT:
                if (hasPermission(RECEIVE_SMS)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "fbkit");
                    goToLogin(true);
                } else {
                    permissionsRejected.add(RECEIVE_SMS);
                    clearMarkAsAsked(RECEIVE_SMS);
                    String message = "permission for send and view SMS was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }
    }

    public void goToLogin(boolean isSMSLogin) {

        LoginType loginType = isSMSLogin ? LoginType.PHONE : LoginType.EMAIL;

        final Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        loginType,
                        AccountKitActivity.ResponseType.TOKEN);
        configurationBuilder.setTitleType(AccountKitActivity.TitleType.LOGIN);
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        this.startActivityForResult(intent, APP_REQUEST_CODE);
    }

    private void showErrorActivity(final AccountKitError error) {
        Log.println(Log.ASSERT, "AccountKit", error.toString());
    }

    private void goToMyLoggedInActivity() {
        /*Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);*/
        this.setUserInformation();
    }

    private void doLoginWithAccountKit(final String mobile) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(LoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().postLoginFacebookAccountKit(getFromPrefs(SaregamaConstants.IMEI), mobile, "fbkit", "signin", new Callback<LogInPojo>() {
                @Override
                public void success(LogInPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            // analytics code for source of registration start
                            articleParams.put("sign_in_fbkit", mobile);
                            articleParams.put("User_Device", "Android");
                            articleParams.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            articleParams.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            articleParams.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            articleParams.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));

                            Map<String, Object> eventValue = new HashMap<String, Object>();
                            // checks value of LOGGEDINFROM if fb then registration through Facebook otherwise registration through Google Plus
                            eventValue.put("sign_in_fbkit", mobile);
                            eventValue.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            eventValue.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            eventValue.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            eventValue.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));

                            if (basePojo.getData().getListener().isReturning_user()) {
                                FlurryAgent.logEvent("sign_in_source", articleParams);
                                AppsFlyerLib.getInstance().trackEvent(ctx, "Login", eventValue);
                            } else {
                                FlurryAgent.logEvent("sign_up_source", articleParams);
                                AppsFlyerLib.getInstance().trackEvent(ctx, "SignUp", eventValue);
                            }
                            // analytics code for source of registration end

                            checkCommonURL(response.getUrl());
                            navigateToHomeActivity(basePojo);
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    public void setUserInformation() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                // Get Account Kit ID
                String accountKitId = account.getId();
                Log.println(Log.ASSERT, "AccountKit", "ID: " + accountKitId);

                boolean SMSLoginMode = false;

                // Get phone number
                PhoneNumber phoneNumber = account.getPhoneNumber();

                String phoneNumberString = "";
                String phn = "";
                if (phoneNumber != null) {
                    phoneNumberString = phoneNumber.toString();
                    String country_code = account.getPhoneNumber().getCountryCode();
                    phn = account.getPhoneNumber().getPhoneNumber();
                    Log.println(Log.ASSERT, "AccountKit", "Phone: " + phoneNumberString);
                    SMSLoginMode = true;
                }

                // Get email
                String email = account.getEmail();
                Log.println(Log.ASSERT, "AccountKit", "Email: " + email);

                doLoginWithAccountKit(phn);
            }

            @Override
            public void onError(final AccountKitError error) {
                Log.println(Log.ASSERT, "AccountKit", "Error: " + error.toString());
            }
        });
    }

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // make sure to initiate connection
        mGoogleApiClient.connect();
    }

    private void resolveSignInError() {
        if (mConnectionResult != null && mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mSignInClicked = false;
        getProfileInformation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this,
                    0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = connectionResult;
            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

    private void getProfileInformation() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                        final String SCOPES = "oauth2:profile email";
//                        final String SCOPES = "oauth2:" +Scopes.PLUS_LOGIN+" "/*+Scopes.PROFILE+" "*/+Scopes.EMAIL;
//                        final String SCOPES = "oauth2:https://www.googleapis.com/auth/plus.login";

                        try {
                            token = GoogleAuthUtil.getToken(
                                    LoginActivity.this,
                                    Plus.AccountApi.getAccountName(mGoogleApiClient),
                                    SCOPES);

                            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                            googleId = currentPerson.getId();

                        } catch (SecurityException e) {
                            System.out.println("hh PERMISSION_NOT_GRANTED");
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("hh IOException ");
                } catch (UserRecoverableAuthException e) {
                    e.printStackTrace();
                    Intent recover = e.getIntent();
                    startActivityForResult(recover, 125);
                    System.out.println("hh UserRecoverableAuthException ");
                } catch (GoogleAuthException authEx) {
                    // The call is not ever expected to succeed
                    // assuming you have already verified that
                    // Google Play services is installed.
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (cd.isConnectingToInternet()) {
                    if (token != null)
                        doLoginWithSocialMedia("gp", token, googleId);
                }
            }
        }.execute();

    }
}
