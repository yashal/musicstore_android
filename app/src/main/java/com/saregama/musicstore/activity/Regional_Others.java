package com.saregama.musicstore.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.Mp3HindiSongAdapter;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.MP3HindiSongPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class Regional_Others extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {
    private Regional_Others ctx;
    private ConnectionDetector cd;
    private SaregamaDialogs dialog;
//    private EditText mp3song_search;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private int start = 0;
    private ArrayList<MP3HindiSongListPojo> arrdata;

    private CircularSeekBar seekbar;
    private MP3HindiSongListPojo data_playing;
    private ImageView previous_view;
    private ImageView imageView;

    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 5, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0;

    private String str_alphbet = "";
    private String asyncTaskUrl;
//    private boolean flag_editText = false;
//    private TextView no_result_found;
    private int c_type;
    private String search_word = "";
    /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regional__others);

        ctx = this;
        dialog = new SaregamaDialogs(this);
        cd = new ConnectionDetector(getApplicationContext());
        setDrawerAndToolbar("Others");

        mRecyclerView = (RecyclerView) findViewById(R.id.mp3songs_recycler_view);
//        mp3song_search = (EditText) findViewById(R.id.search_song);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        arrdata = new ArrayList<>();
        rowTextView = (TextView) findViewById(R.id.row_text_view);
        c_type = getIntent().getIntExtra("c_type", 1);

//        no_result_found = (TextView) findViewById(R.id.no_result_found);

        /* ****scrollable alphabet list code as per change request**** */
        rowTextView = (TextView) findViewById(R.id.row_text_view);
        alphabetslist = (ListView)findViewById(R.id.mp3_landing_list);

        if (getIntent().getStringExtra("notification_url") != null) {
            asyncTaskUrl = getIntent().getStringExtra("notification_url");
        } else {
            asyncTaskUrl = getIntent().getStringExtra("url");
        }

        /* ****scrollable alphabet list code as per change request start**** */
        final AlphabetListAdapter alpha_adapter = new AlphabetListAdapter(ctx, SaregamaConstants.items);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                str_alphbet =  convertAlphabet(SaregamaConstants.items[position].toLowerCase());
                arrdata.clear();
                alpha_adapter.setSelectedIndex(position);
                start = 0;
                new GetSongsList().execute();

                setBubblePosition(view, rowTextView,position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

        new GetSongsList().execute();

//        mp3song_search.setHint("Search Songs, Albums");
//        ctx.softKeyboardDoneClickListener(mp3song_search);
//
//        mp3song_search.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int starts, int before, int count) {
//
//                if (mp3song_search.getText().toString().trim().length() == 0 && flag_editText) {
//                    flag_editText = false;
//                    start = 0;
//                    previousTotal = 0;
//                    arrdata.clear();
//                    search_word = "";
//                    new GetSongsList().execute();
//                } else {
//                    if (mp3song_search.getText().toString().trim().length() > 2) {
//                        flag_editText = true;
//                        start = 0;
//                        previousTotal = 0;
//                        arrdata.clear();
//                        search_word = mp3song_search.getText().toString().trim();
//                        new GetSearchSongsList().execute(mp3song_search.getText().toString().trim().replaceAll(" ", "%20"));
//                    }
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        });
        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    /* **** scrollable alphabet list code as per change request comment previous code**** */
    /*public void showListBubble(View v) {
        hideSoftKeyboard();
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        arrdata.clear();
        str_alphbet = showListBubbleCommon(v, rowTextView);
//        if (mp3song_search.length() > 0){
//            mp3song_search.setText("");
//        } else {
            new GetSongsList().execute();
//        }
    }*/

    private void setAdapter(ArrayList<MP3HindiSongListPojo> arr) {
        mAdapter = new Mp3HindiSongAdapter(ctx, arr, "regional", search_word);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void addScrollListner() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
//                        if (mp3song_search.getText().toString().trim().length() > 2) {
//                            new GetSearchSongsList().execute(mp3song_search.getText().toString().trim().replaceAll(" ", "%20"));
//                        } else {
                            new GetSongsList().execute();
//                        }
                        loading = true;
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
//                AddToCartWithoutLogin(c_type+"", data2.getSong_id());
                CheckBox addToCart = (CheckBox)v.getTag(R.string.addtocart_id);

                QuickAction quickAction =   setupQuickAction(data_addtocart.getSong_id(), c_type);
                quickAction.show(addToCart);
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            default:
                break;
        }
    }

    @Override
    public void notifyAdapter() {
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    // Regional Other list
    private class GetSearchSongsList extends AsyncTask<String,Void,Void> {
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... querry) {

            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(SaregamaConstants.BASE_URL + "inner_search?type=search" + "&ctype=" + ctx.getAppConfigJson().getC_type().getSONG() + "&query=" + querry[0] + "&mode=" + "regional_films"+ "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), MP3HindiSongPojo.class);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
                            if (arrdata.size() > 0 && start > 0) {
                                arrdata.addAll(basePojo.getData().getList());
                            } else {
                                arrdata.clear();
                                arrdata = basePojo.getData().getList();
                                setAdapter(arrdata);
                                previousTotal = 0;
                            }
                        }
//                        no_result_found.setVisibility(View.GONE);
                        if (basePojo.getStatus()) {
                            mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), arrdata.size() - mAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);

//                        if (arrdata.size() == 0) {
//                            no_result_found.setText(SaregamaConstants.NO_RESULT);
//                            no_result_found.setVisibility(View.VISIBLE);
//                        } else {
//                            no_result_found.setVisibility(View.GONE);
//                        }
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }
    }

    class GetSongsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private MP3HindiSongPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                d = SaregamaDialogs.showLoading(Regional_Others.this);
                d.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(ctx.getAsyncTaskData(asyncTaskUrl + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT + "&alpha=" + str_alphbet), MP3HindiSongPojo.class);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {

                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
//                    no_result_found.setVisibility(View.GONE);
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getData().getList() != null) {
//                            mp3song_search.setText("");
                            if (arrdata.size() > 0) {
                                arrdata.addAll(basePojo.getData().getList());
                            } else {
                                arrdata.clear();
                                arrdata = basePojo.getData().getList();
                                setAdapter(arrdata);
                                previousTotal = 0;
                            }
                        }

                        if (basePojo.getStatus()) {
                            mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), arrdata.size() - mAdapter.getItemCount());
                            addScrollListner();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        start = 0;
                        firstVisibleItemPosition = 0;
                        mRecyclerView.clearOnScrollListeners();
                        mRecyclerView.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private void playMP3Song(ImageView imageView, View v) {
        hideSoftKeyboard();
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ctx.notifyAdapter();
        }
        if (data_playing.is_playing()) {
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ctx.notifyAdapter();
        } else {
            for (int i = 0; i < arrdata.size(); i++)
                arrdata.get(i).setIs_playing(false);
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            data_playing.setIs_playing(true);
            imageView.setImageResource(R.mipmap.pause_icon);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            ctx.notifyAdapter();
        }
        previous_view = imageView;
        seekbar.setVisibility(View.VISIBLE);
        playPauseButtonClickListener(c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }
}