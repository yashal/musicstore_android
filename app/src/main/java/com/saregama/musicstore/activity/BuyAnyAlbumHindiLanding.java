package com.saregama.musicstore.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.BuyAnyAlbumViewPagerAdapter;
import com.saregama.musicstore.pojo.MP3HindiAlbumListPojo;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;

public class BuyAnyAlbumHindiLanding extends BaseActivity implements View.OnClickListener {
    private LinearLayout album, geetmala, album_selector, geetmala_selector;
    private TextView album_text, geetmala_text;
    private ViewPager viewPager;
    private Activity ctx = this;
    private BuyAnyAlbumViewPagerAdapter mAdapter;

    private int fragment_pos = 0;
    private int c_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_any_album);

        setDrawerAndToolbarWithDropDown("Hindi Films");

        SaregamaConstants.ACTIVITIES.add(ctx);
        c_type = getIntent().getIntExtra("c_type", 1);

        mAdapter = new BuyAnyAlbumViewPagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(mAdapter);

        album = (LinearLayout) findViewById(R.id.tab1_layout);
        geetmala = (LinearLayout) findViewById(R.id.tab2_layout);
        LinearLayout tabs_layout_root = (LinearLayout) findViewById(R.id.tabs_layout_root);
//        geetmala.setVisibility(View.GONE);
//        album.setVisibility(View.GONE);
        tabs_layout_root.setVisibility(View.GONE);

        album_selector = (LinearLayout) findViewById(R.id.tab1_selector);
        geetmala_selector = (LinearLayout) findViewById(R.id.tab2_selector);

        album_text = (TextView) findViewById(R.id.tab1_text);
        geetmala_text = (TextView) findViewById(R.id.tab2_text);

        album_text.setText("Movies/Albums");
        geetmala_text.setText("Geetmala");

        album.setOnClickListener(this);
        geetmala.setOnClickListener(this);

//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//            @Override
//            public void onPageSelected(int position) {
//                setStyle(position);
//            }
//
//            @Override
//            public void onPageScrolled(int arg0, float arg1, int arg2) {
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int arg0) {
//            }
//        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab1_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.tab2_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            case R.id.albumLL:
                hideSoftKeyboard();
                Intent intent = new Intent(ctx, Mp3HindiAlbumDetail.class);
                MP3HindiAlbumListPojo data = (MP3HindiAlbumListPojo) v.getTag(R.string.data);
                intent.putExtra("album_id", data.getAlbum_id());
                intent.putExtra("c_type", c_type);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;

            case R.id.album_addtocart:
                MP3HindiAlbumListPojo data_album = (MP3HindiAlbumListPojo) v.getTag(R.string.data);

//                AddToCartWithoutLogin(c_type+"", data_album.getAlbum_id());
                CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);
                QuickAction quickActionAlbum = setupQuickActionAlbumLeft(data_album.getAlbum_id(), c_type, data_album.getCurrency(), data_album.getPrice(), data_album.getPrice_hd());
                quickActionAlbum.show(addToCart);
                break;

            default:
                break;
        }
    }

    private void setStyle(int position) {
        fragment_pos = position;
        if (position == 1) {
            album_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));

            geetmala_selector.setVisibility(View.VISIBLE);
            album_selector.setVisibility(View.GONE);
        } else if (position == 0) {
            album_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_selected_text_color));
            geetmala_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.header_text_color));

            geetmala_selector.setVisibility(View.GONE);
            album_selector.setVisibility(View.VISIBLE);
        }
    }

    /*public void showListBubble(View v) {
        if (fragment_pos == 0) {
            BuyAnyAlbum_AlbumFragment song_fragment = (BuyAnyAlbum_AlbumFragment) mAdapter.getFragment(fragment_pos);
            if (song_fragment != null)
                song_fragment.showListBubble(v);
        }
    }*/
}