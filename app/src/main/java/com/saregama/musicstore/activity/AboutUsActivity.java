package com.saregama.musicstore.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.saregama.musicstore.R;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

public class AboutUsActivity extends BaseActivity {

    private WebView webview;
    private AboutUsActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        if (getIntent().getStringExtra("url").contains("about_us"))
            setDrawerAndToolbarTitle("About Us");
        else if (getIntent().getStringExtra("url").contains("terms_of_service"))
            setDrawerAndToolbarTitle("Terms of Service");
        else if (getIntent().getStringExtra("url").contains("faq"))
            setDrawerAndToolbarTitle("FAQ");
        else if (getIntent().getStringExtra("url").contains("privacy_policy"))
            setDrawerAndToolbarTitle("Privacy Policy");
        else if (getIntent().getStringExtra("url").contains("contact_us"))
            setDrawerAndToolbarTitle("Contact Us");

        RelativeLayout cart_layout = (RelativeLayout) findViewById(R.id.cart_layout);
        RelativeLayout notification_layout = (RelativeLayout) findViewById(R.id.notification_layout);
        LinearLayout global_search = (LinearLayout) findViewById(R.id.global_search);
        cart_layout.setVisibility(View.GONE);
        global_search.setVisibility(View.GONE);
        notification_layout.setVisibility(View.GONE);

        SaregamaConstants.ACTIVITIES.add(ctx);

        webview = (WebView) findViewById(R.id.webview_about);

        final ProgressDialog alertDialog = SaregamaDialogs.showLoading(ctx);

        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 85) {
                    if (alertDialog != null && alertDialog.isShowing())
                        alertDialog.dismiss();
                }
            }
        });

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.toLowerCase().startsWith("http") || url.toLowerCase().startsWith("https") || url.toLowerCase().startsWith("file")) {
                    view.loadUrl(url);
                } else {
                    try {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.d("JSLogs", "Webview Error:" + e.getMessage());
                    }
                }
                return (true);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
        });

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        SaregamaDialogs dialog = new SaregamaDialogs(ctx);
        if (cd.isConnectingToInternet()) {
            openURL(getIntent().getStringExtra("url"));
        } else {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail()+"\n");
        }
    }

    private void openURL(String url) {
        webview.loadUrl(url);
        webview.requestFocus();
    }
}
