package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.AlphabetListAdapter;
import com.saregama.musicstore.adapter.ArtistRowGridAdapter;
import com.saregama.musicstore.adapter.SearchArtistDetailAdapter;
import com.saregama.musicstore.pojo.ArtistListPojo;
import com.saregama.musicstore.pojo.ArtistPojo;
import com.saregama.musicstore.pojo.MP3HindiSongListPojo;
import com.saregama.musicstore.pojo.Mp3ArtistPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.NotifyAdapterInterface;
import com.saregama.musicstore.util.QuickAction;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;
import com.saregama.musicstore.views.CustomTextView;

import java.util.ArrayList;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class SearchArtistSongListActivity extends BaseActivity implements View.OnClickListener, NotifyAdapterInterface {

    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;
    private RecyclerView mRecyclerView, mRecyclerViewDialog;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private String search_artist_name;
    private int totalItemCount, firstVisibleItemPosition = 0, visibleThreshold = 1, visibleItemCount;
    private boolean loading = true;
    private int previousTotal = 0, start = 0;
    private int role = 0;
    private CustomTextView dialog_all, dialog_popular;
    private ArrayList<MP3HindiSongListPojo> arrdata;
    private GridView artistGrid;
    private ArrayList<ArtistListPojo> arr_main, arr_all;
    private ArrayList<ArtistListPojo> artistNameArr, artistIDsArr;
    private Dialog artiste_singers_dialog, artiste_list_dialog;
    private LinearLayout mp3hindi_artist_ActorLL, mp3hindi_artist_SingerLL, mp3hindi_artist_musicLL;
    private boolean dialog_object = false;
    private String str_alphbet = "";
    private CustomTextView actor_count, singer_count, composer_count, total_songs;
    private String search_string = "";
    private ArtistListAdapter adapter;
    private ArrayList<ArtistListPojo> arrActor, arrSinger, arrComposer;
    private Mp3ArtistPojo basePojo;
    //    private TextView rowTextView;
    private EditText search_song;
    private CircularSeekBar seekbar;
    private MP3HindiSongListPojo data_playing;
    private ImageView previous_view;
    private ImageView imageView;
    private Typeface face_normal, face_bold;
    private LinearLayout alpha_layout;
    private boolean mFlag = false;
    private int c_type;

    /* ****scrollable alphabet list code as per change request**** */
    private TextView rowTextView;
    private ListView alphabetslist;
    private AlphabetListAdapter alpha_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_artist_song_list);
        setDrawerAndToolbar("Hindi Films");

        dialog = new SaregamaDialogs(ctx);

        arrdata = new ArrayList<>();
        c_type = getIntent().getIntExtra("c_type", 1);
        search_song = (EditText) findViewById(R.id.search_song);
        total_songs = (CustomTextView) findViewById(R.id.total_songs);

        search_string = getIntent().getStringExtra("search_string");
        search_artist_name = getIntent().getStringExtra("search_artist_name");
        arrActor = (ArrayList<ArtistListPojo>) getIntent().getSerializableExtra("arrActor");
        arrSinger = (ArrayList<ArtistListPojo>) getIntent().getSerializableExtra("arrSinger");
        arrComposer = (ArrayList<ArtistListPojo>) getIntent().getSerializableExtra("arrComposer");

        face_normal = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/SourceSansPro_Regular.otf");
        face_bold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/SourceSansPro_Semibold.otf");

        search_song.setHint("Search Songs");

        artistGrid = (GridView) findViewById(R.id.artist_name_grid);

        arrdata = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.songs_list_artist);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(getApplicationContext());

        arr_main = new ArrayList<>();
        arr_all = new ArrayList<>();

        new GetArtistSongList().execute();

        getArtistNameIdArray();

        artistGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == artistNameArr.size()) {
                    DisplayDialog();
                } else {
                    hideSoftKeyboard();
                    if (artistNameArr.size() > 1) {
                        search_song.setText("");
                        String removableID = "" + artistIDsArr.get(position).getId();
                        String removableName = "" + artistNameArr.get(position).getId();
                        int role = artistNameArr.get(position).getRole();
                        artistNameArr.remove(position);
                        artistIDsArr.remove(position);

                        removeDataFromGridFilter(role, removableID);
                    } else
                        dialog.displayCommonDialogWithHeaderSmall("Remove Artiste", getAppConfigJson().getRemove_artist());
                }
            }
        });

        final View rootView = findViewById(R.id.root_view);

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
//                int heightDiff = rootView.getRootView().getHeight() - (r.bottom - r.top);
                int heightDiffa = rootView.getRootView().getHeight() - rootView.getHeight();
                if (heightDiffa > getResources().getInteger(R.integer.artist_height)) {
                    artistGrid.setVisibility(View.GONE);
                } else {
                    artistGrid.setVisibility(View.VISIBLE);
                }
            }
        });

        setOnTextChangeListener();

        softKeyboardDoneClickListener(search_song);

        // for play songs
        seekbar = (CircularSeekBar) findViewById(R.id.seek_bar);
    }

    private void addScrollListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {

                        start += SaregamaConstants.LIMIT;
                        new GetArtistSongList().execute();
                        loading = true;
                    }
                }
            }
        });
    }

    private void setOnTextChangeListener() {
        search_song.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (search_song.getText().toString().length() > 0 && arrdata != null) {
                    ArrayList<MP3HindiSongListPojo> arrFiltered = new ArrayList<>();
                    for (int i = 0; i < arrdata.size(); i++) {
                        if (arrdata.get(i).getSong_name() != null && arrdata.get(i).getSong_name().toLowerCase().contains(search_song.getText().toString().toLowerCase()) || arrdata.get(i).getAlbum_name().toLowerCase().contains(search_song.getText().toString().toLowerCase()))
                            arrFiltered.add(arrdata.get(i));
                    }
                    total_songs.setText(arrFiltered.size() + " Songs");
                    setAdapter(arrFiltered);
                } else if (basePojo.getData() != null) {
                    total_songs.setText(basePojo.getData().getCount() + " Songs");
                    setAdapter(arrdata);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void setAdapter(ArrayList<MP3HindiSongListPojo> arr) {
        mAdapter = new SearchArtistDetailAdapter(ctx, arr, search_song.getText().toString().toLowerCase());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addtocart:
                MP3HindiSongListPojo data_addtocart = (MP3HindiSongListPojo) v.getTag(R.string.data);
//                AddToCartWithoutLogin(c_type+"", data2.getSong_id());
                CheckBox addToCart = (CheckBox) v.getTag(R.string.addtocart_id);

                QuickAction quickAction = setupQuickAction(data_addtocart.getSong_id(), c_type);
                quickAction.show(addToCart);
                break;

            case R.id.playIcon:
                imageView = (ImageView) v;
                playMP3Song(imageView, v);
                break;

            case R.id.mp3hindi_songlist_songname:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            case R.id.slider_transparent:
                imageView = (ImageView) v.getTag(R.string.seekbar_id);
                playMP3Song(imageView, v);
                break;

            default:
                break;
        }
    }

    @Override
    public void notifyAdapter() {
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    class GetArtistSongList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            d = SaregamaDialogs.showLoading(SearchArtistSongListActivity.this);
            d.setCanceledOnTouchOutside(false);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(getAsyncTaskData(SaregamaConstants.BASE_URL + "artistsong?id=" + search_string + "&start=" + start + "&limit=" + SaregamaConstants.LIMIT), Mp3ArtistPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (d != null && d.isShowing()) {
                d.dismiss();
            }

            if (basePojo != null) {
                if (basePojo.getData().getList() != null) {
                    if (arrdata.size() > 0 && start > 0) {
                        arrdata.addAll(basePojo.getData().getList());
                    } else {
                        arrdata.clear();
                        arrdata = basePojo.getData().getList();
                        setAdapter(arrdata);
                        previousTotal = 0;
                    }
                }

                if (basePojo.getStatus()) {
                    total_songs.setText(basePojo.getData().getCount() + " Songs");
                    if (basePojo.getData().getCount() > 0) {
                        mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), arrdata.size() - mAdapter.getItemCount());
                        if (basePojo.getData().getCount() >= SaregamaConstants.LIMIT)
                            addScrollListener();

                        if (artistNameArr.size() <= 5) {
                            ViewGroup.LayoutParams params = artistGrid.getLayoutParams();
                            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            artistGrid.setLayoutParams(params);
                        } else if (artistNameArr.size() > 5) {
                            ViewGroup.LayoutParams params = artistGrid.getLayoutParams();
                            params.height = getResources().getInteger(R.integer.artist_layout_height);
                            artistGrid.setLayoutParams(params);
                        }

                    } else {
                        start = 0;
                        previousTotal = 0;
                        mRecyclerView.clearOnScrollListeners();
                        if (basePojo.getData().getCount() == 0)
                            Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(basePojo.getError());
                }
            } else {
                dialog.displayCommonDialog(getAppConfigJson().getServer_error());
            }
        }
    }

    private void DisplayDialog() {
        artiste_singers_dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        artiste_singers_dialog.setContentView(R.layout.artist_list_dialog);

        mp3hindi_artist_musicLL = (LinearLayout) artiste_singers_dialog.findViewById(R.id.artist_list_dialog_musicLL);
        mp3hindi_artist_SingerLL = (LinearLayout) artiste_singers_dialog.findViewById(R.id.artist_list_dialog_SingerLL);
        mp3hindi_artist_ActorLL = (LinearLayout) artiste_singers_dialog.findViewById(R.id.artist_list_dialog_ActorLL);
        TextView artist_list_dialog_search = (TextView) artiste_singers_dialog.findViewById(R.id.artist_list_dialog_search);
        actor_count = (CustomTextView) artiste_singers_dialog.findViewById(R.id.actor_count);
        singer_count = (CustomTextView) artiste_singers_dialog.findViewById(R.id.singer_count);
        composer_count = (CustomTextView) artiste_singers_dialog.findViewById(R.id.composer_count);

        if (search_string.contains("~1")) {
            role = 1;
            addCountInLayout();
        }
        if (search_string.contains("~8")) {
            role = 8;
            addCountInLayout();
        }
        if (search_string.contains("~2")) {
            role = 2;
            addCountInLayout();
        }

        artist_list_dialog_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((arrActor != null && arrActor.size() > 0) || (arrSinger != null && arrSinger.size() > 0) || (arrComposer != null && arrComposer.size() > 0)) {
                    generateSearchString();
                    artiste_singers_dialog.dismiss();
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getArtist_search());
                }
            }
        });
        mp3hindi_artist_ActorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                role = 1;
                arr_main.clear();
                new GetPopularArtisteList().execute(role);
            }
        });

        mp3hindi_artist_SingerLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                role = 8;
                arr_main.clear();
                new GetPopularArtisteList().execute(role);
            }
        });

        mp3hindi_artist_musicLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                role = 2;
                arr_main.clear();
                new GetPopularArtisteList().execute(role);
            }
        });

        Window window = artiste_singers_dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);

        artiste_singers_dialog.show();
    }

    private class GetPopularArtisteList extends AsyncTask<Integer, Integer, Void> {
        ProgressDialog d;
        private ArtistPojo basePojo;

        @Override
        protected void onPreExecute() {
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(ctx);
                d.setCanceledOnTouchOutside(false);
            } else {
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }
            }
        }

        @Override
        protected Void doInBackground(Integer... role) {
            if (isCancelled())
                return null;
            if (dialog_object) {
                if (!artiste_list_dialog.isShowing()) {
                    start = 0;
                    str_alphbet = "";
                }
            }

            Gson gsonObj = new Gson();

            basePojo = gsonObj.fromJson(getAsyncTaskData(SaregamaConstants.BASE_URL + "artist?role=" + role[0] + "&start=" + start + "&limit=" + SaregamaConstants.ARTIST_LIMIT + "&d_type=" + getIntFromPrefs(SaregamaConstants.D_TYPE) + "&order=" + SaregamaConstants.ORDER + "&alpha=" + str_alphbet), ArtistPojo.class);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (rowTextView != null)
                rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {

                            if (dialog_object && !artiste_list_dialog.isShowing())
                                arr_main.clear();
                            if (basePojo.getData().getList() != null) {
                                if (arr_main.size() > 0)
                                    arr_main.addAll(basePojo.getData().getList());
                                else
                                    arr_main = basePojo.getData().getList();
                            }

                            generateArrayList();

                            if (dialog_object) {
                                if (!artiste_list_dialog.isShowing()) {
                                    loading = false;
                                    previousTotal = 0;
                                    displayDialog();
                                } else {
                                    setAdapterArtiste(arr_all);
                                    mRecyclerView.scrollToPosition(firstVisibleItemPosition);
                                    addScrollListnerDialog();
                                }
                            } else
                                displayDialog();

                        } else {
                            firstVisibleItemPosition = 0;
                            start = 0;
                            mRecyclerView.clearOnScrollListeners();
                            mRecyclerView.scrollToPosition(0);
                            Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private class GetArtisteList extends AsyncTask<Integer, Integer, Void> {
        ProgressDialog d;
        private ArtistPojo basePojo;

        @Override
        protected void onPreExecute() {
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                d = SaregamaDialogs.showLoading(ctx);
                d.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            }
        }

        @Override
        protected Void doInBackground(Integer... role) {
            if (isCancelled())
                return null;
            if (dialog_object) {
                if (!artiste_list_dialog.isShowing()) {
                    start = 0;
                    str_alphbet = "";
                }
            }

            Gson gsonObj = new Gson();

            basePojo = gsonObj.fromJson(getAsyncTaskData(SaregamaConstants.BASE_URL + "artist?role=" + role[0] + "&start=" + start + "&limit=" + SaregamaConstants.ARTIST_LIMIT + "&d_type=" + getIntFromPrefs(SaregamaConstants.D_TYPE) + "&alpha=" + str_alphbet), ArtistPojo.class);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (d != null && d.isShowing()) {
                d.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (rowTextView != null)
                rowTextView.setVisibility(View.GONE);
            if (cd.isConnectingToInternet()) {
                if (d != null && d.isShowing()) {
                    d.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getData().getCount() > 0) {
                        if (basePojo.getStatus()) {
                            if (dialog_object && !artiste_list_dialog.isShowing())
                                arr_main.clear();
                            if (basePojo.getData().getList() != null) {
                                if (arr_main.size() > 0)
                                    arr_main.addAll(basePojo.getData().getList());
                                else
                                    arr_main = basePojo.getData().getList();
                            }
                            generateArrayList();

                            if (dialog_object) {
                                if (!artiste_list_dialog.isShowing()) {
                                    loading = false;
                                    previousTotal = 0;
                                    displayDialog();
                                } else {
                                    setAdapterArtiste(arr_all);
                                    mRecyclerViewDialog.scrollToPosition(firstVisibleItemPosition);
                                    addScrollListnerDialog();
                                }
                            } else
                                displayDialog();
                        } else {
                            dialog.displayCommonDialog(basePojo.getError());
                        }
                    } else {
                        firstVisibleItemPosition = 0;
                        start = 0;
                        mRecyclerViewDialog.clearOnScrollListeners();
                        mRecyclerViewDialog.scrollToPosition(0);
                        Toast.makeText(ctx, SaregamaConstants.NO_DATA, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }
    }

    private void addScrollListnerDialog() {
        mRecyclerViewDialog.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) //check for scroll down
                {
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItemPosition + visibleThreshold)) {
                        start += SaregamaConstants.ARTIST_LIMIT;
                        if (mFlag) {
                            new GetArtisteList().execute(role);
                        } else {
                            new GetPopularArtisteList().execute(role);
                        }
                        loading = true;
                    }
                }
            }
        });
    }

    private void displayDialog() {

        artiste_list_dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        artiste_list_dialog.setContentView(R.layout.artist_data_dialog);
        dialog_object = true;

        mRecyclerViewDialog = (RecyclerView) artiste_list_dialog.findViewById(R.id.mp3songs_artistdialog_recycler);
        TextView dialog_done = (TextView) artiste_list_dialog.findViewById(R.id.dialog_done);
        CustomTextView dialog_header = (CustomTextView) artiste_list_dialog.findViewById(R.id.artistdialog_header);
        ImageView close_dialog = (ImageView) artiste_list_dialog.findViewById(R.id.close_dialog);
        dialog_all = (CustomTextView) artiste_list_dialog.findViewById(R.id.artist_dialog_alltext);
        dialog_popular = (CustomTextView) artiste_list_dialog.findViewById(R.id.artist_dialog_populartext);
        rowTextView = (TextView) artiste_list_dialog.findViewById(R.id.row_text_view);
        alphabetslist = (ListView) artiste_list_dialog.findViewById(R.id.mp3_landing_list);
        alpha_layout = (LinearLayout) artiste_list_dialog.findViewById(R.id.alpha_layout);
        final EditText search_artist_song = (EditText) artiste_list_dialog.findViewById(R.id.search_artist_song);

        if (ctx != null)
            softKeyboardDoneClickListener(search_artist_song);

         /* ****scrollable alphabet list code as per change request start**** */
        alpha_adapter = new AlphabetListAdapter(ctx, SaregamaConstants.artist_arr);
        alphabetslist.setVisibility(View.VISIBLE);
        alphabetslist.setAdapter(alpha_adapter);

        alphabetslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                str_alphbet = SaregamaConstants.artist_arr[position].toLowerCase();
//                str_alphbet =  convertAlphabet(SaregamaConstants.items_artist[position].toLowerCase());
                alpha_adapter.setSelectedIndex(position);
                arr_main.clear();
                arr_all.clear();
                start = 0;
                previousTotal = 0;
                if (mFlag) {
                    new GetArtisteList().execute(role);
                } else {
                    new GetPopularArtisteList().execute(role);
                }

                setBubblePositionforArtiste(view, rowTextView, position);
            }
        });

         /* ****scrollable alphabet list code as per change request end**** */

//        setAlphabetClickListener();
        setStyle(1);
        artiste_list_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (role == 1) {
            dialog_header.setText("Actors");
        } else if (role == 8) {
            dialog_header.setText("Singers");
        } else {
            dialog_header.setText("Music Directors");
        }
        mRecyclerViewDialog.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerViewDialog.setLayoutManager(mLayoutManager);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                artiste_list_dialog.dismiss();
                hideSoftKeyboard();
            }
        });

        artiste_list_dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                removeFromArray();
            }
        });

        dialog_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCountInLayout();
                str_alphbet = "";
                artiste_list_dialog.dismiss();
            }
        });

        dialog_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFlag = true;
                start = 0;
                previousTotal = 0;
                totalItemCount = 0;
                visibleItemCount = 0;
                str_alphbet = "";
                setStyle(0);
                arr_main.clear();
                alpha_adapter = new AlphabetListAdapter(ctx, SaregamaConstants.artist_arr);
                alphabetslist.setAdapter(alpha_adapter);
                new GetArtisteList().execute(role);
                addScrollListnerDialog();

            }
        });
        dialog_popular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFlag = false;
                start = 0;
                previousTotal = 0;
                totalItemCount = 0;
                visibleItemCount = 0;
                str_alphbet = "";
                setStyle(1);
                arr_main.clear();
                new GetPopularArtisteList().execute(role);
                addScrollListnerDialog();

            }
        });

        search_artist_song.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                TextView no_result_found = (TextView) view.findViewById(R.id.no_result_found);

                if (search_artist_song.getText().toString().trim().length() > 2) {
                    String search_keyword = search_artist_song.getText().toString().trim();
                    search_keyword = search_keyword.replaceAll(" ", "%20");
                    new SearchArtisteList(role, search_keyword).execute();
                } else {
//                    if (no_result_found != null)
//                        no_result_found.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        setAdapterArtiste(arr_all);
        mRecyclerViewDialog.scrollToPosition(firstVisibleItemPosition);

        addScrollListnerDialog();

        artiste_list_dialog.show();
    }

    public String convertAlphabet(String alphabet) {
        if (alphabet.equals("#"))
            alphabet = SaregamaConstants.SPECIAL;
        else if (alphabet.equalsIgnoreCase("ALL"))
            alphabet = SaregamaConstants.ALL;

        return alphabet;
    }

    private void setStyle(int position) {
        if (position == 0) {
            if (alphabetslist != null) {
                alphabetslist.setVisibility(View.VISIBLE);
                alpha_adapter = new AlphabetListAdapter(ctx, SaregamaConstants.artist_arr);
                alphabetslist.setAdapter(alpha_adapter);
            }
            dialog_all.setTextColor(ContextCompat.getColor(ctx, R.color.white));
            dialog_popular.setTextColor(ContextCompat.getColor(ctx, R.color.black));
            dialog_all.setBackgroundResource(R.drawable.artist_all_tab_layout);
            dialog_popular.setBackgroundResource(R.drawable.artist_popular_tab_layout);
        } else if (position == 1) {
            if (alphabetslist != null) {
                alphabetslist.setVisibility(View.GONE);
            }
            dialog_all.setTextColor(ContextCompat.getColor(ctx, R.color.black));
            dialog_popular.setTextColor(ContextCompat.getColor(ctx, R.color.white));
            dialog_all.setBackgroundResource(R.drawable.artist_popular_tab_layout);
            dialog_popular.setBackgroundResource(R.drawable.artist_all_tab_layout);
        }
    }

    private void setAlphabetClickListener() {
        LinearLayout linearAll = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearAll);
        linearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        linearAll.setVisibility(View.GONE);
        LinearLayout linearHash = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearHash);
        linearHash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        linearHash.setVisibility(View.GONE);
        LinearLayout linearA = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearA);
        linearA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearB = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearB);
        linearB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearC = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearC);
        linearC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearD = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearD);
        linearD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearE = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearE);
        linearE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearF = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearF);
        linearF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });

        LinearLayout linearG = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearG);
        linearG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearH = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearH);
        linearH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearI = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearI);
        linearI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearJ = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearJ);
        linearJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearK = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearK);
        linearK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearL = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearL);
        linearL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearM = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearM);
        linearM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });


        LinearLayout linearN = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearN);
        linearN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearO = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearO);
        linearO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearP = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearP);
        linearP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearQ = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearQ);
        linearQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearR = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearR);
        linearR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearS = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearS);
        linearS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });

        LinearLayout linearT = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearT);
        linearT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearU = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearU);
        linearU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearV = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearV);
        linearV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearW = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearW);
        linearW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearX = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearX);
        linearX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearY = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearY);
        linearY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
        LinearLayout linearZ = (LinearLayout) artiste_list_dialog.findViewById(R.id.linearZ);
        linearZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListBubble(view);
            }
        });
    }

    public void showListBubble(View v) {
        rowTextView.setVisibility(View.VISIBLE);
        start = 0;
        previousTotal = 0;
        arr_main.clear();
        arr_all.clear();
        str_alphbet = showListBubbleCommon(v, rowTextView);

        if (mFlag) {
            new GetArtisteList().execute(role);
        } else {
            new GetPopularArtisteList().execute(role);
        }
    }

    // abcd click listener
    public String showListBubbleCommon(View v, TextView rowTextView) {
        //find the index of the separator row view
        rowTextView = (TextView) artiste_list_dialog.findViewById(R.id.row_text_view);
        int count = alpha_layout.getChildCount();
        String alphabet = "";
        for (int i = 0; i < count; i++) {
            View inner_layout = alpha_layout.getChildAt(i);
            View view = ((ViewGroup) inner_layout).getChildAt(0);

            if (view.isPressed()) {
                rowTextView.setText(view.getTag() + "");
                alphabet = (String) view.getTag();
                rowTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.alphabetic_text_size));
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(ctx, R.drawable.pressed));
                view.setBackgroundResource(R.drawable.pressed);
            } else {
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{}, ContextCompat.getDrawable(ctx, R.drawable.normal));
                view.setBackgroundResource(R.drawable.normal);
            }
        }

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, v.getTop() - 5, 0, 0);
        rowTextView.setLayoutParams(layoutParams);
        rowTextView.setPadding(0, 0, 6, 0);
        rowTextView.setVisibility(View.VISIBLE);
        return alphabet;
    }

    public class ArtistListAdapter extends RecyclerView.Adapter<ArtistListAdapter.ViewHolder> {

        private ArrayList<ArtistListPojo> arrayArtistList;
        private Activity context;

        public ArtistListAdapter(Activity context, ArrayList<ArtistListPojo> arrayArtistList) {
            this.context = context;
            this.arrayArtistList = arrayArtistList;
        }

        @Override
        public ArtistListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.artist_row_layout, parent, false);
            v.setOnClickListener(new MyOnClickListener());
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ArtistListAdapter.ViewHolder holder, int position) {
            final ArtistListPojo data = arrayArtistList.get(position);

            holder.artist_name.setText(data.getName());
            if (role == 1) {
                for (int i = 0; i < arrActor.size(); i++) {
                    if (data.getId().equals(arrActor.get(i).getId()) && !arrActor.get(i).isRemoved()) {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist_selected);
                        holder.check.setChecked(true);
                        holder.artist_name.setTextColor(ContextCompat.getColor(SearchArtistSongListActivity.this, R.color.light_black));
                        holder.artist_name.setTypeface(face_bold);
                        break;
                    } else {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist);
                        holder.check.setChecked(false);
                        holder.artist_name.setTextColor(ContextCompat.getColor(SearchArtistSongListActivity.this, R.color.header_selected_text_color));
                        holder.artist_name.setTypeface(face_normal);
                    }
                }
            } else if (role == 8) {
                for (int i = 0; i < arrSinger.size(); i++) {
                    if (data.getId().equals(arrSinger.get(i).getId()) && !arrSinger.get(i).isRemoved()) {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist_selected);
                        holder.check.setChecked(true);
                        holder.artist_name.setTextColor(ContextCompat.getColor(SearchArtistSongListActivity.this, R.color.light_black));
                        holder.artist_name.setTypeface(face_bold);
                        break;
                    } else {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist);
                        holder.check.setChecked(false);
                        holder.artist_name.setTextColor(ContextCompat.getColor(SearchArtistSongListActivity.this, R.color.header_selected_text_color));
                        holder.artist_name.setTypeface(face_normal);
                    }
                }
            } else {
                for (int i = 0; i < arrComposer.size(); i++) {
                    if (data.getId().equals(arrComposer.get(i).getId()) && !arrComposer.get(i).isRemoved()) {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist_selected);
                        holder.check.setChecked(true);
                        holder.artist_name.setTextColor(ContextCompat.getColor(SearchArtistSongListActivity.this, R.color.light_black));
                        holder.artist_name.setTypeface(face_bold);
                        break;
                    } else {
                        holder.check.setButtonDrawable(R.mipmap.check_box_artist);
                        holder.check.setChecked(false);
                        holder.artist_name.setTextColor(ContextCompat.getColor(SearchArtistSongListActivity.this, R.color.header_selected_text_color));
                        holder.artist_name.setTypeface(face_normal);
                    }
                }
            }
        }

        class MyOnClickListener implements View.OnClickListener {
            @Override
            public void onClick(View v) {
                int itemPosition = mRecyclerView.getChildPosition(v);
                final ArtistListPojo data = arrayArtistList.get(itemPosition);
                CheckBox check = (CheckBox) v.findViewById(R.id.check_box);
                TextView artist_name = (TextView) v.findViewById(R.id.artist_name);
                if (!check.isChecked()) {
                    check.setButtonDrawable(R.mipmap.check_box_artist_selected);
                    check.setChecked(true);
                    addDataInString(data.getId(), data.getName());
                    artist_name.setTextColor(ContextCompat.getColor(SearchArtistSongListActivity.this, R.color.light_black));
                    artist_name.setTypeface(face_bold);
                } else {
                    check.setButtonDrawable(R.mipmap.check_box_artist);
                    check.setChecked(false);
                    removeDataFromString(data.getId(), data.getName());
                    artist_name.setTextColor(ContextCompat.getColor(SearchArtistSongListActivity.this, R.color.header_selected_text_color));
                    artist_name.setTypeface(face_normal);
                }
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private TextView artist_name;
            private CheckBox check;

            public ViewHolder(View itemView) {
                super(itemView);
                artist_name = (TextView) itemView.findViewById(R.id.artist_name);
                check = (CheckBox) itemView.findViewById(R.id.check_box);
            }
        }

        @Override
        public int getItemCount() {
            return arrayArtistList.size();
        }
    }

    private void removeDataFromGridFilter(int role, String id) {
        if (role == 1) {
            for (int i = 0; i < arrActor.size(); i++) {
                if (arrActor.get(i).getId().equals(id)) {
                    arrActor.remove(i);
                    break;
                }
            }
        } else if (role == 8) {
            for (int i = 0; i < arrSinger.size(); i++) {
                if (arrSinger.get(i).getId().equals(id)) {
                    arrSinger.remove(i);
                    break;
                }
            }
        } else {
            for (int i = 0; i < arrComposer.size(); i++) {
                if (arrComposer.get(i).getId().equals(id)) {
                    arrComposer.remove(i);
                    break;
                }
            }
        }

        generateSearchString();
    }

    private void removeDataFromString(String id, String name) {
        if (role == 1) {
            for (int i = 0; i < arrActor.size(); i++) {
                if (arrActor.get(i).getId().equals(id)) {
                    if (arrActor.get(i).isDone())
                        arrActor.get(i).setRemoved(true);
                    else
                        arrActor.remove(i);
                    break;
                }
            }
        } else if (role == 8) {
            for (int i = 0; i < arrSinger.size(); i++) {
                if (arrSinger.get(i).getId().equals(id)) {
                    if (arrSinger.get(i).isDone())
                        arrSinger.get(i).setRemoved(true);
                    else
                        arrSinger.remove(i);
                    break;
                }
            }
        } else {
            for (int i = 0; i < arrComposer.size(); i++) {
                if (arrComposer.get(i).getId().equals(id)) {
                    if (arrComposer.get(i).isDone())
                        arrComposer.get(i).setRemoved(true);
                    else
                        arrComposer.remove(i);
                    break;
                }
            }
        }
    }

    private void addDataInString(String id, String name) {
        ArtistListPojo obj = new ArtistListPojo();
        obj.setSelected(true);
        obj.setDone(false);
        obj.setId(id);
        obj.setName(name);
        obj.setRole(role);

        if (role == 1) {
            boolean add = true;
            for (int i = 0; i < arrActor.size(); i++) {
                if (arrActor.get(i).getId().equals(id)) {
                    add = false;
                    arrActor.get(i).setRemoved(false);
                    break;
                }
            }
            if (add)
                arrActor.add(obj);
        } else if (role == 8) {
            boolean add = true;
            for (int i = 0; i < arrSinger.size(); i++) {
                if (arrSinger.get(i).getId().equals(id)) {
                    add = false;
                    arrSinger.get(i).setRemoved(false);
                    break;
                }
            }
            if (add)
                arrSinger.add(obj);
        } else {
            boolean add = true;
            for (int i = 0; i < arrComposer.size(); i++) {
                if (arrComposer.get(i).getId().equals(id)) {
                    add = false;
                    arrComposer.get(i).setRemoved(false);
                    break;
                }
            }
            if (add)
                arrComposer.add(obj);
        }
        generateArrayList();
    }

    private void removeFromArray() {
        if (role == 1) {
            if (arrActor != null && arrActor.size() > 0) {
                for (int i = arrActor.size() - 1; i >= 0; i--) {
                    arrActor.get(i).setRemoved(false);
                    if (!arrActor.get(i).isDone()) {
                        arrActor.remove(i);
                    }
                }
            }
        } else if (role == 8) {
            if (arrSinger != null && arrSinger.size() > 0) {
                for (int i = arrSinger.size() - 1; i >= 0; i--) {
                    arrSinger.get(i).setRemoved(false);
                    if (!arrSinger.get(i).isDone()) {
                        arrSinger.remove(i);
                    }
                }
            }
        } else {
            if (arrComposer != null && arrComposer.size() > 0) {
                for (int i = arrComposer.size() - 1; i >= 0; i--) {
                    arrComposer.get(i).setRemoved(false);
                    if (!arrComposer.get(i).isDone()) {
                        arrComposer.remove(i);
                    }
                }
            }
        }

        addCountInLayout();
    }

    private void addCountInLayout() {
        if (role == 1) {
            if (arrActor != null && arrActor.size() > 0) {
                for (int i = arrActor.size() - 1; i >= 0; i--) {
                    arrActor.get(i).setDone(true);
                    if (arrActor.get(i).isRemoved()) {
                        arrActor.remove(i);
                    }
                }
                actor_count.setVisibility(View.VISIBLE);
                actor_count.setText(arrActor.size() + "");
                if (arrActor.size() == 0)
                    actor_count.setVisibility(View.GONE);
            } else
                actor_count.setVisibility(View.GONE);
        } else if (role == 8) {
            if (arrSinger != null && arrSinger.size() > 0) {
                for (int i = arrSinger.size() - 1; i >= 0; i--) {
                    arrSinger.get(i).setDone(true);
                    if (arrSinger.get(i).isRemoved()) {
                        arrSinger.remove(i);
                    }
                }
                singer_count.setVisibility(View.VISIBLE);
                singer_count.setText(arrSinger.size() + "");
            } else
                singer_count.setVisibility(View.GONE);
        } else {
            if (arrComposer != null && arrComposer.size() > 0) {
                for (int i = arrComposer.size() - 1; i >= 0; i--) {
                    arrComposer.get(i).setDone(true);
                    if (arrComposer.get(i).isRemoved()) {
                        arrComposer.remove(i);
                    }
                    composer_count.setVisibility(View.VISIBLE);
                    composer_count.setText(arrComposer.size() + "");
                }
            } else
                composer_count.setVisibility(View.GONE);

        }
    }

    private void generateSearchString() {
        search_string = "";
        search_artist_name = "";
        if (arrActor != null && arrActor.size() > 0) {
            for (int i = 0; i < arrActor.size(); i++) {
                search_string += arrActor.get(i).getId() + ",";
                search_artist_name += arrActor.get(i).getName() + ",";
            }
            search_string = search_string.substring(0, search_string.length() - 1) + "~1|";
        }
        if (arrSinger != null && arrSinger.size() > 0) {
            for (int i = 0; i < arrSinger.size(); i++) {
                search_string += arrSinger.get(i).getId() + ",";
                search_artist_name += arrSinger.get(i).getName() + ",";
            }
            search_string = search_string.substring(0, search_string.length() - 1) + "~8|";
        }
        if (arrComposer != null && arrComposer.size() > 0) {
            for (int i = 0; i < arrComposer.size(); i++) {
                search_string += arrComposer.get(i).getId() + ",";
                search_artist_name += arrComposer.get(i).getName() + ",";
            }
            search_string = search_string.substring(0, search_string.length() - 1) + "~2|";
        }
        if (!search_string.equals("")) {
            search_string = search_string.substring(0, search_string.length() - 1);
            search_artist_name = search_artist_name.substring(0, search_artist_name.length() - 1);
        }

        if (arrdata != null)
            arrdata.clear();

        start = 0;
        previousTotal = 0;

        new GetArtistSongList().execute();

        getArtistNameIdArray();
    }

    private void getArtistNameIdArray() {
        artistNameArr = new ArrayList<>();
        artistIDsArr = new ArrayList<>();

        if (arrActor != null) {
            artistNameArr.addAll(arrActor);
            artistIDsArr.addAll(arrActor);
        }
        if (arrSinger != null) {
            artistNameArr.addAll(arrSinger);
            artistIDsArr.addAll(arrSinger);
        }
        if (arrComposer != null) {
            artistNameArr.addAll(arrComposer);
            artistIDsArr.addAll(arrComposer);
        }

        ArtistRowGridAdapter adapter = new ArtistRowGridAdapter(SearchArtistSongListActivity.this, artistNameArr);
        artistGrid.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void generateArrayList() {
        String id_list = ",";
        arr_all.clear();
        if (role == 1) {
            if (arrActor != null && arrActor.size() > 0) {
                for (int j = 0; j < arrActor.size(); j++) {
                    if (!arrActor.get(j).isRemoved()) {
                        id_list += arrActor.get(j).getId() + ",";
                        arr_all.add(arrActor.get(j));
                    }
                }
            }
        } else if (role == 8) {
            if (arrSinger != null && arrSinger.size() > 0) {
                for (int j = 0; j < arrSinger.size(); j++) {
                    if (!arrSinger.get(j).isRemoved()) {
                        id_list += arrSinger.get(j).getId() + ",";
                        arr_all.add(arrSinger.get(j));
                    }
                }
            }
        } else {
            if (arrComposer != null && arrComposer.size() > 0) {
                for (int j = 0; j < arrComposer.size(); j++) {
                    if (!arrComposer.get(j).isRemoved()) {
                        id_list += arrComposer.get(j).getId() + ",";
                        arr_all.add(arrComposer.get(j));
                    }
                }
            }
        }
        for (int k = 0; k < arr_main.size(); k++) {
            if (id_list.contains("," + arr_main.get(k).getId() + ","))
                arr_main.get(k).setSelected(true);
            else
                arr_main.get(k).setSelected(false);
        }
        for (int i = 0; i < arr_main.size(); i++) {
            if (!arr_main.get(i).isSelected()) {
                arr_all.add(arr_main.get(i));
            }
        }
        setAdapterArtiste(arr_all);
    }

    private class SearchArtisteList extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        private ArtistPojo basePojo;
        int role_artist;
        String searchQuery;

        public SearchArtisteList(int role_artist, String searchQuery) {
            this.role_artist = role_artist;
            this.searchQuery = searchQuery;
        }

        @Override
        protected void onPreExecute() {
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
            }
        }

        @Override
        protected Void doInBackground(Void... role) {
            if (isCancelled())
                return null;

            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(getAsyncTaskDataArtisteSearch(SaregamaConstants.BASE_URL + "search?type=search" + "&ctype=3" + "&query=" + searchQuery + "&criteria=" + role_artist + "&mode=hindi"), ArtistPojo.class);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (cd.isConnectingToInternet()) {
                if (basePojo != null) {
                    if (basePojo.getStatus()) {
                        if (basePojo.getData().getCount() > 0) {
                            arr_main.clear();

                            if (basePojo.getData().getList() != null) {
                                arr_main = basePojo.getData().getList();
                            }
                            adapter = new ArtistListAdapter(ctx, arr_main);
                            mRecyclerViewDialog.setAdapter(adapter);

                        }
                    } else {
//                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
//                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }

    }

    private void setAdapterArtiste(ArrayList<ArtistListPojo> arr) {
        adapter = new ArtistListAdapter(ctx, arr);
        if (mRecyclerViewDialog != null)
            mRecyclerViewDialog.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void playMP3Song(ImageView imageView, View v) {
        int position = (int) v.getTag(R.string.key);
        data_playing = (MP3HindiSongListPojo) v.getTag(R.string.data);

        if (previous_view != null) {
            previous_view.setImageResource(R.mipmap.play_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        if (data_playing.is_playing()) {
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, "");
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            imageView.setImageResource(R.mipmap.play_icon);
            seekbar.setBackgroundResource(R.mipmap.play_icon_miniplayer);
            data_playing.setIs_playing(false);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        } else {
            for (int i = 0; i < arrdata.size(); i++)
                arrdata.get(i).setIs_playing(false);
//            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_ID, data_playing.getSong_id());
            saveIntoPrefs(SaregamaConstants.PLAYING_SONG_TYPE, "");
            data_playing.setIs_playing(true);
            seekbar.setBackgroundResource(R.mipmap.pause_icon_miniplayer);
            imageView.setImageResource(R.mipmap.pause_icon);
            ((NotifyAdapterInterface) ctx).notifyAdapter();
        }
        previous_view = imageView;
        seekbar.setVisibility(View.VISIBLE);
        playPauseButtonClickListener(c_type, imageView, seekbar, data_playing.getIsrc(), data_playing.getSong_id(), data_playing, position);
    }

}
