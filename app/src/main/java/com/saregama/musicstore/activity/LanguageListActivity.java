package com.saregama.musicstore.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.saregama.musicstore.R;
import com.saregama.musicstore.adapter.CategoryListAdapter;
import com.saregama.musicstore.pojo.AppConfigDataPojo;
import com.saregama.musicstore.pojo.HomeBannerPojo;
import com.saregama.musicstore.pojo.HomeDataPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

/**
 * Created by Arpit on 29-Feb-16.
 */
public class LanguageListActivity extends BaseActivity implements View.OnClickListener {

    private ConnectionDetector cd;
    private Activity ctx = this;
    private SaregamaDialogs dialog;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private String asyncTaskUrl;
    private AppConfigDataPojo appConfigData;
    private int c_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_list);

        setDrawerAndToolbarWithDropDown("Regional Films");

        SaregamaConstants.ACTIVITIES.add(ctx);
        c_type = getIntent().getIntExtra("c_type", 1);
        dialog = new SaregamaDialogs(ctx);

        mRecyclerView = (RecyclerView) findViewById(R.id.languagelist_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        cd = new ConnectionDetector(getApplicationContext());

        appConfigData = getAppConfigJson();

        if (getIntent().getStringExtra("notification_url") != null) {
            asyncTaskUrl = getIntent().getStringExtra("notification_url");
        } else {
            asyncTaskUrl = getIntent().getStringExtra("url");
        }
        new GetLanguageList().execute();
    }

    @Override
    public void onClick(View v) {

        HomeBannerPojo data = (HomeBannerPojo) v.getTag(R.string.data);
        saveIntIntoPrefs(SaregamaConstants.D_TYPE, data.getD_type());

        if (data.getC_type() == appConfigData.getC_type().getSONG()) {
            if (data.getD_type() == appConfigData.getD_type().getREGIONAL_FILMS()) {
                Intent i = new Intent(getApplicationContext(), Mp3RegionalActivity.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("from", "mp3_regional");
                i.putExtra("languageid", data.getLanguage_id());
                i.putExtra("languagename", data.getLanguage_name());
                i.putExtra("d_type", getIntent().getIntExtra("d_type", 0));
                i.putExtra("c_type", c_type);
                startActivity(i);
            } else {
                Intent i = new Intent(getApplicationContext(), Regional_Others.class);
                i.putExtra("url", data.getUrl());
                i.putExtra("languageid", data.getLanguage_id());
                i.putExtra("c_type", c_type);
                startActivity(i);
            }
        } else if (data.getC_type() == appConfigData.getC_type().getALBUM()) {
            Intent i = new Intent(getApplicationContext(), BuyAnyAlbumListing.class);
            i.putExtra("languagename", data.getLanguage_name());
            i.putExtra("from", "mp3_regional");
            i.putExtra("languageid", data.getLanguage_id());
            i.putExtra("c_type", c_type);
            i.putExtra("url", data.getUrl());
            startActivity(i);
        }
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    class GetLanguageList extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog_p;
        HomeDataPojo basePojo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (cd.isConnectingToInternet()) {
                super.onPreExecute();
                dialog_p = SaregamaDialogs.showLoading(ctx);
                dialog_p.setCanceledOnTouchOutside(false);
            } else {
                dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (isCancelled())
                return null;
            Gson gsonObj = new Gson();
            basePojo = gsonObj.fromJson(getAsyncTaskData(asyncTaskUrl), HomeDataPojo.class);
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (dialog_p != null && dialog_p.isShowing()) {
                dialog_p.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (cd.isConnectingToInternet()) {

                if (dialog_p != null && dialog_p.isShowing()) {
                    dialog_p.dismiss();
                }

                if (basePojo != null) {
                    if (basePojo.getStatus()) {
                        mAdapter = new CategoryListAdapter(ctx, basePojo.getData(), "language");
                        mRecyclerView.setAdapter(mAdapter);
                    } else {
                        dialog.displayCommonDialog(basePojo.getError());
                    }
                } else {
                    dialog.displayCommonDialog(getAppConfigJson().getServer_error());
                }
            }
        }
    }
}