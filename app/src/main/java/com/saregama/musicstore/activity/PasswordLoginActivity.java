package com.saregama.musicstore.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appsflyer.AppsFlyerLib;
import com.flurry.android.FlurryAgent;
import com.saregama.musicstore.R;
import com.saregama.musicstore.model.RestClient;
import com.saregama.musicstore.pojo.ForgotPasswordPojo;
import com.saregama.musicstore.pojo.LogInPojo;
import com.saregama.musicstore.util.ConnectionDetector;
import com.saregama.musicstore.util.SaregamaConstants;
import com.saregama.musicstore.util.SaregamaDialogs;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Yash ji on 9/6/2016.
 */
public class PasswordLoginActivity extends BaseActivity {

    private ConnectionDetector cd;
    private PasswordLoginActivity ctx = this;
    private SaregamaDialogs dialog;
    private String email;
    private Map<String, String> articleParams;
    private String notification_key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_login);

        articleParams = new HashMap<>();

        if (getIntent().getStringExtra("email") != null) {
            email = getIntent().getStringExtra("email");
        }
        if (getIntent().getStringExtra("notification_key") != null) {
            notification_key = getIntent().getStringExtra("notification_key");
        }
        dialog = new SaregamaDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        SaregamaConstants.ACTIVITIES.add(ctx);

        final ImageView password_login_back = (ImageView) findViewById(R.id.password_login_back);
        password_login_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        TextView forgot = (TextView) findViewById(R.id.forgotPassword);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (email.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.FORGOT_PASSWORD, getAppConfigJson().getForgotpassword1());
                } else if (!isValidEmail(email)) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.FORGOT_PASSWORD, getAppConfigJson().getValid_email());
                } else {
                    doForgotPassword(email);
                }

            }
        });

        final EditText login_password = (EditText) findViewById(R.id.login_password);
        final LinearLayout password_login_ok = (LinearLayout) findViewById(R.id.password_login_ok);
        password_login_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String login_passwordText = login_password.getText().toString();
                if (login_passwordText.equals("")) {
                    dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getBlank_password());
                } else {
                    saveIntoPrefs(SaregamaConstants.LOGGEDINFROM, "normal");
                    hideSoftKeyboard();
                    doLoginWithEmail(email, login_passwordText);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    private void doLoginWithEmail(final String username, String password) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = SaregamaDialogs.showLoading(PasswordLoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().postLoginEmail(getFromPrefs(SaregamaConstants.IMEI), username, password, "signin", new Callback<LogInPojo>() {
                @Override
                public void success(LogInPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            // analytics code for source of signin start
                            articleParams.put("sign_in_email", username);
                            articleParams.put("User_Device", "Android");
                            articleParams.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            articleParams.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            articleParams.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            articleParams.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));
                            FlurryAgent.logEvent("sign_in_source", articleParams);

                            Map<String, Object> eventValue = new HashMap<String, Object>();
                            eventValue.put("sign_in_email", username);
                            eventValue.put("User_IP", getFromPrefs(SaregamaConstants.IP));
                            eventValue.put("User_Country", getFromPrefs(SaregamaConstants.COUNTRY));
                            eventValue.put("User_State", getFromPrefs(SaregamaConstants.STATE));
                            eventValue.put("User_City", getFromPrefs(SaregamaConstants.CITY_USER));
                            AppsFlyerLib.getInstance().trackEvent(ctx, "Login", eventValue);

                            // analytics code for source of signin End
                            checkCommonURL(response.getUrl());
                            navigateToHomeActivity(basePojo);
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void navigateToHomeActivity(LogInPojo basePojo) {
        if (basePojo.getData().getListener().getId() != null && basePojo.getData().getListener().getSession_id() != null) {
            saveIntoPrefs(SaregamaConstants.EMAIL, basePojo.getData().getListener().getEmail());
            saveIntoPrefs(SaregamaConstants.NAME, basePojo.getData().getListener().getTitle());
            saveIntoPrefs(SaregamaConstants.MOBILE, basePojo.getData().getListener().getMobile());
            saveIntoPrefs(SaregamaConstants.SESSION_ID, basePojo.getData().getListener().getSession_id());
            saveIntoPrefs(SaregamaConstants.ID, basePojo.getData().getListener().getId());
            saveIntoPrefs(SaregamaConstants.CART, basePojo.getData().getCart());
            saveIntoPrefs(SaregamaConstants.DOWNLOAD, basePojo.getData().getDownload());
            saveIntoPrefs(SaregamaConstants.USERIMAGE, basePojo.getData().getListener().getImage());
            getCartDataFirstTime();
            getNotificationsSize();
            getNumberofDownload();
            getDownloadedCount();
            if (getFromPrefs(SaregamaConstants.IMEI) != null && getFromPrefs(SaregamaConstants.IMEI).length() > 0 && getFromPrefs(SaregamaConstants.SESSION_ID) != null)
                sendRegistrationId(getFromPrefs(SaregamaConstants.DEVICEID), getFromPrefs(SaregamaConstants.IMEI));
            saveIntoPrefs(SaregamaConstants.REFRESH_NEEDED, "yes");
            if (notification_key.equals("download")) {
                Intent mIntent = new Intent(ctx, DownloadManagerActivity.class);
                startActivity(mIntent);
            }
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.LOGIN_FAILED, SaregamaConstants.LOGIN_ERROR);
        }
    }


    private void doForgotPassword(final String email) {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = SaregamaDialogs.showLoading(PasswordLoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().postForgotPassword(getFromPrefs(SaregamaConstants.IMEI), email, "verifyemail", new Callback<ForgotPasswordPojo>() {
                @Override
                public void success(ForgotPasswordPojo basePojo, Response response) {
                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
//                            dialog.displayCommonDialogWithHeader("PASSWORD SENT", "Temporary password has been sent on mail successfully.");
                            checkCommonURL(response.getUrl());
                            if (basePojo.getData().getMsg() != null && !basePojo.getData().getMsg().isEmpty())
                                emailDialog(basePojo.getData().getMsg());
                        } else {
                            dialog.displayCommonDialogWithHeader(SaregamaConstants.FORGOT_PASSWORD, basePojo.getError());
                        }
                    } else {
                        dialog.displayCommonDialogWithHeader(SaregamaConstants.FORGOT_PASSWORD, getAppConfigJson().getServer_error());
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialogWithHeader(SaregamaConstants.INTERNET_FAIL, getAppConfigJson().getInternat_fail() + "\n");
        }
    }

    private void emailDialog(String msg) {
        Button OkButtonEmail;
        final Dialog dialogEmail = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialogEmail.setContentView(R.layout.email_dialog_small);
        if (!dialogEmail.isShowing()) {
            TextView msg_textView = (TextView) dialogEmail.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) dialogEmail.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(msg);
            dialog_header.setText("Email Sent");
            OkButtonEmail = (Button) dialogEmail.findViewById(R.id.btn_yes_exit);
            OkButtonEmail.setText("OK");
            TextView text_resend = (TextView) dialogEmail.findViewById(R.id.text_resend);
            SpannableString ss = new SpannableString(getResources().getString(R.string.forgor_password_resend_text));
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    dialogEmail.dismiss();
                    doForgotPassword(email);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };
            ss.setSpan(clickableSpan, 44, text_resend.getText().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            text_resend.setText(ss);
            text_resend.setMovementMethod(LinkMovementMethod.getInstance());
            text_resend.setHighlightColor(Color.parseColor(ctx.getResources().getString(R.string.highlightedText)));

            ImageView dialog_header_cross = (ImageView) dialogEmail.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogEmail.dismiss();
                }
            });

            OkButtonEmail.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogEmail.dismiss();
                }
            });

            dialogEmail.show();
        }

    }
}
